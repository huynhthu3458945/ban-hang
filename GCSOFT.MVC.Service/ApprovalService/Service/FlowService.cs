﻿using GCSOFT.MVC.Data.Repositories.ApprovalRepositories.Interface;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.ApprovalService.Service
{
    public class FlowService : IFlowService
    {
        private readonly IFlowRepository _flowRepo;
        public FlowService
            (
                IFlowRepository flowRepo
            )
        {
            _flowRepo = flowRepo;
        }

        public IEnumerable<M_Flow> FindAll()
        {
            return _flowRepo.GetAll();
        }

        public IEnumerable<M_Flow> FindAll(ApprovalType type, string code)
        {
            return _flowRepo.GetMany(d => d.Type == type && d.Code.Equals(code, StringComparison.OrdinalIgnoreCase)).OrderByDescending(d => d.Priority);
        }
        public IEnumerable<M_Flow> FindAll(ApprovalType type, string code, int priority)
        {
            return _flowRepo.GetMany(d => d.Type == type && d.Code.Equals(code, StringComparison.OrdinalIgnoreCase) && d.Priority == priority);
        }
        public M_Flow GetNextFlow(ApprovalType type, string code, int priority)
        {
            return _flowRepo.GetMany(d => d.Type == type && d.Code.Equals(code, StringComparison.OrdinalIgnoreCase) && d.Priority > priority).OrderBy(d => d.Priority).FirstOrDefault();
        }
        public M_Flow GetBy(ApprovalType type, string code, int lineNo)
        {
            return _flowRepo.Get(d => d.Type == type && d.Code.Equals(code, StringComparison.OrdinalIgnoreCase) && d.LineNo == lineNo) ?? new M_Flow();
        }
        public IEnumerable<M_Flow> FindAll(string approvalNo, string notStatusCode)
        {
            return _flowRepo.GetMany(d => d.ApprovalNo.Equals(approvalNo) && !d.StatusCode.Equals(notStatusCode));
        }
        
        public M_Flow GetBy(ApprovalType type, string code, string ApprovalNo)
        {
            return _flowRepo.Get(d => d.Type == type && d.Code.Equals(code, StringComparison.OrdinalIgnoreCase) && d.ApprovalNo.Equals(ApprovalNo, StringComparison.OrdinalIgnoreCase));
        }
        public M_Flow GetByGroupCode(ApprovalType type, string code, string groupCode)
        {
            return _flowRepo.Get(d => d.Type == type && d.Code.Equals(code, StringComparison.OrdinalIgnoreCase) && d.GroupCode.Equals(groupCode, StringComparison.OrdinalIgnoreCase));
        }

        public string Insert(M_Flow Flow)
        {
            try
            {
                _flowRepo.Add(Flow);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<M_Flow> Flows)
        {
            try
            {
                _flowRepo.Add(Flows);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Flow Flow)
        {
            try
            {
                _flowRepo.Update(Flow);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_Flow> Flows)
        {
            try
            {
                _flowRepo.Update(Flows);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(ApprovalType type, string code)
        {
            try
            {
                _flowRepo.Delete(d => d.Type == type && d.Code.Equals(code));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(ApprovalType type, string code, int lineNo)
        {
            try
            {
                _flowRepo.Delete(d => d.Type == type && d.Code.Equals(code) && d.LineNo == lineNo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

    }
}
