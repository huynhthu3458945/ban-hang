﻿using GCSOFT.MVC.Model.Approval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.ApprovalService.Interface
{
    public interface IFlowService
    {
        IEnumerable<M_Flow> FindAll();
        IEnumerable<M_Flow> FindAll(ApprovalType type, string code);
        IEnumerable<M_Flow> FindAll(ApprovalType type, string code, int priority);
        IEnumerable<M_Flow> FindAll(string approvalNo, string notStatusCode);
        M_Flow GetBy(ApprovalType type, string code, string ApprovalNo);
        M_Flow GetBy(ApprovalType type, string code, int lineNo);
        M_Flow GetByGroupCode(ApprovalType type, string code, string groupCode);
        M_Flow GetNextFlow(ApprovalType type, string code, int priority);
        
        string Insert(M_Flow Flow);
        string Update(M_Flow Flow);
        string Insert(IEnumerable<M_Flow> Flows);
        string Update(IEnumerable<M_Flow> Flows);
        string Delete(ApprovalType type, string code);
    }
}
