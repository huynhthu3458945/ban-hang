﻿//####################################################################
//# Copyright (C) 2010-2011,  JSC.  All Rights Reserved. 
//#
//# History:
//#     Date Time       Updater         Comment
//#     30/08/2021      Huỳnh Thử      Tạo mới
//####################################################################

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
namespace GCSOFT.MVC.Service.ExeclOfficeEngine
{
    public static class ExcelDataSource
    {
        /// <summary>
        /// Convert List sang DataTable
        /// Cho phép add Object sử dụng nullable column
        /// </summary>
        public static DataTable ConvertToDataTable<T>(this List<T> iList)
        {
            var dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptorCollection =
                TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            var values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        /// <summary>
        /// Convert DataTable to crosstab DataTable
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static DataTable ConvertToCrossTabTable(DataTable table)
        {
            var newTable = new DataTable();

            for (int row = 0; row <= table.Rows.Count; row++)
            {
                newTable.Columns.Add(row.ToString(CultureInfo.InvariantCulture));
            }

            for (int column = 0; column < table.Columns.Count; column++)
            {
                DataRow newRow = newTable.NewRow();
                var rowValues = new object[table.Rows.Count + 1];

                rowValues[0] = table.Columns[column].Caption;

                for (int row = 1; row <= table.Rows.Count; row++)
                {
                    rowValues[row] = table.Rows[row - 1][column].ToString();
                }

                newRow.ItemArray = rowValues;
                newTable.Rows.Add(newRow);
            }

            return newTable;
        }

    }
}
