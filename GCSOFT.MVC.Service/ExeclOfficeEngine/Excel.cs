﻿//####################################################################
//# Copyright (C) 2010-2011,  JSC.  All Rights Reserved. 
//#
//# History:
//#     Date Time       Updater         Comment
//#     30/08/2021      Huỳnh Thử      Tạo mới
//####################################################################

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
//using System.Drawing;
using AsposeCells = Aspose.Cells;
using System.Web;
using System.Text.RegularExpressions;
using System.Linq;
using SoftArtisans.OfficeWriter.ExcelWriter;

namespace GCSOFT.MVC.Service.ExeclOfficeEngine
{
    /// <summary>
    /// Excel Application
    /// Tạo workbook mới hoặc ghi đè dựa trên template
    /// </summary>
    public class Excel : ExcelBase
    {
        #region ---- Const ----

        /// <summary>
        /// Mặc định active sheet Report
        /// </summary>
        private const string DEFAULTSHEETNAME = "Report";

        /// <summary>
        /// Mặc định active sheet 0
        /// </summary>
        private const int DEFAULTSHEETINDEX = 0;

        #endregion ---- Const ----

        #region ---- Member variables ----

        private AsposeCells.Workbook WorkBook { get; set; }
        private AsposeCells.Worksheet WorkSheet { get; set; }

        public static int CountDataSource { get; set; }

        #endregion ---- Member variables ----

        #region ---- Colors, Fonts & Styles ----

        /// <summary>
        /// Get color
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        /// <returns></returns>
        public static Color GetColor(Workbook workbook, int red, int green, int blue)
        {
            Palette palette = workbook.Palette;
            return palette.GetColor(red, green, blue);
        }

        /// <summary>
        /// Create font
        /// </summary>
        /// <param name="workbook">The workbook</param>
        /// <param name="fontName">The font name as "Time New Ronman", "Arrial", "Tahoma",...</param>
        /// <param name="fontSize">The font size value</param>
        /// <param name="bold">Text is bold</param>
        /// <param name="italic">Text is italic</param>
        /// <param name="underlineStyle">Style underline: default none</param>
        /// <param name="color">Text color: default black</param>
        /// <returns>Return object Font</returns>
        public static Font CreateFont(Workbook workbook, string fontName, int fontSize, bool bold, bool italic,
                                      Font.UnderlineStyle underlineStyle = null, Color color = null)
        {
            // Init style: default - color black, underline none
            color = color ?? Color.SystemColor.Black;
            underlineStyle = underlineStyle ?? Font.UnderlineStyle.None;
            Style fontStyle = workbook.CreateStyle();

            // Set font properties
            fontStyle.Font.Name = fontName;
            fontStyle.Font.Size = fontSize;
            fontStyle.Font.Italic = italic;
            fontStyle.Font.Bold = bold;
            fontStyle.Font.Underline = underlineStyle;
            fontStyle.Font.Color = color;

            return fontStyle.Font;
        }

        /// <summary>
        /// Create Style
        /// </summary>
        /// <param name="workbook">The workbook</param>
        /// <param name="fontStyle">Font object</param>
        /// <param name="formatString">Format string</param>
        /// <param name="wrapText">Wraptext</param>
        /// <param name="border">Border object</param>
        /// <param name="verticalAlign">Vertical Align</param>
        /// <param name="horizontalAlign">Horizontal Align</param>
        /// <param name="backgroudColor">Background Color: default white</param>
        /// <param name="borderColor">Border Color: default black</param>
        /// <returns>Return Style Object</returns>
        public static Style CreateStyle(Workbook workbook, Font fontStyle, string formatString, bool wrapText,
                                        Border.LineStyle border, Style.VAlign verticalAlign,
                                        Style.HAlign horizontalAlign,
                                        Color backgroudColor = null, Color borderColor = null)
        {
            // Init style
            backgroudColor = backgroudColor ?? Color.SystemColor.White;
            borderColor = borderColor ?? Color.SystemColor.Black;
            Style styles = workbook.CreateStyle();

            // Set font: Bold, Italic, Underline, size
            styles.Font = fontStyle;

            // Format string
            styles.NumberFormat = formatString;

            // Wraptext
            styles.WrapText = wrapText;

            // Border style: default - Thin, color black
            styles.Border.Style = border;
            styles.Border.Color = borderColor;

            // Set backgroud color: default - color white
            styles.BackgroundColor = backgroudColor;

            // Vertical alignment
            styles.VerticalAlignment = verticalAlign;

            // Horizontal alignment
            styles.HorizontalAlignment = horizontalAlign;

            // return style
            return styles;
        }

        #endregion ---- Colors, Fonts & Styles ----

        #region ---- Row & Column properties ----

        /// <summary>
        /// Set width of column
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="column">Column Index</param>
        /// <param name="width">Width - Inch</param>
        /// <param name="autoFitWidth">Set auto fit width</param>
        public static void SetColumnWidth(ref Worksheet worksheet, int column, int width, bool autoFitWidth)
        {
            // Get column properties
            ColumnProperties columnProperties = worksheet.GetColumnProperties(column);

            // Set column width
            columnProperties.Width = width;

            // Set auto fit column width
            if (autoFitWidth)
            {
                columnProperties.AutoFitWidth();
            }
        }

        /// <summary>
        /// Set column properties
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="column">Column Index</param>
        /// <param name="style">Column Styles</param>
        /// <param name="width">Width</param>
        /// <param name="autoFitWidth">Set auto fit width</param>
        public static void SetColumnProperties(ref Worksheet worksheet, int column, Style style, int width,
                                               bool autoFitWidth)
        {
            // Get column properties
            ColumnProperties columnProperties = worksheet.GetColumnProperties(column);

            columnProperties.Style = style;
            columnProperties.Width = width;
            // Set auto fit column width
            if (autoFitWidth)
            {
                columnProperties.AutoFitWidth();
            }
        }

        /// <summary>
        /// Set row height
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="row">Row Index</param>
        /// <param name="height">Height</param>
        /// <param name="autoFitHeight">Set auto fit height</param>
        public static void SetRowHeight(ref Worksheet worksheet, int row, int height, bool autoFitHeight)
        {
            // Get row properties
            RowProperties rowProperties = worksheet.GetRowProperties(row);

            // Set row height
            rowProperties.Height = height;

            // Set auto fit row height
            if (autoFitHeight)
            {
                rowProperties.AutoFitHeight();
            }
        }

        /// <summary>
        /// Set row properties
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="row">Row Index</param>
        /// <param name="style">Row Styles</param>
        /// <param name="height">Height</param>
        /// <param name="autoFitHeight">Set auto fit height</param>
        public static void SetRowProperties(ref Worksheet worksheet, int row, Style style, int height,
                                            bool autoFitHeight)
        {
            // Get row properties
            RowProperties rowProperties = worksheet.GetRowProperties(row);


            rowProperties.Style = style;
            rowProperties.Height = height;
            // Set auto fit row height
            if (autoFitHeight)
            {
                rowProperties.AutoFitHeight();
            }
        }

        /// <summary>
        /// Hide row, use for suppress row
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="row">Row Index</param>
        public static void HideRow(Worksheet worksheet, int row)
        {
            RowProperties rowProperties = worksheet.GetRowProperties(row);
            rowProperties.Hidden = true;
        }

        /// <summary>
        /// Hide Column, use for suppress column
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="column">Column Index</param>
        public static void HideColumn(Worksheet worksheet, int column)
        {
            ColumnProperties columnProperties = worksheet.GetColumnProperties(column);
            columnProperties.Hidden = true;
        }

        #endregion ---- Row & Column properties ----

        #region ---- Cells ----

        /// <summary>
        /// Get cell by index
        /// </summary>
        /// <param name="worksheet">Current worksheet</param>
        /// <param name="rowNumber">From row number</param>
        /// <param name="columnNumber">From column number</param>
        /// <returns>Cell</returns>
        public static Cell GetCell(Worksheet worksheet, int rowNumber, int columnNumber)
        {
            return worksheet.Cells[rowNumber, columnNumber];
        }

        /// <summary>
        /// Get cell
        /// </summary>
        /// <param name="worksheet">Current worksheet</param>
        /// <param name="name">cell name</param>
        /// <returns>Cell</returns>
        public static Cell GetCell(Worksheet worksheet, string name)
        {
            return worksheet.Cells[name];
        }

        /// <summary>
        /// Add cell
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="row">Row Index</param>
        /// <param name="column">Column Index</param>
        /// <param name="value">Value of cell</param>
        /// <param name="style">Cell styles</param>
        public static void AddCell(Worksheet worksheet, int row, int column, object value, Style style)
        {
            // Set cell value
            worksheet[row, column].Value = value;

            // Add cell style
            worksheet[row, column].Style = style;
        }

        /// <summary>
        /// Add cell with formula
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="row">Row Index</param>
        /// <param name="column">Column Index</param>
        /// <param name="stringFormula">String Formula</param>
        /// <param name="style">Cell styles</param>
        public static void AddCellFormula(Worksheet worksheet, int row, int column, string stringFormula, Style style)
        {
            // Set cell formula
            worksheet[row, column].SetAddInFormula(stringFormula);

            // Add cell style
            worksheet[row, column].Style = style;
        }

        /// <summary>
        /// Add an merged cell
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="firstRow">First row - start row index</param>
        /// <param name="firstColumn">First column - start column index</param>
        /// <param name="numRows">Number of row is being merged</param>
        /// <param name="numColumns">Number of column is being merged</param>
        /// <param name="value">Value of merged cell</param>
        /// <param name="style">Cell or area styles</param>
        public static void AddMergedCell(Worksheet worksheet, int firstRow, int firstColumn,
                                         int numRows, int numColumns, object value, Style style = null)
        {
            // Create area
            Area area = worksheet.CreateArea(firstRow, firstColumn, numRows, numColumns);

            // Set value for this area
            worksheet[firstRow, firstColumn].Value = value;

            // Merge this area
            area.MergeCells();

            // Add style
            if (style != null)
            {
                area.SetStyle(style);
            }
        }

        public static void AddCellHyperlink(Worksheet worksheet, int row, int column,
                                            object value, string hyperlink, Style style)
        {
            // Init cell
            Cell cell = worksheet[row, column];

            // Set value and style
            cell.Value = value;
            cell.Style = style;

            // Create hyperlink
            cell.CreateHyperlink(hyperlink);
        }

        public static void AddMergedCellHyperlink(Worksheet worksheet, int firstRow, int firstColumn, int numRows,
                                                  int numColumns, object value, string hyperlink, Style style)
        {
            // Init cell
            Cell cell = worksheet[firstRow, firstColumn];

            // Set value and style
            cell.Value = value;
            cell.Style = style;

            // Create hyperlink
            cell.CreateHyperlink(hyperlink);

            // Create area
            Area area = worksheet.CreateArea(firstRow, firstColumn, numRows, numColumns);

            // Merge cell
            area.MergeCells();
        }

        public static void MergeCellOnly(Worksheet worksheet, int firstRow, int firstColumn, int numRows, int numColumns)
        {
            // Init area
            Area area = worksheet.CreateArea(firstRow, firstColumn, numRows, numColumns);

            // Merge cell
            area.MergeCells();
        }

        #endregion ---- Cells ----

        #region ---- Charts ----

        /// <summary>
        /// Add chart
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="chartType">Type of chart</param>
        /// <param name="chartName">Chart name</param>
        /// <param name="chartTitle">Chart title</param>
        /// <param name="categoryDate">Primary key: "Data!A3:C3"</param>
        /// <param name="series">Sum data: "Data!A25:C25"</param>
        /// <param name="row">Row Index - start position at this row</param>
        /// <param name="column">Column Index - start position at this column</param>
        /// <param name="percentageCellWidth">Width of area cell's as percentage in range 0,100</param>
        /// <param name="percentageCellHeight">Height of area cell's as percentage in range 0,100</param>
        public static void AddChart(Worksheet worksheet, ChartType chartType, string chartName, string chartTitle,
                                    string categoryDate, string series,
                                    int row, int column, int percentageCellWidth = 0, int percentageCellHeight = 0)
        {
            // Create an Anchor on which the Chart will be placed 
            Anchor anchor = worksheet.CreateAnchor(row, column, percentageCellWidth, percentageCellHeight);

            // Create a chart on the Worksheet
            Chart columnChart = worksheet.Charts.CreateChart(chartType, anchor);

            // Use the chart's SeriesCollection to add Series items and set dategory data 
            SeriesCollection seriesCol = columnChart.SeriesCollection;

            // Set the Primary category data 
            seriesCol.CategoryData = categoryDate;

            // Add series
            Series chartSeries = seriesCol.CreateSeries(series);
            chartSeries.Name = chartName;

            // Change the chart's title 
            columnChart.Title.Text = chartTitle;
        }

        #endregion ---- Charts ----

        #region ---- Pictures ----

        /// <summary>
        /// Add picture
        /// </summary>
        /// <param name="worksheet">The current worksheet being used</param>
        /// <param name="pictureFile">Picture file path in references</param>
        /// <param name="row">Row Index</param>
        /// <param name="column">Column Index</param>
        /// <param name="percentageCellWidth">Width of area cell's as percentage in range 0,100</param>
        /// <param name="percentageCellHeight">Height of area cell's as percentage in range 0,100</param>
        public static void AddPicture(Worksheet worksheet, string pictureFile, int row, int column,
                                      int percentageCellWidth = 0, int percentageCellHeight = 0)
        {
            Anchor anchor = worksheet.CreateAnchor(row, column, percentageCellWidth, percentageCellHeight);
            worksheet.Pictures.CreatePicture(pictureFile, anchor);
        }

        /// <summary>
        /// Add picture
        /// </summary>
        /// <param name="worksheet">The current worksheets being used</param>
        /// <param name="pictureStream">Picture input streams</param>
        /// <param name="row">Row Index</param>
        /// <param name="column">Column Index</param>
        /// <param name="percentageCellWidth">Width of area cell's as percentage in range 0,100</param>
        /// <param name="percentageCellHeight">Height of area cell's as percentage in range 0,100</param>
        public static void AddPicture(Worksheet worksheet, Stream pictureStream, int row, int column,
                                      int percentageCellWidth = 0, int percentageCellHeight = 0)
        {
            Anchor anchor = worksheet.CreateAnchor(row, column, percentageCellWidth, percentageCellHeight);
            worksheet.Pictures.CreatePicture(pictureStream, anchor);
        }

        #endregion ---- Pictures ----

        #region ---- Document properties ----

        /// <summary>
        /// Set Document properties
        /// </summary>
        /// <param name="workbook">The current workbooks being used</param>
        /// <param name="author">Author of this document</param>
        /// <param name="comments">Comments text</param>
        /// <param name="company">Company name</param>
        /// <param name="title">Title</param>
        public static void AddDocumentProperties(Workbook workbook,
                                                 string author, string comments, string company, string title)
        {
            // Get the DocumentProperties
            DocumentProperties docprops = workbook.DocumentProperties;

            // Set built-in DocumentProperties values 
            docprops.Author = author;
            docprops.Comments = comments;
            docprops.Company = company;
            docprops.Title = title;
        }

        /// <summary>
        /// Set Custom properties
        /// </summary>
        /// <param name="workbook">The current workbook being used</param>
        /// <param name="key">Key: Title, Comments, Keywords, Company, Author, Subject, Category, Source, Revision Number</param>
        /// <param name="value">Value</param>
        public static void AddCustomDocumentProperties(Workbook workbook, string key, object value)
        {
            // Get the DocumentProperties
            DocumentProperties docprops = workbook.DocumentProperties;

            // Set custom document properties
            docprops.SetCustomProperty(key, value);
        }

        #endregion ---- Document properties ----

        #region ---- Conditional Formats ----

        /// <summary>
        /// Init default workbook conditional formats
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <returns>ConditionalFormat</returns>
        public static ConditionalFormat InitConditionalFormat(Workbook workbook)
        {
            // Init condition format for workbook
            ConditionalFormat conditionalFormat = workbook.CreateConditionalFormat();

            // return condition
            return conditionalFormat;
        }

        /// <summary>
        /// Add conditional member
        /// </summary>
        /// <param name="conditionalFormat">From initialize conditional format</param>
        /// <param name="style">cell styles</param>
        /// <param name="comparisonType">
        /// Between, Not between, Equal, Not equal, Greated than, Greated than and equal, Less than, Less than and equal.
        /// </param>
        /// <param name="formula1">string formula 1</param>
        /// <param name="formula2">string formula 2</param>
        /// <remarks>
        /// [comparison: Between, formula1: =1, formula2: =100]
        /// [comparison: Equal, formula1: =1] 
        /// 
        /// >> must be declared conditional format from initialize methods
        /// >> cell styles will be applied if its true
        /// </remarks>
        public static void AddConditionalMember(ConditionalFormat conditionalFormat, Style style,
                                                Condition.Comparison comparisonType, string formula1,
                                                string formula2 = null)
        {
            // create condition
            Condition condition = string.IsNullOrEmpty(formula2)
                                      ? conditionalFormat.CreateCondition(comparisonType, formula1)
                                      : conditionalFormat.CreateCondition(comparisonType, formula1, formula2);

            // Add style format
            condition.ApplyStyle(style);
        }

        /// <summary>
        /// Remove condition from conditional format by index
        /// </summary>
        /// <param name="conditionalFormat"></param>
        /// <param name="index"></param>
        public static void RemoveConditionalMember(ConditionalFormat conditionalFormat, int index)
        {
            conditionalFormat.RemoveCondition(index);
        }

        /// <summary>
        /// Remove condition from conditional format
        /// </summary>
        /// <param name="conditionalFormat"></param>
        /// <param name="condition"></param>
        public static void RemoveConditionalMember(ConditionalFormat conditionalFormat, Condition condition)
        {
            conditionalFormat.RemoveCondition(condition);
        }

        /// <summary>
        /// Apply conditional format
        /// </summary>
        /// <param name="areaFormat">To area</param>
        /// <param name="conditionalFormat">conditional format</param>
        public static void ApplyConditionalFormat(Area areaFormat, ConditionalFormat conditionalFormat)
        {
            areaFormat.SetConditionalFormat(conditionalFormat);
        }

        #endregion ---- Conditional Formats ----

        #region ---- Areas ----

        /// <summary>
        /// Create or get area from an worksheet
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="firstRow"></param>
        /// <param name="firstColumn"></param>
        /// <param name="numRow"></param>
        /// <param name="numColumn"></param>
        /// <returns></returns>
        public static Area CreateArea(Worksheet worksheet, int firstRow, int firstColumn, int numRow, int numColumn)
        {
            Area area = worksheet.CreateArea(firstRow, firstColumn, numRow, numColumn);
            return area;
        }

        /// <summary>
        /// Create or get area from an worksheet by column only
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="firstColumn"></param>
        /// <param name="numColumn"></param>
        /// <returns></returns>
        public static Area CreateAreaColumn(Worksheet worksheet, int firstColumn, int numColumn)
        {
            Area area = worksheet.CreateAreaOfColumns(firstColumn, numColumn);
            return area;
        }

        /// <summary>
        /// Create or get area from an worksheet by row only
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="firstRow"></param>
        /// <param name="numRow"></param>
        /// <returns></returns>
        public static Area CreateAreaRow(Worksheet worksheet, int firstRow, int numRow)
        {
            Area area = worksheet.CreateAreaOfRows(firstRow, numRow);
            return area;
        }

        #endregion ---- Areas ----

        #region ---- Copy and Paste Properties ----

        /// <summary>
        /// Create custom copy-paste properties
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <param name="copyColumnWidth">True: allow copy column widths | False: none</param>
        /// <param name="copyComment">True: allow copy comments | False: none</param>
        /// <param name="copyFormattings">True: allow copy formattings| False: none</param>
        /// <param name="copyFormulas">True: allow copy formulas| False: none</param>
        /// <param name="copyMergedCells">True: allow copy merged cell style| False: none</param>
        /// <param name="copyRowHeight">True: allow copy row heights| False: none</param>
        /// <param name="copyValues">True: allow copy values| False: none</param>
        /// <returns>CopyPasteProperties</returns>
        public static CopyPasteProperties CreateCopyPasteProperties(Workbook workbook, bool copyColumnWidth,
                                                                    bool copyComment, bool copyFormattings,
                                                                    bool copyFormulas, bool copyMergedCells,
                                                                    bool copyRowHeight, bool copyValues)
        {
            CopyPasteProperties.CopyPasteType copyPasteType = CopyPasteProperties.CopyPasteType.None;

            // Create override properties
            CopyPasteProperties properties = workbook.CreateCopyPasteProperties(copyPasteType);
            properties.CopyColumnWidth = copyColumnWidth;
            properties.CopyComments = copyComment;
            properties.CopyFormatting = copyFormattings;
            properties.CopyMergedCells = copyMergedCells;
            properties.CopyRowHeight = copyRowHeight;
            properties.CopyValues = copyValues;

            // return copy-paste properties
            return properties;
        }

        /// <summary>
        /// Create copy-paste properties
        /// without row heights and column widths
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <returns>CopyPasteProperties</returns>
        public static CopyPasteProperties CreateCopyPastePropertiesAll(Workbook workbook)
        {
            CopyPasteProperties.CopyPasteType copyPasteType = CopyPasteProperties.CopyPasteType.All;

            // Create override properties
            CopyPasteProperties properties = workbook.CreateCopyPasteProperties(copyPasteType);

            // return copy-paste properties
            return properties;
        }

        /// <summary>
        /// Create copy-paste properties
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <returns>CopyPasteProperties</returns>
        public static CopyPasteProperties CreateCopyPastePropertiesAllPlus(Workbook workbook)
        {
            CopyPasteProperties.CopyPasteType copyPasteType =
                CopyPasteProperties.CopyPasteType.AllPlusRowHeightAndColumnWidth;

            // Create override properties
            CopyPasteProperties properties = workbook.CreateCopyPasteProperties(copyPasteType);

            // return copy-paste properties
            return properties;
        }

        /// <summary>
        /// Create copy-paste properties with cell values only
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <returns>CopyPasteProperties</returns>
        public static CopyPasteProperties CreateCopyPastePropertiesValuesOnly(Workbook workbook)
        {
            CopyPasteProperties.CopyPasteType copyPasteType = CopyPasteProperties.CopyPasteType.ValuesOnly;

            // Create override properties
            CopyPasteProperties properties = workbook.CreateCopyPasteProperties(copyPasteType);

            // return copy-paste properties
            return properties;
        }

        /// <summary>
        /// Create copy-paste properties with cells, formats and formulas
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <returns>CopyPasteProperties</returns>
        public static CopyPasteProperties CreateCopyPastePropertiesFormulasAndFormatting(Workbook workbook)
        {
            CopyPasteProperties.CopyPasteType copyPasteType = CopyPasteProperties.CopyPasteType.FormulasAndFormatting;

            // Create override properties
            CopyPasteProperties properties = workbook.CreateCopyPasteProperties(copyPasteType);

            // return copy-paste properties
            return properties;
        }

        /// <summary>
        /// Create copy-paste properties with cell values, formulas and cell and number formatting
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <returns>CopyPasteProperties</returns>
        public static CopyPasteProperties CreateCopyPastePropertiesValuesFormulasAndFormatting(Workbook workbook)
        {
            CopyPasteProperties.CopyPasteType copyPasteType =
                CopyPasteProperties.CopyPasteType.ValuesFormulasAndFormatting;

            // Create override properties
            CopyPasteProperties properties = workbook.CreateCopyPasteProperties(copyPasteType);

            // return copy-paste properties
            return properties;
        }

        /// <summary>
        /// Copy and Paste Area
        /// </summary>
        /// <param name="destinationWorksheet">Destination worksheet</param>
        /// <param name="sourceArea">from source</param>
        /// <param name="destinationCell">to cell</param>
        /// <param name="copyPasteProperties">copy-paste properties</param>
        /// <remarks>
        /// 
        /// </remarks>
        public static void CopyAndPaste(Worksheet destinationWorksheet, Area sourceArea, Cell destinationCell,
                                        CopyPasteProperties copyPasteProperties)
        {
            destinationWorksheet.CopyPaste(destinationCell, sourceArea, copyPasteProperties);
        }

        #endregion ---- Copy and Parse ----

        #region ---- PivotTable/CrossTab ----

        /// <summary>
        /// Init PivotTable
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="area">data for PivotTable</param>
        /// <param name="pivotTableName"> </param>
        /// <param name="alignRow">margin top by row</param>
        /// <param name="alignColumn">margin left by column</param>
        public static PivotTable InitPivotTable(Worksheet worksheet, Area area,
                                                string pivotTableName = ExcelConvert.DEFAULTPIVOTTABLENAME,
                                                int alignRow = 0, int alignColumn = 0)
        {
            PivotTable pivotTable = worksheet.PivotTables.CreatePivotTable(area, alignRow, alignColumn);
            pivotTable.Name = pivotTableName;

            // Setting
            // Never true by converted to PDF reason
            pivotTable.PivotTableSettings.RefreshOnOpen = false;
            pivotTable.PivotTableSettings.ShowEmptyValueCaption = false;

            return pivotTable;
        }

        /// <summary>
        /// Add row label
        /// </summary>
        /// <param name="pivotTable"></param>
        /// <param name="memberName"></param>
        /// <param name="displayName"></param>
        /// <param name="displaySubtotalsAtTop"></param>
        /// <param name="numberStringFormat"></param>
        public static void AddPivotTableRowLabelMember(PivotTable pivotTable, string memberName, string displayName,
                                                       bool displaySubtotalsAtTop = false,
                                                       string numberStringFormat = "")
        {
            SourceField sourceField = pivotTable.SourceFields[memberName];
            PivotTableField field = pivotTable.RowLabels.CreateField(sourceField);

            field.DisplayName = displayName;
            field.DisplaySubtotalsAtTop = displaySubtotalsAtTop;
            field.NumberFormat = numberStringFormat;
        }

        /// <summary>
        /// Add column label
        /// </summary>
        /// <param name="pivotTable"></param>
        /// <param name="memberName"></param>
        /// <param name="displayName"></param>
        /// <param name="displaySubtotalsAtTop"></param>
        /// <param name="numberStringFormat"></param>
        public static void AddPivotTableColumnLabelMember(PivotTable pivotTable, string memberName, string displayName,
                                                          bool displaySubtotalsAtTop = false,
                                                          string numberStringFormat = "")
        {
            SourceField sourceField = pivotTable.SourceFields[memberName];
            PivotTableField field = pivotTable.ColumnLabels.CreateField(sourceField);

            field.DisplayName = displayName;
            field.DisplaySubtotalsAtTop = displaySubtotalsAtTop;
            field.NumberFormat = numberStringFormat;
        }

        /// <summary>
        /// Add value label
        /// </summary>
        /// <param name="pivotTable"></param>
        /// <param name="memberName"></param>
        /// <param name="displayName"></param>
        /// <param name="summarizeByType"></param>
        /// <param name="displaySubtotalsAtTop"></param>
        /// <param name="numberStringFormat"></param>
        public static void AddPivotTableValueLabelMember(PivotTable pivotTable, string memberName, string displayName,
                                                         PivotTableField.SummarizeByType summarizeByType =
                                                             PivotTableField.SummarizeByType.Product,
                                                         bool displaySubtotalsAtTop = false,
                                                         string numberStringFormat = "")
        {
            SourceField sourceField = pivotTable.SourceFields[memberName];
            PivotTableField field = pivotTable.DataFields.CreateField(sourceField);

            field.DisplayName = displayName;
            field.DisplaySubtotalsAtTop = displaySubtotalsAtTop;
            field.NumberFormat = numberStringFormat;
            field.SummarizeBy = summarizeByType;
        }

        /// <summary>
        /// Setting layout pivot table
        /// </summary>
        /// <param name="pivotTable"></param>
        public static void SettingPivotTableLayout(PivotTable pivotTable
            /*, PivotTableLayout.LayoutOptions options = PivotTableLayout.LayoutOptions.Tabular*/)
        {
            // Updating in build 9.1

            // pivotTable = options
        }

        /// <summary>
        /// Change pivot table data source
        /// </summary>
        /// <param name="pivotTable"></param>
        /// <param name="area"></param>
        public static void ChangePivotTableDataSource(PivotTable pivotTable, Area area)
        {
            pivotTable.ChangeDataSource(area);
        }

        #endregion ---- PivotTable/CrossTab ----

        #region ---- Others ----

        /// <summary>
        /// Build string formula
        /// =[formulaName]([fromCell]:[toCell])
        /// </summary>
        /// <param name="formulaName">SUM, AVERAGE, COUNT, MAX, MIN,...</param>
        /// <param name="fromCell">From Cell: A1, D2,...</param>
        /// <param name="toCell">To Cell: A1, D2,...</param>
        /// <returns>String Formula</returns>
        /// <remarks>
        /// [=SUM(A1:D3)]
        /// </remarks>
        public static string BuildStringFormula(string formulaName, Cell fromCell, Cell toCell)
        {
            string formula = string.Format("={0}({1}:{2})", formulaName, fromCell.Name, toCell.Name);

            return formula;
        }

        /// <summary>
        /// Thiết lập nội dung Header - Footer
        /// </summary>
        /// <param name="wooksheet">Current worksheet</param>
        /// <param name="isHeader">false:Footer | true:Header</param>
        /// <param name="section">Right, Center, Left</param>
        /// <param name="content">Content</param>
        /// <param name="picture">Picture Path</param>
        public static void FormattingHeaderFooter(ref Worksheet wooksheet, bool isHeader,
                                                  HeaderFooterSection.Section section, string content, string picture)
        {
            // Init PageSetUp
            PageSetup pgsetup = wooksheet.PageSetup;

            // Get section
            HeaderFooterSection sectionHeaderFooter = isHeader
                                                          ? pgsetup.GetHeader(section)
                                                          : pgsetup.GetFooter(section);

            // Set content
            if (string.IsNullOrEmpty(picture))
                sectionHeaderFooter.SetContent(content);
            else
                sectionHeaderFooter.SetContent(content, picture);
        }

        /// <summary>
        /// Thiết lập nội dung Header - Footer
        /// </summary>
        /// <param name="wooksheet">Current worksheet</param>
        /// <param name="isHeader">false:Footer | true:Header</param>
        /// <param name="section">Right, Center, Left</param>
        /// <param name="content">Content</param>
        /// <param name="picture">Picture Path</param>
        public static void FormattingHeaderFooter(ref Worksheet wooksheet, bool isHeader,
                                                  HeaderFooterSection.Section section, string content, Stream picture)
        {
            // Init PageSetUp
            PageSetup pgsetup = wooksheet.PageSetup;

            // Get section
            HeaderFooterSection sectionHeaderFooter = isHeader
                                                          ? pgsetup.GetHeader(section)
                                                          : pgsetup.GetFooter(section);

            // Set content
            if (picture == null)
                sectionHeaderFooter.SetContent(content);
            else
                sectionHeaderFooter.SetContent(content, picture);
        }

        #endregion ---- Others ----

        #region ---- Processing Templates ----

        #region ---- Override Templates ----

        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code
        /// </summary>
        /// <param name="bindDataSource"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="templatePath"></param>
        public static Workbook OverrideTemplate(DataTable bindDataSource, string dataSourceName, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();

            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Binding data
                template.BindData(bindDataSource, dataSourceName, basicProperties);

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                // Throw errors
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }

        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code.
        /// </summary>
        /// <param name="bindDataSources">Dictionary [DataSource|DataSourceName]</param>
        /// <param name="templatePath"></param>
        public static Workbook OverrideTemplate(Dictionary<string, DataTable> bindDataSources, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();

            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Binding list dataSource
                foreach (var bindDataSource in bindDataSources)
                {
                    // Binding data
                    template.BindData(bindDataSource.Value, bindDataSource.Key, basicProperties);
                }

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                // Throw errors
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }

        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code
        /// </summary>
        /// <param name="bindDataSource"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="templatePath"></param>
        public static Workbook OverrideTemplate(DataSet bindDataSource, string dataSourceName, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();

            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Binding data
                template.BindData(bindDataSource, dataSourceName, basicProperties);

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }

        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code.
        /// </summary>
        /// <param name="bindDataSources">Dictionary [DataSource|DataSourceName]</param>
        /// <param name="templatePath"></param>
        public static Workbook OverrideTemplate(Dictionary<string, DataSet> bindDataSources, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();

            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Binding list dataSource
                foreach (var bindDataSource in bindDataSources)
                {
                    // Binding data
                    template.BindData(bindDataSource.Value, bindDataSource.Key, basicProperties);
                }

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                // Throw errors
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }

        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code
        /// </summary>
        /// <param name="bindDataSource"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="templatePath"></param>
        public static Workbook OverrideTemplate(IDataReader bindDataSource, string dataSourceName, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();

            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Binding data
                template.BindData(bindDataSource, dataSourceName, basicProperties);

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                // Throw errors
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }

        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code.
        /// </summary>
        /// <param name="bindDataSources">Dictionary [DataSource|DataSourceName]</param>
        /// <param name="templatePath"></param>
        public static Workbook OverrideTemplate(Dictionary<string, IDataReader> bindDataSources, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();

            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Binding list dataSource
                foreach (var bindDataSource in bindDataSources)
                {
                    // Binding data
                    template.BindData(bindDataSource.Value, bindDataSource.Key, basicProperties);
                }

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                // Throw errors
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }

        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code
        /// </summary>
        /// <param name="bindDataSource"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="templatePath"></param>
        public static Workbook OverrideTemplate<T>(List<T> bindDataSource, string dataSourceName, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();

            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Binding data
                template.BindData(bindDataSource.ConvertToDataTable(), dataSourceName, basicProperties);

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                // Throw errors
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }


        /// <summary>
        /// Xử lý excel với template chuẩn
        /// kết hợp với format code
        /// Sử dụng nhiều dataSource (Model)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="bindDataSource">Dictionany[Name, DataSource]</param>
        /// <param name="templatePath"></param>
        /// <returns></returns>
        public static Workbook OverrideTemplate<T>(Dictionary<string, List<T>> bindDataSource, string templatePath)
        {
            // Init new instance
            var template = new ExcelTemplate();
            var application = new ExcelApplication();


            try
            {
                // Will not parse to string for numerics value
                template.PreserveStrings = false;

                // Open this template
                template.Open(templatePath);

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameter for textboxs
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Bind multi dataSource
                foreach (var source in bindDataSource)
                {
                    string dataSourceName = source.Key;
                    List<T> dataSource = source.Value;
                    // Binding data
                    template.BindData(dataSource.ConvertToDataTable(), dataSourceName, basicProperties);
                }

                // Process template
                template.Process();

                // Return workbook
                Workbook workbook = application.Open(template);

                return workbook;
            }
            catch (Exception ex)
            {
                // Throw errors
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                ExcelItems = null;
                application = null;
                template.Dispose();
            }
        }

        #endregion ---- Override Templates ----

        /// <summary>
        /// Generate report with DataTable
        /// </summary>
        /// <param name="bindDataSource">DataTable Source</param>
        /// <param name="dataSourceName">DataSource Name</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport(DataTable bindDataSource, string dataSourceName, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Bind data
                template.BindData(bindDataSource, dataSourceName, basicProperties);

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        /// <summary>
        /// Generate report with DataTable
        /// </summary>
        /// <param name="bindDataSources">Dictionary [DataSource|DataSourceName]</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport(Dictionary<string, DataTable> bindDataSources, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                foreach (var bindDataSource in bindDataSources)
                {
                    // Bind data
                    template.BindData(bindDataSource.Value, bindDataSource.Key, basicProperties);
                }

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        /// <summary>
        /// Generate report with DataSet
        /// </summary>
        /// <param name="bindDataSource">DataTable Source</param>
        /// <param name="dataSourceName">DataSource Name</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport(DataSet bindDataSource, string dataSourceName, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Bind data
                template.BindData(bindDataSource, dataSourceName, basicProperties);

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        /// <summary>
        /// Generate report with DataSet
        /// </summary>
        /// <param name="bindDataSources">Dictionary [DataSource|DataSourceName]</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport(Dictionary<string, DataSet> bindDataSources, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                foreach (var bindDataSource in bindDataSources)
                {
                    // Bind data
                    template.BindData(bindDataSource.Value, bindDataSource.Key, basicProperties);
                }

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        /// <summary>
        /// Generate report with IDataReader
        /// </summary>
        /// <param name="bindDataSource">DataTable Source</param>
        /// <param name="dataSourceName">DataSource Name</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport(IDataReader bindDataSource, string dataSourceName, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Bind data
                template.BindData(bindDataSource, dataSourceName, basicProperties);

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        /// <summary>
        /// Generate report with IDataReader
        /// </summary>
        /// <param name="bindDataSources">Dictionary [DataSource|DataSourceName]</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport(Dictionary<string, IDataReader> bindDataSources, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                foreach (var bindDataSource in bindDataSources)
                {
                    // Bind data
                    template.BindData(bindDataSource.Value, bindDataSource.Key, basicProperties);
                }

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        /// <summary>
        /// Generate report with List of Objects
        /// </summary>
        /// <param name="bindDataSource">DataTable Source</param>
        /// <param name="dataSourceName">DataSource Name</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport<T>(List<T> bindDataSource, string dataSourceName, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                // Bind data
                template.BindData(bindDataSource.ConvertToDataTable(), dataSourceName, basicProperties);

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        /// <summary>
        /// Generate report with List Objects
        /// </summary>
        /// <param name="bindDataSources">Dictionary [DataSource|DataSourceName]</param>
        /// <param name="templatePath">String templates file path</param>
        /// <returns>Stream</returns>
        public static Stream GenerateReport<T>(Dictionary<string, List<T>> bindDataSources, string templatePath)
        {
            try
            {
                // Init template
                var template = new ExcelTemplate();

                // Init properties
                DataBindingProperties basicProperties = template.CreateDataBindingProperties();

                // Open template
                template.Open(templatePath);

                // Add default parameters
                MergeWithDefaultParameters();

                // Add in parameters
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        template.BindCellData(item.Value, item.Key, basicProperties);
                    }
                }

                foreach (var bindDataSource in bindDataSources)
                {
                    // Bind data
                    template.BindData(bindDataSource.Value.ConvertToDataTable(), bindDataSource.Key, basicProperties);
                }

                // Process
                template.Process();

                // Return IO.Stream
                return template.SaveToStream();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ExcelItems = null;
            }
        }

        #region ---[Aspose] Export Excel/PDF---

        /// <summary>
        /// Export datatable with divisionId and Period columns
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="firstRow"></param>
        /// <param name="firstColumn"></param>
        /// <returns>DataTable | null if it export fail</returns>
        public static DataTable ExportDataTable(string fileName, int firstRow, int firstColumn,
            List<DataColumn> extendedColumns, out List<KeyValuePair<DataColumn, object>> extendedValues, bool removeFirst = true)
        {
            DataTable dtReturn = null;
            FileStream fstream = null;
            extendedValues = new List<KeyValuePair<DataColumn, object>>();

            try
            {
                //Creating a file stream containing the Excel file to be opened
                fstream = new FileStream(fileName, FileMode.Open);

                //Instantiating a Workbook object
                //Opening the Excel file through the file stream
                AsposeCells.Workbook workbook = new AsposeCells.Workbook(fstream);

                //Accessing the first worksheet in the Excel file
                AsposeCells.Worksheet worksheet = workbook.Worksheets[0];

                int maxDataRow = worksheet.Cells.MaxDataRow - firstRow + 1;

                //Exporting the contents to DataTable
                dtReturn = worksheet.Cells.ExportDataTable(firstRow, firstColumn, maxDataRow, worksheet.Cells.MaxDataColumn + 1, true);

                // Delete first row [default as description datatype of column]
                if (dtReturn != null)
                {
                    dtReturn.TableName = "Data";
                    if (dtReturn.Rows.Count > 0 && removeFirst)
                    {
                        // Remove first row
                        dtReturn.Rows.RemoveAt(0);
                    }
                }

                if (extendedColumns != null && extendedColumns.Count > 0)
                {
                    // Alter table, add new column range
                    dtReturn.Columns.AddRange(extendedColumns.ToArray());

                    foreach (DataColumn col in extendedColumns)
                    {
                        // col.Caption: save the postion of this cell before
                        Aspose.Cells.Cell cellValue = worksheet.Cells[col.Caption];
                        if (cellValue != null)
                        {
                            extendedValues.Add(new KeyValuePair<DataColumn, object>(col, cellValue.Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if (debug)
                  
                    throw ex;
                
#endif
                //dtReturn = new DataTable();
                //throw;
            }
            finally
            {
                if (fstream != null)
                {
                    //Closing the file stream to free all resources
                    fstream.Close();
                }
            }

            // Return table
            return dtReturn;
        }

        /// <summary>
        /// Export datatable
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="firstRow"></param>
        /// <param name="firstColumn"></param>
        /// <returns>DataTable | null if it export fail</returns>
        public static DataTable ExportDataTable(string fileName, int firstRow, int firstColumn)
        {
            DataTable dtReturn = null;
            FileStream fstream = null;
            try
            {
                //Creating a file stream containing the Excel file to be opened
                //fstream = new FileStream(fileName, FileMode.Open);

                //Instantiating a Workbook object
                //Opening the Excel file through the file stream
                AsposeCells.Workbook workbook = new AsposeCells.Workbook(fileName);

                //Accessing the first worksheet in the Excel file
                AsposeCells.Worksheet worksheet = workbook.Worksheets[0];

                int maxDataRow = worksheet.Cells.MaxDataRow - firstRow + 1;

                //Exporting the contents to DataTable
                dtReturn = worksheet.Cells.ExportDataTable(firstRow, firstColumn, maxDataRow, worksheet.Cells.MaxDataColumn, true);
            }
            catch (Exception)
            {
                //throw;
            }
            finally
            {
                //if (fstream != null)
                //{
                //    //Closing the file stream to free all resources
                //    fstream.Close();
                //}
            }

            // Return table
            return dtReturn;
        }

        /// <summary>
        /// Export datatable
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="firstRow"></param>
        /// <param name="firstColumn"></param>
        /// <returns>DataTable | null if it export fail</returns>
        public static DataTable ExportDataTable(Stream fileName, int firstRow, int firstColumn)
        {
            DataTable dtReturn = null;
            try
            {
                //Instantiating a Workbook object
                //Opening the Excel file through the file stream
                AsposeCells.Workbook workbook = new AsposeCells.Workbook(fileName);

                //Accessing the first worksheet in the Excel file
                AsposeCells.Worksheet worksheet = workbook.Worksheets[0];

                int maxDataRow = worksheet.Cells.MaxDataRow - firstRow + 1;

                //Exporting the contents to DataTable
                dtReturn = worksheet.Cells.ExportDataTable(firstRow, firstColumn, maxDataRow, worksheet.Cells.MaxDataColumn + 1, true);
            }
            catch (Exception)
            {
                //throw;
            }
            finally
            {
            }

            // Return table
            return dtReturn;
        }

        /// <summary>
        /// [Aspose] Export DataTable from worksheet
        /// </summary>
        /// <param name="workSheet"></param>
        /// <param name="firstRow"></param>
        /// <param name="firstColumn"></param>
        /// <param name="totalRows"></param>
        /// <param name="totalColumns"></param>
        /// <returns></returns>
        public static DataTable ExportDataTable(AsposeCells.Worksheet workSheet,
            int firstRow, int firstColumn, int totalRows, int totalColumns)
        {
            //Export data table
            DataTable dt = workSheet.Cells.ExportDataTable(firstRow, firstColumn,
                totalRows, totalColumns, true);
            return dt;
        }

        /// <summary>
        /// [Aspose]
        /// Set range for NSeries
        /// </summary>
        /// <param name="chart"></param>
        public static void SetNSeriesRange(AsposeCells.Charts.Chart chart)
        {
            var countColAxis = 0;
            var firstColAxis = string.Empty;
            var nameColAxis = string.Empty;

            countColAxis = chart.NSeries.CategoryData.LastIndexOf(":");
            firstColAxis = chart.NSeries.CategoryData.Substring(chart.NSeries.CategoryData.LastIndexOf("$") + 1);
            nameColAxis = chart.NSeries.CategoryData.Substring(chart.NSeries.CategoryData.LastIndexOf("$") - 2, 3);

            if (countColAxis >= 0)
            {
                chart.NSeries.CategoryData = chart.NSeries.CategoryData.Replace(nameColAxis + firstColAxis,
                    nameColAxis + ((CountDataSource - 1) + int.Parse(firstColAxis)).ToString());
            }
            else
            {
                chart.NSeries.CategoryData = string.Format("{0}:{1}{2}", chart.NSeries.CategoryData, nameColAxis,
                    ((CountDataSource - 1) + int.Parse(firstColAxis)).ToString());
            }
        }

        /// <summary>
        /// [Aspose]
        /// Set range for Lengend
        /// </summary>
        /// <param name="chart"></param>
        public static void SetLengendRange(AsposeCells.Charts.Chart chart)
        {
            var countColLengend = 0;
            var firstColLengend = string.Empty;
            var nameColLengend = string.Empty;

            foreach (AsposeCells.Charts.Series lengend in chart.NSeries)
            {
                countColLengend = lengend.Values.LastIndexOf(":");
                firstColLengend = lengend.Values.Substring(lengend.Values.LastIndexOf("$") + 1);
                nameColLengend = lengend.Values.Substring(lengend.Values.LastIndexOf("$") - 2, 3);
                if (countColLengend >= 0)
                {
                    lengend.Values = lengend.Values.Replace(nameColLengend + firstColLengend,
                        nameColLengend + ((CountDataSource - 1) + int.Parse(firstColLengend)).ToString());
                }
                else
                {
                    lengend.Values = string.Format("{0}:{1}{2}", lengend.Values, nameColLengend,
                     ((CountDataSource - 1) + int.Parse(firstColLengend)).ToString());
                }
            }
        }

        /// <summary>
        /// [Aspose]
        /// Get chart and refresh data
        /// </summary>
        /// <param name="charts"></param>
        public static void Chart(AsposeCells.Charts.ChartCollection charts)
        {
            if (charts != null && charts.Count > 0)
            {
                foreach (AsposeCells.Charts.Chart chart in charts)
                {
                    chart.RefreshPivotData();
                    chart.Calculate();

                    if (string.IsNullOrEmpty(chart.PivotSource))
                    {
                        SetNSeriesRange(chart);
                        SetLengendRange(chart);
                        chart.Legend.IsAutomaticSize = true;
                        chart.AutoScaling = true;
                    }
                }
            }
        }

        /// <summary>
        /// [Aspose] Get pivot and calculate data for pivot table
        /// </summary>
        /// <param name="workBook"></param>
        public static void WorkSheetRefresh(AsposeCells.WorksheetCollection workSheets, bool isAutoFitMergedCells = false)
        {
            foreach (AsposeCells.Worksheet workSheet in workSheets)
            {
                //Auto fit height rows
                if (!isAutoFitMergedCells)
                {
                    workSheet.AutoFitRows();
                }
                else
                {
                    AsposeCells.AutoFitterOptions options = new AsposeCells.AutoFitterOptions();
                    options.AutoFitMergedCells = true;
                    workSheet.AutoFitRows(options);
                }

                //Get the pivot tables in the sheet
                AsposeCells.Pivot.PivotTableCollection pivotTables = workSheet.PivotTables;
                AsposeCells.Charts.ChartCollection charts = workSheet.Charts;

                if (ImageList != null && ImageList.Count > 0)
                {
                    foreach (var image in ImageList)
                    {
                        AddImage(workSheet, image.Key, image.Value);
                    }
                }

                if (ImagePicture != null && ImagePicture.Count > 0)
                {
                    foreach (var image in ImagePicture)
                    {
                        AddImagePicture(workSheet, image.Key, image.Value);
                    }
                }

                if (pivotTables != null && pivotTables.Count > 0)
                {
                    foreach (AsposeCells.Pivot.PivotTable pivotTable in pivotTables)
                    {
                        //pivotTable.RefreshDataOnOpeningFile = true;
                        //Set the refresh data flag on
                        pivotTable.RefreshDataFlag = true;

                        //Refresh and calculate the pivot table data
                        //pivotTable.MergeLabels = true;
                        pivotTable.PreserveFormatting = true;
                        pivotTable.SaveData = true;

                        pivotTable.RefreshData();
                        pivotTable.CalculateData();
                        pivotTable.CalculateRange();
                    }
                }

                //Get chart and calculate data
                Chart(charts);
            }
        }

        /// <summary>
        /// [Aspose]
        /// Add Image
        /// </summary>
        /// <param name="workSheet"></param>
        /// <param name="paramName"></param>
        /// <param name="pathImage"></param>
        /// <returns></returns>
        public static AsposeCells.Cell AddImage(AsposeCells.Worksheet workSheet, string paramName, string pathImage)
        {
            var cell = FindCell(workSheet, paramName);
            if (cell != null)
            {
                cell.Value = "";
                workSheet.Pictures.Add(cell.Row, cell.Column, pathImage);
            }
            return cell;
        }

        /// <summary>
        /// [Aspose]
        /// Add Image
        /// </summary>
        /// <param name="workSheet"></param>
        /// <param name="paramName"></param>
        /// <param name="streamImage"></param>
        /// <returns></returns>
        public static AsposeCells.Cell AddImage(AsposeCells.Worksheet workSheet, string paramName, Stream streamImage)
        {
            var cell = FindCell(workSheet, string.Format("$${0}", paramName));
            if (cell != null)
            {
                cell.Value = "";
                if (streamImage.Length != 0)
                {
                    workSheet.Pictures.Add(cell.Row, cell.Column, streamImage);
                }
                else
                {
                    workSheet.Cells[cell.Row, cell.Column].Value = "";
                }
            }
            return cell;
        }

        /// <summary>
        /// [Aspose]
        /// Add Image Picture
        /// </summary>
        /// <param name="workSheet"></param>
        /// <param name="paramName"></param>
        /// <param name="streamImage"></param>
        /// <returns></returns>
        public static byte[] AddImagePicture(AsposeCells.Worksheet workSheet, string paramName, byte[] Image)
        {
            byte[] images = Image ?? new byte[0];

            if (images.Length != 0)
            {
                var logo = workSheet.Pictures.Cast<Aspose.Cells.Drawing.Picture>().ToList().Find(m => m.Name == paramName);
                if (logo != null)
                {
                    logo.Data = images;
                }
            }

            return images;
        }

        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Using DataSet
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportReport(DataSet dataSource, string templatePath,
            bool isPDF = false, OfficeType type = OfficeType.XLSX, bool isHideCXR = false, Dictionary<string, string> format = null)
        {
            try
            {
                Stream stream = new MemoryStream();

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Get all worksheet
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                //workSheets[DEFAULTSHEETNAME].AutoFitRows();

                // Add default parameters
                MergeWithDefaultParameters();

                // Set value for params on template
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        designer.SetDataSource(item.Key, item.Value);
                    }
                }

                if (format != null && format.Count > 0)
                {
                    foreach (AsposeCells.Worksheet workSheet in workSheets)
                    {
                        foreach (DataTable table in dataSource.Tables)
                        {
                            foreach (var item in format)
                            {
                                var cell = FindCell(workSheet, string.Format("&={0}.{1}", table.TableName, item.Key));
                                var cellSumm = FindCell(workSheet, string.Format("sum({0}.{1})", table.TableName, item.Key), lookInType: AsposeCells.LookInType.Comments);
                                if (cell != null)
                                {
                                    var style = cell.GetStyle();
                                    style.Number = 4;
                                    style.Custom = item.Value.Equals("#,0.") ? "#,0" : item.Value;
                                    cell.SetStyle(style);
                                }
                                if (cellSumm != null)
                                {
                                    var style = cellSumm.GetStyle();
                                    style.Number = 4;
                                    style.Custom = item.Value.Equals("#,0.") ? "#,0" : item.Value;
                                    cellSumm.SetStyle(style);
                                }
                            }
                        }
                    }
                }

                //Set data source
                designer.SetDataSource(dataSource);


                //Process the markers.
                designer.Process();
                if (templatePath.Contains("DRR3019_4"))
                {
                    if (dataSource.Tables[0].Rows.Count == 2 && isHideCXR)
                    {
                        workSheets["Report"].Cells.HideColumn(2);
                    }
                }
                //Get and refresh pivot table
                if (workSheets != null && workSheets.Count > 0)
                {
                    WorkSheetRefresh(workSheets);
                }

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
            }
        }

        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Using DataTable
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportReport(DataTable dataSource, string templatePath,
            bool isPDF = false, OfficeType type = OfficeType.XLSX, Dictionary<string, string> format = null, bool isAutoFitMergedCells = false)
        {
            try
            {
                Stream stream = new MemoryStream();

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Get all worksheet
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                //workSheets[DEFAULTSHEETNAME].AutoFitRows();

                // Add default parameters
                MergeWithDefaultParameters();

                // Set value for params on template
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        designer.SetDataSource(item.Key, item.Value);
                    }
                }

                if (format != null && format.Count > 0)
                {
                    foreach (AsposeCells.Worksheet workSheet in workSheets)
                    {
                        foreach (var item in format)
                        {
                            var cell = FindCell(workSheet, string.Format("&={0}.{1}", dataSource.TableName, item.Key));
                            if (cell != null)
                            {
                                var style = cell.GetStyle();
                                style.Number = 4;
                                style.Custom = item.Value.Equals("#,0.") ? "#,0" : item.Value;
                                cell.SetStyle(style);
                            }
                        }
                    }
                }

                //Set data source
                designer.SetDataSource(dataSource);

                //Process the markers.
                designer.Process();

                //Get and refresh pivot table
                if (workSheets != null && workSheets.Count > 0)
                {
                    WorkSheetRefresh(workSheets, isAutoFitMergedCells);
                }

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
            }
        }

        //test thu------------------------
        public static Stream ExportReport(List<DataTable> dataSource, string templatePath,
            bool isPDF = false, OfficeType type = OfficeType.XLSX)
        {
            try
            {
                Stream stream = new MemoryStream();

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Get all worksheet
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                //workSheets[DEFAULTSHEETNAME].AutoFitRows();

                // Add default parameters
                MergeWithDefaultParameters();

                // Set value for params on template
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        designer.SetDataSource(item.Key, item.Value);
                    }
                }

                //Set data source
                foreach (DataTable dt in dataSource)
                {
                    designer.SetDataSource(dt);
                }
                //Process the markers.
                designer.Process();

                //Get and refresh pivot table
                if (workSheets != null && workSheets.Count > 0)
                {
                    WorkSheetRefresh(workSheets);
                }

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
            }
        }



        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Grouping Data
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportReport(DataSet dataSource, string templatePath,
            int firstRow, string fieldGroupID, string fieldGroupName,
            string workSheetName, bool isPDF = false,
            string firstColumn = "", string lastColumn = "",
            OfficeType type = OfficeType.XLSX)
        {
            try
            {
                //int mergeCount = 0;
                //int countColumn = 0;
                //string fieldGroupData = string.Empty;
                //var style = new AsposeCells.Style();
                Stream stream = new MemoryStream();

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Get all worksheet
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                AsposeCells.Worksheet sheetReport = designer.Workbook.Worksheets[workSheetName];
                //workSheets[DEFAULTSHEETNAME].AutoFitRows();

                if (sheetReport == null)
                {
                    return stream;
                }

                // Add default parameters
                MergeWithDefaultParameters();

                // Set value for params on template
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        designer.SetDataSource(item.Key, item.Value);
                    }
                }

                GroupingData(sheetReport, dataSource,
                    fieldGroupID, fieldGroupName,
                    firstRow, firstColumn, lastColumn);

                //Process the markers.
                designer.Process();

                //Get and refresh pivot table
                if (workSheets != null && workSheets.Count > 0)
                {
                    WorkSheetRefresh(workSheets);
                }

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
                ColumnsList = null;
            }
        }

        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Gắn hyperlink vào cell
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportReport(DataSet dataSource, string templatePath, string sheetName, string host,
            bool isPDF = false, OfficeType type = OfficeType.XLSX)
        {
            try
            {
                Stream stream = new MemoryStream();

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Get all worksheet
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                AsposeCells.Worksheet sheetReport = workSheets[sheetName];

                // Add default parameters
                MergeWithDefaultParameters();

                // Set value for params on template
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        designer.SetDataSource(item.Key, item.Value);
                    }
                }

                //Set data source
                designer.SetDataSource(dataSource);


                //Process the markers.
                designer.Process();

                //Tạo hyperlink cho cell
                int i = 2;
                foreach (DataRow dr in dataSource.Tables[0].Rows)
                {
                    string a = dr["APK"].ToString();
                    if (!String.IsNullOrEmpty(a))
                    {
                        sheetReport.Hyperlinks.Add("F" + i.ToString() + "", 1, 1, "http://" + host + "/DRM/DRF2020/DRF2022/?apk=" + a);
                    }
                    i++;
                }


                //Get and refresh pivot table
                if (workSheets != null && workSheets.Count > 0)
                {
                    WorkSheetRefresh(workSheets);
                }

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
            }
        }
        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Download file excel
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportReport(string templatePath, bool isPDF = false, OfficeType type = OfficeType.XLSX)
        {
            try
            {
                Stream stream = new MemoryStream();

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Process the markers.
                designer.Process();

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
            }
        }

        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Grouping Data with two field
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportGroupReportUsingSmarker(DataSet dataSource, string templatePath,
            int firstRow, string workSheetName, int numberRowHeader,
            string groupFieldID, string groupFieldName,
            string groupFieldID1 = "", string groupFieldName1 = "",
            bool isPDF = false,
            string firstColumn = "", string lastColumn = "",
            OfficeType type = OfficeType.XLSX)
        {
            try
            {
                int mergeCount = 0;
                int groupParent = 0;
                int rowHeader = 0;
                int currentRow = 0;
                DataRow[] dataRows = null;
                var style = new AsposeCells.Style();
                Stream stream = new MemoryStream();

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Get all worksheet
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                AsposeCells.Worksheet sheetReport = designer.Workbook.Worksheets[workSheetName];

                if (sheetReport == null)
                {
                    return stream;
                }

                // Add default parameters
                MergeWithDefaultParameters();

                // Set value for params on template
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        designer.SetDataSource(item.Key, item.Value);
                    }
                }

                if (dataSource != null && dataSource.Tables.Count > 0 && dataSource.Tables[0].Rows.Count > 0)
                {
                    string fieldGroupData1 = Convert.ToString(dataSource.Tables[0].Rows[0][groupFieldID]);
                    string fieldGroupData2 = string.Empty; //Convert.ToString(dataSource.Tables[0].Rows[0][groupFieldID1]);
                    var styleFirstCell = sheetReport.Cells[string.Format("{0}{1}",
                        string.IsNullOrEmpty(firstColumn) ? ColumnsList[0].Key : firstColumn,
                        firstRow - 1)].GetStyle();

                    for (int i = 0; i < dataSource.Tables[0].Rows.Count; i++)
                    {
                        //Level 1
                        if (!Convert.ToString(dataSource.Tables[0].Rows[i][groupFieldID]).ToLower().Equals(fieldGroupData1.ToLower()))
                        {
                            if (groupParent != 0)
                            {
                                rowHeader += 2;
                            }
                            fieldGroupData1 = Convert.ToString(dataSource.Tables[0].Rows[i][groupFieldID]);
                            fieldGroupData2 = Convert.ToString(dataSource.Tables[0].Rows[i][groupFieldID1]);

                            groupParent = 0;
                            mergeCount = 0;
                        }

                        //Level 2
                        if (!Convert.ToString(dataSource.Tables[0].Rows[i][groupFieldID1]).ToLower().Equals(fieldGroupData2.ToLower()))
                        {
                            fieldGroupData2 = Convert.ToString(dataSource.Tables[0].Rows[i][groupFieldID1]);
                            mergeCount = 0;
                        }

                        mergeCount++;
                        groupParent++;

                        if (groupParent == 1)
                        {
                            sheetReport.Cells[string.Format("{0}{1}",
                                     string.IsNullOrEmpty(firstColumn) ? ColumnsList[0].Key : firstColumn,
                                     firstRow + rowHeader)].Value = dataSource.Tables[0].Rows[i][groupFieldName];
                            for (var k = 0; k < numberRowHeader; k++)
                            {
                                sheetReport.Cells.CopyRow(sheetReport.Cells, (firstRow - 1) + (k + 1), (firstRow - 1) + rowHeader + (k + 1));
                            }
                            rowHeader += numberRowHeader;
                        }

                        if (mergeCount == 1)
                        {
                            //Get the range of cells i.e.., B1:C1.
                            sheetReport.Cells[string.Format("{0}{1}",
                                string.IsNullOrEmpty(firstColumn) ? ColumnsList[0].Key : firstColumn,
                                firstRow + (rowHeader + 1))].Value = dataSource.Tables[0].Rows[i][groupFieldName1];

                            var rangeGroup = sheetReport.Cells.CreateRange(string.Format("{0}{1}",
                                string.IsNullOrEmpty(firstColumn) ? ColumnsList[0].Key : firstColumn,
                                firstRow + (rowHeader + 1)),
                                string.Format("{0}{1}",
                                string.IsNullOrEmpty(lastColumn) ? ColumnsList[ColumnsList.Count - 1].Key : lastColumn,
                                firstRow + (rowHeader + 1)));

                            //Merge the cells.
                            rangeGroup.Merge();
                            styleFirstCell.HorizontalAlignment = AsposeCells.TextAlignmentType.Left;
                            rangeGroup.SetStyle(styleFirstCell);
                            rowHeader++;

                            //Filter datasoure => group field
                            if (dataSource.Tables[0].Rows[i][groupFieldID].GetType() == typeof(DateTime))
                            {
                                dataRows = dataSource.Tables[0].Select(string.Format("{0} = '{1}' AND {2} = '{3}'",
                                    groupFieldID,
                                    string.IsNullOrEmpty(fieldGroupData1) ? ((DateTime)dataSource.Tables[0].Rows[i][groupFieldID]).ToString("yyyy/MM/dd") : DateTime.Parse(fieldGroupData1).ToString("yyyy/MM/dd"),
                                    groupFieldID1,
                                    string.IsNullOrEmpty(fieldGroupData2) ? dataSource.Tables[0].Rows[i][groupFieldID1] : fieldGroupData2));
                            }
                            else
                            {
                                dataRows = dataSource.Tables[0].Select(string.Format("ISNULL({0}, '') = '{1}' AND ISNULL({2}, '') = '{3}'",
                                    groupFieldID,
                                    string.IsNullOrEmpty(fieldGroupData1) ? Convert.ToString(dataSource.Tables[0].Rows[i][groupFieldID]) : fieldGroupData1,
                                    groupFieldID1,
                                    string.IsNullOrEmpty(fieldGroupData2) ? Convert.ToString(dataSource.Tables[0].Rows[i][groupFieldID1]) : fieldGroupData2));
                            }

                            sheetReport.Cells.CopyRow(sheetReport.Cells, (firstRow - 1) - 1, (firstRow - 1) + rowHeader + 1);

                            //Copy dòng template mẫu
                            if (ColumnsList != null && ColumnsList.Count > 0)
                            {
                                foreach (var column in ColumnsList)
                                {
                                    if (column.Key.Equals(ColumnsList[0].Key))
                                    {
                                        sheetReport.Cells[string.Format("{0}{1}", column.Key, firstRow + rowHeader + 1)].Value =
                                            string.Format("&=&=ROW() - {0}", (firstRow + (rowHeader - 1)) + currentRow);
                                        continue;
                                    }
                                    sheetReport.Cells[string.Format("{0}{1}", column.Key, firstRow + rowHeader + 1)].Value =
                                                Convert.ToString(sheetReport.Cells[string.Format("{0}{1}",
                                                column.Key, firstRow + rowHeader + 1)].Value).Replace(dataSource.Tables[0].TableName,
                                                string.Format("{0}{1}", dataSource.Tables[0].TableName, i));
                                }

                                //TODO: Test sum
                                SumFormula(sheetReport, firstRow + rowHeader + 2,
                                    Convert.ToString(ColumnsList[0].Key), Convert.ToString(ColumnsList[ColumnsList.Count - 2].Key));
                                sheetReport.Cells[string.Format("{0}{1}",
                                    Convert.ToString(ColumnsList[ColumnsList.Count - 1].Key), firstRow + rowHeader + 2)].Formula =
                                    string.Format("=SUM({0}{1}:{0}{1})", Convert.ToString(ColumnsList[ColumnsList.Count - 1].Key), firstRow + rowHeader + 1);
                                rowHeader++;

                                currentRow += (dataRows.Length - 1);
                            }

                            //Bind data source
                            if (dataRows.Length > 0)
                            {
                                var dataTable = dataRows.CopyToDataTable();
                                dataTable.TableName = string.Format("{0}{1}", dataSource.Tables[0].TableName, i);
                                designer.SetDataSource(dataTable);
                                i += dataRows.Length;
                            }
                            rowHeader++;
                        }
                    }
                    sheetReport.Cells.DeleteRow((firstRow - 2));
                }

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Process the markers.
                //designer.Process();

                //Get and refresh pivot table
                if (workSheets != null && workSheets.Count > 0)
                {
                    WorkSheetRefresh(workSheets);
                }

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
                ColumnsList = null;
            }
        }

        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Grouping Data with two field
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportGroupReportUsingSmarker(DataSet dataSource,
            string templatePath, bool isSum = false, bool isPDF = false,
            OfficeType type = OfficeType.XLSX)
        {
            try
            {
                //DataSet for template
                var dataSet = new DataSet();

                string fieldGroup = string.Empty;
                DataRow[] dataRows = null;
                var style = new AsposeCells.Style();
                Stream stream = new MemoryStream();
                int rowTemplateIndex = 0;
                int rowGroupIndex = 0;
                int rowSumIndex = 0;
                int rowSTTIndex = 0;

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Get all worksheet
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                AsposeCells.Worksheet sheetReport = designer.Workbook.Worksheets[0];

                //Cell cần group
                AsposeCells.Cell cellStartGroupParent = FindCell(workSheets[0], "%%Start", AsposeCells.LookAtType.StartWith);
                AsposeCells.Cell cellEndGroupParent = FindCell(workSheets[0], "%%End", AsposeCells.LookAtType.StartWith);
                AsposeCells.Cell cellGroup = FindCell(workSheets[0], "%%%", AsposeCells.LookAtType.StartWith);

                if (cellGroup != null)
                {
                    rowSTTIndex = cellGroup.Row + 1;
                    fieldGroup = Convert.ToString(cellGroup.Value).Replace("%%%=", "");
                }

                if (sheetReport == null)
                {
                    return stream;
                }

                // Add default parameters
                MergeWithDefaultParameters();

                // Set value for params on template
                if (ExcelItems != null && ExcelItems.Count > 0)
                {
                    foreach (var item in ExcelItems)
                    {
                        designer.SetDataSource(item.Key, item.Value);
                    }
                }

                if (dataSource != null && dataSource.Tables.Count > 0 && dataSource.Tables[0].Rows.Count > 0)
                {
                    //Sort data table theo field group
                    var dv = dataSource.Tables[0].DefaultView;
                    dv.Sort = fieldGroup;
                    var ds = dv.ToTable();
                    ds.TableName = dataSource.Tables[0].TableName;

                    string fieldGroupValue = Convert.ToString(ds.Rows[0][fieldGroup]);
                    cellGroup.Value = fieldGroupValue;
                    int columnCount = 0;
                    int groupCount = 0;
                    int j = 0;

                    for (int i = 0; i < ds.Rows.Count; i++)
                    {
                        //Set STT cho mỗi group
                        rowSTTIndex += dataRows == null ? 0 : dataRows.Length + 2;
                        if (i != 0)
                        {
                            groupCount += isSum ? 3 : 2;
                            rowGroupIndex = cellGroup.Row + groupCount;
                            rowTemplateIndex = (cellGroup.Row + 1) + groupCount;
                            rowSumIndex = (cellGroup.Row + 2) + groupCount;

                            sheetReport.Cells.InsertRows(rowGroupIndex, isSum ? 3 : 2);
                            sheetReport.Cells.CopyRow(sheetReport.Cells, cellGroup.Row, rowGroupIndex);
                            sheetReport.Cells.CopyRow(sheetReport.Cells, cellGroup.Row + 1, rowTemplateIndex);

                            if (isSum) //Add thêm dòng tính tổng
                            {
                                sheetReport.Cells.CopyRow(sheetReport.Cells, cellGroup.Row + 2, rowSumIndex);
                            }

                            //Set text cell group
                            sheetReport.Cells[rowGroupIndex, 0].Value = ds.Rows[i][fieldGroup];

                            columnCount = sheetReport.Cells.Rows[rowTemplateIndex].LastDataCell.Column;
                            for (j = 0; j < columnCount; j++)
                            {
                                if (j == 0)//Set STT cho mỗi group
                                {
                                    sheetReport.Cells[rowTemplateIndex, j].Value = string.Format("&=&=ROW() - {0}", rowSTTIndex);
                                }
                                if (Convert.ToString(sheetReport.Cells[rowTemplateIndex, j].Value).Contains(string.Format("{0}", ds.TableName)))
                                {
                                    sheetReport.Cells[rowTemplateIndex, j].Value = Convert.ToString(sheetReport.Cells[rowTemplateIndex, j].Value).Replace(ds.TableName, ds.TableName + (i));
                                }
                            }
                        }

                        //Filter datasoure => group field
                        dataRows = dataSource.Tables[0].Select(string.Format("ISNULL({0}, '') = '{1}'",
                                fieldGroup, Convert.ToString(ds.Rows[i][fieldGroup])));

                        //Bind data source
                        if (dataRows.Length > 0)
                        {
                            //Set model for group
                            var dataTable = dataRows.CopyToDataTable();
                            dataTable.TableName = string.Format("{0}{1}", ds.TableName, i == 0 ? "" : i.ToString());

                            //Add datatable in dataset
                            dataSet.Tables.Add(dataTable);
                        }

                        //Tăng chỉ số i length của dataRows
                        i += dataRows.Length;
                    }
                }

                //Set datasource for template
                designer.SetDataSource(dataSet);

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Process the markers.
                designer.Process();

                //Set printarea theo data
                sheetReport.PageSetup.PrintArea = sheetReport.Cells.MaxDisplayRange.Name;

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
                ColumnsList = null;
            }
        }

        /// <summary>
        /// [Aspose] Export Excel/PDF
        /// Grouping Data with two field
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="templatePath"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream ExportGroupReportSmarker(DataSet dataSource,
            string templatePath, bool isPDF = false,
            OfficeType type = OfficeType.XLSX)
        {
            try
            {
                //DataSet for template
                var dataSet = new DataSet();

                AsposeCells.Range sourceRange = null;
                string fieldParentGroup = string.Empty;
                string fieldGroup = string.Empty;
                DataRow[] dataGroupParentRows = null;
                DataRow[] dataGroupChidRows = null;
                var style = new AsposeCells.Style();
                Stream stream = new MemoryStream();
                var regx = new Regex(@"[A-z+](.*?)");
                int maxDataRow = 0;
                int indexSTT = 0;
                int dataSourceCount = 0;
                int maxDataColumn = 0;
                int rowGroupParentCount = 0;
                int rowGroupChildCount = 0;
                int dataSourceIndex = 0;
                int modelIndex = 0;
                int rowStartParentIndex = 0;
                int rowStartChildIndex = 0;

                //Create WorkbookDesigner object.
                AsposeCells.WorkbookDesigner designer = new AsposeCells.WorkbookDesigner();
                //AsposeCells.Workbook workBook = designer.Workbook;
                designer.Workbook = OpenExcelFile(templatePath, type);

                //Get all worksheet
                var sheetTempIndex = designer.Workbook.Worksheets.Count;
                AsposeCells.WorksheetCollection workSheets = designer.Workbook.Worksheets;
                AsposeCells.Worksheet sheetReport = designer.Workbook.Worksheets[0];
                AsposeCells.Worksheet sheetTemp = designer.Workbook.Worksheets.Insert(sheetTempIndex, AsposeCells.SheetType.Worksheet);

                //Cell cần group
                AsposeCells.Cell cellStartGroupParent = FindCell(workSheets[0], "%%Start", AsposeCells.LookAtType.StartWith);
                AsposeCells.Cell cellEndGroupParent = FindCell(workSheets[0], "%%End", AsposeCells.LookAtType.StartWith);
                AsposeCells.Cell cellStartGroupChild = FindCell(workSheets[0], "%%%Start", AsposeCells.LookAtType.StartWith);
                AsposeCells.Cell cellEndGroupChild = FindCell(workSheets[0], "%%%End", AsposeCells.LookAtType.StartWith);
                rowGroupParentCount = cellEndGroupParent.Row - cellStartGroupParent.Row;
                rowGroupChildCount = (cellEndGroupChild.Row - cellStartGroupChild.Row);
                maxDataColumn = sheetReport.Cells.MaxDataColumn;

                //Group cha
                if (cellStartGroupParent != null)
                {
                    fieldParentGroup = Convert.ToString(cellStartGroupParent.Value).Replace("%%Start=", "");
                }

                //Group con
                if (cellStartGroupChild != null)
                {
                    fieldGroup = Convert.ToString(cellStartGroupChild.Value).Replace("%%%Start=", "");
                }

                if (sheetReport == null)
                {
                    return stream;
                }

                // Add default parameters
                MergeWithDefaultParameters();

                if (dataSource != null && dataSource.Tables.Count > 0 && dataSource.Tables[0].Rows.Count > 0)
                {
                    //Sort data table theo field group
                    var dvParent = dataSource.Tables[0].DefaultView;
                    dvParent.Sort = fieldParentGroup;
                    var dsParent = dvParent.ToTable();
                    dsParent.TableName = dataSource.Tables[0].TableName;

                    string fieldGroupParentValue = Convert.ToString(dsParent.Rows[0][fieldParentGroup]);
                    cellStartGroupParent.Value = fieldGroupParentValue;
                    int columnCount = 0;
                    int groupCount = cellStartGroupChild.Row - cellStartGroupParent.Row;
                    int groupChildCount = 0;
                    int j = 0;

                    //Row index parent
                    rowStartParentIndex = cellStartGroupParent.Row;
                    rowStartChildIndex = cellStartGroupChild.Row;

                    //Get Cell Name by row index and column index (Group parent)
                    var lastSourceCellName = AsposeCells.CellsHelper.CellIndexToName(cellEndGroupParent.Row, maxDataColumn);
                    //Source Range template (Group parent)
                    sourceRange = sheetReport.Cells.CreateRange(cellStartGroupParent.Name, lastSourceCellName);
                    var rangeParentTemp = sheetTemp.Cells.CreateRange(cellStartGroupParent.Name, lastSourceCellName);
                    rangeParentTemp.Copy(sourceRange);

                    //Get Cell Name by row index and column index (Group children)
                    var lastSourceChildCellName = AsposeCells.CellsHelper.CellIndexToName(cellEndGroupChild.Row - 1, maxDataColumn);
                    //Source Range template (Group children)
                    var rangeGroupChild = sheetReport.Cells.CreateRange(cellStartGroupChild.Name, lastSourceChildCellName);
                    //Copy range template to sheet temp
                    var rangeChildTemp = sheetTemp.Cells.CreateRange(cellStartGroupChild.Name, lastSourceChildCellName);
                    rangeChildTemp.Copy(rangeGroupChild);

                    //Xóa range template trên sheet report
                    sheetReport.Cells.DeleteRows(rangeGroupChild.FirstRow, rangeGroupChild.RowCount);

                    for (int i = 0; i < dsParent.Rows.Count; i++)
                    {
                        //Filter datasoure => group field
                        dataGroupParentRows = dataSource.Tables[0].Select(string.Format("ISNULL({0}, '') = '{1}'",
                                fieldParentGroup, Convert.ToString(dsParent.Rows[i][fieldParentGroup])));

                        //Filter group children
                        var dsGroup = dataGroupParentRows.CopyToDataTable();
                        var dvGroup = dsGroup.DefaultView;
                        dvGroup.Sort = fieldGroup;
                        dsGroup = dvGroup.ToTable();

                        if (i != 0)
                        {
                            //Find column name by regularexpression
                            var textSearch = regx.Match(cellStartGroupParent.Name);

                            sheetReport.Cells.InsertRows((rowStartParentIndex + 2) + groupCount, rowGroupParentCount);

                            //Get Cell Name by row index and column index
                            var lastDesCellName = AsposeCells.CellsHelper.CellIndexToName((rowStartParentIndex + groupCount) + rowGroupParentCount, maxDataColumn);

                            //Create next range
                            var newRange = sheetReport.Cells.CreateRange(string.Format("{0}{1}", textSearch.Groups[0].Value,
                                (rowStartParentIndex + 2) + groupCount),
                                lastDesCellName);

                            //Copy template
                            newRange.Copy(rangeParentTemp);

                            //Set text cell group
                            sheetReport.Cells[(rowStartParentIndex + 1) + groupCount, 0].Value = dsParent.Rows[i][fieldParentGroup];

                            rowStartChildIndex = (rowStartParentIndex + groupCount + rowGroupParentCount) - rowGroupChildCount;
                            groupChildCount = 0;
                            groupCount += rowGroupParentCount;

                            //Xóa range template trên sheet report
                            sheetReport.Cells.DeleteRows(rowStartChildIndex + 1 + groupChildCount, rowGroupChildCount);
                        }

                        //Group con
                        for (j = 0; j < dsGroup.Rows.Count; j++)
                        {
                            //Find column name by regularexpression
                            var textSearchChildren = regx.Match(cellStartGroupChild.Name);

                            sheetReport.Cells.InsertRows(rowStartChildIndex + 1 + groupChildCount, rowGroupChildCount);

                            //Get Cell Name by row index and column index
                            var lastDesCellName = AsposeCells.CellsHelper.CellIndexToName(((rowStartChildIndex) + groupChildCount) + rowGroupChildCount, maxDataColumn);

                            //Create next range
                            var newRangeChild = sheetReport.Cells.CreateRange(string.Format("{0}{1}", textSearchChildren.Groups[0].Value,
                                (rowStartChildIndex + 1) + groupChildCount),
                                lastDesCellName);

                            //Copy range template
                            newRangeChild.Copy(rangeChildTemp);

                            //Set text cell group
                            sheetReport.Cells[rowStartChildIndex + groupChildCount, 0].Value = dsGroup.Rows[j][fieldGroup];

                            //Set model template
                            dataSourceCount += (dataGroupChidRows == null ? 0 : dataGroupChidRows.Length - 1);
                            indexSTT = (rowStartChildIndex + 1) + groupChildCount + dataSourceCount;
                            var rowTemplate = FindCell(sheetReport, string.Format("&={0}.", dsParent.TableName), AsposeCells.LookAtType.Contains);
                            var rowTemplateIndex = rowTemplate.Row;
                            columnCount = sheetReport.Cells.Rows[rowTemplateIndex].LastDataCell.Column;
                            for (var k = 0; k <= columnCount; k++)
                            {
                                if (k == 0)//Set STT cho mỗi group
                                {
                                    sheetReport.Cells[rowTemplateIndex, k].Value = string.Format("&=&=ROW() - {0}", indexSTT);
                                }
                                if (Convert.ToString(sheetReport.Cells[rowTemplateIndex, k].Value).Contains(dsParent.TableName))
                                {
                                    sheetReport.Cells[rowTemplateIndex, k].Value = Convert.ToString(sheetReport.Cells[rowTemplateIndex, k].Value)
                                        .Replace(dsParent.TableName, dsParent.TableName + (dataSourceIndex));
                                }
                            }

                            //Filter datasource for group children
                            dataGroupChidRows = dsGroup.Select(string.Format("ISNULL({0}, '') = '{1}'",
                                    fieldGroup, dsGroup.Rows[j][fieldGroup]));

                            //Set model for group
                            var dataTable = dataGroupChidRows.CopyToDataTable();
                            dataTable.TableName = string.Format("{0}{1}", dsParent.TableName, dataSourceIndex.ToString());

                            //Add datatable in dataset
                            dataSet.Tables.Add(dataTable);

                            j += dataGroupChidRows.Length;
                            dataSourceIndex++;

                            groupChildCount += rowGroupChildCount;
                            groupCount += rowGroupChildCount;
                        }

                        //Tăng chỉ số i length của dataRows
                        i += dataGroupParentRows.Length;
                    }
                }

                //Set datasource for template
                designer.SetDataSource(dataSet);

                //Calculate formular
                designer.Workbook.CalculateFormula();

                //Process the markers.
                //designer.Process();

                //Set printarea theo data
                sheetReport.PageSetup.PrintArea = sheetReport.Cells.MaxDisplayRange.Name;

                //Delete sheet temp
                designer.Workbook.Worksheets.RemoveAt(sheetTempIndex);

                stream = SaveToStream(designer.Workbook, isPDF, type);

                return stream;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally
            {
                ExcelItems = null;
                ImageList = null;
                ColumnsList = null;
            }
        }

        /// <summary>
        /// Group sub report
        /// </summary>
        public static void GroupingData(AsposeCells.Worksheet sheetReport, DataSet dataSource,
            string fieldGroupID, string fieldGroupName,
            int firstRow, string firstColumn, string lastColumn)
        {
            string fieldGroupData = string.Empty;
            int mergeCount = 0;
            int countColumn = 0;
            var style = new AsposeCells.Style();
            if (dataSource != null && dataSource.Tables[0].Rows.Count > 0)
            {
                fieldGroupData = Convert.ToString(dataSource.Tables[0].Rows[0][fieldGroupID]);
                var styleFirstCell = sheetReport.Cells[string.Format("{0}{1}",
                    string.IsNullOrEmpty(firstColumn) ? ColumnsList[0].Key : firstColumn,
                    firstRow - 1)].GetStyle();
                for (int i = 0; i < dataSource.Tables[0].Rows.Count; i++)
                {
                    if (!Convert.ToString(dataSource.Tables[0].Rows[i][fieldGroupID]).ToLower().Equals(fieldGroupData.ToLower()))
                    {
                        fieldGroupData = Convert.ToString(dataSource.Tables[0].Rows[i][fieldGroupID]);
                        mergeCount = 0;
                    }
                    mergeCount++;

                    if (mergeCount == 1)
                    {
                        //Get the range of cells i.e.., B1:C1.
                        sheetReport.Cells[string.Format("{0}{1}",
                            string.IsNullOrEmpty(firstColumn) ? ColumnsList[0].Key : firstColumn,
                            firstRow + i)].Value = dataSource.Tables[0].Rows[i][fieldGroupName];

                        var rangeGroup = sheetReport.Cells.CreateRange(string.Format("{0}{1}",
                            string.IsNullOrEmpty(firstColumn) ? ColumnsList[0].Key : firstColumn,
                            firstRow),
                            string.Format("{0}{1}",
                            string.IsNullOrEmpty(lastColumn) ? ColumnsList[ColumnsList.Count - 1].Key : lastColumn,
                            firstRow));

                        //Merge the cells.
                        rangeGroup.Merge();
                        styleFirstCell.HorizontalAlignment = AsposeCells.TextAlignmentType.Left;
                        rangeGroup.SetStyle(styleFirstCell);
                    }

                    //Binding data
                    if (ColumnsList != null && ColumnsList.Count > 0)
                    {
                        foreach (var column in ColumnsList)
                        {
                            style = sheetReport.Cells[string.Format("{0}{1}", column.Key, (firstRow - 1))].GetStyle();
                            //Order Column
                            if (countColumn == 0)
                            {
                                sheetReport.Cells[string.Format("{0}{1}", column.Key, (firstRow + i + 1))].Value = mergeCount;
                                sheetReport.Cells[string.Format("{0}{1}", column.Key, (firstRow + i + 1))].SetStyle(style);
                                countColumn++;
                                continue;
                            }

                            sheetReport.Cells[string.Format("{0}{1}", column.Key, (firstRow + i + 1))].Value =
                                    dataSource.Tables[0].Rows[i][Convert.ToString(column.Value)];
                            sheetReport.Cells[string.Format("{0}{1}", column.Key, (firstRow + i + 1))].SetStyle(style);
                            countColumn++;

                            if (ColumnsList.Count == countColumn)
                            {
                                countColumn = 0;
                            }
                        }
                    }

                    if (dataSource.Tables[0].Rows.Count - 1 == i)
                    {
                        sheetReport.Cells.HideRow((firstRow - 2));
                    }
                }
            }
        }


        /// <summary>
        /// In
        /// </summary>
        public static void SumFormula(AsposeCells.Worksheet workSheet,
            int row, string fromColumn, string toColumn)
        {
            var style = new AsposeCells.Style();
            var rangeSum = workSheet.Cells.CreateRange(string.Format("{0}{1}", fromColumn, row),
                string.Format("{0}{1}", toColumn, row));

            //Merge the cells.
            rangeSum.Merge();
            style.HorizontalAlignment = AsposeCells.TextAlignmentType.Right;
            //style.SetBorder(AsposeCells.BorderType.LeftBorder, AsposeCells.CellBorderType.Thin);
            rangeSum.SetStyle(style);
            rangeSum.Value = "Tổng cộng";
        }

        /// <summary>
        /// [Aspose] Find cell of worksheet by value
        /// </summary>
        /// <param name="workSheet"></param>
        /// <param name="paramName"></param>
        /// <returns></returns>
        public static AsposeCells.Cell FindCell(AsposeCells.Worksheet workSheet, string paramName,
            AsposeCells.LookAtType lookAtType = AsposeCells.LookAtType.EntireContent, AsposeCells.LookInType lookInType = AsposeCells.LookInType.Values)
        {
            AsposeCells.FindOptions opts = new AsposeCells.FindOptions();
            opts.LookInType = lookInType;
            opts.LookAtType = lookAtType;

            string param = string.Format("{0}", paramName);
            AsposeCells.Cell cell = workSheet.Cells.Find(param, null, opts);
            return cell;
        }

        /// <summary>
        /// [Aspose] Find cell of worksheet by value
        /// </summary>
        /// <param name="workSheet"></param>
        /// <param name="paramName"></param>
        /// <returns></returns>
        public static AsposeCells.Cell FindCellNext(AsposeCells.Worksheet workSheet, string paramName, bool isSearchNext = false,
            AsposeCells.LookAtType lookAtType = AsposeCells.LookAtType.EntireContent)
        {
            AsposeCells.FindOptions opts = new AsposeCells.FindOptions();
            opts.LookInType = AsposeCells.LookInType.Values;
            opts.LookAtType = lookAtType;
            opts.SearchNext = isSearchNext;

            string param = string.Format("{0}", paramName);
            AsposeCells.Cell cell = workSheet.Cells.Find(param, null, opts);
            return cell;
        }

        #endregion ---[Aspose] Export Excel/PDF---

        #endregion ---- Processing Templates ----

        #region ---- Save as file ----

        /// <summary>
        /// Lưu workbook ra file excel
        /// </summary>
        /// <param name="workbook">Current workbook</param>
        /// <param name="fileOutputPath"></param>
        public static void SaveAs(Workbook workbook, string fileOutputPath)
        {
            var excelApplication = new ExcelApplication();

            try
            {
                excelApplication.Save(workbook, fileOutputPath);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                excelApplication = null;
            }
        }

        /// <summary>
        /// Lưu stream IO ra file excel
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="fileOutputPath"></param>
        public static void SaveAs(Stream streamIO, string fileOutputPath)
        {
            var excelApplication = new ExcelApplication();
            Workbook workbook = null;

            try
            {
                workbook = excelApplication.Open(streamIO);
                excelApplication.Save(workbook, fileOutputPath);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                excelApplication = null;
                workbook = null;
            }
        }

        /// <summary>
        /// Save stream to file with active sheets by custom index
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="fileOutputPath"></param>
        /// <param name="activeSheets"></param>
        public static void SaveAs(Stream streamIO, string fileOutputPath, params int[] activeSheets)
        {
            var excelApplication = new ExcelApplication();
            Workbook workbook = null;

            try
            {
                // open stream to workbook
                workbook = excelApplication.Open(streamIO);

                Worksheets worksheets = workbook.Worksheets;


                if (activeSheets != null && activeSheets.Length > 0
                    && worksheets.Count > 0 && activeSheets.Length <= worksheets.Count)
                {
                    foreach (Worksheet worksheet in worksheets)
                    {
                        // Hide all sheet
                        worksheet.Visibility = Worksheet.SheetVisibility.VeryHidden;
                    }

                    bool hasCompleted = false;

                    foreach (int sheetIndex in activeSheets)
                    {
                        // Set visible for active sheets
                        if (sheetIndex <= worksheets.Count)
                        {
                            worksheets[sheetIndex].Visibility = Worksheet.SheetVisibility.Visible;
                            hasCompleted = true;
                        }
                    }

                    if (!hasCompleted) // If its not completed
                    {
                        foreach (Worksheet worksheet in worksheets)
                        {
                            // reactive all sheet
                            worksheet.Visibility = Worksheet.SheetVisibility.Visible;
                        }
                    }

                    excelApplication.Save(workbook, fileOutputPath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                excelApplication = null;
                workbook = null;
            }
        }

        /// <summary>
        /// Save stream to file with active sheets by custom name
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="fileOutputPath"></param>
        /// <param name="activeSheets"></param>
        public static void SaveAs(Stream streamIO, string fileOutputPath, params string[] activeSheets)
        {
            var excelApplication = new ExcelApplication();
            Workbook workbook = null;

            try
            {
                // open stream to workbook
                workbook = excelApplication.Open(streamIO);

                Worksheets worksheets = workbook.Worksheets;


                if (activeSheets != null && activeSheets.Length > 0
                    && worksheets.Count > 0 && activeSheets.Length <= worksheets.Count)
                {
                    foreach (Worksheet worksheet in worksheets)
                    {
                        // Hide all sheet
                        worksheet.Visibility = Worksheet.SheetVisibility.VeryHidden;
                    }

                    bool hasCompleted = false;

                    foreach (string sheetName in activeSheets)
                    {
                        Worksheet sheet = worksheets[sheetName];
                        // Set visible for active sheets
                        if (sheet != null)
                        {
                            sheet.Visibility = Worksheet.SheetVisibility.Visible;
                            hasCompleted = true;
                        }
                    }

                    if (!hasCompleted) // If its not completed
                    {
                        foreach (Worksheet worksheet in worksheets)
                        {
                            // reactive all sheet
                            worksheet.Visibility = Worksheet.SheetVisibility.Visible;
                        }
                    }

                    excelApplication.Save(workbook, fileOutputPath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                // Xóa bộ nhớ
                excelApplication = null;
                workbook = null;
            }
        }

        /// <summary>
        /// Save stream to file with active sheets by custom index
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelTempPath"></param>
        /// <param name="defaultIndex">True: using default index | False: using default name </param>
        /// <param name="firstOrDefault">Only first or default | all active sheets </param>
        /// <param name="activeSheets"></param>
        public static void SaveAs(Stream streamIO, string excelTempPath, bool firstOrDefault, bool defaultIndex,
                                  params int[] activeSheets)
        {
            if (activeSheets != null && activeSheets.Length > 0)
            {
                SaveAs(streamIO, excelTempPath, activeSheets);
            }
            else
            {
                SaveAs(streamIO, excelTempPath, firstOrDefault, defaultIndex);
            }
        }

        /// <summary>
        /// Save stream to file with active sheets by custom name
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelTempPath"></param>
        /// <param name="defaultIndex">True: using default index | False: using default name </param>
        /// <param name="firstOrDefault">Only first or default | all active sheets </param>
        /// <param name="activeSheets"></param>
        public static void SaveAs(Stream streamIO, string excelTempPath, bool firstOrDefault, bool defaultIndex,
                                  params string[] activeSheets)
        {
            if (activeSheets != null && activeSheets.Length > 0)
            {
                SaveAs(streamIO, excelTempPath, activeSheets);
            }
            else
            {
                SaveAs(streamIO, excelTempPath, firstOrDefault, defaultIndex);
            }
        }

        /// <summary>
        /// Save stream to file with default index/name
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelTempPath"></param>
        /// <param name="defaultIndex">True: using default index | False: using default name </param>
        /// <param name="firstOrDefault">Only first or default | all active sheets </param>
        public static void SaveAs(Stream streamIO, string excelTempPath, bool firstOrDefault, bool defaultIndex)
        {
            if (firstOrDefault)
            {
                if (defaultIndex)
                {
                    SaveAsWithFirstSheet(streamIO, excelTempPath);
                }
                else
                {
                    SaveAsWithSheetOnly(streamIO, excelTempPath);
                }
            }
            else
            {
                SaveAs(streamIO, excelTempPath);
            }
        }

        /// <summary>
        /// Save stream to file with active sheets by index default [0]
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelTempPath"></param>
        public static void SaveAsWithFirstSheet(Stream streamIO, string excelTempPath)
        {
            SaveAs(streamIO, excelTempPath, DEFAULTSHEETINDEX);
        }

        /// <summary>
        /// Save stream to file with active sheets by name default [Report]
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelTempPath"></param>
        public static void SaveAsWithSheetOnly(Stream streamIO, string excelTempPath)
        {
            SaveAs(streamIO, excelTempPath, DEFAULTSHEETNAME);
        }

        #region ---- [Aspose] Save Excel/PDF ----

        /// <summary>
        /// [Aspose] Save to file
        /// </summary>
        /// <param name="workBook"></param>
        /// <param name="pathSave"></param>
        /// <param name="isPDF"></param>
        public static void SaveToFile(AsposeCells.Workbook workBook, string pathSave, bool isPDF = false,
            OfficeType type = OfficeType.XLSX)
        {
            //Save theo định dạng của excel (office97-2003/2007/2010)
            AsposeCells.SaveFormat saveFormat = AsposeCells.SaveFormat.Xlsx;
            if (type == OfficeType.XLSX)
            {
                saveFormat = AsposeCells.SaveFormat.Xlsx;
            }
            else if (type == OfficeType.XLS)
            {
                saveFormat = AsposeCells.SaveFormat.Excel97To2003;
            }

            //Save PDF
            if (isPDF)
            {
                AsposeCells.PdfSaveOptions options = new AsposeCells.PdfSaveOptions(AsposeCells.SaveFormat.Pdf);
                options.CalculateFormula = true;
                workBook.Save(pathSave, options);
            }
            else //Save the Excel file.
            {
                workBook.Save(pathSave, saveFormat);
            }
        }

        /// <summary>
        /// [Aspose] Save to stream
        /// </summary>
        /// <param name="workBook"></param>
        /// <param name="isPDF"></param>
        /// <returns></returns>
        public static Stream SaveToStream(AsposeCells.Workbook workBook, bool isPDF = false,
            OfficeType type = OfficeType.XLSX)
        {
            Stream stream = new MemoryStream();
            //Save theo định dạng của excel (office97-2003/2007/2010)
            AsposeCells.SaveFormat saveFormat = AsposeCells.SaveFormat.Xlsx;
            if (type == OfficeType.XLSX)
            {
                saveFormat = AsposeCells.SaveFormat.Xlsx;
            }
            else if (type == OfficeType.XLS)
            {
                saveFormat = AsposeCells.SaveFormat.Excel97To2003;
            }

            //Save PDF
            if (isPDF)
            {
                AsposeCells.PdfSaveOptions options = new AsposeCells.PdfSaveOptions(AsposeCells.SaveFormat.Pdf);
                options.CalculateFormula = true;
                workBook.CalculateFormula();
                workBook.Save(stream, options);
                return stream;
            }

            //Save the Excel file.
            workBook.Save(stream, saveFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        #endregion ---- [Aspose] Save Excel/PDF ----

        #endregion ---- Save as file ----

        #region ---- Read file ----

        /// <summary>
        /// Read file to memory
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <returns></returns>
        public static Stream ReadFile(string sourcePath)
        {
            Stream memoryStream = new MemoryStream();
            try
            {
                // Reopen and sent stream file back
                if (File.Exists(sourcePath))
                {
                    memoryStream = new FileStream(sourcePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                }

                return memoryStream;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion ---- Read file ----

        #region ---- [Aspose] Open file excel template ----

        /// <summary>
        /// [Aspose] Open excel
        /// </summary>
        /// <param name="excelTemplatePath"></param>
        /// <returns></returns>
        public static AsposeCells.Workbook OpenExcelFile(string excelPath,
            OfficeType type = OfficeType.XLSX)
        {
            //Load theo định dạng của excel (office97-2003/2007/2010)
            AsposeCells.LoadFormat loadFormat = AsposeCells.LoadFormat.Xlsx;
            if (type == OfficeType.XLSX)
            {
                loadFormat = AsposeCells.LoadFormat.Xlsx;
            }
            else if (type == OfficeType.XLS)
            {
                loadFormat = AsposeCells.LoadFormat.Excel97To2003;
            }

            var workBook = new AsposeCells.Workbook(excelPath,
                new AsposeCells.LoadOptions(loadFormat));
            return workBook;
        }

        #endregion ---- [Aspose] Open file excel template ----

    }
}
