﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Interface
{
    public interface IDistanceInventoriesService
    {
        IEnumerable<M_DistanceInventory> FindAll(string inventoryID);
        string Insert(M_DistanceInventory distance);
        string Insert(IEnumerable<M_DistanceInventory> distances);
        string Update(M_DistanceInventory distance);
        string Update(IEnumerable<M_DistanceInventory> distances);
        string Delete(string inventoryID);
    }
}
