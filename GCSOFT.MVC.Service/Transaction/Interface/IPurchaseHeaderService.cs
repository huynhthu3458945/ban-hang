﻿using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IPurchaseHeaderService
    {
        IEnumerable<M_PurchaseHeader> FindAll();
        IEnumerable<M_PurchaseHeader> FindAll(PurchaseCardType type);
        M_PurchaseHeader GetBy(PurchaseCardType type, string code);
        string Insert(M_PurchaseHeader purchaseHdr);
        string Update(M_PurchaseHeader purchaseHdr);
        string Delete(PurchaseCardType type, List<string> codes);
        string GetMax(PurchaseCardType purchType);
    }
}
