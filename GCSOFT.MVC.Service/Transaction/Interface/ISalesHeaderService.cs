﻿using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Interface
{
    public interface ISalesHeaderService
    {
        IEnumerable<M_SalesHeader> FindAll();
        IEnumerable<M_SalesHeader> FindAll(SalesType type);
        IEnumerable<M_SalesHeader> FindAll(SalesType type, string statusCode);
        IEnumerable<M_SalesHeader> FindAllCustomer(SalesType type, int agencyId, ConvertType convertType);
        IEnumerable<M_SalesHeader> FindAll(SalesType type, List<string> statusCodes);
        IEnumerable<M_SalesHeader> FindAll(SalesType type, int customerId);
        IEnumerable<M_SalesHeader> FindAllAgency(SalesType type, AgencyType agencyType, int customerId);
        IEnumerable<M_SalesHeader> FindAllAgency(SalesType type, int? agencyId);
        IEnumerable<M_SalesHeader> FindAllCustomer(SalesType type, int agencyId);
        M_SalesHeader GetBy(SalesType type, string code);
        M_SalesHeader GetBy(SalesType type, string code, int customerId);
        M_SalesHeader GetByFromSource(SalesType type, string fromSource);
        M_SalesHeader GetBy(Guid PrivateKey);
        string Insert(M_SalesHeader salesHeader);
        string Update(M_SalesHeader salesHeader);
        string Delete(SalesType type, string code);
        string GetMax(SalesType type);
    }
}