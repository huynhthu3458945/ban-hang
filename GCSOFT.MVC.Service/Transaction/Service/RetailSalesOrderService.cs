﻿using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.Transaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class RetailSalesOrderService : IRetailSalesOrderService
    {
        private readonly IRetailSalesOrderRepository _retailSalesOrderRepository;

        public RetailSalesOrderService(IRetailSalesOrderRepository retailSalesOrderRepository)
        {
            _retailSalesOrderRepository = retailSalesOrderRepository;
        }

        public string Delete(List<string> transactionIDs)
        {
            try
            {
                _retailSalesOrderRepository.Delete(d => transactionIDs.Contains(d.Code));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_RetailSalesOrder> FindAll(string statusCode)
        {
            return _retailSalesOrderRepository.GetMany(d => d.StatusCode == statusCode);
        }
        public IEnumerable<M_RetailSalesOrder> FindSUCCESS(string statusCode1, string statusCode2)
        {
            return _retailSalesOrderRepository.GetMany(d => d.StatusCode == statusCode1 || d.StatusCode == statusCode2);
        }
        public IEnumerable<M_RetailSalesOrder> FindAllNot(string notStatusCode1, string notStatusCode2)
        {
            return _retailSalesOrderRepository.GetMany(d => d.StatusCode != notStatusCode1 && d.StatusCode != notStatusCode2);
        }
        public M_RetailSalesOrder GetBy(string transactionID)
        {
            return _retailSalesOrderRepository.Get(d => d.Code.Equals(transactionID));
        }

        public string GetMax()
        {
            return _retailSalesOrderRepository.GetAll().OrderByDescending(d => d.Code).Select(d => d.Code).FirstOrDefault();
        }

        public string Insert(M_RetailSalesOrder retailSalesOrder)
        {
            try
            {
                _retailSalesOrderRepository.Add(retailSalesOrder);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_RetailSalesOrder retailSalesOrder)
        {
            try
            {
                _retailSalesOrderRepository.Update(retailSalesOrder);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string UpdateStatus(string transactionID, string statusCode , string transactionInfo, string cardNo)
        {
            try
            {
                M_RetailSalesOrder m_RetailSalesOrder = GetBy(transactionID);
                m_RetailSalesOrder.StatusCode = statusCode;
                m_RetailSalesOrder.StatusName = statusCode;
                m_RetailSalesOrder.TransactionInfo = transactionInfo;
                m_RetailSalesOrder.ActionCode = cardNo;
                _retailSalesOrderRepository.Update(m_RetailSalesOrder);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
