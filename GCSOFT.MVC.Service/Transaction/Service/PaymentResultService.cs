﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class PaymentResultService : IPaymentResultService
    {
        private readonly IPaymentResultRepository _paymentResultRepo;
        public PaymentResultService
            (
                IPaymentResultRepository paymentResultRepo
            )
        {
            _paymentResultRepo = paymentResultRepo;
        }

        public string Delete(PaymentType type, List<string> transactionIDs)
        {
            try
            {
                _paymentResultRepo.Delete(d => d.PaymentType == type && transactionIDs.Contains(d.TransactionID));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_PaymentResult> FindAll(PaymentType type)
        {
            return _paymentResultRepo.GetMany(d => d.PaymentType == type);
        }

        public M_PaymentResult GetBy(PaymentType type, string transactionID)
        {
            return _paymentResultRepo.Get(d => d.PaymentType == type && d.TransactionID.Equals(transactionID));
        }

        public string GetMax(PaymentType type)
        {
            return _paymentResultRepo.GetMany(d => d.PaymentType == type).OrderByDescending(d => d.TransactionID).Select(d => d.TransactionID).FirstOrDefault();
        }

        public string Insert(M_PaymentResult paymentResult)
        {
            try
            {
                _paymentResultRepo.Add(paymentResult);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_PaymentResult paymentResult)
        {
            try
            {
                _paymentResultRepo.Add(paymentResult);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
