﻿using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.Transaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class SalesHeaderService : ISalesHeaderService
    {
        private readonly ISalesHeaderRepository _salesHdrRepo;
        public SalesHeaderService
            (
                ISalesHeaderRepository salesHdrRepo
            )
        {
            _salesHdrRepo = salesHdrRepo;
        }

        public string Delete(SalesType type, string code)
        {
            try
            {
                _salesHdrRepo.Delete(d => d.SalesType == type && d.Code.Equals(code));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_SalesHeader> FindAll()
        {
            return _salesHdrRepo.GetAll();
        }

        public IEnumerable<M_SalesHeader> FindAll(SalesType type)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type);
        }
        public IEnumerable<M_SalesHeader> FindAll(SalesType type, string statusCode)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type && !d.StatusCode.Equals(statusCode));
        }
        public IEnumerable<M_SalesHeader> FindAll(SalesType type, List<string> statusCodes)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type && statusCodes.Contains(d.StatusCode));
        }
        public IEnumerable<M_SalesHeader> FindAll(SalesType type, int customerId)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type && d.CustomerId == customerId);
        }
        public IEnumerable<M_SalesHeader> FindAllAgency(SalesType type, AgencyType agencyType, int customerId)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type && d.CustomerType==agencyType && d.AgencyId == customerId);
        }
        public IEnumerable<M_SalesHeader> FindAllAgency(SalesType type, int? agencyId)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type && d.AgencyId == agencyId);
        }
        public IEnumerable<M_SalesHeader> FindAllCustomer(SalesType type, int agencyId)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type && d.CustomerId == agencyId);
        }
        public IEnumerable<M_SalesHeader> FindAllCustomer(SalesType type, int agencyId, ConvertType convertType)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type && d.CustomerId == agencyId && d.ConvertType == convertType);
        }
        public M_SalesHeader GetBy(SalesType type, string code)
        {
            return _salesHdrRepo.Get(d => d.SalesType == type && d.Code.Equals(code));
        }
        public M_SalesHeader GetBy(SalesType type, string code, int customerId)
        {
            return _salesHdrRepo.Get(d => d.SalesType == type && d.Code.Equals(code) && d.CustomerId == customerId);
        }
        public M_SalesHeader GetByFromSource(SalesType type, string fromSource)
        {
            return _salesHdrRepo.Get(d => d.FromSoure == type && d.SourceNo.Equals(fromSource)) ?? new M_SalesHeader();
        }
        public M_SalesHeader GetBy(Guid PrivateKey)
        {
            return _salesHdrRepo.Get(d => d.PrivateKey == PrivateKey);
        }
        public string GetMax(SalesType type)
        {
            return _salesHdrRepo.GetMany(d => d.SalesType == type).OrderByDescending(d => d.Code).Select(d => d.Code).FirstOrDefault();
        }

        public string Insert(M_SalesHeader salesHeader)
        {
            try
            {
                _salesHdrRepo.Add(salesHeader);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_SalesHeader salesHeader)
        {
            try
            {
                _salesHdrRepo.Update(salesHeader);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
