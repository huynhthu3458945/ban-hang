﻿using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.Transaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class DistanceInvnetoriesService : IDistanceInventoriesService
    {
        private readonly IDistanceInventoriesRepository _distanceInventoriesRepository;

        public DistanceInvnetoriesService(IDistanceInventoriesRepository distanceInventoriesRepository)
        {
            _distanceInventoriesRepository = distanceInventoriesRepository;
        }

        public string Delete(string inventoryID)
        {
            try
            {
                _distanceInventoriesRepository.Delete(d => d.InventoryID.Equals(inventoryID));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_DistanceInventory> FindAll(string inventoryID)
        {
            return _distanceInventoriesRepository.GetMany(d => d.InventoryID.Equals(inventoryID));
        }

        public string Insert(M_DistanceInventory distance)
        {
            try
            {
                _distanceInventoriesRepository.Add(distance);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<M_DistanceInventory> distances)
        {
            try
            {
                _distanceInventoriesRepository.Add(distances);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_DistanceInventory distance)
        {
            try
            {
                _distanceInventoriesRepository.Update(distance);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_DistanceInventory> distances)
        {
            try
            {
                _distanceInventoriesRepository.Update(distances);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}