﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class PurchaseHeaderService : IPurchaseHeaderService
    {
        private readonly IPurchaseHeaderRepository _purchaseHdrRepo;
        public PurchaseHeaderService
            (
                IPurchaseHeaderRepository purchaseHdrRepo
            )
        {
            _purchaseHdrRepo = purchaseHdrRepo;
        }

        public string Delete(PurchaseCardType type, List<string> codes)
        {
            try
            {
                _purchaseHdrRepo.Delete(d => d.Type == type && codes.Contains(d.Code));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_PurchaseHeader> FindAll()
        {
            return _purchaseHdrRepo.GetAll();
        }

        public IEnumerable<M_PurchaseHeader> FindAll(PurchaseCardType type)
        {
            return _purchaseHdrRepo.GetMany(d => d.Type == type);
        }

        public M_PurchaseHeader GetBy(PurchaseCardType type, string code)
        {
            return _purchaseHdrRepo.Get(d => d.Type == type && d.Code.Equals(code));
        }

        public string Insert(M_PurchaseHeader purchaseHdr)
        {
            try
            {
                _purchaseHdrRepo.Add(purchaseHdr);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_PurchaseHeader purchaseHdr)
        {
            try
            {
                _purchaseHdrRepo.Update(purchaseHdr);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string GetMax(PurchaseCardType purchType)
        {
            return _purchaseHdrRepo.GetMany(d => d.Type == purchType).OrderByDescending(d => d.Code).Select(d => d.Code).FirstOrDefault();
        }
    }
}
