﻿using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.AgencyService.Interface
{
    public interface IPackageService
    {
        IEnumerable<M_Package> FindAll();
        M_Package GetBy(int id);
        string Insert(M_Package packed);
        string Update(M_Package packed);
        string Delete(int[] ids);
        int GetID();
    }
}
