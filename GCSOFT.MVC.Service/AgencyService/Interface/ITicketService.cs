﻿using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.AgencyService.Interface
{
    public interface ITicketService
    {
        IEnumerable<M_Ticket> FindAll();
        M_Ticket GetBy(int id);
        string Insert(M_Ticket packed);
        string Update(M_Ticket packed);
        string Delete(int[] ids);
        string GetMax();
    }
}
