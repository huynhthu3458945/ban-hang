﻿using GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.AgencyService.Service
{
    public class AgencyService : IAgencyService
    {
        private readonly IAgencyRepository _agencyRepo;
        private readonly IAgencyCardRepository _agencyCardRepo;
        private readonly ISalesHeaderRepository _salesHeaderRepo;
        public AgencyService
            (
                IAgencyRepository agencyRepo,
                IAgencyCardRepository agencyCardRepo,
                ISalesHeaderRepository salesHeaderRepo
            )
        {
            _agencyRepo = agencyRepo;
            _agencyCardRepo = agencyCardRepo;
            _salesHeaderRepo = salesHeaderRepo;
        }

        public string HasDelete(int[] ids)
        {
            try
            {
                var hasDelete = string.Empty;
                if (_agencyCardRepo.GetMany(d => ids.Contains(d.AgencyId)).Any())
                    return "Người sử dụng đã phát sinh thẻ trong kho, không thể XÓA";
                if (_agencyRepo.GetMany(d => ids.Contains(d.ParentAgencyId ?? 0)).Any())
                    return "Người sử dụng đã phát sinh Đối Tác cấp dưới, không thẻ XÓA";
                if (_salesHeaderRepo.GetMany(d => ids.Contains(d.CustomerId)).Any())
                    return "Người sử dụng đã phát sinh đơn hàng, không thể XÓA";
                if (_salesHeaderRepo.GetMany(d => ids.Contains(d.AgencyId ?? 0)).Any())
                    return "Người sử dụng đã phát sinh đơn hàng, không thể XÓA";
                return "";
            }
            catch
            {
                return "";
            }
        }
        public string Delete(int[] ids)
        {
            try
            {
                _agencyRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string DeleteImg(int id)
        {
            try
            {
                var agency = _agencyRepo.GetById(id);
                agency.Image = null;
                _agencyRepo.Update(agency);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Agency> FindAll()
        {
            return _agencyRepo.GetAll();
        }
        public IEnumerable<M_Agency> FindAll(int packageId)
        {
            return _agencyRepo.GetMany(d => d.PackageId == packageId);
        }
        public IEnumerable<M_Agency> FindAll(AgencyType type, int? parentId)
        {
            return _agencyRepo.GetMany(d => d.AgencyType == type && d.ParentAgencyId == parentId);
        }
        public IEnumerable<M_Agency> FindAll(AgencyType type)
        {
            return _agencyRepo.GetMany(d => d.AgencyType == type);
        }
        public IEnumerable<M_Agency> FindAll(List<AgencyType> types)
        {
            return _agencyRepo.GetMany(d => types.Contains(d.AgencyType));
        }
        public IEnumerable<M_Agency> FindAllByAgency(int? parentId)
        {
            return _agencyRepo.GetMany(d => d.ParentAgencyId == parentId);
        }
        public IEnumerable<M_Agency> FindAll(int[] ids)
        {
            return _agencyRepo.GetMany(d => ids.Contains(d.Id)); 
        }
        public IEnumerable<M_Agency> FindAllHasImage()
        {
            return _agencyRepo.GetMany(d => !string.IsNullOrEmpty(d.Image) && d.ParentAgencyId == null).OrderBy(d => d.DateCreate).ThenBy(d => d.RegistrationDate);
        }
        public M_Agency GetBy(int id)
        {
            return _agencyRepo.GetById(id);
        }
        public M_Agency GetBy2(int id)
        {
            return _agencyRepo.Get(d => d.Id == id);
        }
        public M_Agency GetByUserName(string userName)
        {
            return _agencyRepo.Get(d => d.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }
        public int GetID()
        {
            return _agencyRepo.GetKey(d => d.Id);
        }

        public string Insert(M_Agency agency)
        {
            try
            {
                _agencyRepo.Add(agency);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Agency agency)
        {
            try
            {
                _agencyRepo.Update(agency);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public bool CandAdd(string code, string xCode)
        {
            if (!string.IsNullOrEmpty(xCode) && code.Equals(xCode))
                return false;
            var user = _agencyRepo.Get(d => d.Code.Equals(code));
            if (user == null)
                return false;
            return true;
        }

    }
}
