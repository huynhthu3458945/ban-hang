﻿using GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.AgencyService.Service
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepo;
        public TicketService
            (
                ITicketRepository ticketRepo
            )
        {
            _ticketRepo = ticketRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _ticketRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Ticket> FindAll()
        {
            return _ticketRepo.GetAll();
        }

        public M_Ticket GetBy(int id)
        {
            return _ticketRepo.GetById(id);
        }

        public int GetID()
        {
            return _ticketRepo.GetKey(d => d.Id);
        }

        public string GetMax()
        {
            return _ticketRepo.GetAll().OrderByDescending(d => d.Code).Select(d => d.Code).FirstOrDefault();
        }

        public string Insert(M_Ticket packed)
        {
            try
            {
                _ticketRepo.Add(packed);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Ticket packed)
        {
            try
            {
                _ticketRepo.Update(packed);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
