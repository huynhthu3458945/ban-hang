﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IVoteService
    {
        IEnumerable<M_Vote> FindAll();
        IEnumerable<M_Vote> FindAll(ResultType type);
        IEnumerable<M_Vote> FindAll(ResultType type, int referId);
        IEnumerable<M_Vote> FindAll(List<ResultType> types, int referId);
        M_Vote GetBy(int id);
        string Insert(M_Vote vote);
        string Update(M_Vote vote);
        string Delete(int[] ids);
        string Delete(ResultType resultType, int referId, string userName);
        int NoOfVote(ResultType type, int referId);
        int GetID();
        bool HasVote(ResultType type, int referId, string userName);
    }
}
