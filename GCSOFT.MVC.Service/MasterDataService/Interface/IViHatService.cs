﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IViHatService
    {
        IEnumerable<M_ViHat> FindAll();
        M_ViHat GetByAPK(string apk);
        string Insert(M_ViHat m_ViHat);
        string Update(M_ViHat m_ViHat);
        string Delete(string apk);
    }
}
