﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IQuestionService
    {
        IEnumerable<M_Question> FindAll();
        IEnumerable<M_Question> FindAll(QuestionType questionType);
        IEnumerable<M_Question> Socials(QuestionType questionType, M_QuestionFilter f);
        IEnumerable<M_Question> FindAll(QuestionType questionType, int[] ids);
        IEnumerable<M_Question> FindAllByCourse(int idCourse);
        IEnumerable<M_Question> FindAllByStatus(QuestionType questionType, string statusCode);
        IEnumerable<M_Question> FindAllByUser(QuestionType questionType, string userName);
        M_Question GetByCourse(int idCourse);
        M_Question GetBy(int id);
        M_Question GetBy2(int id);
        string Insert(IEnumerable<M_Question> questions);
        string Insert(M_Question question);
        string Update(M_Question question);
        string Update(IEnumerable<M_Question> questions);
        string Delete(int[] ids);
        int GetID();
    }
}
