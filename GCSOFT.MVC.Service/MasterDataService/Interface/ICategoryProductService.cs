﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICategoryProductService
    {
        CategoriesProduct GetById(int id);
        IEnumerable<CategoriesProduct> FindAll();
        IEnumerable<CategoriesProduct> FindAll(bool status);
        string Insert(CategoriesProduct categoriesProduct);
        string Insert(IEnumerable<CategoriesProduct> categoriesProducts);
        string Update(CategoriesProduct categoriesProduct);
        string Update(IEnumerable<CategoriesProduct> categoriesProducts);
        string Delete(int id);
        string Delete(IEnumerable<int> ids);
    }
}
