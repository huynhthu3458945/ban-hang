﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICustomerService
    {
        IEnumerable<M_Customer> FindAll();
        IEnumerable<M_Customer> FindAllNotActive();
        IEnumerable<M_Customer> FindAll(string lotNumner);
        IEnumerable<M_Customer> FindAll(string lotNumner, string statusEmail, string fullName, string phone, string email);
        M_Customer GetBy(string seriNumber);
        M_Customer GetBy(int id);
        M_Customer GetBy2(int id);
        string Insert(M_Customer customer);
        string Update(M_Customer customer);
        string Insert(IEnumerable<M_Customer> customers);
        string Update(IEnumerable<M_Customer> customers);
        string Delete(string lotNumber);
    }
}
