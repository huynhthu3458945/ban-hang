﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IUserInfoOfGroupService
    {
        IEnumerable<M_UserInfoOfGroup> FindAll();
        M_UserInfoOfGroup GetByGroupCodel(string groupCode);
        string Insert(M_UserInfoOfGroup userInfo);
        string Update(M_UserInfoOfGroup userInfo);
        string Update(IEnumerable<M_UserInfoOfGroup> nhomUser, string groupCode);
        string Delete(string groupCode,string userName);
    }
}
