﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IProductService
    {
        IEnumerable<Product> FindAll();
        IEnumerable<Product> FindAll(int categoryId);
        string Insert(Product product);
        string Insert(IEnumerable<Product> products);
        string Update(Product product);
        string Update(IEnumerable<Product> products);
        string Delete(int id);
        string Delete(IEnumerable<int> ids);
    }
}
