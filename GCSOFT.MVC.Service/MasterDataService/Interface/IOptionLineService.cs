﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IOptionLineService
    {
        int GetID();
        IEnumerable<M_OptionLine> FindAll(int OptionHeaderID);
        M_OptionLine Get(int OptionHeaderID, int lineNo);
        IEnumerable<M_OptionLine> FindAll(string OptionHeaderCode);
        IEnumerable<M_OptionLine> FindAll(string OptionHeaderCode, List<string> statusCodes);
        M_OptionLine Get(string OptionHeaderCode, string code);
        IEnumerable<M_OptionLine> Gets(string OptionHeaderCode, string[] codes); 
        string Insert(M_OptionLine optionLine);
        string Update(M_OptionLine optionLine);
        string Delete(int lineNo);
    }
}
