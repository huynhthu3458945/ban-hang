﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IHistoryCall
    {
        string Insert(M_HistoryCall historyChangeClass);
        List<M_HistoryCall> GetAll(int id, string userName);
        List<M_HistoryCall> GetAll(int id, int potentialId);
    }
}
