﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IMessageMasterService
    {
        IEnumerable<M_MessageMaster> FindAll();
        M_MessageMaster GetByAPK(string apk);
        string Insert(M_MessageMaster m_MessageMaster);
        string Update(M_MessageMaster m_MessageMaster);
        string Delete(string apk);
    }
}
