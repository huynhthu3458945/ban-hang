﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IHistoryChangeClass
    {
        string Insert(M_HistoryChangeClass historyChangeClass);
        IEnumerable<M_HistoryChangeClass> GetByUserName(string userName);
    }
}
