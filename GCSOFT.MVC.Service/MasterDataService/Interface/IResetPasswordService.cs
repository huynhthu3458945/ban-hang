﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IResetPasswordService
    {
        IEnumerable<M_ResetPassword> FindAll();
        M_ResetPassword GetBy(Guid id);
        M_ResetPassword GetByExpiry(Guid id);
        string Insert(M_ResetPassword resetPass);
        string Update(M_ResetPassword resetPass);
    }
}
