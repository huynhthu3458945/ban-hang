﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICourseResourceService
    {
        IEnumerable<E_CourseResource> FindAll(int idCourse);
        IEnumerable<E_CourseResource> FindAll(int[] idCourse);
    }
}
