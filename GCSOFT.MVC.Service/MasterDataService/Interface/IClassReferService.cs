﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IClassReferService
    {
        IEnumerable<E_ClassRefer> FindAll();
        IEnumerable<E_ClassRefer> FindAll(int classId);
        string Insert(IEnumerable<E_ClassRefer> classRefers);
        string Update(IEnumerable<E_ClassRefer> classRefers);
        string Delete(int[] ids);
    }
}
