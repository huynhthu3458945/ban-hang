﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IMessageDetailService
    {
        IEnumerable<M_MessageDetail> FindAll();
        M_MessageDetail GetById(string apk);
        string Insert(M_MessageDetail m_MessageDetail);
        string Insert(IEnumerable<M_MessageDetail> m_MessageDetails, string apk);
        string Update(M_MessageDetail m_MessageDetails);
        string Update(IEnumerable<M_MessageDetail> m_MessageDetails, string apk);
        string Delete(IEnumerable<M_MessageDetail> m_MessageDetails, string apk);
        string Delete(string apk);
    }
}
