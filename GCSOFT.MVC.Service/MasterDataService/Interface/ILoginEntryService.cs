﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ILoginEntryService
    {
        IEnumerable<M_LoginEntry> FindAll();
        IEnumerable<M_LoginEntry> FindAll(string userName);
        M_LoginEntry GetBy(int id);
        bool IsYourLoginStillTrue(string userName, string sid);
        bool IsUserLoggedOnElsewhere(string userName, string sid);
        string LogEveryoneElseOut(string userName, string sid);
        string Insert(M_LoginEntry loginEntry);
        string Update(M_LoginEntry loginEntry);
    }
}
