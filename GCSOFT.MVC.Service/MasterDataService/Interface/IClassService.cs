﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IClassService
    {
        IEnumerable<E_Class> FindAll();
        IEnumerable<E_Class> FindAll(ClassType classType);
        IEnumerable<E_Class> FindAll(ClassType classType, string statusCode);
        IEnumerable<E_Class> FindAll(ClassType classType, List<int> classChooseds);
        IEnumerable<E_Class> FindAll2(ClassType classType, List<int> idClasses);
        IEnumerable<E_Class> FindAll(int[] ids);
        E_Class GetBy(int id);
        E_Class Get(int id);
        string Insert(E_Class teacher);
        string Update(E_Class teacher);
        string Delete(int[] ids);
        int GetID();
    }
}
