﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IPromotionService
    {
        string Insert(M_Promotion promotion);
        string Update(M_Promotion promotion);
        string Remove(string[] codes);
        IEnumerable<M_Promotion> GetAll ();
        M_Promotion GetByCode (string code);
    }
}
