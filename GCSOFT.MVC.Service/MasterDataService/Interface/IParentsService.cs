﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IParentsService
    {
        IEnumerable<M_Parents> FindAll();
        IEnumerable<M_Parents> FindAll(int[] ids);
        M_Parents GetBy(int id);
        string Insert(M_Parents teacher);
        string Update(M_Parents teacher);
        string Delete(int[] ids);
        int GetID();
    }
}
