﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICommentService
    {
        IEnumerable<M_Comment> FindAll();
        IEnumerable<M_Comment> FindAll(ResultType type);
        IEnumerable<M_Comment> FindAll(ResultType type, int referId);
        List<int> IdRefers(ResultType type);
        M_Comment GetBy(int id);
        string Insert(M_Comment comment);
        string Update(M_Comment comment);
        string Delete(int[] ids);
        int NoOfComment(ResultType type, int referId);
        int GetID();
    }
}
