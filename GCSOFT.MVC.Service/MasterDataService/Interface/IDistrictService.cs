﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IDistrictService
    {
        IEnumerable<M_District> FindAll(int province_id);
        M_District GetBy(int id);
    }
}
