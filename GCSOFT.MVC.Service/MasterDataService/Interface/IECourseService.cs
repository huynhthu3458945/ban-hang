﻿using GCSOFT.MVC.Model.FrontEnd;
using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IECourseService
    {
        List<int> GetAllChildCategory(int idCategory);
        IEnumerable<CouseCategory> CourseByCategory(int idCategory);
        IEnumerable<CouseCategory> OtherCourseByCategory(int idCategory, int idCourse, int top);
        E_Course GetCourse(int idCourse);
        E_Category GetCategory(int idCategory);
    }
}
