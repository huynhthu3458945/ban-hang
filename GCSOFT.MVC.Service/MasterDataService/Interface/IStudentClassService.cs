﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IStudentClassService
    {
        IEnumerable<M_StudentClass> FindAll();
        IEnumerable<M_StudentClass> FindAll(string userName);
        IEnumerable<M_StudentClass> FindAllByActive(string userName);
        IEnumerable<M_StudentClass> FindAllByDate(string userName);
        M_StudentClass GetBy(int id);
        string Insert(IEnumerable<M_StudentClass> studentClasses);
        string Update(IEnumerable<M_StudentClass> studentClasses);
        string Delete(int[] ids);
        string Delete(string userName);
        bool HasClass(string userName);
    }
}
