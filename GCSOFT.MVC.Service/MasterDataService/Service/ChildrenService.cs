﻿using System;
using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ChildrenService : IChildrenService
    {
        private readonly IChildrenRepository _childrenRepo;
        public ChildrenService
            (
                IChildrenRepository childrenRepo
            )
        {
            _childrenRepo = childrenRepo;
        }
        public int GetID()
        {
            return _childrenRepo.GetKey(d => d.Id);
        }
    }
}
