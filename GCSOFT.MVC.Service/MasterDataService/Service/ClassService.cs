﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ClassService : IClassService
    {
        private readonly IClassRepository _classRepo;
        public ClassService
            (
                IClassRepository ClassRepo
            )
        {
            _classRepo = ClassRepo;
        }
        public string Delete(int[] ids)
        {
            try
            {
                _classRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<E_Class> FindAll()
        {
            return _classRepo.GetAll();
        }
        public IEnumerable<E_Class> FindAll(ClassType classType)
        {
            return _classRepo.GetMany(d => d.ClassType == classType);
        }
        public IEnumerable<E_Class> FindAll(ClassType classType, string statusCode)
        {
            return _classRepo.GetMany(d => d.ClassType == classType && d.StatusCode.Equals(statusCode)).OrderBy(d => d.SortOrder);
        }
        public IEnumerable<E_Class> FindAll(ClassType classType, List<int> ClassChooseds)
        {
            return _classRepo.GetMany(d => !ClassChooseds.Contains(d.Id));
        }
        public IEnumerable<E_Class> FindAll2(ClassType classType, List<int> idClasses)
        {
            return _classRepo.GetMany(d => d.ClassType == classType && idClasses.Contains(d.Id));
        }
        public IEnumerable<E_Class> FindAll(int[] ids)
        {
            return _classRepo.GetMany(d => ids.Contains(d.Id));
        }
        public E_Class GetBy(int id)
        {
            var classInfo = _classRepo.GetById(id) ?? new E_Class();
            return classInfo;
        }
        public E_Class Get(int id)
        {
            var classInfo = _classRepo.Get(d => d.Id == id) ?? new E_Class();
            return classInfo;
        }
        public int GetID()
        {
            return _classRepo.GetKey(d => d.Id);
        }
        public string Insert(E_Class Class)
        {
            try
            {
                _classRepo.Add(Class);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(E_Class Class)
        {
            try
            {
                _classRepo.Update(Class);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
