﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class DistrictService : IDistrictService
    {
        private readonly IDistrictRepository _districtRepo;
        public DistrictService
            (
                IDistrictRepository districtRepo
            )
        {
            _districtRepo = districtRepo;
        }

        public IEnumerable<M_District> FindAll(int province_id)
        {
            return _districtRepo.GetMany(d => d.province_id == province_id);
        }
        public M_District GetBy(int id)
        {
            return _districtRepo.GetById(id);
        }
    }
}
