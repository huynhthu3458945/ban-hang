﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepo;
        public QuestionService
            (
                IQuestionRepository questionRepo
            )
        {
            _questionRepo = questionRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _questionRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Question> FindAll()
        {
            return _questionRepo.GetAll();
        }
        public IEnumerable<M_Question> FindAllByCourse(int idCourse)
        {
            return _questionRepo.GetMany(d => d.CourseId == idCourse) ?? new List<M_Question>();
        }
        public IEnumerable<M_Question> FindAll(QuestionType questionType)
        {
            return _questionRepo.GetMany(d => d.Type == questionType).OrderByDescending(d => d.DateQuestion);
        }
        public IEnumerable<M_Question> FindAllByStatus(QuestionType questionType, string statusCode)
        {
            return _questionRepo.GetMany(d => d.Type == questionType && d.StatusCode.Equals(statusCode)).OrderByDescending(d => d.DateQuestion).Take(10);
        }
        public IEnumerable<M_Question> FindAllByUser(QuestionType questionType, string userName)
        {
            return _questionRepo.GetMany(d => d.Type == questionType && (d.userName ?? string.Empty).Equals(userName, StringComparison.OrdinalIgnoreCase)).OrderByDescending(d => d.DateQuestion);
        }
        public IEnumerable<M_Question> Socials(QuestionType questionType, M_QuestionFilter f)
        {
            return _questionRepo.GetMany(d => d.Type == questionType
                                                && (string.IsNullOrEmpty(f.Questioner) || (!string.IsNullOrEmpty(f.Questioner) && d.fullName.Contains(f.Questioner)))
                                                && (string.IsNullOrEmpty(f.QuestionTitle) || (!string.IsNullOrEmpty(f.QuestionTitle) && d.Question.Contains(f.QuestionTitle)))
                                                && (string.IsNullOrEmpty(f.TeacherName) || (!string.IsNullOrEmpty(f.TeacherName) && d.OwnerName.Contains(f.TeacherName)))
                                                && (string.IsNullOrEmpty(f.StatusCode) || (!string.IsNullOrEmpty(f.StatusCode) && d.StatusCode.Contains(f.StatusCode)))
                                        ).OrderByDescending(d => d.DateQuestion);
        }
        public IEnumerable<M_Question> FindAll(QuestionType questionType, int[] ids)
        {
            return _questionRepo.GetMany(d => d.Type == questionType && ids.Contains(d.Id));
        }

        public M_Question GetBy(int id)
        {
            return _questionRepo.GetById(id) ?? new M_Question();
        }
        public M_Question GetBy2(int id)
        {
            return _questionRepo.Get(d => d.Id == id) ?? new M_Question();
        }
        public string Insert(IEnumerable<M_Question> questions)
        {
            try
            {
                _questionRepo.Add(questions);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public int GetID()
        {
            return _questionRepo.GetKey(d => d.Id);
        }
        public string Insert(M_Question question)
        {
            try
            {
                _questionRepo.Add(question);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Question question)
        {
            try
            {
                _questionRepo.Update(question);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(IEnumerable<M_Question> questions)
        {
            try
            {
                _questionRepo.Update(questions);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public M_Question GetByCourse(int idCourse)
        {
            return _questionRepo.Get(d => d.CourseId == idCourse) ?? new M_Question();
        }
    }
}
