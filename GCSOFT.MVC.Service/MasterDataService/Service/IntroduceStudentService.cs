﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;
using GCSOFT.MVC.Data.Infrastructure;
using System.Linq.Expressions;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class IntroduceStudentService : IIntroduceStudentService
    {
        private readonly IIntroduceStudentRepository _introduceStudentRepo;
        private readonly IUnitOfWork _unitOfWork;
        public IntroduceStudentService
            (
                IIntroduceStudentRepository userRepo
                , IUnitOfWork unitOfWork
            )
        {
            _introduceStudentRepo = userRepo;
            _unitOfWork = unitOfWork;
        }

        public string Delete(int id)
        {
            try
            {
                _introduceStudentRepo.Delete(z=>z.Id == id);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_IntroduceStudent> FindAll()
        {
            return _introduceStudentRepo.GetAll();
        }
        public M_IntroduceStudent GetById(int id)
        {
            return _introduceStudentRepo.GetMany(z=>z.Id == id).FirstOrDefault();
        }
        public string Insert(M_IntroduceStudent m_IntroduceStudent)
        {
            try
            {
                _introduceStudentRepo.Add(m_IntroduceStudent);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_IntroduceStudent m_IntroduceStudent)
        {
            try
            {
                _introduceStudentRepo.Update(m_IntroduceStudent);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<M_IntroduceStudent> FindUserNameOrFullName(string userName, string fullName)
        {
            return _introduceStudentRepo.GetMany(
                z => (string.IsNullOrEmpty(userName) || (!string.IsNullOrEmpty(userName) && z.UserName.Contains(userName)))
                && (string.IsNullOrEmpty(fullName) || (!string.IsNullOrEmpty(fullName) && z.FullName.Contains(fullName))));
        }
    }
}
