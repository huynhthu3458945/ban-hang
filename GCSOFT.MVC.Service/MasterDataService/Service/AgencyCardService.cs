﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class AgencyCardService : IAgencyCardService
    {
        private readonly IAgencyCardRepository _agencyCardRepo;
        public AgencyCardService
            (
                IAgencyCardRepository agencyCardRepo
            )
        {
            _agencyCardRepo = agencyCardRepo;
        }

        public IEnumerable<M_AgencyAndCard> FindAll()
        {
            return _agencyCardRepo.GetAll();
        }

        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId).OrderBy(d => d.StatusId);
        }
        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId 
                                                && ( string.IsNullOrEmpty(statusCode) || (!string.IsNullOrEmpty(statusCode) && d.StatusCode.Equals(statusCode)))
                                                && (string.IsNullOrEmpty(seriNumner) || (!string.IsNullOrEmpty(seriNumner) && d.SerialNumber.Equals(seriNumner)))
                                            ).OrderBy(d => d.StatusId);
        }
        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner, string levelCode)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId
                                                && (string.IsNullOrEmpty(statusCode) || (!string.IsNullOrEmpty(statusCode) && d.StatusCode.Equals(statusCode)))
                                                && (string.IsNullOrEmpty(seriNumner) || (!string.IsNullOrEmpty(seriNumner) && d.SerialNumber.Equals(seriNumner)))
                                                && (string.IsNullOrEmpty(levelCode) || (!string.IsNullOrEmpty(levelCode) && (d.LevelCode ?? "100").Equals(levelCode)))
                                            ).OrderBy(d => d.StatusId);
        }
        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, int start, int offset)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId).OrderBy(d => d.StatusId).Skip(start).Take(offset);
        }
        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner, int start, int offset)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId
                                                    && (string.IsNullOrEmpty(statusCode) || (!string.IsNullOrEmpty(statusCode) && d.StatusCode.Equals(statusCode)))
                                                    && (string.IsNullOrEmpty(seriNumner) || (!string.IsNullOrEmpty(seriNumner) && d.SerialNumber.Equals(seriNumner)))
                                                ).OrderBy(d => d.StatusId).Skip(start).Take(offset);
        }
        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner, string levelCode, int start, int offset)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId
                                                    && (string.IsNullOrEmpty(statusCode) || (!string.IsNullOrEmpty(statusCode) && d.StatusCode.Equals(statusCode)))
                                                    && (string.IsNullOrEmpty(seriNumner) || (!string.IsNullOrEmpty(seriNumner) && d.SerialNumber.Equals(seriNumner)))
                                                    && (string.IsNullOrEmpty(levelCode) || (!string.IsNullOrEmpty(levelCode) && (d.LevelCode ?? "100").Equals(levelCode)))
                                                ).OrderBy(d => d.StatusId).Skip(start).Take(offset);
        }
        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, int offset)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId && d.StatusCode.Equals(statusCode)).Take(offset);
        }
        public IEnumerable<M_AgencyAndCard> GetNewCards(int AgencyId, string levelCode, string statusCode, int offset)//FindAll
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId && (d.LevelCode ?? "100").Equals(levelCode) && d.StatusCode.Equals(statusCode)).Take(offset);
        }
        public IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string lotNumber)
        {
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId && d.LotNumber.Equals(lotNumber)).OrderBy(d => d.StatusId);
        }
        public M_AgencyAndCard GetBy(int AgencyId, string SerialNumber)
        {
            return _agencyCardRepo.Get(d => d.AgencyId == AgencyId && d.SerialNumber.Equals(SerialNumber));
        }
        public M_AgencyAndCard GetByStatus(string SerialNumber, string statusCode)
        {
            return _agencyCardRepo.Get(d => d.SerialNumber.Equals(SerialNumber) && d.StatusCode.Equals(statusCode));
        }
        public M_AgencyAndCard GetBy(int AgencyId, long id)
        {
            return _agencyCardRepo.Get(d => d.AgencyId == AgencyId && d.Id == id);
        }
        public M_AgencyAndCard GetByStatus(int AgencyId, string statusCode)
        {
            return _agencyCardRepo.Get(d => d.AgencyId == AgencyId && d.StatusCode.Equals(statusCode)) ?? new M_AgencyAndCard();
        }
        public M_AgencyAndCard GetByStatus(int AgencyId, string statusCode, string levelCode)
        {
            return _agencyCardRepo.Get(d => d.AgencyId == AgencyId && d.StatusCode.Equals(statusCode) && (d.LevelCode ?? "100").Equals(levelCode)) ?? new M_AgencyAndCard();
        }
        public M_AgencyAndCard GetBy(int AgencyId, string SerialNumber, string statusCode)
        {
            return _agencyCardRepo.Get(d => d.AgencyId == AgencyId && d.SerialNumber.Equals(SerialNumber) && d.StatusCode.Equals(statusCode)) ?? new M_AgencyAndCard();
        }
        public string Insert(M_AgencyAndCard agencyCard)
        {
            try
            {
                _agencyCardRepo.Add(agencyCard);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<M_AgencyAndCard> agencyCards)
        {
            try
            {
                _agencyCardRepo.Add(agencyCards);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_AgencyAndCard agencyCard)
        {
            try
            {
                _agencyCardRepo.Update(agencyCard);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_AgencyAndCard> agencyCards)
        {
            try
            {
                _agencyCardRepo.Update(agencyCards);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public int TotalCard(int AgencyId, List<string> statusCodes)
        {
            if (statusCodes.Count() == 0)
                return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId).Count();
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId && statusCodes.Contains(d.StatusCode)).Count();
        }
        public int TotalCard(int AgencyId, List<string> statusCodes, string levelCode)
        {
            if (statusCodes.Count() == 0)
                return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId && (d.LevelCode ?? "100").Equals(levelCode)).Count();
            return _agencyCardRepo.GetMany(d => d.AgencyId == AgencyId && statusCodes.Contains(d.StatusCode) && (d.LevelCode ?? "100").Equals(levelCode)).Count();
        }
        public string Delete(int agencyId, List<long> ids)
        {
            try
            {
                _agencyCardRepo.Delete(d => d.AgencyId == agencyId && ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
