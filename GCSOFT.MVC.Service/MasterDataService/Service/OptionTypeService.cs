﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Linq;
namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class OptionTypeService : IOptionTypeService
    {
        private readonly IOptionTypeRepository _optionTypeRepo;
        private readonly IOptionLineRepository _optionLineRepo;
        public OptionTypeService
            (
                IOptionTypeRepository optionTypeRepo
                , IOptionLineRepository optionLineRepo
            )
        {
            _optionTypeRepo = optionTypeRepo;
            _optionLineRepo = optionLineRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _optionTypeRepo.Delete(d => ids.Contains(d.ID));
                _optionLineRepo.Delete(d => ids.Contains(d.OptionHeaderID));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_OptionType> FindAll()
        {
            return _optionTypeRepo.GetAll();
        }

        public M_OptionType GetBy(int id)
        {
            var optionHdr = _optionTypeRepo.GetById(id);
            optionHdr.Optionlines = _optionLineRepo.GetMany(d => d.OptionHeaderID == id);
            return optionHdr;
        }

        public int GetID()
        {
            return _optionTypeRepo.GetKey(d => d.ID);
        }

        public string Insert(M_OptionType optionType)
        {
            try
            {
                _optionTypeRepo.Add(optionType);
                _optionLineRepo.Add(optionType.Optionlines);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Update(M_OptionType optionType)
        {
            try
            {
                //-- Delete Line has been delete on UI
                int[] idLineNos = optionType.Optionlines.Select(d => d.LineNo).ToArray();
                _optionLineRepo.Delete(d => d.OptionHeaderID == optionType.ID && !idLineNos.Contains(d.LineNo));
                //-- Update Header
                _optionTypeRepo.Update(optionType);
                //-- Get IdLines In Database
                idLineNos = _optionLineRepo.GetMany(d => d.OptionHeaderID == optionType.ID).Select(d => d.LineNo).ToArray();
                //-- Update
                _optionLineRepo.Update(optionType.Optionlines.Where(d => idLineNos.Contains(d.LineNo)));
                //-- Add
                _optionLineRepo.Add(optionType.Optionlines.Where(d => !idLineNos.Contains(d.LineNo)));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
