﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepo;
        public CommentService
            (
                ICommentRepository commentRepo
            )
        {
            _commentRepo = commentRepo;
        }
        public string Delete(int[] ids)
        {
            try
            {
                _commentRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<M_Comment> FindAll()
        {
            return _commentRepo.GetAll();
        }
        public IEnumerable<M_Comment> FindAll(ResultType type)
        {
            return _commentRepo.GetMany(d => d.Type == type);
        }
        public List<int> IdRefers(ResultType type)
        {
            return _commentRepo.GetMany(d => d.Type == type)?.Select(d => d.ReferId).Distinct().ToList();
        }
        public IEnumerable<M_Comment> FindAll(ResultType type, int referId)
        {
            return _commentRepo.GetMany(d => d.Type == type && d.ReferId == referId) ?? new List<M_Comment>();
        }
        public M_Comment GetBy(int id)
        {
            return _commentRepo.GetById(id) ?? new M_Comment();
        }
        public int NoOfComment(ResultType type, int referId)
        {
            return _commentRepo.GetMany(d => d.Type == type && d.ReferId == referId).Count();
        }
        public string Insert(M_Comment comment)
        {
            try
            {
                _commentRepo.Add(comment);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(M_Comment comment)
        {
            try
            {
                _commentRepo.Update(comment);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public int GetID()
        {
            return _commentRepo.GetKey(d => d.Id);
        }
    }
}
