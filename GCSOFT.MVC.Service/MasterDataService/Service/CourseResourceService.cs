﻿using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CourseResourceService : ICourseResourceService
    {
        private readonly ICourseResourceRepository _courseResourceRepo;
        public CourseResourceService
            (
                ICourseResourceRepository courseResourceRepo
            )
        {
            _courseResourceRepo = courseResourceRepo;
        }
        public IEnumerable<E_CourseResource> FindAll(int idCourse)
        {
            return _courseResourceRepo.GetMany(d => d.IdCourse == idCourse);
        }
        public IEnumerable<E_CourseResource> FindAll(int[] idCourse)
        {
            return _courseResourceRepo.GetMany(d => idCourse.Contains(d.IdCourse) && !string.IsNullOrEmpty(d.Image));
        }
    }
}
