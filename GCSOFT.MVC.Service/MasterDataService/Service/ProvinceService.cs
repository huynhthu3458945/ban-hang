﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ProvinceService : IProvinceService
    {
        private readonly IProvinceRepository _provinceRepo;
        public ProvinceService
            (
                IProvinceRepository provinceRepo
            )
        {
            _provinceRepo = provinceRepo;
        }

        public IEnumerable<M_Province> FindAll()
        {
            return _provinceRepo.GetAll();
        }
        public M_Province GetBy(int id)
        {
            return _provinceRepo.GetById(id);
        }
    }
}
