﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ResultEntryService : IResultEntryService
    {
        private readonly IResultEntryRepository _resultEntryRepo;
        public ResultEntryService
            (
                IResultEntryRepository resultEntryRepo
            )
        {
            _resultEntryRepo = resultEntryRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _resultEntryRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(ResultType resultType, int[] referIds)
        {
            try
            {
                _resultEntryRepo.Delete(d => d.Type == resultType && referIds.Contains(d.ReferId));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(ResultType resultType, int referId)
        {
            try
            {
                _resultEntryRepo.Delete(d => d.Type == resultType && d.ReferId == referId);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(ResultType resultType, int referId, int lineNo)
        {
            try
            {
                _resultEntryRepo.Delete(d => d.Type == resultType && d.ReferId == referId && d.LineNo == lineNo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<M_ResultEntry> FindAll()
        {
            return _resultEntryRepo.GetAll();
        }
        public M_ResultEntry GetBy(int id)
        {
            return _resultEntryRepo.GetById(id) ?? new M_ResultEntry();
        }

        public string Insert(M_ResultEntry question)
        {
            try
            {
                _resultEntryRepo.Add(question);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_ResultEntry question)
        {
            try
            {
                _resultEntryRepo.Update(question);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
