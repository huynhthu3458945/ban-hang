﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository _itemRepo;
        public ItemService
            (
                IItemRepository itemRepo
            )
        {
            _itemRepo = itemRepo;
        }

        public M_Item GetBy(string code)
        {
            return _itemRepo.Get(d => d.Code.Equals(code));
        }
    }
}
