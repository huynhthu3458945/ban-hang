﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.FrontEnd;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ECourseService : IECourseService
    {
        private readonly ICategoryRepository _cateRepo;
        private readonly ICourseRepository _courseRepo;
        private readonly ICourseCategoryRepository _courseCateRepo;
        private readonly ICourseContentRepository _courseContentRepo;
        private readonly ICourseSupportRepository _courseSupportRepo;
        public ECourseService
            (
                ICategoryRepository cateRepo
                , ICourseRepository courseRepo
                , ICourseCategoryRepository courseCateRepo
                , ICourseContentRepository courseContentRepo
                , ICourseSupportRepository courseSupportRepo
            )
        {
            _cateRepo = cateRepo;
            _courseRepo = courseRepo;
            _courseCateRepo = courseCateRepo;
            _courseContentRepo = courseContentRepo;
            _courseSupportRepo = courseSupportRepo;
        }

        public List<int> GetAllChildCategory(int idCategory)
        {
            try
            {
                var idCategories = new List<int>();
                var idSubCategories = new List<int>();
                idSubCategories.Add(idCategory);
                idCategories.Add(idCategory);

                bool hasSubCategory = true;
                while (hasSubCategory)
                {
                    var categories = _cateRepo.GetMany(d => idSubCategories.Contains(d.ParentNo ?? 0));
                    hasSubCategory = categories.Count() > 0;

                    idSubCategories = new List<int>();
                    idSubCategories = categories.Select(d => d.Id).ToList();

                    idCategories.AddRange(idSubCategories);
                }
                return idCategories;
            }
            catch (Exception ex) { return new List<int>(); }
        }

        public IEnumerable<CouseCategory> CourseByCategory(int idCategory)
        {
            var idCates = GetAllChildCategory(idCategory);
            var sql = from p in _courseRepo.GetAll()
                      join cc in _courseCateRepo.GetMany(d => idCates.Contains(d.IdCategory)) on p.Id equals cc.IdCourse
                      select new CouseCategory
                      {
                          Id = p.Id,
                          IdTeacher = p.IdTeacher,
                          Title = p.Title,
                          URL = p.URL,
                          ShortDescription = p.ShortDescription,
                          DateModify = p.DateModify,
                          Image = p.Image,
                          LinkYoutube = p.LinkYoutube,
                          IdCategory = cc.IdCategory
                      };
            return sql;
        }
        public IEnumerable<CouseCategory> OtherCourseByCategory(int idCategory, int idCourse, int top)
        {
            var idCates = GetAllChildCategory(idCategory);
            var sql = from p in _courseRepo.GetAll()
                      join cc in _courseCateRepo.GetMany(d => idCates.Contains(d.IdCategory)) on p.Id equals cc.IdCourse
                      where p.Id != idCourse
                      select new CouseCategory
                      {
                          Id = p.Id,
                          IdTeacher = p.IdTeacher,
                          Title = p.Title,
                          URL = p.URL,
                          ShortDescription = p.ShortDescription,
                          DateModify = p.DateModify,
                          Image = p.Image,
                          LinkYoutube = p.LinkYoutube,
                          IdCategory = cc.IdCategory
                      };
            return sql.Take(top);
        }
        public E_Course GetCourse(int idCourse)
        {
            try
            {
                var course = _courseRepo.GetById(idCourse);
                course.CourseContents = _courseContentRepo.GetMany(d => d.IdCourse == idCourse);
                course.CourseSupports = _courseSupportRepo.GetMany(d => d.IdCourse == idCourse);
                if (course.CourseContents == null)
                    course.CourseContents = new List<E_CourseContent>();
                if (course.CourseSupports == null)
                    course.CourseSupports = new List<E_CourseSupport>();
                return course;
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                return new E_Course();
            }
        }

        public E_Category GetCategory(int idCategory)
        {
            return _cateRepo.GetById(idCategory);
        }
    }
}
