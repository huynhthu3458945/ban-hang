﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CategoryProductService : ICategoryProductService
    {
        private readonly ICategoryProductRepository _categoryProductRepository;
        public CategoryProductService
            (
                ICategoryProductRepository categoryProductRepository
            )
        {
            _categoryProductRepository = categoryProductRepository;
        }


        public string Delete(int id)
        {
            try
            {
                _categoryProductRepository.Delete(d => d.Id  == id);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Delete(IEnumerable<int> ids)
        {
            try
            {
                _categoryProductRepository.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public IEnumerable<CategoriesProduct> FindAll()
        {
            return _categoryProductRepository.GetAll();
        }

        public IEnumerable<CategoriesProduct> FindAll(bool status)
        {
            return _categoryProductRepository.GetMany(z=>z.Status == status);
        }

        public CategoriesProduct GetById(int id)
        {
            return _categoryProductRepository.GetById(id);
        }

        public string Insert(CategoriesProduct categoriesProduct)
        {
            try
            {
                _categoryProductRepository.Add(categoriesProduct);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<CategoriesProduct> categoriesProducts)
        {
            try
            {
                _categoryProductRepository.Add(categoriesProducts);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(CategoriesProduct categoriesProduct)
        {
            try
            {
                _categoryProductRepository.Update(categoriesProduct);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<CategoriesProduct> categoriesProducts)
        {
            try
            {
                _categoryProductRepository.Update(categoriesProducts);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
