﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepo;
        private readonly ICardLineRepository _cardLineRepo;
        public CustomerService
            (
                ICustomerRepository customerRepo
                , ICardLineRepository cardLineRepo
            )
        {
            _customerRepo = customerRepo;
            _cardLineRepo = cardLineRepo;
        }

        public string Delete(string lotNumber)
        {
            try
            {
                _customerRepo.Delete(d => d.LotNumber.Equals(lotNumber));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Customer> FindAll()
        {
            return _customerRepo.GetAll();
        }
        public IEnumerable<M_Customer> FindAllNotActive()
        {
            var sql = from p in _customerRepo.GetMany(d => !string.IsNullOrEmpty(d.SerialNumber))
                      join c in _cardLineRepo.GetMany(d => !d.IsActive) on p.SerialNumber equals c.SerialNumber
                      select p;
            return sql;
        }
        public IEnumerable<M_Customer> FindAll(string lotNumner)
        {
            return _customerRepo.GetMany(d => d.LotNumber.Equals(lotNumner));
        }
        public IEnumerable<M_Customer> FindAll(string lotNumner, string statusEmail, string fullName, string phone, string email)
        {
            return _customerRepo.GetMany(d => d.LotNumber.Equals(lotNumner)
                                            && (string.IsNullOrEmpty(statusEmail) || (!string.IsNullOrEmpty(statusEmail) && d.IsEmail.Equals(statusEmail)))
                                            && (string.IsNullOrEmpty(fullName) || (!string.IsNullOrEmpty(fullName) && d.FullName.Contains(fullName) ))
                                            && (string.IsNullOrEmpty(phone) || (!string.IsNullOrEmpty(phone) && d.PhoneNo.Contains(phone)))
                                            && (string.IsNullOrEmpty(email) || (!string.IsNullOrEmpty(email) && d.Email.Contains(email)))
                                            );
        }
        public M_Customer GetBy(string seriNumber)
        {
            return _customerRepo.Get(d => d.SerialNumber.Equals(seriNumber));
        }
        public M_Customer GetBy(int id)
        {
            return _customerRepo.GetById(id);
        }
        public M_Customer GetBy2(int id)
        {
            return _customerRepo.Get(d => d.Id == id);
        }
        public string Insert(M_Customer customer)
        {
            try
            {
                _customerRepo.Add(customer);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<M_Customer> customers)
        {
            try
            {
                _customerRepo.Add(customers);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Customer customer)
        {
            try
            {
                _customerRepo.Update(customer);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_Customer> customers)
        {
            try
            {
                _customerRepo.Update(customers);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
