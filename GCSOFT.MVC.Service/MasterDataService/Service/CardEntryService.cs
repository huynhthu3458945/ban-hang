﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CardEntryService : ICardEntryService
    {
        private readonly ICardEntryRepository _cardEntryRepo;
        public CardEntryService
            (
                ICardEntryRepository cardEntryRepo
            )
        {
            _cardEntryRepo = cardEntryRepo;
        }

        public IEnumerable<M_CardEntry> FindAll()
        {
            return _cardEntryRepo.GetAll();
        }

        public IEnumerable<M_CardEntry> FindAllByCardNo(string cardNo)
        {
            return _cardEntryRepo.GetMany(d => d.CardNo.Equals(cardNo, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<M_CardEntry> FindAllByLotNumber(string lotNumber)
        {
            return _cardEntryRepo.GetMany(d => d.LotNumber.Equals(lotNumber, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<M_CardEntry> FindAllBySeriNumber(string seriNumner)
        {
            return _cardEntryRepo.GetMany(d => d.SerialNumber.Equals(seriNumner, StringComparison.OrdinalIgnoreCase));
        }
        public M_CardEntry GetBy(string seriNumber)
        {
            return _cardEntryRepo.GetMany(d => d.SerialNumber.Equals(seriNumber)).OrderByDescending(d => d.Date).FirstOrDefault() ?? new M_CardEntry();
        }
        public long GetID()
        {
            return _cardEntryRepo.GetKey(d => d.Id);
        }

        public string Insert(M_CardEntry cardEntry)
        {
            try
            {
                _cardEntryRepo.Add(cardEntry);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<M_CardEntry> cardEntries)
        {
            try
            {
                _cardEntryRepo.Add(cardEntries);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_CardEntry cardEntry)
        {
            try
            {
                _cardEntryRepo.Update(cardEntry);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_CardEntry> cardEntries)
        {
            try
            {
                _cardEntryRepo.Update(cardEntries);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
