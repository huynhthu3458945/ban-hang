﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class HistoryCallService : IHistoryCall
    {
        private readonly IHistoryCallRepository _historyCallRepository;
        public HistoryCallService
            (
                IHistoryCallRepository historyCallRepository
            )
        {
            _historyCallRepository = historyCallRepository;
        }

        public List<M_HistoryCall> GetAll(int id, string userName)
        {
            try
            {
                return _historyCallRepository.GetAll().Where(z=>z.AgencyId == id && z.UserName == userName).ToList();
            }
            catch { return new List<M_HistoryCall>(); }
        }
        public List<M_HistoryCall> GetAll(int id, int potentialId)
        {
            try
            {
                return _historyCallRepository.GetAll().Where(z => z.AgencyId == id && z.PotentialId == potentialId).ToList();
            }
            catch  { return new List<M_HistoryCall>(); }
        }

            public string Insert(M_HistoryCall historyCall)
        {
            try
            {
                _historyCallRepository.Add(historyCall);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
      
    }
}
