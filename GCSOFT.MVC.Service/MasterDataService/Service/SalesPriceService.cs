﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class SalesPriceService : ISalesPriceService
    {
        private readonly ISalesPriceRepository _salesPriceRepo;
        public SalesPriceService
            (
                ISalesPriceRepository salesPriceRepo
            )
        {
            _salesPriceRepo = salesPriceRepo;
        }

        public M_SalesPrice GetBy(string levelCode, DateTime dateInput)
        {
            return _salesPriceRepo.Get(d => d.LevelCode.Equals(levelCode) && d.DateInput >= dateInput);
        }
    }
}
