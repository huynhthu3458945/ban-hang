﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;
using GCSOFT.MVC.Data.Infrastructure;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class MessageDetailService : IMessageDetailService
    {
        private readonly IMessageDetailRepository _messageDetailRepository;
        private readonly IUnitOfWork _unitOfWork;
        public MessageDetailService
            (
                IMessageDetailRepository messageDetailRepository
                , IUnitOfWork unitOfWork
            )
        {
            _messageDetailRepository = messageDetailRepository;
            _unitOfWork = unitOfWork;
        }

        public string Delete(string apk)
        {
            try
            {
                _messageDetailRepository.Delete(z => z.APKMaster == apk);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_MessageDetail> FindAll()
        {
            return _messageDetailRepository.GetAll();
        }
        public M_MessageDetail GetById(string apk)
        {
            return _messageDetailRepository.GetMany(z => z.APKMaster == apk).FirstOrDefault();
        }
        public string Insert(M_MessageDetail M_MessageDetail)
        {
            try
            {
                _messageDetailRepository.Add(M_MessageDetail);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_MessageDetail M_MessageDetail)
        {
            try
            {
                _messageDetailRepository.Update(M_MessageDetail);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public string Insert(IEnumerable<M_MessageDetail> m_MessageDetail, string apk)
        {
            _messageDetailRepository.Delete(z => z.APKMaster == apk);
            if(m_MessageDetail != null)
                foreach (var item in m_MessageDetail)
                {
                    _messageDetailRepository.Add(item);
                }
            return null;
        }

        public string Update(IEnumerable<M_MessageDetail> m_MessageDetail, string apk)
        {
            foreach (var item in m_MessageDetail)
            {
                _messageDetailRepository.Add(item);
            }
            Commit();
            return null;
        }
        public string Delete(IEnumerable<M_MessageDetail> m_MessageDetail, string apk)
        {
            _messageDetailRepository.Delete(z => z.APKMaster == apk);
            return null;
        }

    }
}
