﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class StudentClassService : IStudentClassService
    {
        private readonly IStudentClassRepository _studentClassRepo;
        public StudentClassService
            (
                IStudentClassRepository studentClassRepo
            )
        {
            _studentClassRepo = studentClassRepo;
        }
        public string Delete(int[] ids)
        {
            try
            {
                _studentClassRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(string userName)
        {
            try
            {
                _studentClassRepo.Delete(d => d.userName.Equals(userName));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<M_StudentClass> FindAll()
        {
            return _studentClassRepo.GetAll();
        }

        public IEnumerable<M_StudentClass> FindAll(string userName)
        {
            return _studentClassRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }
        public IEnumerable<M_StudentClass> FindAllByDate(string userName)
        {
            return _studentClassRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && d.FromDate <= DateTime.Now && (d.ToDate == null || d.ToDate >= DateTime.Now));
        }
        public M_StudentClass GetBy(int id)
        {
            return _studentClassRepo.GetById(id);
        }

        public string Insert(IEnumerable<M_StudentClass> studentClasses)
        {
            try
            {
                _studentClassRepo.Add(studentClasses);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_StudentClass> studentClasses)
        {
            try
            {
                _studentClassRepo.Update(studentClasses);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public bool HasClass(string userName)
        {
            return _studentClassRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase)).Any();
        }
        public IEnumerable<M_StudentClass> FindAllByActive(string userName)
        {
            return _studentClassRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && d.IsActive).OrderBy(d => d.ClassId);
        }
    }
}
