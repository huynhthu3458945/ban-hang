﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ClassReferService : IClassReferService
    {
        private readonly IClassReferRepository _classReferRepo;
        public ClassReferService
            (
                IClassReferRepository classReferRepo
            )
        {
            _classReferRepo = classReferRepo;
        }
        public string Delete(int[] ids)
        {
            try
            {
                _classReferRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<E_ClassRefer> FindAll()
        {
            return _classReferRepo.GetAll();
        }
        public IEnumerable<E_ClassRefer> FindAll(int classId)
        {
            return _classReferRepo.GetMany(d => d.ClassId == classId);
        }
        public string Insert(IEnumerable<E_ClassRefer> classRefers)
        {
            try
            {
                _classReferRepo.Add(classRefers);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(IEnumerable<E_ClassRefer> classRefers)
        {
            try
            {
                _classReferRepo.Update(classRefers);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
