﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _cateRepo;
        public CategoryService
            (
                ICategoryRepository cateRepo
            )
        {
            _cateRepo = cateRepo;
        }

        public bool CandAddTextLink(int id, string linkWeb)
        {
            E_Category category;
            if (id == 0)
                category = _cateRepo.Get(d => d.LinkWeb.ToLower().Equals(linkWeb.ToLower()));
            else
                category = _cateRepo.Get(d => d.LinkWeb.ToLower().Equals(linkWeb.ToLower()) && d.Id != id);
            if (category != null)
                return true;
            return false;
        }

        public string Delete(List<int> ids)
        {
            try
            {
                _cateRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<E_Category> FindAll()
        {
            return _cateRepo.GetAll();
        }
        public IEnumerable<E_Category> FindAll(List<int> categoryChooseds)
        {
            return _cateRepo.GetMany(d => !categoryChooseds.Contains(d.Id));
        }
        public E_Category GetBy(int id)
        {
            return _cateRepo.GetById(id);
        }

        public int GetKeyCategory()
        {
            return _cateRepo.GetKey(d => d.Id);
        }

        public int GetMaxLevel(int IdParentNo)
        {
            int? level = 0;
            if (IdParentNo == 0)
                level = _cateRepo.GetMany(d => d.ParentNo == null).Max(d => (int?)d.Level);
            else
                level = _cateRepo.GetMany(d => d.ParentNo == IdParentNo).Max(d => (int?)d.Level);
            if (level == 0 || level == null)
                return 100;
            return (int)level + 100;
        }

        public IEnumerable<E_Category> GetParentCategory(int id)
        {
            var cateParents = new List<E_Category>();
            cateParents = _cateRepo.GetMany(d => d.Id != id).OrderBy(d => d.LevelStr).ToList();
            if (cateParents == null)
                cateParents = new List<E_Category>();
            return cateParents.AsEnumerable();
        }

        public string GetSearchName(string name, string url, int id, int? idParentNo, ref int levelCatetory, ref string breadCrumb)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<BreadCrumb> breadCrumbs = new List<BreadCrumb>();
            breadCrumbs.Add(new BreadCrumb() { Id = id, Name = name, URL = url, IsMain = true });
            var parentInfo = _cateRepo.Get(d => d.Id == idParentNo);
            if (parentInfo == null)
                parentInfo = new E_Category();
            breadCrumbs.Add(new BreadCrumb() { Id = parentInfo.Id, Name = parentInfo.Name, URL = parentInfo.LinkWeb, IsMain = false });
            while (parentInfo.ParentNo != null)
            {
                parentInfo = _cateRepo.Get(d => d.Id == parentInfo.ParentNo);
                breadCrumbs.Add(new BreadCrumb() { Id = parentInfo.Id, Name = parentInfo.Name, URL = parentInfo.LinkWeb, IsMain = false });
            }
            breadCrumbs = breadCrumbs.Where(d => !string.IsNullOrEmpty(d.Name)).OrderBy(d => d.Id).ToList();
            breadCrumbs.ForEach(item => {
                if (item.IsMain)
                    stringBuilder.Append(item.Name);
                else
                    stringBuilder.Append(item.Name + " > ");
            });
            levelCatetory = breadCrumbs.Count();
            breadCrumb = JsonConvert.SerializeObject(breadCrumbs);
            return stringBuilder.ToString();
        }

        public int GetSortOrder(int? idParent)
        {
            try
            {
                return _cateRepo.GetMany(d => d.ParentNo == idParent).Max(d => d.SortOrder) + 100;
            }
            catch { return 100; }
        }

        public string Insert(E_Category cate)
        {
            try
            {
                _cateRepo.Add(cate);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(E_Category cate)
        {
            try
            {
                _cateRepo.Update(cate);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
