﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class SupporterService : ISupporterService
    {
        private readonly ISupporterRepository _supporterRepo;
        public SupporterService
            (
                ISupporterRepository supporterRepo
            )
        {
            _supporterRepo = supporterRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _supporterRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Supporter> FindAll()
        {
            return _supporterRepo.GetAll().OrderBy(d => d.SortOrder);
        }

        public M_Supporter GetBy(int id)
        {
            return _supporterRepo.GetById(id);
        }

        public int GetID()
        {
            return _supporterRepo.GetKey(d => d.Id);
        }

        public string Insert(M_Supporter supporter)
        {
            try
            {
                _supporterRepo.Add(supporter);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Supporter supporter)
        {
            try
            {
                _supporterRepo.Update(supporter);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
