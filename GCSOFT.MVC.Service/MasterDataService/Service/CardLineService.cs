﻿using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CardLineService : ICardLineService
    {
        private readonly ICardLineRepository _cardLineRepo;
        public CardLineService
            (
                ICardLineRepository cardLineRepo
            )
        {
            _cardLineRepo = cardLineRepo;
        }

        public IEnumerable<M_CardLine> FindAll()
        {
            return _cardLineRepo.GetAll();
        }

        public IEnumerable<M_CardLine> FindAll(string lotNumber)
        {
            return _cardLineRepo.GetMany(d => d.LotNumber.Equals(lotNumber));
        }
        public IEnumerable<M_CardLine> FindAll(string lotNumber, int start, int offset)
        {
            return _cardLineRepo.GetMany(d => d.LotNumber.Equals(lotNumber)).Skip(start).Take(offset);
        }
        public IEnumerable<M_CardLine> FindAll(string statusCode, int offset)
        {
            return _cardLineRepo.GetMany(d => d.StatusCode.Equals(statusCode)).Take(offset);
        }
        public M_CardLine GetByCardNo(string cardNo)
        {
            return _cardLineRepo.Get(d => d.CardNo.Equals(cardNo)) ?? new M_CardLine();
        }

        public M_CardLine GetBySerialNumber(string seriNumner)
        {
            return _cardLineRepo.Get(d => d.SerialNumber.Equals(seriNumner)) ?? new M_CardLine();
        }
        public M_CardLine GetBySerialNumber(string seriNumner, string status)
        {
            return _cardLineRepo.Get(d => d.SerialNumber.Equals(seriNumner) && d.StatusCode.Equals(status)) ?? new M_CardLine();
        }
        public M_CardLine GetById(long id)
        {
            return _cardLineRepo.GetById(id);
        }
        public string Insert(M_CardLine cardLine)
        {
            try
            {
                _cardLineRepo.Add(cardLine);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string Insert(IEnumerable<M_CardLine> cardLines)
        {
            try
            {
                _cardLineRepo.Add(cardLines);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(M_CardLine cardLine)
        {
            try
            {
                _cardLineRepo.Update(cardLine);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string Update(IEnumerable<M_CardLine> cardLines)
        {
            try
            {
                _cardLineRepo.Update(cardLines);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public long GetID()
        {
            return _cardLineRepo.GetKey(d => d.Id);
        }
        public string CheckActiveCode(string activeCode)
        {
            var _activeCode = CryptorEngine.Encrypt(activeCode, true, "TTLSTNHD@CARD");
            var cardInfo = _cardLineRepo.Get(d => d.CardNo.Equals(_activeCode));
            if (cardInfo == null)
                return "Nothing";
            cardInfo = _cardLineRepo.Get(d => d.CardNo.Equals(_activeCode) && d.IsActive);
            if (cardInfo != null)
                return "IsActive";
            cardInfo = _cardLineRepo.Get(d => d.CardNo.Equals(_activeCode) && !d.IsActive);
            return cardInfo.SerialNumber;
        }

        public bool CheckExist(string cardNo)
        {
            return _cardLineRepo.GetMany(d => d.CardNo.Equals(cardNo)).Any();
        }
        public M_CardLine GetByIdCard(long idCard)
        {
            return _cardLineRepo.Get(d => d.Id.Equals(idCard)) ?? new M_CardLine();
        }

        public bool UpdateCardNo(string cardNo, long Id)
        {
            try
            {
                var _cardNo = _cardLineRepo.Get(d => d.Id.Equals(Id)) ?? new M_CardLine();
                if (_cardNo != null && _cardNo is M_CardLine model)
                {
                    model.CardNo = cardNo;
                    _cardLineRepo.Update(model);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<M_CardLine> FindPhone(string phone)
        {
            return _cardLineRepo.GetMany(d => d.PhoneNo.Equals(phone)) ?? new List<M_CardLine>();
        }
    }
}
