﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public string Delete(int id)
        {
            try
            {
                _productRepository.Delete(d => d.Id == id);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Delete(IEnumerable<int> ids)
        {
            try
            {
                _productRepository.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public IEnumerable<Product> FindAll()
        {

            return _productRepository.GetAll();
        }

        public IEnumerable<Product> FindAll(int categoryId)
        {

            return _productRepository.GetMany(z=>z.CategoryProductId == categoryId);
        }

        public string Insert(Product product)
        {
            try
            {
                _productRepository.Add(product);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<Product> products)
        {
            try
            {
                _productRepository.Add(products);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(Product product)
        {
            try
            {
                _productRepository.Update(product);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<Product> products)
        {
            try
            {
                _productRepository.Update(products);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
