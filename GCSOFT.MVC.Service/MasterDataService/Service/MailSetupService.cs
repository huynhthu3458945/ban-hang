﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class MailSetupService : IMailSetupService
    {
        private readonly IMailSetupRepository _mailSetupRepo;
        public MailSetupService
            (
                IMailSetupRepository mailSetupRepo
            )
        {
            _mailSetupRepo = mailSetupRepo;
        }

        public M_MailSetup Get()
        {
            return _mailSetupRepo.Get(d => d.Id == 10);
        }
    }
}
