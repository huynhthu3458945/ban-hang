﻿using GCSOFT.MVC.Service.StoreProcedure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.StoreProcedure;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.StoreProcedue;

namespace GCSOFT.MVC.Service.StoreProcedure.Service
{
    public class StoreProcedureService : IStoreProcedureService
    {
        private readonly IStoreProcedureRepository _storeProcedureRepo;
        public StoreProcedureService
            (
                IStoreProcedureRepository storeProcedureRepo
            )
        {
            _storeProcedureRepo = storeProcedureRepo;
        }
        public IEnumerable<SP_SYS_Categroy_Tree> SP_SYS_Categroy_Tree(string Code)
        {
            return _storeProcedureRepo.SP_SYS_Categroy_Tree(Code);
        }
    }
}