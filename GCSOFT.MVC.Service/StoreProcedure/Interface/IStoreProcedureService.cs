﻿using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.StoreProcedue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.StoreProcedure.Interface
{
    public interface IStoreProcedureService
    {
        IEnumerable<SP_SYS_Categroy_Tree> SP_SYS_Categroy_Tree(string Code);
    }
}