﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;

namespace GCSOFT.MVC.Service.SystemService.Service
{
    public class UserThuocNhomService : IUserThuocNhomService
    {
        private readonly IUserThuocNhomRepository _userThuocNhomRepo;
        private readonly IUnitOfWork _unitOfWork;

        public UserThuocNhomService
            (
                IUserThuocNhomRepository userThuocNhomRepo
                , IUnitOfWork unitOfWork
            )
        {
            _userThuocNhomRepo = userThuocNhomRepo;
            _unitOfWork = unitOfWork;
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public string Delete(string userGroupCode, int idUser)
        {
            try
            {
                _userThuocNhomRepo.Delete(d => d.UserGroupCode.Equals(userGroupCode) && d.UserID == idUser);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Delete(int idUser)
        {
            try
            {
                _userThuocNhomRepo.Delete(d => d.UserID == idUser);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<HT_UserThuocNhom> FindAll()
        {
            return _userThuocNhomRepo.GetAll();
        }

        public IEnumerable<HT_UserThuocNhom> GetMany(int idUser)
        {
            return _userThuocNhomRepo.GetMany(d => d.UserID == idUser);
        }
        public IEnumerable<HT_UserThuocNhom> GetMany(string groupCode)
        {
            return _userThuocNhomRepo.GetMany(d => d.UserGroupCode.Equals(groupCode));
        }
        public string Insert(HT_UserThuocNhom userOfGrp)
        {
            try
            {
                _userThuocNhomRepo.Add(userOfGrp);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Insert2(HT_UserThuocNhom userOfGrp)
        {
            try
            {
                _userThuocNhomRepo.Add(userOfGrp);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(HT_UserThuocNhom userOfGrp)
        {
            try
            {
                _userThuocNhomRepo.Update(userOfGrp);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}