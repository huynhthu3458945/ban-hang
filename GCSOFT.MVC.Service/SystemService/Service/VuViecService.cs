﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;
using System.Collections.Generic;
using System.Linq;

namespace GCSOFT.MVC.Service.SystemService.Service
{
    public class VuViecService : IVuViecService
    {
        private readonly IVuViecRepository _vuViecRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOptionLineRepository _optionLineRepo;
        private readonly ICourseRepository _courseRepo;
        private readonly ICourseContentRepository _courseContentRepo;
        private readonly ICongViecRepository _congViecRepository;
        public VuViecService
            (
                IVuViecRepository vuViecRepository
                , IOptionLineRepository optionLineRepo
                , ICourseRepository courseRepo
                , ICourseContentRepository courseContentRepo
                , IUnitOfWork unitOfWork
                , ICongViecRepository congViecRepository
           )
        {
            _vuViecRepository = vuViecRepository;
            _unitOfWork = unitOfWork;
            _optionLineRepo = optionLineRepo;
            _courseRepo = courseRepo;
            _courseContentRepo = courseContentRepo;
            _congViecRepository = congViecRepository;
        }

        public IEnumerable<HT_VuViec> FindAll()
        {
            return _vuViecRepository.GetAll();
        }

        public IEnumerable<HT_VuViec> GetRoleBy(string categoryCode, string userName)
        {
            return _vuViecRepository.GetRoleBy(categoryCode, userName);
        }
        public string Commit()
        {
            try
            {
                _unitOfWork.Commit();
                return null;
            }
            catch (System.Exception ex)
            {
                return ex.ToString();
                throw;
            }
        }
        public M_OptionLine Get(int OptionHeaderID, int lineNo)
        {
            return _optionLineRepo.Get(d => d.OptionHeaderID == OptionHeaderID && d.LineNo == lineNo) ?? new M_OptionLine();
        }
        public M_OptionLine Get(int OptionHeaderID, string OptionCode)
        {
            return _optionLineRepo.Get(d => d.OptionHeaderID == OptionHeaderID && d.Code == OptionCode) ?? new M_OptionLine();
        }
        public IEnumerable<HT_CongViec> GetCategoryBy(string username)
        {
            return _congViecRepository.GetCategoryBy(username);
        }
    }
}
