﻿using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.SystemService.Interface
{
    public interface IUserGroupServices
    {
        IEnumerable<HT_NhomUser> FindAll();
        HT_NhomUser GetBy(string code);
        string Insert(HT_NhomUser nhomUser);
        string Update(HT_NhomUser nhomUser);
        string Delete(string code);
        string Delete(string[] codes);
        bool CanAdd(string code, string xCode);
        void Commit();
    }
}
