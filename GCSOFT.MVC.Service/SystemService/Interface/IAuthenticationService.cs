﻿using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.SystemService.Interface
{
    public interface IAuthenticationService
    {
        IEnumerable<Permission> SystemFuncForTree(string groupCode);
        IEnumerable<PermissionOnSystemFunc> GetAllPermissionOnSystemFunc(string categoryCode, string groupCode);
    }
}
