﻿using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.SystemService.Interface
{
    public interface IUserThuocNhomService
    {
        IEnumerable<HT_UserThuocNhom> FindAll();
        IEnumerable<HT_UserThuocNhom> GetMany(int idUser);
        IEnumerable<HT_UserThuocNhom> GetMany(string groupCode);
        string Insert2(HT_UserThuocNhom userOfGrp);
        string Insert(HT_UserThuocNhom userOfGrp);
        string Update(HT_UserThuocNhom userOfGrp);
        string Delete(string userGroupCode, int idUser);
        string Delete(int idUser);
        void Commit();
    }
}
