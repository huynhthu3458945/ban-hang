﻿using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.SystemService.Interface
{
    public interface ICongViecVaVuViecService
    {
        HT_CongViecVaVuViec Get(string categoryCode, string userGroupCode);
        string Insert(IEnumerable<HT_CongViecVaVuViec> congViecVaVuViecs);
        string Update(IEnumerable<HT_CongViecVaVuViec> congViecVaVuViecs);
        void Commit();
    }
}
