﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class TrongTaiService : ITrongTaiService
    {
        private readonly ITrongTaiRepository _trongTaiRepo;
        public TrongTaiService
            (
                ITrongTaiRepository trongTaiRepo
            )
        {
            _trongTaiRepo = trongTaiRepo;
        }

        public string Delete(int id)
        {
            try
            {
                _trongTaiRepo.Delete(d => d.Id == id);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<TB_TrongTai> FindAll()
        {
            return _trongTaiRepo.GetAll();
        }
        public IEnumerable<TB_TrongTai> FindAll(string trongTaiType)
        {
            return _trongTaiRepo.GetMany(d => d.TrongTaiCode.Equals(trongTaiType));
        }
        public IEnumerable<TB_TrongTai> FindAll(List<string> trongTaiTypes)
        {
            return _trongTaiRepo.GetMany(d => trongTaiTypes.Contains(d.TrongTaiCode));
        }

        public TB_TrongTai GetBy(int Id)
        {
            return _trongTaiRepo.GetById(Id);
        }

        public string Insert(TB_TrongTai trongTai)
        {
            try
            {
                _trongTaiRepo.Add(trongTai);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(TB_TrongTai trongTai)
        {
            try
            {
                _trongTaiRepo.Update(trongTai);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public TB_TrongTai Get(int Id)
        {
            return _trongTaiRepo.Get(d => d.Id == Id);
        }
        public int GetMax()
        {
            return _trongTaiRepo.GetAll().Max(d => d.Id);
        }
    }
}
