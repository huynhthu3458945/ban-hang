﻿using GCSOFT.MVC.Data.Repositories.NamPhutThuocBaiRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class PointEntryService : IPointEntryService
    {
        private readonly IPointEntryRepository _pointEntryRepo;
        public PointEntryService
            (
                IPointEntryRepository pointEntryRepo
            )
        {
            _pointEntryRepo = pointEntryRepo;
        }

        public IEnumerable<TB_PointEntry> FinAll(string userName)
        {
            return _pointEntryRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }

        public int TotalPoint(string userName)
        {
            try
            {
                return _pointEntryRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && !d.FromSource.Equals("QUAYSO")).Sum(d => d.Point);
            }
            catch { return 0; }
        }
        public string Insert(TB_PointEntry pointEntry)
        {
            try
            {
                _pointEntryRepo.Add(pointEntry);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
                throw;
            }
        }
    }
}
