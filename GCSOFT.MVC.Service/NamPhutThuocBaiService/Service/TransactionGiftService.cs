﻿using GCSOFT.MVC.Data.Repositories.NamPhutThuocBaiRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class TransactionGiftService : ITransactionGiftService
    {
        private readonly ITransactionGiftRepository _transactionGiftRepo;
        public TransactionGiftService
            (
                ITransactionGiftRepository transactionGiftRepo
            )
        {
            _transactionGiftRepo = transactionGiftRepo;
        }

        public string Delete(string code)
        {
            try
            {
                _transactionGiftRepo.Delete(d => d.Code.Equals(code));
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public IEnumerable<TB_TransactionGift> FindAll()
        {
            return _transactionGiftRepo.GetAll();
        }

        public IEnumerable<TB_TransactionGift> FindAll(string userName)
        {
            return _transactionGiftRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }

        public string Insert(TB_TransactionGift transaction)
        {
            try
            {
                _transactionGiftRepo.Add(transaction);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(TB_TransactionGift transaction)
        {
            try
            {
                _transactionGiftRepo.Update(transaction);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string GetMax()
        {
            return _transactionGiftRepo.GetAll().OrderByDescending(d => d.Code).Select(d => d.Code).FirstOrDefault();
        }
        public TB_TransactionGift GetBy(string code)
        {
            return _transactionGiftRepo.Get(d => d.Code.Equals(code));
        }
    }
}
