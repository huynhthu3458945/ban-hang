﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class DeThiService : IDeThiService
    {
        private readonly IDeThiRepository _dethiRepo;
        public DeThiService
            (
                IDeThiRepository dethiRepo
            )
        {
            _dethiRepo = dethiRepo;
        }

        public string Delete(int id)
        {
            try
            {
                _dethiRepo.Delete(d => d.Id == id);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<TB_DeThi> FindAll()
        {
            return _dethiRepo.GetAll();
        }

        public TB_DeThi GetBy(int Id)
        {
            return _dethiRepo.GetById(Id);
        }
        public TB_DeThi GetBy(string code)
        {
            return _dethiRepo.Get(d => d.Code.Equals(code));
        }
        public string Insert(TB_DeThi dethi)
        {
            try
            {
                _dethiRepo.Add(dethi);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(TB_DeThi dethi)
        {
            try
            {
                _dethiRepo.Update(dethi);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
