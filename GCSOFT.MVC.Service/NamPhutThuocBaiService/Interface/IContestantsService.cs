﻿using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface IContestantsService
    {
        IEnumerable<TB_Contestants> FindAll();
        IEnumerable<TB_Contestants> FindAll(string phoneNumber);
        TB_Contestants GetBy(int id);
        TB_Contestants GetByUserName(string userName);
        TB_Contestants GetByUserName(string userName, string passWord);
        TB_Contestants GetByIDNo(string id);
        string Insert(TB_Contestants contestants);
        string Update(TB_Contestants contestants);
        string GetIdentificationNumber();
    }
}
