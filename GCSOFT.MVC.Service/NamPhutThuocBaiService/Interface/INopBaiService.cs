﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface INopBaiService
    {
        IEnumerable<TB_NopBai> FindAll();
        IEnumerable<TB_NopBai> FindAll(List<string> techerUserNames);
        IEnumerable<TB_NopBai> FindAll(string techerUserName);
        IEnumerable<TB_NopBai> FindAllUserName(string userName);
        IEnumerable<TB_NopBai> FindAll(int classId);
        IEnumerable<TB_NopBai> FindAllChuyenMon1();
        IEnumerable<TB_NopBai> FindAllChuyenMon1(string techerUserName);
        IEnumerable<TB_NopBai> FindAllChuyenMon1(List<string> techerUserNames);
        IEnumerable<TB_NopBai> FindAllChuyenMon2();
        IEnumerable<TB_NopBai> FindAllChuyenMon2(string techerUserName);
        IEnumerable<TB_NopBai> FindAllChuyenMon2(List<string> techerUserNames);
        TB_NopBai GetBy(int Id);
        TB_NopBai Get(int Id);
        TB_NopBai GetBy(string userName);
        bool DaNopBai(string userName);
        string Insert(TB_NopBai nopBai);
        string Update(TB_NopBai nopBai);
        string Update(IEnumerable<TB_NopBai> nopBais);
    }
}
