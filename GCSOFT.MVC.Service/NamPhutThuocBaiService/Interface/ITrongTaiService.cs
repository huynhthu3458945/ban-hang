﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface ITrongTaiService
    {
        IEnumerable<TB_TrongTai> FindAll();
        IEnumerable<TB_TrongTai> FindAll(string trongTaiType);
        IEnumerable<TB_TrongTai> FindAll(List<string> trongTaiTypes);
        TB_TrongTai GetBy(int Id);
        TB_TrongTai Get(int Id);
        string Insert(TB_TrongTai trongTai);
        string Update(TB_TrongTai trongTai);
        string Delete(int id);
        int GetMax();
    }
}
