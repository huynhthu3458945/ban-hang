﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface IGiftService
    {
        IEnumerable<TB_Gift> FindAll();
        IEnumerable<TB_Gift> GetAllIsShow();
        TB_Gift GetBy(string code);
    }
}
