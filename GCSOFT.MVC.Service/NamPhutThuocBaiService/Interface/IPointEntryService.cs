﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface IPointEntryService
    {
        IEnumerable<TB_PointEntry> FinAll(string userName);
        int TotalPoint(string userName);
        string Insert(TB_PointEntry pointEntry);
    }
}
