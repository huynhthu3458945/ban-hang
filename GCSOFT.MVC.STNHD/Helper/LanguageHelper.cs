﻿using GCSOFT.MVC.STNHD.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace GCSOFT.MVC.STNHD.Helper
{
    public class LanguageHelper
    {
        public static List<Languages> AvailableLanguages(List<LanguageViewModel> langs)
        {
            var languages = new List<Languages>();
            langs.ForEach(lang =>
            {
                languages.Add(new Languages() { LanguageFullName = lang.Name, LanguageCultureName = lang.Code });
            });
            return languages;
        }
        public static bool IsLanguageAvailable(string lang, List<LanguageViewModel> langs)
        {
            return AvailableLanguages(langs).Where(a => a.LanguageCultureName.Equals(lang)).FirstOrDefault() != null ? true : false;
        }
        public static string GetDefaultLanguage(List<LanguageViewModel> langs)
        {
            return AvailableLanguages(langs)[0].LanguageCultureName;
        }
        public void SetLanguage(string lang, List<LanguageViewModel> langs)
        {
            try
            {
                if (HttpContext.Current.Request.Cookies["culture"] != null)
                {
                    HttpContext.Current.Request.Cookies["culture"].Expires = DateTime.Now.AddDays(-1);
                }

                if (!IsLanguageAvailable(lang, langs)) lang = GetDefaultLanguage(langs);
                var cultureInfo = new CultureInfo(lang);
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
                HttpCookie langCookie = new HttpCookie("culture", lang);
                langCookie.Expires = DateTime.Now.AddYears(8);
                HttpContext.Current.Response.Cookies.Add(langCookie);
            }
            catch (Exception) { }
        }
    }
    public class Languages
    {
        public string LanguageFullName { get; set; }
        public string LanguageCultureName { get; set; }
    }
}