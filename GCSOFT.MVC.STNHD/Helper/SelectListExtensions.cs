﻿using GCSOFT.MVC.STNHD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Helper
{
    public static class SelectListExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ClassList> classes, int selectedId)
        {
            return classes.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.Name,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ProvinceViewModel> provinces, int selectedId)
        {
            return provinces.Select(item => new SelectListItem
            {
                Selected = (item.id == selectedId),
                Text = item.name,
                Value = item.id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<DistrictViewModel> districts, int selectedId)
        {
            return districts.Select(item => new SelectListItem
            {
                Selected = (item.id == selectedId),
                Text = item.name,
                Value = item.id.ToString()
            });
        }
    }
}