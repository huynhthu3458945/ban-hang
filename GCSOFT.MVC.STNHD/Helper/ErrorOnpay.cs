﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Helper
{

    public class ErrorOnpay
    {
        public string ID { get; set; }
        public string Message { get; set; }
    }

    public static class ErrorOnpayService
    {
        public static List<ErrorOnpay> ErrorOnpays()
        {
            List<ErrorOnpay> data = new List<ErrorOnpay>();
            data.Add(new ErrorOnpay { ID = "0", Message = "Transaction is successful Giao dịch thành công" });
            data.Add(new ErrorOnpay { ID = "1", Message = "Giao dịch không thành công, Ngân hàng phát hành thẻ không cấp phép cho giao dịch hoặc thẻ chưa được kích hoạt dịch vụ thanh toán trên Internet. Vui lòng liên hệ ngân hàng theo số điện thoại sau mặt thẻ được hỗ trợ chi tiết." });
            data.Add(new ErrorOnpay { ID = "2", Message = "Giao dịch không thành công, Ngân hàng phát hành thẻ từ chối cấp phép cho giao dịch. Vui lòng liên hệ ngân hàng theo số điện thoại sau mặt thẻ để biết chính xác nguyên nhân Ngân hàng từ chối." });
            data.Add(new ErrorOnpay { ID = "3", Message = "Giao dịch không thành công,  Cổng thanh toán không nhận được kết quả trả về từ ngân hàng phát hành thẻ. Vui lòng liên hệ với ngân hàng theo số điện thoại sau mặt thẻ để biết chính xác trạng thái giao dịch và thực hiện thanh toán lại" });
            data.Add(new ErrorOnpay { ID = "4", Message = "Giao dịch không thành công do thẻ hết hạn sử dụng hoặc nhập sai thông tin tháng/ năm hết hạn của thẻ. Vui lòng kiểm tra lại thông tin và thanh toán lại " });
            data.Add(new ErrorOnpay { ID = "5", Message = "Giao dịch không thành công, Thẻ không đủ hạn mức hoặc tài khoản không đủ số dư để thanh toán. Vui lòng kiểm tra lại thông tin và thanh toán lại " });
            data.Add(new ErrorOnpay { ID = "6", Message = "Giao dịch không thành công, Quá trình xử lý giao dịch phát sinh lỗi từ ngân hàng phát hành thẻ. Vui lòng liên hệ ngân hàng theo số điện thoại sau mặt thẻ được hỗ trợ chi tiết." });
            data.Add(new ErrorOnpay { ID = "7", Message = "Giao dịch không thành công, Đã có lỗi phát sinh trong quá trình xử lý giao dịch. Vui lòng thực hiện thanh toán lại." });
            data.Add(new ErrorOnpay { ID = "8", Message = "Giao dịch không thành công. Số thẻ không đúng. Vui lòng kiểm tra và thực hiện thanh toán lại" });
            data.Add(new ErrorOnpay { ID = "9", Message = "Giao dịch không thành công. Tên chủ thẻ không đúng. Vui lòng kiểm tra và thực hiện thanh toán lại " });
            data.Add(new ErrorOnpay { ID = "10", Message = "Giao dịch không thành công. Thẻ hết hạn/Thẻ bị khóa. Vui lòng kiểm tra và thực hiện thanh toán lại" });
            data.Add(new ErrorOnpay { ID = "11", Message = "Giao dịch không thành công. Thẻ chưa đăng ký sử dụng dịch vụ thanh toán trên Internet. Vui lòng liên hê ngân hàng theo số điện thoại sau mặt thẻ để được hỗ trợ." });
            data.Add(new ErrorOnpay { ID = "12", Message = "Giao dịch không thành công. Ngày phát hành/Hết hạn không đúng. Vui lòng kiểm tra và thực hiện thanh toán lại" });
            data.Add(new ErrorOnpay { ID = "13", Message = "Giao dịch không thành công. thẻ/ tài khoản đã vượt quá hạn mức thanh toán. Vui lòng kiểm tra và thực hiện thanh toán lại " });

            data.Add(new ErrorOnpay { ID = "21", Message = "Giao dịch không thành công. Số tiền không đủ để thanh toán. Vui lòng kiểm tra và thực hiện thanh toán lại " });
            data.Add(new ErrorOnpay { ID = "22", Message = "Giao dịch không thành công. Thông tin tài khoản không đúng. Vui lòng kiểm tra và thực hiện thanh toán lại " });
            data.Add(new ErrorOnpay { ID = "23", Message = "Giao dịch không thành công. Tài khoản bị khóa. Vui lòng liên hê ngân hàng theo số điện thoại sau mặt thẻ để được hỗ trợ " });
            data.Add(new ErrorOnpay { ID = "24", Message = "Giao dịch không thành công. Thông tin thẻ không đúng. Vui lòng kiểm tra và thực hiện thanh toán lại" });
            data.Add(new ErrorOnpay { ID = "25", Message = "Giao dịch không thành công. OTP không đúng. Vui lòng kiểm tra và thực hiện thanh toán lại" });
            data.Add(new ErrorOnpay { ID = "235", Message = "Giao dịch không thành công. Quá thời gian thanh toán. Vui lòng thực hiện thanh toán lại" });
            data.Add(new ErrorOnpay { ID = "99", Message = "Giao dịch không thành công.Người sử dụng hủy giao dịch " });

            data.Add(new ErrorOnpay { ID = "B", Message = "Giao dịch không thành công do không xác thực được 3D-Secure. Vui lòng liên hệ ngân hàng theo số điện thoại sau mặt thẻ được hỗ trợ chi tiết. " });
            data.Add(new ErrorOnpay { ID = "E", Message = "Giao dịch không thành công do nhập sai CSC (Card Security Card) hoặc ngân hàng từ chối cấp phép cho giao dịch. Vui lòng liên hệ ngân hàng theo số điện thoại sau mặt thẻ được hỗ trợ chi tiết." });
            data.Add(new ErrorOnpay { ID = "F", Message = "Giao dịch không thành công do không xác thực được 3D-Secure. Vui lòng liên hệ ngân hàng theo số điện thoại sau mặt thẻ được hỗ trợ chi tiết." });
            data.Add(new ErrorOnpay { ID = "Z", Message = "Giao dịch không thành công do vi phạm quy định của hệ thống. Vui lòng liên hệ với OnePAY để được hỗ trợ (Hotline: 1900 633 927)" });
            data.Add(new ErrorOnpay { ID = "Other", Message = "Giao dịch không thành công. Vui lòng liên hệ với OnePAY để được hỗ trợ (Hotline: 1900 633 927) " });

            return data;
        }

    }
}