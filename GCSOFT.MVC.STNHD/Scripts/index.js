$(document).ready(() => {
    $(".icon-menu-responsive").click(() => {
        toggle_menu();
        toggle_bg();
    });

    $(".icon-close-responsive").click(() => {
        toggle_menu();
        toggle_bg();
    });

    $(".bg-opacity").click(() => {
        toggle_menu();
        toggle_bg();
    });

    $(".support-course .radio-label").click(function () {
        $(".support-course .radio-label .fa").css("display", "none");
        $(this).children(".fa").css("display", "block");
    });

    $(".agree .radio-label").click(() => {
        $(".agree .radio-label").children(".fa").fadeToggle('fast');
    });
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu1", //menu DIV id
        orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'ddsmoothmenu', //class added to menu's outer DIV
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    });
});

function toggle_menu() {
    $(".menu-responsive").toggleClass('dp-transform');
}

function toggle_bg() {
    $(".bg-opacity").toggleClass('bg-opacity-block');
}
String.format = function () {
    var theString = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
}