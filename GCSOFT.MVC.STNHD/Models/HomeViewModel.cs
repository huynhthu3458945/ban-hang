﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class HomeViewModel
    {
        public IEnumerable<ClassList> ClassList { get; set; }
        public IEnumerable<StudentClassViewModel> StudentClasses { get; set; }
        public IEnumerable<CourseList> Courses { get; set; }
        public IEnumerable<SupporterViewModel> Supporters { get; set; }
        public IEnumerable<VM_FileResource> FileResources { get; set; }
        public CourseList Course { get; set; }
        public LoginResult UserInfo { get; set; }
        public HomeViewModel()
        {
            Course = new CourseList();
            UserInfo = new LoginResult();
        }
    }
}