﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class RetailSalesOrderModel
    {

        public string Code { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string Address { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public double TotalAmount { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public DateTime CreateTime { get; set; }
        public string TransactionInfo { get; set; }
        public string ActionCode { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
    }
}