﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class OnePayment
    {
        /// <summary>
        /// Cổng thanh toán mặc định  = 2
        /// </summary>
        public int vpc_Version { get; set; } 
        /// <summary>
        /// Loại tiền : mặc định = VND
        /// </summary>
        public string vpc_Currency { get; set; }
        /// <summary>
        /// Chức năng mặc định = pay
        /// </summary>
        public string vpc_Command { get; set; }
        public string vpc_AccessCode { get; set; } // nhà cung cấp gửi 6BEB2566
        public string vpc_Merchant { get; set; }// nhà cung cấp gửi TESTONEPAY27
        /// <summary>
        /// Ngôn ngữ hiển thị khi thanh toán. Tiếng Việt: vn, tiếng Anh: en 
        /// </summary>
        public string vpc_Locale { get; set; }
        public string vpc_ReturnURL { get; set; } // url trả kết quả
        /// <summary>
        /// Mã giao dịch, biến số này yêu cầu là duy nhất mỗi lần gửi sang OnePAY 
        /// </summary>
        public string vpc_MerchTxnRef { get; set; }
        /// <summary>
        /// Thông tin đơn hàng, thường là mã đơn hàng hoặc mô tả ngắn gọn về đơn hàng 
        /// </summary>
        public string vpc_OrderInfo { get; set; }
        /// <summary>
        /// Khoản tiền thanh toán, không có dấu ngăn cách thập phân. thêm “00” trước khi chuyển sang cổng thanh toán. Nếu số tiền giao dịch là VND 25,000 thì số tiền gởi qua là: 2500000 
        /// </summary>
        public string vpc_Amount { get; set; }
        /// <summary>
        /// Địa chỉ IP khách hàng thực hiện thanh toán – Không được đặt cố định 1 IP 
        /// </summary>
        public string vpc_TicketNo { get; set; }
        public string vpc_CardList { get; set; }
        /// <summary>
        /// Link trước khi chuyển thanh toán
        /// </summary>
        public string AgainLink { get; set; }
        /// <summary>
        /// Tiêu đề cổng thanh toán hiển thị trên trình duyệt của chủ thẻ. 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Số điện thoại khách hàng 
        /// </summary>
        public string vpc_Customer_Phone { get; set; }
        /// <summary>
        /// Email của khách hàng 
        /// </summary>
        public string vpc_Customer_Email { get; set; }
        /// <summary>
        /// hông tin User ID 
        /// </summary>
        public string vpc_Customer_Id { get; set; }
        /// <summary>
        /// Chuỗi chữ ký đảm bảo toàn vẹn dữ liệu. 
        /// </summary>
        public string vpc_SecureHash { get; set; }
    }
}