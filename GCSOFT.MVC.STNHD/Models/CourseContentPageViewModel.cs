﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class CourseContentPageViewModel
    {
        public CourseViewModel CourseInfo { get; set; }
        public CourseContentCard CourseContent { get; set; }
    }
}