﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class SMSViewModel
    {
        public long Id { get; set; }
        public SMSType Type { get; set; }
        public string Serinumber { get; set; }
        public string PhoneNo { get; set; }
        public string OTP { get; set; }
        public string Content { get; set; }
        public DateTime DateInput { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
    }
}