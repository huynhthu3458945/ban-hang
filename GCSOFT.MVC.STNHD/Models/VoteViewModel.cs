﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class VoteViewModel
    {
        public int Id { get; set; }
        public ResultType Type { get; set; }
        public int ReferId { get; set; }
        public DateTime DateInput { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int NoOfVote { get; set; }
    }
}