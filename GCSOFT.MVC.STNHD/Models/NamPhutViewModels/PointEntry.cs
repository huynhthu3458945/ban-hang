﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.NamPhutViewModels
{
    public class PointEntry
    {
        public int Id { get; set; }
        public string userName { get; set; }
        public string SBD { get; set; }
        public string Deskcripton { get; set; }
        public int Point { get; set; }
        public string FromSource { get; set; }
        public string FromSourceNo { get; set; }
    }
}