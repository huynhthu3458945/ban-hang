﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Models.NamPhutViewModels
{
    public class ForgetUserAndPass
    {
        public string phonenumber { get; set; }
        public string OTP { get; set; }
    }
    public class ThongTinThiSinh
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string ChildName { get; set; }
    }
    public class Contestants
    {
        public int Id { get; set; }
        public bool IsAccountSTNHD { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public string IdentificationNumber { get; set; }
        public string ParentName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ChildName { get; set; }
        public string SchoolName { get; set; }
        public int GradeId { get; set; }
        public string GradeName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public string OTP { get; set; }
        public DateTime DateCreate { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
    }
}