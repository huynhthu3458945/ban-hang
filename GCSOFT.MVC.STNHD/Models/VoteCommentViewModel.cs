﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class VoteCommentViewModel
    {
        public int NoOfVote { get; set; }
        public int NoOfComment { get; set; }
        public bool HasVote { get; set; }
        public string IconVote { get; set; }
        public string IconComment { get; set; }
        public int Id { get; set; }
        public VoteCommentViewModel()
        {
            IconVote = "fa fa-heart-o";
            IconComment = "zmdi-comment-outline";
        }
    }
}