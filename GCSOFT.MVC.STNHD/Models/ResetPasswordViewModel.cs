﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class ResetPasswordViewModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public ResetType Type { get; set; }
        public string Description { get; set; }
        public string VerifyInfo { get; set; }
    }
}