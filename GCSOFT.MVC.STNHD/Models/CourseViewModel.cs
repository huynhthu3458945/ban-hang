﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class CourseViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
    }
    public class CourseContentList
    {
        public int IdCourse { get; set; }
        public int LineNo { get; set; }
        public string Times { get; set; }
        public string Title { get; set; }
        public bool Bold { get; set; }
        public bool Italic { get; set; }
        public bool IsHidden { get; set; }
        
        public string FullDescription { get; set; }
        public int SortOrder { get; set; }
        public int? ParentSortOrder { get; set; }
        public string LinkYoutube { get; set; }
        public string CurrentUrl { get { return string.Format("https://cdn.stnhd.com/streamer/embed.php?v={0}", LinkYoutube); } }
        public string LinkVideoVN { get; set; }
        public string LinkVideoEN { get; set; }
        public bool IsHiddenHK2 { get; set; }
    }
    public class CourseContentCard
    {
        public int IdCourse { get; set; }
        public int LineNo { get; set; }
        public string Title { get; set; }
        public string FullDescription { get; set; }
    }
}