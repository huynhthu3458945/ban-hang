﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class LoginEntryViewModel
    {
        public int Id { get; set; }
        public string userName { get; set; }
        public string SessionId { get; set; }
        public bool LoggedIn { get; set; }
        public DateTime DateLogin { get; set; }
        public string IP { get; set; }
        public string IpType { get; set; }
        public string continent_code { get; set; }
        public string continent_name { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string region_name { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
}