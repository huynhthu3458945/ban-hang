﻿using GCSOFT.MVC.STNHD.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.PageViewModels
{
    public class TeacherViewModel
    {
        public LoginResult UserInfo { get; set; }
        public Paging paging { get; set; }
        public string ActiveClass { get; set; }
        public IEnumerable<Teacher> Teachers { get; set; }
        public IEnumerable<OptionLineViewModel> Levels { get; set; }
    }
    public class Teacher
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public string Image { get; set; }
        public string Position { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string SchoolName { get; set; }
    }
}