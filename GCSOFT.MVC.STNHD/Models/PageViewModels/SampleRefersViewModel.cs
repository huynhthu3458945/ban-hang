﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.PageViewModels
{
    public class SampleRefersViewModel
    {
        public CourseViewModel CourseInfo { get; set; }
        public CourseContentList CourseContent { get; set; }
        public QuestionSuggestion QuestionSuggestion { get; set; }
        public IEnumerable<QuestionViewModel> QuestionsInfos { get; set; }
        //public IEnumerable<QuestionViewModel> QuestionsOthers { get; set; }
        public IEnumerable<VM_FileResource> files { get; set; }
        //public IEnumerable<VM_FileResource> fileOthers { get; set; }
        public LoginResult UserInfo { get; set; }
        public IEnumerable<CourseContentList> CourseContents { get; set; }
        public IEnumerable<CommentPage> CommentPages { get; set; }
    }
}