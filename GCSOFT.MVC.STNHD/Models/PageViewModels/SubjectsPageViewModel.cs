﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.PageViewModels
{
    public class SubjectsPageViewModel
    {
        public ClassList ClassInfo { get; set; }
        public IEnumerable<CourseList> Courses { get; set; }
        public IEnumerable<CourseContentList> CourseContents { get; set; }
    }
}