﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.PageViewModels
{
    public class MemoVideosPage
    {
        public LoginResult UserInfo { get; set; }
        public IEnumerable<MemoVideos> VideosList { get; set; }
        public MemoVideos Video { get; set; }
        public string UrlToPlay { get; set; }
        public int idCurrentLesson { get; set; }
        public int idFirstVideos { get; set; }
        public int idLastVideos { get; set; }
    }
}