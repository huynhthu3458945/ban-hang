﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class LanguageViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string RegionCode { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public int? IdStatus { get; set; }
        public string StatusName { get; set; }
        public string ImagePathForWeb { get; set; }
    }
}