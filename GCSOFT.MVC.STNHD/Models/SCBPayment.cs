﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class SCBPayment
    {
        public string AccessKey { get; set; }
        public string Address { get; set; }
        public string CancelUrl { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public string District { get; set; }
        public decimal DomesticFee { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public decimal InternationalFee { get; set; }
        public bool IsTokenRequest { get; set; }
        public string Language { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string PostalCode { get; set; }
        public string ProfileID { get; set; }
        public string ReturnUrl { get; set; }
        public string SSN { get; set; }
        public decimal TotalAmount { get; set; }
        public string Signature { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public bool SubscribeOnly { get; set; }
        public bool SubscribeWithMin { get; set; }
        public string CardNumber { get; set; }
    }
    public class DataPayload
    {
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public decimal TotalAmount { get; set; }
        public string CardNumber { get; set; }
        public decimal ExpiryDate { get; set; }
        public string SubscriptionType { get; set; }
        public string CardType { get; set; }
        public string AccountNo { get; set; }
        public string SubscriptionSource { get; set; }
        public string Token { get; set; }
        public string ResponseCode { get; set; } 
    }
}