﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class ShareInfo
    {
        public IEnumerable<ClassList> classList { get; set; }
        public IEnumerable<StudentClassViewModel> StudentClasses { get; set; }
        public IEnumerable<CourseList> courseList { get; set; }
    }
    public class ClassList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameEN { get; set; }

    }
    public class CourseList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string TitleEN { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int AvartarLineNo { get; set; }
        public string Avatar { get; set; }
        public int IdVideos { get; set; }
    }
}