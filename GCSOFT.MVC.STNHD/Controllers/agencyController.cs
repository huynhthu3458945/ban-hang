﻿using AutoMapper;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class agencyController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IAgencyService _agencyService;
        public agencyController
            (
                IShareInfoService shareInfoService
                , IAgencyService agencyService
            ) : base(shareInfoService)
        {
            _agencyService = agencyService;
            _shareInfoService = shareInfoService;
        }
        // GET: agency
        public ActionResult Index()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            var agencyInfo = new AgencyViewModel();
            agencyInfo.UserInfo = GetUserInfo() ?? new LoginResult();
            agencyInfo.AgencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllHasImage()).OrderByDescending(d => d.ShowWebsite);
            return View(agencyInfo);
        }
    }
}