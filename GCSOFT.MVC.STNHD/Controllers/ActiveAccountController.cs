﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class ActiveAccountController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IVuViecService _vuViecRepo;
        private readonly IUserInfoService _userInfoService;
        private readonly ISetupService _setupService;
        private readonly ICardLineService _cardLineService;
        private readonly IPaymentResultService _paymentResultService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string hasValue;
        public ActiveAccountController
            (
                IShareInfoService shareInfoService
                , IVuViecService vuViecRepo
                , IUserInfoService userInfoService
                , ISetupService setupService
                , ICardLineService cardLineService
                , IPaymentResultService paymentResultService
                , IOptionLineService optionLineService
                , ICardEntryService cardEntryService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _vuViecRepo = vuViecRepo;
            _userInfoService = userInfoService;
            _setupService = setupService;
            _cardLineService = cardLineService;
            _paymentResultService = paymentResultService;
            _optionLineService = optionLineService;
            _cardEntryService = cardEntryService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
        public ActionResult Index()
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            ViewBag.ActivePayment = new ActiveAccountPage()
            {
                UserInfo = GetUserInfo()
            };
            
            var orderId = Guid.NewGuid().ToString().Split('-')[0];
            var sacomPaymnet = new SCBPayment()
            {
                ProfileID = "TAMTRILUCsymbl",
                AccessKey = "apwswlbzywcgjpfcunfgnoys",
                TransactionID = orderId,
                TransactionDateTime = "2021-03-31T07:42:14Z",
                Language = "vi",
                IsTokenRequest = false,
                SubscribeOnly = false,
                SubscribeWithMin = false,
                Description = "phutt25424",
                TotalAmount = 1000,
                InternationalFee = 0,
                DomesticFee = 0,
                Currency = "VND",
                SSN = "025240307",
                FirstName = "TRAN",
                LastName = "THANH PHU",
                Gender = "M",
                Address = "266 nam ky khoi nghia",
                District = "Quan 3",
                City = "Ho Chi Minh",
                PostalCode = "700000",
                Country = "VN",
                Mobile = "0905505048",
                Email = "khanhln26169@gmail.com",
                CancelUrl = "https://cardtest.sacombank.com.vn:9448/samplemerchant",
                ReturnUrl = "https://localhost:44357/ActiveAccount/CheckoutResult"
            };
            return View(sacomPaymnet);
        }
        public JsonResult Signature(SCBPayment payment)
        {
            var jsonString = JsonConvert.SerializeObject(payment);
            string privateKey = System.IO.File.ReadAllText(Server.MapPath(@"~/Helper/privateKey.txt"));
            string publicKey = System.IO.File.ReadAllText(Server.MapPath(@"~/Helper/publicKey.txt"));
            var paymentHelper = new PaymentHelper();
            var dataToSign = PaymentHelper.CreateDataToSign(payment);
            var signature = paymentHelper.GenerateSignature(privateKey, dataToSign);
            var verifySignature = PaymentHelper.VerifySignature(dataToSign, signature, publicKey);
            return Json(signature, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PaymentSucess(string id)
        {
            var AccessKey = "apwswlbzywcgjpfcunfgnoys";
            var profileId = "TAMTRILUCsymbl";
            var dataPayload = new DataPayload()
            {
                TransactionID = id,
                TransactionDateTime = "2021-03-31T07:42:14Z",
                TotalAmount = 100000
            };
            var data = JsonConvert.SerializeObject(dataPayload);
            //var desDecrypt = PaymentHelper.TrippleDESDecrypt(data, "ypbzcrxrkbkjhhneqcemzhju");
            string privateKey = System.IO.File.ReadAllText(Server.MapPath(@"~/Helper/privateKey.txt"));
            var dataToSign = AccessKey + data + profileId + id;//"AccessKey+Data+ProfileID+RequestID";
            PaymentHelper paymentHelper = new PaymentHelper();
            var signature = paymentHelper.GenerateSignature(privateKey, dataToSign);
            return View();
        }
        public ActionResult CheckoutResult(string ResponseCode, string Description, string TransactionID, string Token, string CardNumber
                                    , string ExpiryDate, string AccountNo, string SubscriptionSource, string SubscriptionType, string CardType, string Signature, decimal? TotalAmount)
        {
            var dto = new { ResponseCode, TransactionID, Token, CardNumber, ExpiryDate, AccountNo, SubscriptionSource, SubscriptionType, CardType };
            ViewBag.result = JsonConvert.SerializeObject(dto);
            string publicKey = System.IO.File.ReadAllText(Server.MapPath(@"~/Helper/publicKey.txt"));
            var live_verified = PaymentHelper.VerifySignature(PaymentHelper.CreateDataToSign(dto), Signature, publicKey);
            //ViewBag.Result = ResponseCode == "00" ? "Success" : "Unsuccess";
            //ViewBag.Message = "Checkout Result";
            //ViewBag.TransactionID = TransactionID;
            //ViewBag.CardNumber = CardNumber;
            //ViewBag.ResponseCode = ResponseCode;
            //ViewBag.Token = Token;
            //ViewBag.ExpiryDate = ExpiryDate;
            //ViewBag.SubscriptionSource = SubscriptionSource;
            //ViewBag.SubscriptionType = SubscriptionType;
            //ViewBag.AccountNo = AccountNo;
            //ViewBag.CardType = CardType;
            //if (Description != null)
            //{
            //    ViewBag.Description = Description;
            //}
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetById(int.Parse(TransactionID.Split('-')[0])));
            ViewBag.UserInfo = userInfo;
            var returnView = ResponseCode == "00" ? "CheckoutResult" : "CheckoutError";
            //-- Insert to DB --
            var cardInfo = Mapper.Map<OptionLineViewModel>(_optionLineService.Get("CT", CardType)) ?? new OptionLineViewModel();
            var paymentResult = new PaymentResultViewModel()
            {
                PaymentType = PaymentType.Order,
                TransactionID = TransactionID,
                TransactionDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                TotalAmount = 0,
                CardNumber = CardNumber,
                ExpiryDate = ExpiryDate,
                SubscriptionType = SubscriptionType,
                CardType = CardType,
                CardName = cardInfo.Name,
                AccountNo = AccountNo,
                SubscriptionSource = SubscriptionSource,
                Token = Token,
                ResponseCode = ResponseCode,
                DatePayment = DateTime.Now,
                UserId = userInfo.Id,
                Username = userInfo.userName
            };
            //-- Insert to DB --
            hasValue = _paymentResultService.Insert(Mapper.Map<M_PaymentResult>(paymentResult));
            hasValue = _vuViecRepo.Commit();
            if (ResponseCode.Equals("00"))
                AssignCode(1468000, userInfo);
            
            return View(returnView);
        }
        private string AssignCode(decimal price, UserInfoViewModel _userInfo)
        {
            var idCard = _cardLineService.GetID();
            //-- Build Ma Kich Hoat
            var builder = new StringBuilder();
            Guid g = Guid.NewGuid();
            builder.Append(g.ToString().Split('-')[0].ToString().ToUpper());
            var randomNumber = new RandomNumberGenerator();
            builder.Append(randomNumber.RandomNumber(10, 99));
            var _cardNo = builder.ToString();
            //-- Build Ma Kich Hoat +
            var cryptorEngine = new CryptorEngine2();
            var statusInfo = Mapper.Map<OptionLineViewModel>(_optionLineService.Get("CARD", "450"));
            var cardLine = new CardLine()
            {
                Id = idCard,
                LotNumber = "LO2103-013",
                SerialNumber = StringUtil.GetProductID("C", (idCard - 1).ToString(), 9),
                CardNo = cryptorEngine.Encrypt(_cardNo, true, "TTLSTNHD@CARD"),
                Price = price,
                UserName = _userInfo.userName,
                UserFullName = _userInfo.fullName,
                DateCreate = DateTime.Now,

                UserType = UserType.TTL,
                UserNameActive = _userInfo.userName,
                UserActiveFullName = _userInfo.fullName,
                LastDateUpdate = DateTime.Now,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                LastStatusId = statusInfo.LineNo,
                LastStatusCode = statusInfo.Code,
                LastStatusName = statusInfo.Name,

                DurationId = 44,
                DurationCode = "ALL",
                DurationName = "Vĩnh Viễn",

                ReasonId = 45,
                ReasonCode = "BH",
                ReasonName = "Bán Hàng"
            };
            var cardEntry = new CardEntry()
            {
                LotNumber = cardLine.LotNumber,
                CardLineId = idCard,
                SerialNumber = cardLine.SerialNumber,
                CardNo = cardLine.CardNo,
                Price = cardLine.Price,
                UserName = cardLine.UserName,
                UserFullName = cardLine.UserFullName,
                DateCreate = cardLine.DateCreate,
                UserType = UserType.TTL,
                UserNameActive = cardLine.UserNameActive,
                UserActiveFullName = cardLine.UserActiveFullName,
                Date = cardLine.DateCreate,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                Source = CardSource.CardSTNHD,
                SourceNo = cardLine.LotNumber,
                DurationId = cardLine.DurationId,
                DurationCode = cardLine.DurationCode,
                DurationName = cardLine.DurationName,
                ReasonId = cardLine.ReasonId,
                ReasonCode = cardLine.ReasonCode,
                ReasonName = cardLine.ReasonName
            };
            hasValue = _cardLineService.Insert(Mapper.Map<M_CardLine>(cardLine));
            hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
            hasValue = SendCardViaSMS(_userInfo, _cardNo);
            hasValue = _vuViecRepo.Commit();
            return null;
        }
        private string SendCardViaSMS(UserInfoViewModel _userInfo, string cardNo)
        {
            try
            {
                if (StringUtil.Validate(_userInfo.Email))
                {
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", _userInfo.fullName);
                    mailContent = mailContent.Replace("{{ACTIVECODE}}", cardNo);
                    var emailTos = new List<string>();
                    emailTos.Add(_userInfo.Email);
                    MailHelper _mailHelper = new MailHelper()
                    {
                        EMailFrom = emailInfo.Email,
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        Host = emailInfo.Host,
                        Port = emailInfo.Port,
                        Password = emailInfo.Password,
                        EnableSsl = emailInfo.EnableSsl,
                        UseDefaultCredentials = emailInfo.UseDefaultCredentials,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.DoSendMail();
                }
                if (!string.IsNullOrEmpty(_userInfo.Phone))
                {
                    var newPhoneNo = _userInfo.Phone.Trim();
                    var firstNumber = int.Parse(newPhoneNo[0].ToString());
                    if (firstNumber != 0)
                    {
                        newPhoneNo = "0" + newPhoneNo;
                    }
                    var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                    var smsHelper = new SMSHelper()
                    {
                        PhoneNo = newPhoneNo,
                        Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", cardNo).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                    };
                    hasValue = smsHelper.Send();
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}