﻿ using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class AccountEbookController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IUserInfoService _userInfoService;
        private readonly IVuViecService _vuViecRepo;
        private readonly ISetupService _setupService;
        private readonly IClassService _classService;
        private readonly IStudentClassService _studentClassService;
        private readonly ITemplateService _templateService;
        private readonly ISMSService _smsService;
        private readonly IResetPasswordService _resetPassService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ILoginEntryService _loginEntryService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly ICardLineService _cardLineService;
        private readonly IEbookAccountService _ebooAccountService;
        private string hasValue;
        private string API_URL = ConfigurationManager.AppSettings["apiBaseAddress"];
        private StringBuilder contentInfos;
        private M_LoginEntry loginEntry;
        public AccountEbookController
            (
                IShareInfoService shareInfoService
                , IUserInfoService userInfoService
                , IVuViecService vuViecRepo
                , ISetupService setupService
                , IClassService classService
                , IStudentClassService studentClassService
                , ITemplateService templateService
                , ISMSService smsService
                , IResetPasswordService resetPassService
                , IMailSetupService mailSetupService
                , ILoginEntryService loginEntryService
                , IProvinceService provinceService
                , IDistrictService districtService
                , ICardLineService cardLineService
                , IEbookAccountService ebooAccountService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _userInfoService = userInfoService;
            _vuViecRepo = vuViecRepo;
            _setupService = setupService;
            _classService = classService;
            _studentClassService = studentClassService;
            _templateService = templateService;
            _smsService = smsService;
            _resetPassService = resetPassService;
            _mailSetupService = mailSetupService;
            _loginEntryService = loginEntryService;
            _provinceService = provinceService;
            _districtService = districtService;
            _cardLineService = cardLineService;
            _ebooAccountService = ebooAccountService;
        }
        public ActionResult Index()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            var userInfo = new UserInfoViewModel()
            {
                ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(0),
                CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0),
                DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0)
            };
            ViewBag.returnUrl = Request.Params["ReturnUrl"];
            return View(userInfo);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginResult u)
        {
            try
            {
                var userInfo = new LoginResult();
                if (u.userName.Contains("@"))
                {
                    userInfo = Mapper.Map<LoginResult>(_userInfoService.CheckByEmail(u.userName, u.password));
                }
                else
                {
                    userInfo = Mapper.Map<LoginResult>(_userInfoService.CheckByUsername(u.userName, u.password));
                }
                if (string.IsNullOrEmpty(userInfo.userName))
                    return Json(new { s = "error", url = "Thông tin đăng nhập chưa đúng<BR /> Vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                FormsAuthentication.SetAuthCookie(userInfo.userName, true);
                userInfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                CookieHelper.StoreInCookie("UserInfo", "", "LoginResult", Server.UrlEncode(JsonConvert.SerializeObject(userInfo)), null);
                //-- Update Userinfo
                SetDataLoginEntry(userInfo);
                hasValue = _loginEntryService.Insert(loginEntry);
                hasValue = _vuViecRepo.Commit();
                //-- Update Userinfo +
                u.returnUrl = Url.Action("Book", "STNHD");
                //-- Post Userinfo to PVD
                if (!userInfo.HasToken)
                    UploadUsertoCloud(u.userName);
                //-- Post Userinfo to PVD +
                return Json(new { s = "Oke", url = u.returnUrl }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "error", url = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        protected string UploadUsertoCloud(string userName)
        {
            try
            {
                var _userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(userName));
                if (!_userInfo.HasToken)
                {
                    var client = new RestClient("https://cdn.stnhd.com/video/push-userinfo.php");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("id", _userInfo.Id);
                    request.AddParameter("hovaten", _userInfo.fullName);
                    request.AddParameter("username", _userInfo.userName);
                    request.AddParameter("ho", "");
                    request.AddParameter("ten", "");
                    request.AddParameter("email", _userInfo.Email);
                    request.AddParameter("sdt", _userInfo.Phone);
                    request.AddParameter("diachi", _userInfo.Address);
                    IRestResponse response = client.Execute(request);
                    _userInfo.HasToken = true;
                    hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(_userInfo));
                    hasValue = _vuViecRepo.Commit();
                }
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        // GET: Student
        public ActionResult registration()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        [HttpPost]
        public ActionResult Create(UserInfoViewModel _userInfo)
        {
            var validCode = string.Empty;
            var hasUserName = !string.IsNullOrEmpty(_userInfoService.GetBy(_userInfo.userName).userName);
            if (hasUserName)
                validCode = "Tên Đăng Nhập đã tồn tại";
            if (!string.IsNullOrEmpty(validCode))
                return Json(new { s = "error", result = validCode }, JsonRequestBehavior.AllowGet);

            var otpStatus = _smsService.CheckOTP(_userInfo.Phone, _userInfo.OTP);
            var msg = "";
            if (otpStatus.Equals("Nothing"))
                msg = "Mã OTP không tồn tại";
            if (otpStatus.Equals("expired"))
                msg = "Mã OTP đã hết hạn";
            if (!string.IsNullOrEmpty(msg))
                return Json(new { s = "error", result = msg }, JsonRequestBehavior.AllowGet);

            var userInfo = Mapper.Map<M_UserInfo>(_userInfo);
            userInfo.token = CryptorEngine.Encrypt(_userInfo.Email + "_0", true, "stnhdttl");
            userInfo.Phone2 = userInfo.Phone;
            userInfo.HasClass = true;
            userInfo.ActivePhone = false;
            userInfo.HasEbook = true;
            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(userInfo.CityId ?? 0)) ?? new ProvinceViewModel();
            userInfo.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(userInfo.DistrictId ?? 0)) ?? new DistrictViewModel();
            userInfo.DistrictName = districtInfo.name;

            hasValue = _userInfoService.Insert(userInfo);
            hasValue = _vuViecRepo.Commit();

            if (string.IsNullOrEmpty(hasValue))
            {
                var loginResult = Mapper.Map<LoginResult>(_userInfoService.CheckByUsername(_userInfo.userName, _userInfo.Password));
                FormsAuthentication.SetAuthCookie(loginResult.userName, true);
                loginResult.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                CookieHelper.StoreInCookie("UserInfo", "", "LoginResult", Server.UrlEncode(JsonConvert.SerializeObject(loginResult)), null);
                SetDataLoginEntry(loginResult);
            }
         
            
            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? Url.Action("book", "STNHD") : hasValue }, JsonRequestBehavior.AllowGet);
        }
        private void SetDataLoginEntry(LoginResult loginResult)
        {
            var loginEntryVM = new LoginEntryViewModel();
            var ipClient = GetIPAddress();
            //-- Get ip info
            if (!string.IsNullOrEmpty(ipClient))
            {
                var client = new RestClient(string.Format("http://api.ipstack.com/{0}?access_key=c2547274e399416b3c9e3f1afdf37fe9", ipClient));
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                loginEntryVM = JsonConvert.DeserializeObject<LoginEntryViewModel>(response.Content);
            }

            //-- Get ip info +
            loginEntryVM.userName = loginResult.userName;
            loginEntryVM.SessionId = loginResult.SessionId;
            loginEntryVM.LoggedIn = true;
            loginEntryVM.DateLogin = DateTime.Now;
            loginEntry = Mapper.Map<M_LoginEntry>(loginEntryVM);
            hasValue = _loginEntryService.Insert(loginEntry);
            hasValue = _vuViecRepo.Commit();
        }
        /// <summary>
        /// Gửi OTP Cho Đăng Ký Thành Viên
        /// </summary>
        /// <param name="p">Phone No.</param>
        /// <param name="s">Seri Number</param>
        /// <returns></returns>
        public JsonResult SendOTPRegister(string p)
        {
            
            if (!_smsService.AllowSendSMS(p, 3))
                return Json(string.Format("Số điện thoại {0} đã được gửi {1} lần ngày hôm nay, vui lòng quay lại ngày hôm sau để tiếp tục", p, 3), JsonRequestBehavior.AllowGet);
            RandomNumberGenerator randomNumber = new RandomNumberGenerator();
            var otp = randomNumber.RandomNumber(1000, 9999);
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_OTP"));
            var _content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content.Replace("{{NAME}}", "5 Phut Thuoc Bai"));
            _content = _content.Replace("{{OTP}}", otp.ToString());
            _content = _content.Replace("{{HIEULUC}}", "hieu luc 5 phut");
            _content = _content.Replace("{{NOIDUNG}}", "xac thuc SDT dang ky");
            var smsInfo = new SMSViewModel()
            {
                Type = SMSType.OTP,
                PhoneNo = p,
                OTP = otp.ToString(),
                Content = _content,
                DateInput = DateTime.Now.AddMinutes(5)
            };
            var smsHelper = new SMSHelper()
            {
                PhoneNo = smsInfo.PhoneNo,
                Content = smsInfo.Content
            };
            if (smsHelper.IsMobiphone())
            {
                smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_OTP"));
                smsHelper.Content = string.Format(smsTemplate.SMSContent, "dang ky", otp.ToString(), "hieu luc 5 phut", "xac thuc SDT dang ky");
                hasValue = smsHelper.SendMobiphone();
            }
            else
                hasValue = smsHelper.Send();
            hasValue = _smsService.Insert(Mapper.Map<M_SMS>(smsInfo));
            hasValue = _vuViecRepo.Commit();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult ResetPassword()
        {
            return View(new UserInfoViewModel());
        }
        public ActionResult ForgetUsername()
        {
            return View(new ResetPasswordViewModel());
        }
        [HttpPost]
        public ActionResult ResetPassword(UserInfoViewModel _userInfo)
        {
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.CheckByUsernameAndEmail(_userInfo.userName, _userInfo.Email));
            if (string.IsNullOrEmpty(userInfo.userName))
                return Json(new { s = "error", result = "Tên Đăng Nhập và Email không trùng nhau" }, JsonRequestBehavior.AllowGet);
            var resetPassword = new ResetPasswordViewModel() { 
                Id = Guid.NewGuid(),
                Email = userInfo.Email,
                DocumentDate = DateTime.Now,
                ExpiryDate = DateTime.Now.AddMinutes(30),
                PhoneNo = userInfo.Phone,
                UserName = userInfo.userName
            };
            hasValue = _resetPassService.Insert(Mapper.Map<M_ResetPassword>(resetPassword));
            hasValue = _vuViecRepo.Commit();
            //-- Send Mail
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("E_FORGOTPASS"));
            var mailTitle = mailTemplate.Title;
            var mailContent = mailTemplate.Content.Replace("{{id}}", resetPassword.Id.ToString());
            var emailTos = new List<string>();
            emailTos.Add(resetPassword.Email);
            MailHelper _mailHelper = new MailHelper()
            {
                EMailFrom = emailInfo.Email,
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTitle,
                Body = mailContent,
                Host = emailInfo.Host,
                Port = emailInfo.Port,
                Password = emailInfo.Password,
                EnableSsl = emailInfo.EnableSsl,
                UseDefaultCredentials = emailInfo.UseDefaultCredentials,
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            hasValue = _mailHelper.SendEmailsThread();
            //-- Send Mail +
            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? "" : hasValue }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult forgetpass(string id)
        {
            var resetPassInfo = Mapper.Map<ResetPasswordViewModel>(_resetPassService.GetByExpiry(Guid.Parse(id)));
            return View(resetPassInfo);
        }
        [HttpPost]
        public ActionResult forgetpass(ResetPasswordViewModel _resetPassword)
        {
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(_resetPassword.UserName));
            userInfo.Password = _resetPassword.Password;
            hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(userInfo));
            hasValue = _vuViecRepo.Commit();
            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? "" : hasValue }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Gửi OTP Cho Đăng Ký Thành Viên
        /// </summary>
        /// <param name="p">Phone No.</param>
        /// <param name="u">User name</param>
        /// <returns></returns>
        public JsonResult SendOTPForgetPass(string p, string u)
        {
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.CheckByUsernameAndPhone(u, p));
            if (string.IsNullOrEmpty(userInfo.userName))
                return Json("invaildUsername", JsonRequestBehavior.AllowGet);
            RandomNumberGenerator randomNumber = new RandomNumberGenerator();
            var otp = randomNumber.RandomNumber(1000, 9999);
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_OTP"));
            var _content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content.Replace("{{NAME}}", "5 Phut Thuoc Bai"));
            _content = _content.Replace("{{OTP}}", otp.ToString());
            _content = _content.Replace("{{HIEULUC}}", "hieu luc 5 phut");
            _content = _content.Replace("{{NOIDUNG}}", "xac thuc SDT dang ky tai khoan STNHD");
            var smsInfo = new SMSViewModel()
            {
                Type = SMSType.OTP,
                PhoneNo = p,
                OTP = otp.ToString(),
                Content = _content,
                DateInput = DateTime.Now.AddMinutes(5)
            };
            var smsHelper = new SMSHelper()
            {
                PhoneNo = smsInfo.PhoneNo,
                Content = smsInfo.Content
            };
            hasValue = smsHelper.Send();
            hasValue = _smsService.Insert(Mapper.Map<M_SMS>(smsInfo));
            hasValue = _vuViecRepo.Commit();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult forgetusername(ResetPasswordViewModel _resetPassword)
        {
            var verifyName = "số điện thoại";
            bool hasAccount = true;
            var userInfos = new List<UserInfoViewModel>();
            if (_resetPassword.VerifyInfo.Contains("@"))
            {
                hasAccount = _userInfoService.hasUsernameByEmail(_resetPassword.VerifyInfo);
                userInfos = Mapper.Map<List<UserInfoViewModel>>(_userInfoService.GetUserByEmails(_resetPassword.VerifyInfo));
                verifyName = "Email";
            }
            else
            {
                hasAccount = _userInfoService.HasUsernameByPhone(_resetPassword.VerifyInfo);
                userInfos = Mapper.Map<List<UserInfoViewModel>>(_userInfoService.GetUserByPhones(_resetPassword.VerifyInfo));
            }
            if (!hasAccount)
                return Json(new { s = "error", result = string.Format("{0} không có Tên Đăng Nhập Đã Kích Hoạt", _resetPassword.VerifyInfo) }, JsonRequestBehavior.AllowGet);
            var userNames = string.Join(",", userInfos.Select(d => d.userName).ToList());

            var resetPassword = new ResetPasswordViewModel()
            {
                Id = Guid.NewGuid(),
                VerifyInfo = _resetPassword.VerifyInfo,
                Email = _resetPassword.VerifyInfo.Contains("@") ? _resetPassword.VerifyInfo : "",
                PhoneNo = _resetPassword.VerifyInfo.Contains("@") ? "" : _resetPassword.VerifyInfo,
                DocumentDate = DateTime.Now,
                ExpiryDate = DateTime.Now,
                Description = ""
            };
            hasValue = _resetPassService.Insert(Mapper.Map<M_ResetPassword>(resetPassword));
            hasValue = _vuViecRepo.Commit();
            //-- Get template Email or SMS
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("ESU"));
            if (verifyName.Equals("Email"))
            {
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTitle = mailTemplate.Title;
                var mailContent = BuildEmailSendUsername(userInfos, mailTemplate.Content);
                var emailTos = new List<string>();
                emailTos.Add(_resetPassword.VerifyInfo);
                MailHelper _mailHelper = new MailHelper()
                {
                    EMailFrom = emailInfo.Email,
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = mailContent,
                    Host = emailInfo.Host,
                    Port = emailInfo.Port,
                    Password = emailInfo.Password,
                    EnableSsl = emailInfo.EnableSsl,
                    UseDefaultCredentials = emailInfo.UseDefaultCredentials,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.SendEmailsThread();
            } else
            {
                var content = BuildSMSSendUsername(userInfos, mailTemplate.SMSContent);
                var smsHelper = new SMSHelper()
                {
                    PhoneNo = _resetPassword.VerifyInfo,
                    Content = content
                };
                hasValue = smsHelper.Send();
            }
            //-- Get template Email or SMS +
            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = "oke", result = string.Format("Tên Đăng Nhập đã được gửi vào {0}: {1}", verifyName, _resetPassword.VerifyInfo) }, JsonRequestBehavior.AllowGet);
        }
        private string GetIPAddress()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        private string BuildEmailSendUsername(List<UserInfoViewModel> userInfos, string template)
        {
            contentInfos = new StringBuilder();
            contentInfos.Append("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" style=\"width:100%;\">");
            contentInfos.AppendLine("<thead>");
            contentInfos.AppendLine("<tr>");
            contentInfos.AppendLine("<th style=\"width: 30%;\"><b>Tên Đăng Nhập</b></th>");
            contentInfos.AppendLine("<th style=\"width: 30%;\"><b>Mật Khẩu</b></th>");
            contentInfos.AppendLine("<th style=\"width: 30%;\"><b>Họ Tên</b></th>");
            contentInfos.AppendLine("</tr>");
            contentInfos.AppendLine("</thead>");
            contentInfos.AppendLine("<tbody>");
            userInfos.ForEach(item => {
                contentInfos.AppendLine("<tr>");
                contentInfos.AppendLine(string.Format("<td>{0}</td>", item.userName));
                contentInfos.AppendLine(string.Format("<td>{0}</td>", item.Password));
                contentInfos.AppendLine(string.Format("<td>{0}</td>", item.fullName));
                contentInfos.AppendLine("</tr>");
            });
            contentInfos.AppendLine("</tbody>");
            contentInfos.AppendLine("</table>");
            template = template.Replace("{{USERINFO}}", contentInfos.ToString());
            return template;
        }
        private string BuildSMSSendUsername(List<UserInfoViewModel> userInfos, string template)
        {
            contentInfos = new StringBuilder();
            userInfos.ForEach(item => {
                contentInfos.AppendLine(string.Format("Ten dang nhap: {0} Mat Khau: {1} ", item.userName, item.Password));
            });
            template = template.Replace("{{USERINFO}}", contentInfos.ToString());
            return template;
        }
        public ActionResult ChangePass()
        {
            var resetPassInfo = new ResetPasswordViewModel();
            resetPassInfo.UserName = (GetUserInfo() ?? new LoginResult()).userName;
            return View(resetPassInfo);
        }
        [HttpPost]
        public ActionResult ChangePass(ResetPasswordViewModel _resetPassword)
        {
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.CheckByUsername(_resetPassword.UserName, _resetPassword.OldPassword));
            if  (string.IsNullOrEmpty(userInfo.userName))
            {
                return Json(new { s = "error", result = "Mật khẩu cũ không đúng, Vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);
            }
            userInfo.Password = _resetPassword.Password;
            hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(userInfo));
            hasValue = _vuViecRepo.Commit();
            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? "" : hasValue }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UserInfo()
        {
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(GetUserInfo().userName));
            return View(userInfo);
        }
        [HttpPost]
        public ActionResult UserInfo(UserInfoViewModel _userInfo)
        {
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(GetUserInfo().userName));
            userInfo.fullName = _userInfo.fullName;
            userInfo.Phone = _userInfo.Phone;
            userInfo.Email = _userInfo.Email;
            userInfo.Address = _userInfo.Address;
            hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(userInfo));
            hasValue = _vuViecRepo.Commit();
            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? "" : hasValue }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchDistricts(int provinceId)
        {
            return Json(Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(provinceId)), JsonRequestBehavior.AllowGet);
        }
    }
}