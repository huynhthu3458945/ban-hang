﻿using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class stnhdController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IVuViecService _vuViecRepo;
        private string hasValue;
        public stnhdController
            (
                IShareInfoService shareInfoService
                , IVuViecService vuViecRepo
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _vuViecRepo = vuViecRepo;
        }
        // GET: stnhd
        public ActionResult Index()
        {
            
            //ViewBag.ShareInfo = GetShareInfo();
            //var id = CryptorEngine.Encrypt("hieult_1", true, "stnhdtt");
            return View();
        }
        public ActionResult About()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult GetBrochure()
        {
            string filePath = "~/Files/Brochure/Brochure.pdf";
            Response.AddHeader("Content-Disposition", "inline; filename=SieuTriNhoHocDuong");

            return File(filePath, "application/pdf");
        }
        public ActionResult book()
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "AccountEbook", new { url = CurrentUrl() });
            if (!_userInfo.HasEbook)
                _userInfo.HasEbook = _userInfo.ActivePhone;
            if (!_userInfo.HasEbook)
                return RedirectToAction("Index", "AccountEbook", new { url = CurrentUrl() });
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult ebook()
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult Ranking()
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");

            //ViewBag.ShareInfo = GetShareInfo();
            var id = CryptorEngine.Encrypt("hieult_1", true, "stnhdtt");
            return View();
        }
    }
}