﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Data.Enum;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class ItemController : BaseController
    {
        private readonly IRetailSalesOrderService _retailSalesOrderService;
        private readonly IVuViecService _vuViecRepo;
        private readonly ICardLineService _cardLineService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private readonly IUserInfoService _userInfoService;
        private readonly ISetupService _setupService;
        private readonly IItemService _itemService;
        private string hasValue;

        public ItemController(
            IRetailSalesOrderService retailSalesOrderService
            , IShareInfoService shareInfoService
            , IVuViecService vuViecRepo
            , ICardLineService cardLineService
            , IOptionLineService optionLineService
            , ICardEntryService cardEntryService
            , IMailSetupService mailSetupService
            , ITemplateService templateService
            , ISetupService setupService
            , IUserInfoService userInfoService
            , IItemService itemService
        ) : base(shareInfoService)
        {
            _retailSalesOrderService = retailSalesOrderService;
            _vuViecRepo = vuViecRepo;
            _userInfoService = userInfoService;
            _setupService = setupService;
            _cardLineService = cardLineService;
            _optionLineService = optionLineService;
            _cardEntryService = cardEntryService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
            _itemService = itemService;
        }

        public ActionResult Index()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                _userInfo = new LoginResult();

            //khoi tao thong tin mua hàng
            PurchaseInfoModel model = new PurchaseInfoModel()
            {
                FullName = _userInfo.fullName,
                Phone = _userInfo.Phone,
                Email = _userInfo.email,
                Address = _userInfo.Address
            };
            model.Course = "LOP";
            var item = _itemService.GetBy(model.Course);
            model.TotalAmount = ((double)item.Price).ToString();// init price
            return View(model);
        }

        //kiem tra va tao du lieu truoc khi thanh toan
        [HttpPost]
        public ActionResult CreateRedirectData(PurchaseInfoModel model)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                _userInfo = new LoginResult();
            var item = _itemService.GetBy(model.Course);
            model.TotalAmount = ((double)item.Price).ToString();
            //kiem tra lai
            //if (model.Course == "VIP")
            //{
            //    model.TotalAmount = ((double)item.Price).ToString();
            //}
            //else (model.Course == "LOP")
            //{
            //    model.TotalAmount = ((double)item.Price).ToString();
            //}

            string code = StringUtil.CheckLetter("S", _retailSalesOrderService.GetMax(), 4);

            #region insert order
            RetailSalesOrderModel retailSalesOrderModel = new RetailSalesOrderModel();
            retailSalesOrderModel.Code = code;
            retailSalesOrderModel.UserId = _userInfo.Id;
            retailSalesOrderModel.Username = _userInfo.userName;
            retailSalesOrderModel.CustomerName = model.FullName;
            retailSalesOrderModel.CustomerPhone = model.Phone;
            retailSalesOrderModel.CustomerEmail = model.Email;
            retailSalesOrderModel.Address = model.Address;
            retailSalesOrderModel.ItemCode = model.Course;
            retailSalesOrderModel.ItemName = string.Format("{0}-{1}", model.Course, code);
            retailSalesOrderModel.TotalAmount = Convert.ToDouble(model.TotalAmount);
            retailSalesOrderModel.StatusCode = EnumStatusCode.Incomplete;// chưa hoàn thành
            retailSalesOrderModel.StatusName = EnumStatusCode.Incomplete;// chưa hoàn thành
            retailSalesOrderModel.CreateTime = DateTime.Now;
            retailSalesOrderModel.LevelId = item.LevelId;
            retailSalesOrderModel.LevelCode = item.LevelCode;
            retailSalesOrderModel.LevelName = item.LevelName;
            retailSalesOrderModel.DurationId = item.DurationId;
            retailSalesOrderModel.DurationCode = item.DurationCode;
            retailSalesOrderModel.DurationName = item.DurationName;
            _retailSalesOrderService.Insert(Mapper.Map<M_RetailSalesOrder>(retailSalesOrderModel));
            hasValue = _vuViecRepo.Commit();
            #endregion

            string SECURE_SECRET = ConfigurationManager.AppSettings["SECURE_SECRET"];
            string virtualPaymentClientURL = ConfigurationManager.AppSettings["virtualPaymentClientURL"];
            string host = ConfigurationManager.AppSettings["host"];
            // create datas
            #region thong tin cau hinh
            OnePayment onePayment = new OnePayment();
            onePayment.vpc_Version = 2;
            onePayment.vpc_Currency = "VND";
            onePayment.vpc_Command = "pay";
            onePayment.vpc_AccessCode = ConfigurationManager.AppSettings["vpc_AccessCode"];
            onePayment.vpc_Merchant = ConfigurationManager.AppSettings["vpc_Merchant"];
            onePayment.vpc_Locale = "vn";
            onePayment.vpc_ReturnURL = string.Format("{0}/Item/ResultPay", host);
            //thong tin giao dich
            onePayment.vpc_MerchTxnRef = code;
            onePayment.vpc_OrderInfo = string.Format("{0}-{1}", model.Course, code);
            onePayment.vpc_Amount = string.Format("{0}{1}", model.TotalAmount, "00");//lam theo yeu cau mo ta
            onePayment.vpc_TicketNo = "192.168.1.2";//GetIpAddress();
            onePayment.vpc_CardList = null;
            onePayment.AgainLink = string.Format("{0}/Item", host);//link test
            onePayment.Title = ConvertToUnSign2("CỔNG THANH TOÁN TÂM TRÍ LỰC");
            onePayment.vpc_Customer_Phone = model.Phone;
            onePayment.vpc_Customer_Email = model.Email;
            onePayment.vpc_Customer_Id = model.Email;
            #endregion

            OnPayHelper conn = new OnPayHelper(virtualPaymentClientURL);
            conn.SetSecureSecret(SECURE_SECRET);
            // Add the Digital Order Fields for the functionality you wish to use
            // Core Transaction Fields
            conn.AddDigitalOrderField("AgainLink", onePayment.AgainLink);
            conn.AddDigitalOrderField("Title", onePayment.Title);
            conn.AddDigitalOrderField("vpc_Locale", onePayment.vpc_Locale);//Chon ngon ngu hien thi tren cong thanh toan (vn/en)
            conn.AddDigitalOrderField("vpc_Version", onePayment.vpc_Version.ToString());
            conn.AddDigitalOrderField("vpc_Currency", onePayment.vpc_Currency);
            conn.AddDigitalOrderField("vpc_Command", onePayment.vpc_Command);
            conn.AddDigitalOrderField("vpc_Merchant", onePayment.vpc_Merchant);
            conn.AddDigitalOrderField("vpc_AccessCode", onePayment.vpc_AccessCode);
            conn.AddDigitalOrderField("vpc_MerchTxnRef", onePayment.vpc_MerchTxnRef);
            conn.AddDigitalOrderField("vpc_OrderInfo", onePayment.vpc_OrderInfo);
            conn.AddDigitalOrderField("vpc_Amount", onePayment.vpc_Amount);
            conn.AddDigitalOrderField("vpc_ReturnURL", onePayment.vpc_ReturnURL);
            // Thong tin them ve khach hang. De trong neu khong co thong tin
            conn.AddDigitalOrderField("vpc_Customer_Phone", onePayment.vpc_Customer_Phone);
            conn.AddDigitalOrderField("vpc_Customer_Email", onePayment.vpc_Customer_Email);
            conn.AddDigitalOrderField("vpc_Customer_Id", onePayment.vpc_Customer_Id);
            // Dia chi IP cua khach hang
            conn.AddDigitalOrderField("vpc_TicketNo", onePayment.vpc_TicketNo);
            // Chuyen huong trinh duyet sang cong thanh toan
            String url = conn.Create3PartyQueryString();
            return Redirect(url);
        }

        //nhan ket qua từ doi tac thanh toan
        public ActionResult ResultPay()
        {
            
            string SECURE_SECRET = ConfigurationManager.AppSettings["SECURE_SECRET"];
            string virtualPaymentClientURL = ConfigurationManager.AppSettings["virtualPaymentClientURL"];
            string hashvalidateResult = "";
            // Khoi tao lop thu vien
            OnPayHelper conn = new OnPayHelper(virtualPaymentClientURL);
            conn.SetSecureSecret(SECURE_SECRET);
            // Xu ly tham so tra ve va kiem tra chuoi du lieu ma hoa
            hashvalidateResult = conn.Process3PartyResponse(Request.QueryString);

            // Lay gia tri tham so tra ve tu cong thanh toan
            var responseCode = string.Empty;
            String vpc_TxnResponseCode = conn.GetResultField("vpc_TxnResponseCode", "Unknown");
            string amount = conn.GetResultField("vpc_Amount", "Unknown");
            string localed = conn.GetResultField("vpc_Locale", "Unknown");
            string command = conn.GetResultField("vpc_Command", "Unknown");
            string version = conn.GetResultField("vpc_Version", "Unknown");
            string cardType = conn.GetResultField("vpc_Card", "Unknown");
            string orderInfo = conn.GetResultField("vpc_OrderInfo", "Unknown");
            string merchantID = conn.GetResultField("vpc_Merchant", "Unknown");
            string authorizeID = conn.GetResultField("vpc_AuthorizeId", "Unknown");
            string merchTxnRef = conn.GetResultField("vpc_MerchTxnRef", "Unknown");
            string transactionNo = conn.GetResultField("vpc_TransactionNo", "Unknown");
            string acqResponseCode = conn.GetResultField("vpc_AcqResponseCode", "Unknown");
            responseCode = vpc_TxnResponseCode;
            string message = conn.GetResultField("vpc_Message", "Unknown");
            string cardUid = conn.GetResultField("vpc_CardUid", "Unknown");

            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            keyValuePairs.Add("", "");
            //get data Request
            Dictionary<string, string> dataTransaction = new Dictionary<string, string>();
            if (Request.QueryString.AllKeys.Length > 0)
            {
                for (int i = 0; i < Request.QueryString.AllKeys.Length; i++)
                {
                    string key = Request.QueryString.AllKeys[i].ToString();
                    dataTransaction.Add(key, conn.GetResultField(key, "Unknown"));
                }
            }

            string jsonStr = JsonConvert.SerializeObject(dataTransaction);

            //thanh cong
            string result = string.Empty;
            //if (recive_SecureHash !=doSecureHash)
            if (hashvalidateResult == "CORRECTED" && responseCode == "0")
            {
                result = ErrorOnpayService.ErrorOnpays().FirstOrDefault(x => x.ID == responseCode).Message;
                //update status code successful
                #region update order
                var retailSalesOrder = Mapper.Map<RetailSalesOrderModel>(_retailSalesOrderService.GetBy(merchTxnRef));
                var cardNo = string.IsNullOrEmpty(retailSalesOrder.ActionCode) ? AssignCode(retailSalesOrder) : retailSalesOrder.ActionCode;
                _retailSalesOrderService.UpdateStatus(merchTxnRef, EnumStatusCode.PaySuccess, jsonStr, cardNo);
                hasValue = _vuViecRepo.Commit();
                #endregion

            }
            else if (hashvalidateResult == "INVALIDATED" && responseCode == "0")
            {
                result = ErrorOnpayService.ErrorOnpays().FirstOrDefault(x => x.ID == responseCode).Message;
                #region update order
                _retailSalesOrderService.UpdateStatus(merchTxnRef, EnumStatusCode.PayPending, jsonStr, "");
                hasValue = _vuViecRepo.Commit();
                #endregion
                //kiem tra lại giao dịch
            }
            else
            {
                #region update order
                _retailSalesOrderService.UpdateStatus(merchTxnRef, EnumStatusCode.PayFaile, jsonStr, "");
                hasValue = _vuViecRepo.Commit();
                #endregion
                result = ErrorOnpayService.ErrorOnpays().FirstOrDefault(x => x.ID == responseCode).Message;
            }
            ViewBag.Result = result;
            ViewBag.Order = Mapper.Map<RetailSalesOrderModel>(_retailSalesOrderService.GetBy(merchTxnRef));
            ViewBag.responseCode = responseCode;
            return View();
        }

        public string ConvertToUnSign2(string s)
        {
            string stFormD = s.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int ich = 0; ich < stFormD.Length; ich++)
            {
                System.Globalization.UnicodeCategory uc = System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != System.Globalization.UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[ich]);
                }
            }
            sb = sb.Replace('Đ', 'D');
            sb = sb.Replace('đ', 'd');
            return (sb.ToString().Normalize(NormalizationForm.FormD));
        }

        private string GetIpAddress()
        {
            string ipaddress;
            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ipaddress == "" || ipaddress == null)
                ipaddress = Request.ServerVariables["REMOTE_ADDR"];
            return ipaddress;
        }
        private string AssignCode(RetailSalesOrderModel _retailOrder)
        {
            var idCard = _cardLineService.GetID();
            //-- Build Ma Kich Hoat
            var builder = new StringBuilder();
            Guid g = Guid.NewGuid();
            builder.Append(g.ToString().Split('-')[0].ToString().ToUpper());
            var randomNumber = new RandomNumberGenerator();
            builder.Append(randomNumber.RandomNumber(10, 99));
            var _cardNo = builder.ToString();
            //-- Build Ma Kich Hoat +
            var cryptorEngine = new CryptorEngine2();
            var statusInfo = Mapper.Map<OptionLineViewModel>(_optionLineService.Get("CARD", "450"));
            var cardLine = new CardLine()
            {
                Id = idCard,
                LotNumber = _retailOrder.Code,
                SerialNumber = StringUtil.GetProductID("W", (idCard - 1).ToString(), 9),
                CardNo = cryptorEngine.Encrypt(_cardNo, true, "TTLSTNHD@CARD"),
                Price = (decimal)_retailOrder.TotalAmount,
                UserName = _retailOrder.Username,
                UserFullName = _retailOrder.CustomerName,
                DateCreate = DateTime.Now,

                UserType = UserType.TTL,
                UserNameActive = _retailOrder.Username,
                UserActiveFullName = _retailOrder.CustomerName,
                LastDateUpdate = DateTime.Now,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                LastStatusId = statusInfo.LineNo,
                LastStatusCode = statusInfo.Code,
                LastStatusName = statusInfo.Name,

                DurationId = _retailOrder.DurationId,
                DurationCode = _retailOrder.DurationCode,
                DurationName = _retailOrder.DurationName,

                ReasonId = 45,
                ReasonCode = "BH",
                ReasonName = "Bán Hàng",

                LevelId = _retailOrder.LevelId,
                LevelCode = _retailOrder.LevelCode,
                LevelName = _retailOrder.LevelName
            };
            var cardEntry = new CardEntry()
            {
                LotNumber = cardLine.LotNumber,
                CardLineId = idCard,
                SerialNumber = cardLine.SerialNumber,
                CardNo = cardLine.CardNo,
                Price = cardLine.Price,
                UserName = cardLine.UserName,
                UserFullName = cardLine.UserFullName,
                DateCreate = cardLine.DateCreate,
                UserType = UserType.TTL,
                UserNameActive = cardLine.UserNameActive,
                UserActiveFullName = cardLine.UserActiveFullName,
                Date = cardLine.DateCreate,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                Source = CardSource.CardSTNHD,
                SourceNo = cardLine.LotNumber,
                DurationId = cardLine.DurationId,
                DurationCode = cardLine.DurationCode,
                DurationName = cardLine.DurationName,
                ReasonId = cardLine.ReasonId,
                ReasonCode = cardLine.ReasonCode,
                ReasonName = cardLine.ReasonName,
                LevelId = cardLine.LevelId,
                LevelCode = cardLine.LevelCode,
                LevelName = cardLine.LevelName
            };
            hasValue = _cardLineService.Insert(Mapper.Map<M_CardLine>(cardLine));
            hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
            hasValue = SendCardViaSMS(_retailOrder, _cardNo);
            hasValue = _vuViecRepo.Commit();
            return _cardNo;
        }
        private string SendCardViaSMS(RetailSalesOrderModel _retailOrder, string cardNo)
        {
            try
            {
                if (StringUtil.Validate(_retailOrder.CustomerEmail))
                {
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", _retailOrder.CustomerName);
                    mailContent = mailContent.Replace("{{ACTIVECODE}}", cardNo);
                    var emailTos = new List<string>();
                    emailTos.Add(_retailOrder.CustomerEmail);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.SendEmailsThread();
                }
                if (!string.IsNullOrEmpty(_retailOrder.CustomerPhone))
                {
                    var newPhoneNo = _retailOrder.CustomerPhone.Trim();
                    var firstNumber = int.Parse(newPhoneNo[0].ToString());
                    if (firstNumber != 0)
                    {
                        newPhoneNo = "0" + newPhoneNo;
                    }
                    var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                    var smsInfo = new SMSViewModel()
                    {
                        Type = SMSType.SMS,
                        PhoneNo = newPhoneNo,
                        OTP = null,
                        Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", cardNo).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student"),
                        DateInput = DateTime.Now,
                        Username = _retailOrder.Username,
                        FullName = _retailOrder.CustomerName
                    };
                    var smsHelper = new SMSHelper()
                    {
                        PhoneNo = smsInfo.PhoneNo,
                        Content = smsInfo.Content
                    };
                    if (smsHelper.IsMobiphone())
                    {
                        smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_KH"));
                        smsHelper.Content = string.Format(smsTemplate.SMSContent, "5 Phut Thuoc Bai", cardNo);
                        hasValue = smsHelper.SendMobiphone();
                    }
                    else
                        hasValue = smsHelper.Send();
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
