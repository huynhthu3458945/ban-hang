﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class SocialController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IQuestionService _questionService;
        private readonly IVuViecService _vuviecService;
        public SocialController
            (
                IShareInfoService shareInfoService
                , IQuestionService questionService
                , IVuViecService vuviecService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _questionService = questionService;
            _vuviecService = vuviecService;
        }
        public ActionResult Index()
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();
            var _socialVM = new SocialViewModel();
            _socialVM.UserInfo = GetUserInfo();
            //_socialVM.Socials = Mapper.Map<IEnumerable<QuestionViewModel>>(_questionService.FindAll(QuestionType.Question)).Where(d => d.StatusCode.Equals("DDTL")).OrderByDescending(d => d.DateQuestion).Take(20);
            //_socialVM.MyQuestions = Mapper.Map<IEnumerable<QuestionViewModel>>(_questionService.FindAll(QuestionType.Question)).Where(d => (d.userName??string.Empty).Equals(_userInfo.userName, StringComparison.OrdinalIgnoreCase)).OrderByDescending(d => d.DateQuestion);
            _socialVM.Socials = Mapper.Map<IEnumerable<QuestionViewModel>>(_questionService.FindAllByStatus(QuestionType.Question, "DDTL"));
            _socialVM.MyQuestions = Mapper.Map<IEnumerable<QuestionViewModel>>(_questionService.FindAllByUser(QuestionType.Question, _userInfo.userName));
            return View(_socialVM);
        }
        /// <summary>
        /// Insert content
        /// </summary>
        /// <param name="c">Content</param>
        /// <returns></returns>
        public ActionResult Insert(string c)
        {
            var userInfo = GetUserInfo();
            var question = new M_Question();
            question.Id = _questionService.GetID();
            question.Question = c;
            question.Type = QuestionType.Question;
            question.DateQuestion = DateTime.Now;
            question.userName = userInfo.userName;
            question.fullName = userInfo.fullName;
            question.StatusId = 13;
            question.StatusCode = "CTL";
            question.StatusName = "Chờ Trả Lời";
            string hasValue = _questionService.Insert(question);
            hasValue = _vuviecService.Commit();

            var _socialVM = new SocialViewModel();
            _socialVM.Socials = Mapper.Map<IEnumerable<QuestionViewModel>>(_questionService.FindAll(QuestionType.Question));
            return PartialView("_index", _socialVM);
        }
        public ActionResult Rules()
        {
            return PartialView("_rules");
        }
        public ActionResult NoiQuyDienDan()
        {
            return View();
        }
    }
}