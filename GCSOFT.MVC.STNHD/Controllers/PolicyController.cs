﻿using GCSOFT.MVC.Service.STNHDService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class PolicyController : BaseController
    {
        public PolicyController
            (
                IShareInfoService shareInfoService
            ) : base(shareInfoService)
        {

        }
        // GET: Policy
        public ActionResult privacy()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult payment()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult Complaint()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult Return()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult PaymentGuide()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            return View();
        }
        public ActionResult Window7()
        {
            return View();
        }
    }
}