﻿using AutoMapper;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.STNHD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class QuestionController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IQuestionPageService _questionService;
        public QuestionController
            (
                IShareInfoService shareInfoService
                , IQuestionPageService questionService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _questionService = questionService;
        }
        public ActionResult Answer(int id)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();
            return View(Mapper.Map<QuestionViewModel>(_questionService.GetQuestion(id)));
        }
    }
}