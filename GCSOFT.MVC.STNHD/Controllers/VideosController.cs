﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.VideosService.Interface;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class VideosController : BaseController
    {
        private string API_URL = ConfigurationManager.AppSettings["apiBaseAddress"];
        private readonly IShareInfoService _shareInfoService;
        private readonly ISetupService _setupService;
        private readonly IStudentClassService _studentClassService;
        private readonly ISupporterService _supportService;
        private readonly IFileResourceService _fileResourceService;
        private readonly ICoursePageService _courseService;
        private readonly IMemoVideosService _memoVideosService;
        public VideosController
            (
                IShareInfoService shareInfoService
                , ISetupService setupService
                , IStudentClassService studentClassService
                , ISupporterService supportService
                , IFileResourceService fileResourceService
                , ICoursePageService courseService
                , IMemoVideosService memoVideosService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _setupService = setupService;
            _studentClassService = studentClassService;
            _supportService = supportService;
            _fileResourceService = fileResourceService;
            _courseService = courseService;
            _memoVideosService = memoVideosService;
        }
        public ActionResult Index(int? id)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            var memoVideosPage = new MemoVideosPage();
            memoVideosPage.UserInfo = _userInfo;
            memoVideosPage.VideosList = Mapper.Map<IEnumerable<MemoVideos>>(_memoVideosService.FindAll());
            memoVideosPage.Video = Mapper.Map<MemoVideos>(_memoVideosService.GetBy(id ?? 200));
            //-- Get token
            var clientUrl = "https://th.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
            if (memoVideosPage.Video.LinkVN.Contains("th.s"))
                clientUrl = "https://th.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
            if (memoVideosPage.Video.LinkVN.Contains("thcs.s"))
                clientUrl = "https://thcs.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
            if (memoVideosPage.Video.LinkVN.Contains("thpt.s"))
                clientUrl = "https://thpt.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
            var client = new RestClient(string.Format(clientUrl, _userInfo.Id));
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            //-- Get token +
            var language = Getlanguage();
            var link2Play = language.Equals("en-US") ? memoVideosPage.Video.LinkEN : memoVideosPage.Video.LinkVN;
            memoVideosPage.UrlToPlay = string.Format("{0}&token={1}", link2Play, response.Content);
            memoVideosPage.idCurrentLesson = id ?? 200;
            memoVideosPage.idFirstVideos = 200;
            memoVideosPage.idLastVideos = memoVideosPage.VideosList.LastOrDefault().Id;
            return View(memoVideosPage);
        }
        // GET: Videos
        //[Authorize]
        //public ActionResult Index(int? id)
        //{
        //    var _userInfo = GetUserInfo();
        //    if (_userInfo == null)
        //        return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
        //    if (!_userInfo.ActivePhone)
        //        return RedirectToAction("Index", "ClassInfo");

        //    ViewBag.ShareInfo = GetShareInfo();
        //    try
        //    {
        //        var userInfo = GetUserInfo();
        //        using (WebClient webClient = new WebClient())
        //        {
        //            var setup = _setupService.Get();
        //            userInfo.specialId = setup.SpecialId;
        //            userInfo.token = setup.Token;

        //            webClient.Encoding = Encoding.UTF8;

        //            webClient.Headers.Add("AUTHORIZATION", "nTZ9J7XhIvHLzRmnkchiPg==");
        //            webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
        //            var url = "/api/Customer/sieutrinhohocduong/courses";
        //            if (id != null)
        //                url = string.Format("/api/Customer/sieutrinhohocduong/courses?videoId={0}", id);
        //            var response = webClient.DownloadString(API_URL + url);
        //            JObject result = JsonConvert.DeserializeObject<JObject>(response);
        //            var lessionInfo = JsonConvert.DeserializeObject<VideosLessionInfo>(response);
        //            List<VideosLesson> lessons = result["items"].ToObject<List<VideosLesson>>();
        //            lessons[2].name = "QUY TRÌNH HỌC THÔNG MINH";
        //            string output = JsonConvert.SerializeObject(lessons);
        //            VideosCourseData courseData = result["courseData"].ToObject<VideosCourseData>();


        //            LearningViewModel model = new LearningViewModel
        //            {
        //                CourseData = courseData,
        //                lessons = lessons,
        //                LessionInfo = lessionInfo,
        //                UserInfo = userInfo
        //            };
        //            ViewBag.idCurrentLesson = id ?? 1769;

        //            return View(model);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.ToString();
        //        LearningViewModel model = new LearningViewModel()
        //        {
        //            CourseData = new VideosCourseData(),
        //            lessons = new List<VideosLesson>(),
        //            LessionInfo = new VideosLessionInfo(),
        //            UserInfo = GetUserInfo()
        //        };
        //        return View(model);
        //    }
        //}
        /// <summary>
        /// Load Videos by Id
        /// </summary>
        /// <param name="v">videoId</param>
        /// <returns></returns>
        public ActionResult LoadVideo(int v)
        {
            var userInfo = GetUserInfo();
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            webClient.Headers.Add("AUTHORIZATION", "Bearer " + userInfo.token);
            webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
            var response = webClient.DownloadString(API_URL + string.Format("api/customer/courses/{0}?videoId={1}", userInfo.specialId, v));
            JObject result = JsonConvert.DeserializeObject<JObject>(response);
            var lessionInfo = JsonConvert.DeserializeObject<VideosLessionInfo>(response);
            List<VideosLesson> lessons = result["items"].ToObject<List<VideosLesson>>();
            VideosCourseData courseData = result["courseData"].ToObject<VideosCourseData>();

            LearningViewModel model = new LearningViewModel
            {
                CourseData = courseData,
                lessons = lessons,
                LessionInfo = lessionInfo
            };
            return PartialView("_videos", model);
        }
        public ActionResult Books()
        {
            var userInfo = GetUserInfo();
            if (userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();
            var homeVM = HomeViewModel(null);
            return View(homeVM);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public ActionResult LoadVideosCourse(int c)
        {
            var language = Getlanguage();
            var setup = _setupService.Get();
            var avatarInfo = new VM_FileResource();
            var courses = Mapper.Map<IEnumerable<CourseList>>(_shareInfoService.CourseList(false, c));
            foreach (var item in courses)
            {
                if (item.AvartarLineNo == 0)
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, 600));
                else
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, item.AvartarLineNo));
                item.Avatar = avatarInfo.ImagePath.Replace("~", setup.DomainFontEnd);
                if (language.Equals("en-US") && !string.IsNullOrEmpty(item.TitleEN))
                    item.Title = item.TitleEN;
            }
            return PartialView("_VideosCourse", courses);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Id Course</param>
        /// <param name="l">Lesson Id</param>
        /// <returns></returns>
        public ActionResult Book(int? id, int? l)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            var language = Getlanguage();
            try
            {
                var courseInfo = HomeViewModel(id);
                ViewBag.courseInfo = courseInfo;
                //ViewBag.ShareInfo = GetShareInfo();
                var learningVM = new CourseVideosViewModel();
                learningVM.CourseInfo = Mapper.Map<CourseViewModel>(_courseService.GetCourse(id ?? 0));
                learningVM.CourseVideos = Mapper.Map<IEnumerable<CourseContentList>>(_courseService.GetCourseContentList(id ?? 0, CourseType.Videos)) ?? new List<CourseContentList>();
                learningVM.CourseVideo = Mapper.Map<CourseContentList>(learningVM.CourseVideos.Where(d => !string.IsNullOrEmpty(d.LinkYoutube) && ((l ?? 0) == 0 || d.LineNo == l)).FirstOrDefault()) ?? new CourseContentList();
                
                ViewBag.ParentTitle = learningVM.CourseVideo.Title;
                if ((learningVM.CourseVideo.ParentSortOrder ?? 0) != 0)
                    ViewBag.ParentTitle = Mapper.Map<CourseContentList>(_courseService.GetParentTitle(learningVM.CourseVideo.IdCourse, learningVM.CourseVideo.ParentSortOrder ?? 0, CourseType.Videos)).Title;
                learningVM.HasVideos = learningVM.CourseVideos.Where(d => !string.IsNullOrEmpty(d.LinkYoutube)).Count() > 0;
                learningVM.UserInfo = _userInfo;

                ViewBag.FirstVideosId = learningVM.CourseVideos.Where(d => !string.IsNullOrEmpty(d.LinkYoutube)).FirstOrDefault().LineNo;
                ViewBag.LastVideosId = learningVM.CourseVideos.Where(d => !string.IsNullOrEmpty(d.LinkYoutube)).LastOrDefault().LineNo;
                ViewBag.idCurrentLesson = l ?? ViewBag.FirstVideosId;
                //-- Get token
                var clientUrl = "https://cdn.stnhd.com/video/get_key_frame.php?user_id={0}";
                //if (!learningVM.CourseVideo.IsHiddenHK2)
                //{
                    if (language.Equals("vi-VN") && !string.IsNullOrEmpty(learningVM.CourseVideo.LinkVideoVN))
                    {
                        if (learningVM.CourseVideo.LinkVideoVN.Contains("th.s"))
                            clientUrl = "https://th.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
                        if (learningVM.CourseVideo.LinkVideoVN.Contains("thcs.s"))
                            clientUrl = "https://thcs.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
                        if (learningVM.CourseVideo.LinkVideoVN.Contains("thpt.s"))
                            clientUrl = "https://thpt.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
                    }
                    if (language.Equals("en-US") && !string.IsNullOrEmpty(learningVM.CourseVideo.LinkVideoEN))
                    {
                        if (learningVM.CourseVideo.LinkVideoEN.Contains("th.s"))
                            clientUrl = "https://th.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
                        if (learningVM.CourseVideo.LinkVideoEN.Contains("thcs.s"))
                            clientUrl = "https://thcs.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
                        if (learningVM.CourseVideo.LinkVideoEN.Contains("thpt.s"))
                            clientUrl = "https://thpt.sieutrinhohocduong.com/video/get_key_frame.php?user_id={0}";
                    }
                //}
                
                var client = new RestClient(string.Format(clientUrl, _userInfo.Id));
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                var videoToPlay = learningVM.CourseVideo.CurrentUrl;
                //if (!learningVM.CourseVideo.IsHiddenHK2)
                //{
                    if (language.Equals("vi-VN") && !string.IsNullOrEmpty(learningVM.CourseVideo.LinkVideoVN))
                        videoToPlay = learningVM.CourseVideo.LinkVideoVN;
                    if (language.Equals("en-US") && !string.IsNullOrEmpty(learningVM.CourseVideo.LinkVideoEN))
                        videoToPlay = learningVM.CourseVideo.LinkVideoEN;
                //}
                ViewBag.url = string.Format("{0}&token={1}", videoToPlay, response.Content);
                //-- Get token +
                return View(learningVM);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                return View(new CourseVideosViewModel()
                {
                    UserInfo = GetUserInfo(),
                    CourseInfo = new CourseViewModel(),
                    CourseVideos = new List<CourseContentList>(),
                    CourseVideo = new CourseContentList()
                }); ;
            }
            
        }
       
        private HomeViewModel HomeViewModel(int? courseId)
        {
            var language = Getlanguage();
            var userInfo = GetUserInfo();
            var setup = _setupService.Get();
            var avatarInfo = new VM_FileResource();
            var homeInfo = new HomeViewModel();
            if (courseId != null)
                homeInfo.Course = Mapper.Map<CourseList>(_shareInfoService.GetCourseBy(courseId ?? 0));
             
            var idClass = userInfo.ClassId;
            if (!_studentClassService.HasClass(userInfo.userName))
            {
                var _studentClassInit = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classList = Mapper.Map<IEnumerable<ClassList>>(_shareInfoService.ClassList(ClassType.Class, "SHOW"));
                foreach (var item in classList)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    if (language.Equals("en-US") && !string.IsNullOrEmpty(item.NameEN))
                        _studentClass.ClassName = item.NameEN;
                    _studentClass.IsActive = false;
                    if (_studentClass.ClassId == idClass)
                        _studentClass.IsActive = true;
                    _studentClassInit.Add(_studentClass);
                }
                homeInfo.StudentClasses = _studentClassInit;
            }
            else
            {
                homeInfo.StudentClasses = Mapper.Map<IEnumerable<StudentClassViewModel>>(_studentClassService.FindAllByActive(userInfo.userName));
                homeInfo.StudentClasses.ToList().ForEach(item => {
                    if (language.Equals("en-US"))
                    {
                        var classNameEN = _shareInfoService.GetClassBy(item.ClassId).NameEN;
                        if (!string.IsNullOrEmpty(classNameEN))
                            item.ClassName = classNameEN;
                    }
                    item.IsActive = false;
                    if (item.ClassId == idClass)
                        item.IsActive = true;
                });
            }
                
            if (homeInfo.Course.ClassId != 0)
                idClass = homeInfo.Course.ClassId;
            homeInfo.Course.ClassId = idClass;
            homeInfo.Courses = Mapper.Map<IEnumerable<CourseList>>(_shareInfoService.CourseList(false, idClass));
            foreach (var item in homeInfo.Courses)
            {
                if (item.AvartarLineNo == 0)
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, 600));
                else
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, item.AvartarLineNo));
                item.Avatar = avatarInfo.ImagePath.Replace("~", setup.DomainFontEnd);
                if (language.Equals("en-US") && !string.IsNullOrEmpty(item.TitleEN))
                    item.Title = item.TitleEN;
            }
            homeInfo.UserInfo = userInfo;
            return homeInfo;
        }
    }
}