﻿using AutoMapper;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class teachersController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly ITeacherService _teacherService;
        private readonly IOptionLineService _optionLineService;
        public teachersController
            (
                IShareInfoService shareInfoService
                , ITeacherService teacherService
                , IOptionLineService optionLineService
            ) : base(shareInfoService)
        {
            _teacherService = teacherService;
            _shareInfoService = shareInfoService;
            _optionLineService = optionLineService;
        }
        // GET: teacher
        /// <summary>
        /// 
        /// </summary>
        /// <param name="l">Level</param>
        /// <param name="p">Paging</param>
        /// <returns></returns>
        public ActionResult Index(int? p, string l)
        {
            if (l == null)
                l = "";
            //ViewBag.ShareInfo = GetShareInfo();
            var teacher = new TeacherViewModel();
            teacher.UserInfo = GetUserInfo() ?? new LoginResult();
            teacher.Levels = Mapper.Map<IEnumerable<OptionLineViewModel>>(_optionLineService.FindAll("LEVEL"));

            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = _teacherService.TotalRecord(l);
            teacher.paging = new Paging(totalRecord, page);
            teacher.Teachers = Mapper.Map<IEnumerable<Teacher>>(_teacherService.FindAll(l, teacher.paging.startIndex, teacher.paging.offset));
            teacher.ActiveClass = l;
            return View(teacher);
        }
    }
}