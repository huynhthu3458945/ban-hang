﻿using GCSOFT.MVC.Data.Configuration;
using GCSOFT.MVC.Data.Configuration.MasterDataConfig;
using GCSOFT.MVC.Data.Configuration.SystemConfig;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.App;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.FrontEnd;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.NewSocial;
using GCSOFT.MVC.Model.QuickQuiz;
using GCSOFT.MVC.Model.Report;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Model.Videos;
using System.Data.Entity;
namespace GCSOFT.MVC.Data
{
    public class GcEntities : DbContext
    {
        //public GcEntities() : base("GCConnection") {
        //    Database.SetInitializer(new MigrateDatabaseToLatestVersion<GcEntities, Migrations.Configuration>("GCConnection"));
        //}
        public GcEntities() : base("GCConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GcEntities, GCSOFT.MVC.Data.Migrations.Configuration>());
        }
        public DbSet<HT_CongViec> CongViecs { get; set; }
        public DbSet<HT_VuViec> VuViecs { get; set; }
        public DbSet<HT_VuViecCuaCongViec> VuViecCuaCongViecs { get; set; }
        public DbSet<HT_NhomUser> NhomUsers { get; set; }
        public DbSet<HT_CongViecVaVuViec> CongViecVaVuViecs { get; set; }
        public DbSet<HT_Users> Users { get; set; }
        public DbSet<HT_UserThuocNhom> UserThuocNhom { get; set; }
        public DbSet<M_OptionType> OptionTypes { get; set; }
        public DbSet<M_OptionLine> OptionLines { get; set; }
        public DbSet<E_Category> Categories { get; set; }
        public DbSet<M_Contact> Contacts { get; set; }
        public DbSet<M_UserInfo> UserInfos { get; set; }
        //public DbSet<M_UserInfoActive> UserInfoActives { get; set; }
        public DbSet<M_Package> Packages { get; set; }
        public DbSet<M_CardEntry> CardEntries { get; set; }
        public DbSet<M_SalesHeader> SalesHeaders { get; set; }
        public DbSet<M_SalesLine> SalesLines { get; set; }
        public DbSet<M_District> Districts { get; set; }
        public DbSet<M_Province> Provinces { get; set; }
        public DbSet<M_MailSetup> MailSetups { get; set; }
        public DbSet<M_Template> Templates { get; set; }
        public DbSet<M_ApprovalInfo> ApprovalInfos { get; set; }
        public DbSet<M_Flow> Flows { get; set; }
        public DbSet<M_PaymentResult> PaymentResults { get; set; }
        public DbSet<M_Ticket> Tickets { get; set; }
        public DbSet<M_Language> Languages { get; set; }
        public DbSet<M_Banner> Banners { get; set; }
        public DbSet<M_RetailSalesOrder> RetailSalesOrders { get; set; }
        public DbSet<M_Feedback> Feedbacks { get; set; }
        public DbSet<M_SalesPrice> SalesPrices { get; set; }
        public DbSet<M_Promotion> Promotion { get; set; }
        public DbSet<TB_TransactionGift> TransactionGifts { get; set; }
        public DbSet<M_UserInfoOfGroup> UserInfoOfGroups { get; set; }
        public DbSet<QR_Question> QRQuestions { get; set; }
        public DbSet<M_UploadImage> UploadImages { get; set; }
        public DbSet<CategoriesProduct> CategoriesProducts { get; set; }
        public DbSet<Product> Products { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}