﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Add(IEnumerable<T> objects);
        void Update(T entity);
        void Update(IEnumerable<T> objects);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);
        T GetById(int Id);
        T GetById(long Id);
        T GetById(string Id);
        T GetById(Guid Id);
        T Get(Expression<Func<T, bool>> where);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
        int GetKey(Expression<Func<T, int?>> key);
        long GetKey(Expression<Func<T, long?>> key);
        int GetKey(Expression<Func<T, bool>> where, Expression<Func<T, int?>> key);
    }
}
