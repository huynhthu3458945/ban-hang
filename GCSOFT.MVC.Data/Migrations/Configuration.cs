﻿namespace GCSOFT.MVC.Data.Migrations
{
    using Common;
    using Model.SystemModels;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GCSOFT.MVC.Data.GcEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "GCSOFT.MVC.Data.GcEntities";
        }

        protected override void Seed(GCSOFT.MVC.Data.GcEntities context)
        {
            //GetVuViec().ForEach(c => context.VuViecs.Add(c));
            //context.NhomUsers.Add(GetNhomUser());
            //context.Users.Add(GetUser());
            //context.UserThuocNhom.Add(GetUserThuocNhom());
            //GetCongViec().ForEach(d => context.CongViecs.Add(d));
            //GetVuViecCuaCongViec().ForEach(d => context.VuViecCuaCongViecs.Add(d));
            //GetCongViecVaVuViec().ForEach(d => context.CongViecVaVuViecs.Add(d));
            //context.Commit();
        }
        private static List<HT_VuViec> GetVuViec()
        {
            return new List<HT_VuViec>
            {
                new HT_VuViec { Code = "001", Name = "Xem", Description = "" }
                , new HT_VuViec { Code = "002", Name = "Thêm", Description = "" }
                , new HT_VuViec { Code = "003", Name = "Cập nhật", Description = "" }
                , new HT_VuViec { Code = "004", Name = "Xóa", Description = "" }
                , new HT_VuViec { Code = "005", Name = "Chi tiết", Description = "" }
                , new HT_VuViec { Code = "006", Name = "In", Description = "" }
                , new HT_VuViec { Code = "007", Name = "Xuất excel", Description = "" }
                , new HT_VuViec { Code = "008", Name = "Quyền truy cập", Description = "Trong form DS Nhom NSD" }
                , new HT_VuViec { Code = "009", Name = "Đổi mật khẩu", Description = "Đổi mật khẩu trong form DS NSD" }
            };
        }
        private static HT_NhomUser GetNhomUser()
        {
            return new HT_NhomUser { Code = "Administrator", Name = "Quản trị hệ thống", Description = "Toàn quyền trên hệ thống" };
        }
        private static HT_Users GetUser()
        {
            return new HT_Users { ID = 1, Username = "admin", Password = CryptorEngine.Encrypt("admin123", true, "Hieu.Le-GC.Soft"), FirstName = "Lê Thanh", LastName = "Hiếu", FullName = "Lê Thanh Hiếu", Email = "gc.soft88@gmail.com", Phone = "0194 129 081", Position = "admin hệ thống", Description = "Tài khoản hệ thống không được xóa" };
        }
        private static HT_UserThuocNhom GetUserThuocNhom()
        {
            return new HT_UserThuocNhom { UserGroupCode = "Administrator", UserID = 1 };
        }
        private static List<HT_CongViec> GetCongViec()
        {
            return new List<HT_CongViec>()
            {
                new HT_CongViec { Code = "HT", Name = "Hệ Thống", Link = "Hệ Thống", AreaName = null, ControllerName = null, ActionName = null, OnMenu = true, Priority = 100, ParentCode = null, ParentPriorityLink = "a", ParentPriority = "a" }
                , new HT_CongViec { Code = "CV", Name = "Công Việc", Link = "Hệ Thống -> Công Việc", AreaName = "Administrator", ControllerName = "SysCategory", ActionName = "Index", OnMenu = true, Priority = 100, ParentCode = "HT", ParentPriorityLink = "a.a", ParentPriority = "a" }
                , new HT_CongViec { Code = "UG", Name = "Nhóm Người Sử Dụng", Link = "Hệ Thống -> Nhóm Người Sử Dụng", AreaName = "Administrator", ControllerName = "SysUserGroup", ActionName = "Index", OnMenu = true, Priority = 200, ParentCode = "HT", ParentPriorityLink = "a.b", ParentPriority = "b" }
                , new HT_CongViec { Code = "U", Name = "Người Sử Dụng", Link = "Hệ Thống -> Người Sử Dụng", AreaName = "Administrator", ControllerName = "SysUser", ActionName = "Index", OnMenu = true, Priority = 200, ParentCode = "HT", ParentPriorityLink = "a.c", ParentPriority = "c" }
                , new HT_CongViec { Code = "DB", Name = "Bảng Điều Khiển", Link = "Bảng Điều Khiển", AreaName = "Administrator", ControllerName = "Dashboard", ActionName = "Index", OnMenu = false, Priority = 0, ParentCode = null, ParentPriorityLink = null, ParentPriority = null }
            };
        }
        private static List<HT_VuViecCuaCongViec> GetVuViecCuaCongViec()
        {
            return new List<HT_VuViecCuaCongViec>()
            {
                new HT_VuViecCuaCongViec { CategoryCode = "HT", FunctionCode = "001" }
                , new HT_VuViecCuaCongViec { CategoryCode = "CV", FunctionCode = "001" }
                , new HT_VuViecCuaCongViec { CategoryCode = "CV", FunctionCode = "002" }
                , new HT_VuViecCuaCongViec { CategoryCode = "CV", FunctionCode = "003" }
                , new HT_VuViecCuaCongViec { CategoryCode = "CV", FunctionCode = "004" }
                , new HT_VuViecCuaCongViec { CategoryCode = "CV", FunctionCode = "005" }
                , new HT_VuViecCuaCongViec { CategoryCode = "UG", FunctionCode = "001" }
                , new HT_VuViecCuaCongViec { CategoryCode = "UG", FunctionCode = "002" }
                , new HT_VuViecCuaCongViec { CategoryCode = "UG", FunctionCode = "003" }
                , new HT_VuViecCuaCongViec { CategoryCode = "UG", FunctionCode = "004" }
                , new HT_VuViecCuaCongViec { CategoryCode = "UG", FunctionCode = "005" }
                , new HT_VuViecCuaCongViec { CategoryCode = "U", FunctionCode = "001" }
                , new HT_VuViecCuaCongViec { CategoryCode = "U", FunctionCode = "002" }
                , new HT_VuViecCuaCongViec { CategoryCode = "U", FunctionCode = "003" }
                , new HT_VuViecCuaCongViec { CategoryCode = "U", FunctionCode = "004" }
                , new HT_VuViecCuaCongViec { CategoryCode = "U", FunctionCode = "005" }
                , new HT_VuViecCuaCongViec { CategoryCode = "DB", FunctionCode = "001" }
            };
        }
        private static List<HT_CongViecVaVuViec> GetCongViecVaVuViec()
        {
            return new List<HT_CongViecVaVuViec>()
            {
                new HT_CongViecVaVuViec { CategoryCode = "HT", FunctionCode = "001", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "CV", FunctionCode = "001", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "CV", FunctionCode = "002", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "CV", FunctionCode = "003", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "CV", FunctionCode = "004", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "CV", FunctionCode = "005", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "UG", FunctionCode = "001", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "UG", FunctionCode = "002", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "UG", FunctionCode = "003", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "UG", FunctionCode = "004", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "UG", FunctionCode = "005", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "U", FunctionCode = "001", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "U", FunctionCode = "002", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "U", FunctionCode = "003", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "U", FunctionCode = "004", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "U", FunctionCode = "005", UserGroupCode = "Administrator" }
                , new HT_CongViecVaVuViec { CategoryCode = "DB", FunctionCode = "001", UserGroupCode = "Administrator" }
            };
        }
    }
}
