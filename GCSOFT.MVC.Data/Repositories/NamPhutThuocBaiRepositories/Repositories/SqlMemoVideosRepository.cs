﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.Videos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.VideosRepositories.Repositories
{
    public class SqlContestantsRepository : RepositoryBase<TB_Contestants>, IContestantsRepository
    {
        public SqlContestantsRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}