﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.NamPhutThuocBaiRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.NamPhutThuocBaiRepositories.Repositories
{
    public class SqlTransactionGiftRepository : RepositoryBase<TB_TransactionGift>, ITransactionGiftRepository
    {
        public SqlTransactionGiftRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
