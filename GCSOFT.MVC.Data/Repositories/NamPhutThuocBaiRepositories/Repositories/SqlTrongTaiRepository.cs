﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.Videos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.VideosRepositories.Repositories
{
    public class SqlTrongTaiRepository : RepositoryBase<TB_TrongTai>, ITrongTaiRepository
    {
        public SqlTrongTaiRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}