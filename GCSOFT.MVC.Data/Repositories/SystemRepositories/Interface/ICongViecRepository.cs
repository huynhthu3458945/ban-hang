﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface
{
    public interface ICongViecRepository : IRepository<HT_CongViec>
    {
        IEnumerable<HT_CongViec> GetCategoryBy(string username);
    }

    public interface IVuViecCuaCongViecRepository : IRepository<HT_VuViecCuaCongViec>
    {

    }
}