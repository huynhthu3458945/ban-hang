﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface
{
    public interface IUserRepository : IRepository<HT_Users>
    {
        int GetID();
    }
}
