﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories
{
    public class SqlCongViecVaVuViecRepository : RepositoryBase<HT_CongViecVaVuViec>, ICongViecVaVuViecRepository
    {
        public SqlCongViecVaVuViecRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}
