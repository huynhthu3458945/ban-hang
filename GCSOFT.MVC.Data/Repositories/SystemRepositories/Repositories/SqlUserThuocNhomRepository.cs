﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories
{
    public class SqlUserThuocNhomRepository : RepositoryBase<HT_UserThuocNhom>, IUserThuocNhomRepository
    {
        public SqlUserThuocNhomRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
            {

        }
    }
}