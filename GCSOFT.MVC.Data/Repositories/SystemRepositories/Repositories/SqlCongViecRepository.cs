﻿using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using System.Text;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories
{
    public class SqlCongViecRepository : RepositoryBase<HT_CongViec>, ICongViecRepository
    {
        public SqlCongViecRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public IEnumerable<HT_CongViec> GetCategoryBy(string username)
        {
            var sql = from cv in DbContext.CongViecVaVuViecs
                      join hnu in DbContext.NhomUsers on cv.UserGroupCode equals hnu.Code
                      join hcv in DbContext.CongViecs on cv.CategoryCode equals hcv.Code
                      join hutn in DbContext.UserThuocNhom on hnu.Code equals hutn.UserGroupCode
                      join hu in DbContext.Users on hutn.UserID equals hu.ID
                      where hcv.OnMenu && hu.Username.ToLower().Equals(username.ToLower())
                      orderby hcv.Priority
                      select hcv;
            return sql.Distinct();
        }
    }

    public class SqlVuViecCuaCongViecRepository : RepositoryBase<HT_VuViecCuaCongViec>, IVuViecCuaCongViecRepository
    {
        public SqlVuViecCuaCongViecRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}