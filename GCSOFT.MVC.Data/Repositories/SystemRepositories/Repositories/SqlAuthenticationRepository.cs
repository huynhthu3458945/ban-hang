﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Data.Infrastructure;
using System.Linq;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories
{
    public class SqlAuthenticationRepository : RepositoryBase<HT_CongViecVaVuViec>, IAuthenticationRepository
    {
        public SqlAuthenticationRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        {

        }

        public IEnumerable<Permission> FindAll()
        {
            var sql = from p in DbContext.CongViecVaVuViecs
                      join s in DbContext.CongViecs on p.CategoryCode equals s.Code
                      select new Permission
                      {
                          CategoryNo = s.Code,
                          CategoryName = s.Name,
                          ParentCategory = s.ParentCode,
                          GroupCode = p.UserGroupCode
                      };
            return sql.Distinct().AsEnumerable();
        }

        public IEnumerable<PermissionOnSystemFunc> GetAllPermissionOnSystemFunc(string categoryCode, string groupCode)
        {
            var sql = from a in DbContext.VuViecCuaCongViecs
                      join r in DbContext.VuViecs on a.FunctionCode equals r.Code
                      join b in DbContext.CongViecVaVuViecs
                      on new { a.CategoryCode, a.FunctionCode } equals new { b.CategoryCode, b.FunctionCode } into leftJoin
                      from c in leftJoin.Where(d => d.UserGroupCode.Equals(groupCode)).DefaultIfEmpty()
                      where a.CategoryCode.Equals(categoryCode)
                      orderby a.FunctionCode
                      select new PermissionOnSystemFunc
                      {
                          CategoryNo = a.CategoryCode,
                          FunctionNo = a.FunctionCode,
                          FunctionName = r.Name,
                          Permission = c.FunctionCode != null ? true : false
                      };
            return sql;
        }
    }
}
