﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.ApprovalRepositories.Interface;
using GCSOFT.MVC.Model.Approval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.ApprovalRepositories.Repositories
{
    public class SqlFlowRepository : RepositoryBase<M_Flow>, IFlowRepository
    {
        public SqlFlowRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
