﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface
{
    public interface ITicketRepository : IRepository<M_Ticket>
    {

    }
}
