﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.Videos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.VideosRepositories.Repositories
{
    public class SqlMemoVideosRepository : RepositoryBase<M_MemoVideos>, IMemoVideosRepository
    {
        public SqlMemoVideosRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
