﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface
{
    public interface IUploadVideoRepository : IRepository<R_UploadVideo>
    {

    }
}
