﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.QuickQuizRepositories.Interface;
using GCSOFT.MVC.Model.QuickQuiz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.QuickQuizRepositories.Repositories
{
    public class SqlQRQuestionRepository : RepositoryBase<QR_Question>, IQRQuestionRepository
    {
        public SqlQRQuestionRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
    public class SqlQRQuestionAnswerRepository : RepositoryBase<QR_Question_Answer>, IQRQuestionAnswerRepository
    {
        public SqlQRQuestionAnswerRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
