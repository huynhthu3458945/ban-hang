﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlPotentialCustomerRepository : RepositoryBase<M_PotentialCustomer>, IPotentialCustomerRepository
    {
        public SqlPotentialCustomerRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
