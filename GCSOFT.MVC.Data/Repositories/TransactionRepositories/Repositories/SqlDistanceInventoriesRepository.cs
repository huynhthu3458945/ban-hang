﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;

namespace GCSOFT.MVC.Data.Repositories.TransactionRepositories.Repositories
{
    public class SqlDistanceInventoriesRepository : RepositoryBase<M_DistanceInventory>, IDistanceInventoriesRepository
    {
        public SqlDistanceInventoriesRepository(IDbFactory databaseFactory)
             : base(databaseFactory)
        { }
    }
}
