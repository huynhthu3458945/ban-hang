﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlCourseRepository : RepositoryBase<E_Course>, ICourseRepository
    {
        public SqlCourseRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
