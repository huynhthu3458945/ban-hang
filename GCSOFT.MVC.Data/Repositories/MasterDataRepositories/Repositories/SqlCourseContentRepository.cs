﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlCourseContentRepository : RepositoryBase<E_CourseContent>, ICourseContentRepository
    {
        public SqlCourseContentRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
    public class SqlSubEcourceContentRepository : RepositoryBase<E_SubEcourceContent>, ISubEcourceContentRepository
    {
        public SqlSubEcourceContentRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
