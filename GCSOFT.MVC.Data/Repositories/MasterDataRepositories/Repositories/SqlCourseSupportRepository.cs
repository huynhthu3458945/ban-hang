﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlCourseSupportRepository : RepositoryBase<E_CourseSupport>, ICourseSupportRepository
    {
        public SqlCourseSupportRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
