﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlTeacherRepository : RepositoryBase<E_Teacher>, ITeacherRepository
    {
        public SqlTeacherRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}