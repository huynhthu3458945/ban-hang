﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlOptionLineRepository : RepositoryBase<M_OptionLine>, IOptionLineRepository
    {
        public SqlOptionLineRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}