﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface
{
    public interface ICourseContentRepository : IRepository<E_CourseContent>
    {
    }
    public interface ISubEcourceContentRepository : IRepository<E_SubEcourceContent>
    {
    }
}
