﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Enum
{
    public class EnumStatusCode
    {
        public const string Incomplete = "Incomplete";//chưa hoàn thành chỉ mới đăng ký
        public const string PayPending = "PayPending"; // thanh toán thành công
        public const string PaySuccess = "PaySuccess"; // thanh toán thành công
        public const string PayFaile = "PayFaile";// thanh toán thất bại or hủy
    }
}
