﻿using GCSOFT.MVC.Model.MasterData;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.MasterDataConfig
{
    public class OptionTypeConfig : EntityTypeConfiguration<M_OptionType>
    {
        public OptionTypeConfig()
        {
            ToTable("M_OptionType");
            Property(p => p.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(p => p.Code).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.Name).HasMaxLength(80);
        }
    }
}