﻿using GCSOFT.MVC.Model.MasterData;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.MasterDataConfig
{
    public class ModelTemplateConfig : EntityTypeConfiguration<M_ModelTemplate>
    {
        public ModelTemplateConfig()
        {
            ToTable("M_ModelTemplate");
            Property(p => p.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(p => p.Name).HasMaxLength(80);
        }
    }
}