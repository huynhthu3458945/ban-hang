﻿using GCSOFT.MVC.Model.SystemModels;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.SystemConfig
{
    public class UserThuocNhomConfig : EntityTypeConfiguration<HT_UserThuocNhom>
    {
        public UserThuocNhomConfig()
        {
            ToTable("Sys_UserAndGroup");
            Property(p => p.UserGroupCode).HasMaxLength(20).HasColumnType("varchar");
        }
    }
}