﻿using GCSOFT.MVC.Model.SystemModels;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.SystemConfig
{
    public class CongViecConfig : EntityTypeConfiguration<HT_CongViec>
    {
        public CongViecConfig()
        {
            ToTable("Sys_Category");
            Property(p => p.Code).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.Name).HasMaxLength(100);
            Property(p => p.Link).HasMaxLength(500);
            Property(p => p.AreaName).HasMaxLength(40).HasColumnType("varchar");
            Property(p => p.ControllerName).HasMaxLength(40).HasColumnType("varchar");
            Property(p => p.ActionName).HasMaxLength(40).HasColumnType("varchar");
            Property(p => p.Description).HasMaxLength(100);
            Property(p => p.ParentCode).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.ParentName).HasMaxLength(100);
            Property(p => p.ParentPriority).HasMaxLength(10).HasColumnType("varchar");
            Property(p => p.ParentPriorityLink).HasMaxLength(20).HasColumnType("varchar");
        }
    }
}
