﻿using GCSOFT.MVC.Model.SystemModels;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.SystemConfig
{
    public class UsersConfig : EntityTypeConfiguration<HT_Users>
    {
        public UsersConfig()
        {
            ToTable("Sys_User");
            Property(p => p.Username).HasMaxLength(50);
            Property(p => p.Password).HasMaxLength(200);
            Property(p => p.Email).HasMaxLength(100);
            Property(p => p.Phone).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.Description).HasMaxLength(200);
            Property(p => p.Position).HasMaxLength(50);
            Property(p => p.FirstName).HasMaxLength(30);
            Property(p => p.LastName).HasMaxLength(30);
            Property(p => p.Address).HasMaxLength(500);
        }
    }
}