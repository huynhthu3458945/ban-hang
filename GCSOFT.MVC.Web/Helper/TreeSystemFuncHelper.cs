﻿using GCSOFT.MVC.Model.SystemModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GCSOFT.MVC.Web.Helper
{
    public static class TreeSystemFuncHelper
    {
        private static StringBuilder buildTree = null;
        private static List<Permission> _permissions = null;
        private static List<HT_CongViec> _congViecs = null;
        private static string url = null;

        #region -- Full Function --
        public static StringBuilder BuildTreeSystemFunc(IEnumerable<HT_CongViec> congViecs)
        {
            _congViecs = congViecs.ToList();
            var parents = _congViecs.Where(d => string.IsNullOrEmpty(d.ParentCode));
            buildTree = new StringBuilder();
            buildTree.Append("<ul id=\"navigation1\">");
            // lay con cap 1                
            foreach (var item in parents)
            {
                var childs = _congViecs.Where(k => k.ParentCode == item.Code);
                if (childs.Count() > 0)
                {
                    buildTree.Append("<li><a style=\"font-weight:bold\" alt=\"" + item.Code + "\" href=\"javascript:void(0);\">" + item.Name + "</a>");
                    GetChildRecursive(childs);
                }
                else
                    buildTree.Append("<li><a style=\"font-weight:bold\" alt=\"" + item.Code + "\" href=\"javascript:void(0);\">" + item.Name + "</a>");

                buildTree.Append("</li>");
            }
            buildTree.Append("</ul>");

            return buildTree;
        }

        // lay con cap 2...n
        private static void GetChildRecursive(IEnumerable<HT_CongViec> childs)
        {
            buildTree.Append("<ul>");
            foreach (var item in childs)
            {
                var child1 = _congViecs.Where(d => d.ParentCode == item.Code);
                if (child1.Count() > 0)
                {
                    buildTree.Append("<li><a style=\"font-weight:bold\" href=\"javascript:void(0);\" alt=\"" + item.Code + "\">" + item.Name + "</a>");
                    GetChildRecursive(child1);
                }
                else
                    buildTree.Append("<li><a href=\"javascript:void(0);\" alt=\"" + item.Code + "\">" + item.Name + "</a>");
                buildTree.Append("</li>");
            }
            buildTree.Append("</ul>");
        }
        #endregion

        #region -- Cong viec cua nhom --
        public static StringBuilder BuildTreeCongViecOfNhom(IEnumerable<Permission> permissions)
        {
            _permissions = permissions.ToList();
            var parents = _permissions.Where(d => string.IsNullOrEmpty(d.ParentCategory));
            buildTree = new StringBuilder();
            buildTree.Append("<ul id=\"navigation1\">");
            // lay con cap 1        
            foreach (var item in parents)
            {
                var child = _permissions.Where(k => k.ParentCategory == item.CategoryNo);
                if (child.Count() > 0)
                {
                    buildTree.Append("<li><a style=\"font-weight:bold\" alt=\"" + item.CategoryNo + "\" href=\"javascript:void(0);\">" + item.CategoryName + "</a>");
                    GetChildRecursive(child, item.CategoryNo);
                }
                else
                    buildTree.Append("<li><a style=\"font-weight:bold\" alt=\"" + item.CategoryNo + "\" href=\"javascript:void(0);\">" + item.CategoryName + "</a>");
                buildTree.Append("</li>");
            }
            buildTree.Append("</ul>");
            return buildTree;
        }

        private static void GetChildRecursive(IEnumerable<Permission> childs, string p)
        {
            buildTree.Append("<ul>");
            foreach (var item in childs)
            {
                var child1 = _permissions.Where(d => d.ParentCategory == item.CategoryNo);
                if (child1.Count() > 0)
                {
                    buildTree.Append("<li><a style=\"font-weight:bold\" href=\"javascript:void(0);\" alt=\"" + item.CategoryNo + "\">" + item.CategoryName + "</a>");
                    GetChildRecursive(child1, item.CategoryNo);
                }
                else
                    buildTree.Append("<li><a href=\"javascript:void(0);\" alt=\"" + item.CategoryNo + "\">" + item.CategoryName + "</a>");
                buildTree.Append("</li>");

            }
            buildTree.Append("</ul>");
        }
        #endregion
    }
}