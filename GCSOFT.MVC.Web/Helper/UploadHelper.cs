﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace GCSOFT.MVC.Web.Helper
{
    public class UploadHelper
    {
        public string PathSave { get; set; }
        public string FileName { get; set; }
        public string FileSave { get; set; }

        public void UploadFile(HttpPostedFileBase file)
        {
            if (file == null)
                return;
            if (file.ContentLength > 0)
            {
                //OK
                string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");
                FileName = Path.GetFileName(file.FileName);
                FileSave = Path.GetFileNameWithoutExtension(file.FileName) + "_" + dateNow + Path.GetExtension(file.FileName);
                if (!Directory.Exists(PathSave))
                    Directory.CreateDirectory(PathSave);
                file.SaveAs(Path.Combine(PathSave, FileSave));
            }
        }

        public void DeleteFileOnServer(string filePath, string fileName)
        {
            try
            {
                FileInfo file = new FileInfo(filePath + fileName);
                if (file.Exists)
                {
                    file.Delete();
                }
            }
            catch
            {
                //GỠ BỎ THUỘC TÍNH READ-ONLY CỦA FILE
                var psi = new ProcessStartInfo("cmd.exe")
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    RedirectStandardError = true,
                    WorkingDirectory = @filePath
                };

                //CHẠY TIẾN TRÌNH CMD
                var proccess = Process.Start(psi);

                //VIẾT DÒNG LỆNH VÀO CMD VÀ THỰC THI
                var streamWriter = proccess.StandardInput;
                streamWriter.WriteLine("attrib -a -h -r -s \"" + @filePath + "/" + fileName + "\"");
                streamWriter.WriteLine("exit");
                proccess.Close();
                streamWriter.Close();

                System.Threading.Thread.Sleep(1000);
                System.IO.File.Delete(@filePath + fileName);
            }
        }
    }
}