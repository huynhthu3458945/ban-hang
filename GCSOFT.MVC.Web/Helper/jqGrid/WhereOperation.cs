﻿namespace GCSOFT.MVC.Web.Helper
{
    /// <summary>
    /// The supported operations in where-extension
    /// </summary>
    public enum WhereOperation
    {
        [StringValue("eq")]
        Equal, //tìm kiếm chính xác
        
        [StringValue("ne")]
        NotEqual,  //không chứa từ
        
        [StringValue("lt")]
        Less,
        
        [StringValue("le")]
        LessEqual,
        
        [StringValue("gt")]
        Greater,
        
        [StringValue("ge")]
        GreaterOrEqual,
        
        [StringValue("bw")] BeginsWith,
        //[StringValue("bn")] NotBeginsWith,
        //[StringValue("in")] IsIn,
        //[StringValue("ni")] IsNotIn,
        [StringValue("ew")] EndsWith,
        //[StringValue("en")] NotEndsWith,
        
        [StringValue("cn")]
        Contains //có chứa từ
        //[StringValue("nc")] NotContains
    }
    /// <summary>
    /// The structure used by the new extension method
    /// </summary>
    public struct SearchCriteria
    {
        public string Column;
        public object Value;
        public WhereOperation Operation;
        public bool CaseSensitive;
    }

    public enum GroupOperator
    {
        AND, //AND
        OR
    }
}