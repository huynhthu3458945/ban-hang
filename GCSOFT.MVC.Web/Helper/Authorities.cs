﻿namespace GCSOFT.MVC.Web.Helper
{
    public class Authorities
    {
        public const string View = "001";
        public const string Add = "002";
        public const string Edit = "003";
        public const string Del = "004";
        public const string ViewDetail = "005";
        public const string Print = "006";
        public const string ExportExcel = "007";
        public const string Access = "008";
        public const string ChangePassword = "009";
    }
}