﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Web.Areas.MasterData.Data;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;

namespace GCSOFT.MVC.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<UserLoginVMs, HT_Users>();
            Mapper.CreateMap<UserVM, HT_Users>();
            Mapper.CreateMap<CategoryUser, HT_CongViec>();
            Mapper.CreateMap<CategoryListVMs, HT_CongViec>();
            Mapper.CreateMap<CategoryCard, HT_CongViec>();
            Mapper.CreateMap<CategoryFunctionVMs, HT_VuViecCuaCongViec>();
            Mapper.CreateMap<NhomUsers, HT_NhomUser>();
            Mapper.CreateMap<NhomUserCard, HT_NhomUser>();
            Mapper.CreateMap<UserThuocNhomVM, HT_UserThuocNhom>();
            Mapper.CreateMap<UserList, HT_Users>();
            Mapper.CreateMap<UserCard, HT_Users>();
            Mapper.CreateMap<VM_OptionType, M_OptionType>();
            Mapper.CreateMap<VM_OptionLine, M_OptionLine>();
            Mapper.CreateMap<VM_Category, E_Category>();
            Mapper.CreateMap<VM_Category_Index, E_Category>();
            Mapper.CreateMap<CategoryMenu, E_Category>();
            Mapper.CreateMap<TeacherList, E_Teacher>();
            Mapper.CreateMap<TeacherCard, E_Teacher>();
            Mapper.CreateMap<CourseList, E_Course>();
            Mapper.CreateMap<CourseCard, E_Course>();
            Mapper.CreateMap<CourseContentLine, E_CourseContent>();
            Mapper.CreateMap<CourseContentCard, E_CourseContent>();
            Mapper.CreateMap<CourseContentReview, E_CourseContent>();
            Mapper.CreateMap<CourseSupportLine, E_CourseSupport>();
            Mapper.CreateMap<CourseCategoryLine, E_CourseCategory>();
            Mapper.CreateMap<CourseResourceLine, E_CourseResource>();

            Mapper.CreateMap<VuViecVMs, HT_VuViec>();
            Mapper.CreateMap<CategoryListVMs, SP_SYS_Categroy_Tree>();

            Mapper.CreateMap<VM_CategoryProduct, CategoriesProduct>();

        }
    }
}