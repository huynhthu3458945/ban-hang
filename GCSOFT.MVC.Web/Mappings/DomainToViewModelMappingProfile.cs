﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Web.Areas.MasterData.Data;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;

namespace GCSOFT.MVC.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<HT_Users, UserLoginVMs>();
            Mapper.CreateMap<HT_Users, UserVM>();
            Mapper.CreateMap<HT_VuViec, VuViecVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryUser>();
            Mapper.CreateMap<HT_CongViec, CategoryListVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryCard>();
            Mapper.CreateMap<HT_VuViecCuaCongViec, CategoryFunctionVMs>();
            //-- Nhom User
            Mapper.CreateMap<HT_NhomUser, NhomUsers>();
            Mapper.CreateMap<HT_NhomUser, NhomUserCard>();
            Mapper.CreateMap<HT_UserThuocNhom, UserThuocNhomVM>();
            //-- Nhom User End
            //-- User
            Mapper.CreateMap<HT_Users, UserList>();
            Mapper.CreateMap<HT_Users, UserCard>();
            //-- User End

            Mapper.CreateMap<SP_SYS_Categroy_Tree, CategoryListVMs>();
            Mapper.CreateMap<CategoriesProduct, VM_CategoryProduct>();
        }
    }
}