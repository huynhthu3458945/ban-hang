﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Web.ViewModels.HienTai;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Reflection;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Extension;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
	[CompressResponseAttribute]
    public class SysUserController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly IUserService _userService;
        private readonly IUserGroupService _userGroupService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IAgencyService _agenctyService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        private string sysCategory = "U";
        private bool? permission = false;
        private UserCard userCard;
        private HT_Users user;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public SysUserController
            (
				IVuViecService vuViecService
                , IUserService userService
                , IUserGroupService userGroupService
                , IUserThuocNhomService userThuocNhomService
                , IAgencyService agenctyService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _userService = userService;
            _userGroupService = userGroupService;
            _userThuocNhomService = userThuocNhomService;
            _agenctyService = agenctyService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
		#endregion
		
		#region -- Get --
		public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
		
		public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            userCard = new UserCard();
            return View(userCard);
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            user = _userService.GetBy(id);
            userCard = Mapper.Map<UserCard>(user);
            userCard.Password2 = CryptorEngine.Decrypt(userCard.Password, true, "Hieu.Le-GC.Soft");
            if (userCard == null) return HttpNotFound();
            foreach (var item in userCard.UserOfGroups)
            {
                item.NhomUserInfo = Mapper.Map<NhomUsers>(_userGroupService.GetBy2(item.UserGroupCode));
            }
            return View(userCard);
        }
		
		public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            user = _userService.GetBy(id);
            userCard = Mapper.Map<UserCard>(user);
            if (userCard == null) return HttpNotFound();
            foreach (var item in userCard.UserOfGroups)
            {
                item.NhomUserInfo = Mapper.Map<NhomUsers>(_userGroupService.GetBy2(item.UserGroupCode));
            }
            return View(userCard);
        }

        public ActionResult OpenPopupUser()
        {
            return PartialView("_PopupUser");
        }

        public ActionResult ChangePassword(int? id)
        {
            if (id == null)
                return PartialView("_ChangePassword", (UserLoginVMs)Session["User"]);
            var user = Mapper.Map<UserLoginVMs>(_userService.GetBy(id ?? 0));
            return PartialView("_ChangePassword", user);
        }
        #endregion

        #region -- Post --
        [HttpPost]
        public ActionResult Create(UserCard userCard2)
        {
            try
            {
                string pass = userCard2.Password;
                userCard2.ID = _userService.GetID();
                userCard2.Password = CryptorEngine.Encrypt(userCard2.Password, true, "Hieu.Le-GC.Soft");
                SetData(userCard2);
                hasValue = _userService.Insert(user);
                
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = user.ID, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
		
		 [HttpPost]
        public ActionResult Edit(UserCard userCard2)
        {
            try
            {
                SetData(userCard2);
                hasValue = _userService.Update(user);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { area = "administrator", id = userCard2.ID, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(UserLoginVMs userVM)
        {
            var user = _userService.GetBy(userVM.ID);
            user.Password = CryptorEngine.Encrypt(userVM.Password, true, "Hieu.Le-GC.Soft");
            return Json(_userService.ChangePassword(user));
        }
        #endregion

        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<UserList>>(_userService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<UserList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
		
		 /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        public JsonResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _ids = id.Split(',').Select(item => int.Parse(item)).ToArray();
                string hasValue = _userService.Delete(_ids);
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _ids.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }

        /// <summary>
        /// Kiểm tra Username đã tồn tại
        /// </summary>
        /// <param name="u"></param>
        /// <param name="xu"></param>
        /// <returns></returns>
        public JsonResult CandAdd(string u, string xu)
        {
            return Json(_userService.CandAdd(u, xu));
        }
        #endregion

        #region -- Functions --
        private void SetData(UserCard userCard2)
        {
            userCard2.FirstName = StringUtil.UppercaseWords(userCard2.FirstName.Trim());
            userCard2.LastName = StringUtil.UppercaseWords(userCard2.LastName.Trim());
            userCard2.FullName = userCard2.FirstName + " " + userCard2.LastName;
            foreach (var item in userCard2.UserOfGroups)
            {
                item.UserID = userCard2.ID;
            }
            user = Mapper.Map<HT_Users>(userCard2);
        }
       
        #endregion
    }
}
