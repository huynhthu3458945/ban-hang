﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
    [CompressResponseAttribute]
    public class SysUserGroupController : BaseController
    {
		#region -- Properties --
		private readonly IUserGroupService _userGrpServices;
        private readonly IUserService _userService;
        private readonly IVuViecService _vuViecService;
        private string sysCategory = "UG";
        private bool? permission = false;
        private HT_NhomUser nhomUser;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public SysUserGroupController
            (
				IVuViecService vuViecService
                , IUserGroupService userGrpServices
                , IUserService userService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _userGrpServices = userGrpServices;
            _userService = userService;
        }
		#endregion
		
		#region -- Get --
		public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

        public ActionResult PopupIndex()
        {
            return PartialView("_PopupIndex");
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View(new NhomUserCard());
        }
		
		public ActionResult Edit(string id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userGrpCard = Mapper.Map<NhomUserCard>(_userGrpServices.GetBy(id));
            if (userGrpCard == null)
				return HttpNotFound();
            UserList userVM = new UserList();
            foreach (var item in userGrpCard.UserOfGroups)
            {
                userVM = Mapper.Map<UserList>(_userService.GetBy(item.UserID));
                item.FirstName = userVM.FirstName;
                item.LastName = userVM.LastName;
                item.Email = userVM.Email;
            }
            return View(userGrpCard);
        }
		
		public ActionResult Details(string id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userGrpCard = Mapper.Map<NhomUserCard>(_userGrpServices.GetBy(id));
            if (userGrpCard == null)
                return HttpNotFound();
            UserList userVM = new UserList();
            foreach (var item in userGrpCard.UserOfGroups)
            {
                userVM = Mapper.Map<UserList>(_userService.GetBy(item.UserID));
                item.FirstName = userVM.FirstName;
                item.LastName = userVM.LastName;
                item.Email = userVM.Email;
            }
            return View(userGrpCard);
        }
		#endregion
		
		#region -- Post --
		[HttpPost]
        public ActionResult Create(NhomUserCard userGrpCard)
        {
            try
            {
                SetData(userGrpCard);
                hasValue = _userGrpServices.Insert(nhomUser);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = userGrpCard.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
		
		 [HttpPost]
        public ActionResult Edit(NhomUserCard userGrpCard)
        {
            try
            {
                SetData(userGrpCard);
                hasValue = _userGrpServices.Update(nhomUser);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { area = "administrator", id = userGrpCard.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
		#endregion
		
		#region -- Json --
		public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<NhomUsers>>(_userGrpServices.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<NhomUsers>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
		
		/// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        public JsonResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                string[] _codes = id.Split(',').Select(item => item).ToArray();
                string hasValue = _userGrpServices.Delete(_codes);
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        /// <summary>
        /// Kiem tra trung ma Usert Group
        /// </summary>
        /// <param name="c">Code</param>
        /// <param name="xc">xCode</param>
        /// <returns></returns>
        public JsonResult CandAdd(string c, string xc)
        {
            return Json(_userGrpServices.CanAdd(c, xc));
        }
        #endregion

        #region -- Functions --
        private void SetData(NhomUserCard userGrpCard)
        {
            foreach (var item in userGrpCard.UserOfGroups)
            {
                item.UserGroupCode = userGrpCard.Code;
            }
            nhomUser = Mapper.Map<HT_NhomUser>(userGrpCard);
        }
		#endregion
    }
}