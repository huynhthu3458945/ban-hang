﻿using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Administrator
{
    public class AdministratorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Administrator";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Administrator_default",
                "Administrator/{controller}/{action}/{id}",
                new { Controller = "Login", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}