﻿using AutoMapper;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GCSOFT.MVC.Web.Areas.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.MasterData.Data;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    public class CategoryProductController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ICategoryProductService _categoryProductService;
        private string sysCategory = "CATEGORYPRODUCT";
        private bool? permission = false;
        private string hasValue;
        private VM_CategoryProduct vm_CategoryProduct;
        private CategoriesProduct m_CategoryProduct;
        #endregion
        #region -- Contructor --
        public CategoryProductController(IVuViecService vuViecService,
           ICategoryProductService categoryProductService
            ) : base(vuViecService)
        {

            _vuViecService = vuViecService;
            _categoryProductService = categoryProductService;
        }
        #endregion

        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_CategoryProduct = Mapper.Map<VM_CategoryProduct>(_categoryProductService.GetById(id ?? 0));
            if (vm_CategoryProduct == null)
                return HttpNotFound();
            return View(vm_CategoryProduct);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_CategoryProduct = new VM_CategoryProduct();
            return View(vm_CategoryProduct);
        }

        [HttpPost]
        public ActionResult Create(VM_CategoryProduct _vm_CategoryProduct)
        {
            try
            {
                SetData(_vm_CategoryProduct, true);
                hasValue = _categoryProductService.Insert(m_CategoryProduct);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = m_CategoryProduct.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_CategoryProduct = Mapper.Map<VM_CategoryProduct>(_categoryProductService.GetById(id ?? 0));
            if (vm_CategoryProduct == null)
                return HttpNotFound();
            return View(vm_CategoryProduct);
        }

        [HttpPost]
        public ActionResult Edit(VM_CategoryProduct model)
        {
            try
            {
                SetData(model, false);
                hasValue = _categoryProductService.Update(m_CategoryProduct);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = model.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Delete(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _categoryProductService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_CategoryProduct>>(_categoryProductService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<VM_CategoryProduct>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region -- Functions --
        private void SetData(VM_CategoryProduct model, bool isCreate)
        {
            vm_CategoryProduct = model;
            //if (isCreate)
            //    vm_CategoryProduct.ID = _optiontypeService.GetID();
            m_CategoryProduct = Mapper.Map<CategoriesProduct>(model);
        }
        #endregion
    }
}