﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Data
{
    public class VM_CategoryProduct
    {
       
        public int Id { get; set; }
        [DisplayName("Tên danh mục sản phẩm")]
        public string Name { get; set; }
        [AllowHtml]
        [DisplayName("Lợi ích")]
        public string Benefit { get; set; }
        [DisplayName("Thứ tự")]
        public int SortOrder { get; set; }
        [DisplayName("Ẩn")]
        public bool Status { get; set; }
        [DisplayName("SEO Tiêu Đề")]
        public string Title_SEO { get; set; }
        [DisplayName("SEO Từ Khóa")]
        public string KeyWord_SEO { get; set; }
        [DisplayName("SEO Nội Dung")]
        public string Description_SEO { get; set; }
    }
}