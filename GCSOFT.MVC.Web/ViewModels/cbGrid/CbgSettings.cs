﻿using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.jqGrid.cbGrid
{
    [ModelBinder(typeof(ComboboxModelBinder))]
    public class CbgSettings
    {
        public double page { get; set; }
        public double limit { get; set; }
        public string sidx { get; set; }
        public string sord { get; set; }
        public string searchTerm { get; set; }
    }

    public class CbGridData
    {
        public double page { get; set; }
        public double total { get; set; }
        public double records { get; set; }
        public object rows { get; set; }
    }
}