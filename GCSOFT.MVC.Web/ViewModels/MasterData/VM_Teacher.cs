﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.MasterData
{
    public class TeacherList
    {
        public int Id { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Điện Thoại")]
        public string PhoneNo { get; set; }
        [DisplayName("Học Vị")]
        public string Position { get; set; }
        [DisplayName("Lớp Trường")]
        public string SchoolName { get; set; }
        [DisplayName("Hình Ảnh")]
        public string Image { get; set; }
    }
    public class TeacherCard
    {
        public int Id { get; set; }
        [DisplayName("Tên")]
        public string FirstName { get; set; }
        [DisplayName("Họ")]
        public string LastName { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Tên (EN)")]
        public string FirstNameEN { get; set; }
        [DisplayName("Họ (EN)")]
        public string LastNameEN { get; set; }
        [DisplayName("Họ Tên (EN)")]
        public string FullNameEN { get; set; }
        [DisplayName("Điện Thoại")]
        public string PhoneNo { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Mô Tả Giáo Viên")]
        [AllowHtml]
        public string Remark { get; set; }
        [DisplayName("Hình Ảnh (*.jpg W:450 x H:450)px")]
        public string Image { get; set; }
        public int SortOrder { get; set; }
        [DisplayName("Học Vị")]
        public string Position { get; set; }
        [DisplayName("Cấp (*)")]
        public int LevelId { get; set; }
        [DisplayName("Cấp (*)")]
        public string LevelCode { get; set; }
        [DisplayName("Lớp Trường (*)")]
        public string SchoolName { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public string NewPhoneNo
        {
            get { return RebuildPhoneNo(PhoneNo); }
        }
        private string RebuildPhoneNo(string phoneNo)
        {
            string newNumber = string.Empty;
            string[] numbers = Regex.Split(phoneNo, @"\D+");
            foreach (string value in numbers)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    newNumber += value;
                }
            }
            return newNumber;
        }
    }
}