﻿using System.Collections.Generic;
using System.ComponentModel;

namespace GCSOFT.MVC.Web.ViewModels.MasterData
{
    public class VM_OptionType
    {
        [DisplayName("Mã")]
        public int ID { get; set; }
        [DisplayName("Mã")]
        public string Code { get; set; }
        [DisplayName("Tên")]
        public string Name { get; set; }
        [DisplayName("Ghi Chú")]
        public string Description { get; set; }
        public virtual IEnumerable<VM_OptionLine> Optionlines { get; set; }
        public VM_OptionType()
        {
            Optionlines = new List<VM_OptionLine>();
        }
    }
}