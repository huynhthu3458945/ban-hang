﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.ViewModels.HienTai
{
    public class O_TeacherInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int? TeacherType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string xUserName { get; set; }
        public DateTime WorkDate { get; set; }
        public int? ParentTeacherId { get; set; }
        public int AgencyId { get; set; }
        public string RoleCode { get; set; }
        public string xRoleCode { get; set; }
        public bool HasAccount { get; set; }
        public int NoOfOutlier { get; set; }
    }
}