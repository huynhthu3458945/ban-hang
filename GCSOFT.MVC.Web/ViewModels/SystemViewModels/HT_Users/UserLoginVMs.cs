﻿using System.Collections.Generic;
using System.ComponentModel;

namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class UserLoginVMs
    {
        public int ID { get; set; }
        [DisplayName("Tên đăng nhập")]
        public string Username { get; set; }
        [DisplayName("Tên người sử dụng")]
        public string FullName { get; set; }
        [DisplayName("Mật khẩu")]
        public string Password { get; set; }
        [DisplayName("Nhập lại mật khẩu")]
        public string RePassword { get; set; }
        public string Email { get; set; }
        //public List<CategoryUser> categoryUserList { get; set; }
    }
}