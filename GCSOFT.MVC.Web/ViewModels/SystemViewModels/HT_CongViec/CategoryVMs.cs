﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class CategoryListVMs
    {
        [DisplayName("Mã Công Việc")]
        [ForeignKey("Name")]
        public string Code { get; set; }
        [DisplayName("Tên Công Việc")]
        public string Name { get; set; }
        [DisplayName("Mã Công Việc Cha")]
        public string ParentName { get; set; }
        [DisplayName("Liên Kết")]
        public string Link { get; set; }
        [Key]
        public bool OnMenu { get; set; }
        [DisplayName("Ẩn/Hiện")]
        public string OnMenuStr
        {
            get
            {
                if (!OnMenu)
                    return "<a name=\"btnShow\" href=\"javascript: void(0);\" data-uk-tooltip=\"{ pos: 'left'}\" title=\"Ẩn\"><i class=\"md-icon material-icons uk-text-danger\">&#xE8F5;</i></a>";
                return "<a name=\"btnShow\" href=\"javascript: void(0);\" data-uk-tooltip=\"{ pos: 'left'}\" title=\"Hiện\"><i class=\"md-icon material-icons uk-text-primary\">&#xE8F4;</i></a>";
            }
        }
        [DisplayName("Độ Ưu Tiên")]
        public int Priority { get; set; }
        [Key]
        public string ParentPriorityLink { get; set; }
    }

    public class CategoryCard
    {
        [DisplayName("Mã Công Việc (*)")]
        public string Code { get; set; }
        [Key]
        public string xCode { get; set; }
        [DisplayName("Tên Công Việc (*)")]
        public string Name { get; set; }
        [DisplayName("Area Name")]
        public string AreaName { get; set; }
        [DisplayName("Controller Name")]
        public string ControllerName { get; set; }
        [DisplayName("Action Name")]
        public string ActionName { get; set; }
        [DisplayName("Mã Công Việc Cha")]
        public string ParentCode { get; set; }
        public string xParentCode { get; set; }
        public string ParentName { get; set; }
        public IEnumerable<SelectListItem> Parents { get; set; } //Load parents
        [DisplayName("Liên Kết")]
        public string Link { get; set; }
        [DisplayName("Ẩn/Hiện")]
        public bool OnMenu { get; set; }
        [DisplayName("Độ Ưu Tiên")]
        public int Priority { get; set; }
        [DisplayName("Ghi Chú")]
        public string Description { get; set; }
        public string ParentPriority { get; set; }
        public string ParentPriorityLink { get; set; }
        public string ParentPriorityLink2 { get; set; }
        public virtual IEnumerable<CategoryFunctionVMs> VuViecCuaCongViecs { get; set; }
        public virtual IEnumerable<VuViecVMs> Functions { get; set; }
        public CategoryCard()
        {
            OnMenu = true;
            Priority = 0;
            VuViecCuaCongViecs = new List<CategoryFunctionVMs>();
            Functions = new List<VuViecVMs>();
        }
    }

    public class CategoryFunctionVMs
    {
        public string CategoryCode { get; set; }
        public string FunctionCode { get; set; }
    }
}