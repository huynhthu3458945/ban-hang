﻿namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class VuViecVMs
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Check { get; set; }
    }
}