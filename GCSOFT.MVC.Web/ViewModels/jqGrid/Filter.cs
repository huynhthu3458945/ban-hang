﻿using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace GCSOFT.MVC.Web.ViewModels.jqGrid
{
    [DataContract]
    public class Filter
    {
        [DataMember]
        public string groupOp { get; set; }
        [DataMember]
        public Rule[] rules { get; set; }

        public static Filter Create(string jsonData)
        {
            try
            {
                var serializer = new DataContractJsonSerializer(typeof(Filter));
                var ms = new System.IO.MemoryStream(Encoding.Unicode.GetBytes(jsonData));
                return serializer.ReadObject(ms) as Filter;
            }
            catch
            {
                return null;
            }
        }
    }
}
