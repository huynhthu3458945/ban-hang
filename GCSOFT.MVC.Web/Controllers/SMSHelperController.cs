﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GCSOFT.MVC.Web.Controllers
{
    public class SMSHelperController : ApiController
    {
        // GET: api/SMSHelper
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SMSHelper/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/SMSHelper
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/SMSHelper/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SMSHelper/5
        public void Delete(int id)
        {
        }
    }
}
