﻿using Autofac;
using Autofac.Integration.Mvc;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories;
using GCSOFT.MVC.Service.SystemService.Service;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Mappings;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.App_Start
{
    public static class Bootstrapper
    {
        public static void Run()
        {
            SetAutofacContainer();
            //Configure AutoMapper
            AutoMapperConfiguration.Configure();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();

            // Repositories
            builder.RegisterAssemblyTypes(typeof(SqlCongViecRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();
            // Services
            builder.RegisterAssemblyTypes(typeof(CongViecService).Assembly)
               .Where(t => t.Name.EndsWith("Service"))
               .AsImplementedInterfaces().InstancePerRequest();

            builder.RegisterType<HttpClientHelper>().InstancePerRequest();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}