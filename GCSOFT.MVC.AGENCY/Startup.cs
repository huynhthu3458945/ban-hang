﻿
using Autofac;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.Owin;
using Owin;
using System;
using System.Threading.Tasks;

[assembly: OwinStartup(typeof(GCSOFT.MVC.AGENCY.Startup))]

namespace GCSOFT.MVC.AGENCY
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseHangfireServer();
        }
    }
}
