﻿using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Web.Helper
{
    public static class MenuHelper
    {
        private static StringBuilder buildTree = null;
        private static List<CategoryUser> categories = null;
        private static string url = null;
        public static StringBuilder BuildMenu(List<CategoryUser> categoryByUser, string parentID)
        {
            categories = categoryByUser;
            buildTree = new StringBuilder();
            buildTree.Append("<ul>");
            // lay con cap 1
            var lst = categoryByUser.Where(d => d.ParentCode != null && d.ParentCode.Equals(parentID)).OrderBy(d => d.Priority);
            foreach (var item in lst.ToList())
            {
                var child = categories.Where(d => d.ParentCode != null && d.ParentCode.Equals(item.Code)).OrderBy(d => d.Priority);

                if (child.Count() > 0)
                {
                    buildTree.Append("<li><a href=\"#\">" + item.Name + "</a>");
                    GetChildRecursive(child.ToList());
                }
                else
                {
                    if (string.IsNullOrEmpty(item.AreaName))
                        url = item.ControllerName + "/" + item.ActionName;
                    else
                        url = item.AreaName + "/" + item.ControllerName + "/" + item.ActionName;
                    buildTree.Append("<li><a href=\"" + GetAppPath() + "/" + url + "\">" + item.Name + "</a>");
                }
                buildTree.Append("</li>");
            }
            buildTree.Append("</ul>");

            return buildTree;
        }

        // lay con cap 2...n
        private static void GetChildRecursive(List<CategoryUser> listChildren)
        {
            buildTree.Append("<ul>");
            foreach (var item in listChildren)
            {
                var child = categories.Where(d => d.ParentCode != null && d.ParentCode == item.Code);
                if (child.Count() > 0)
                {
                    buildTree.Append("<li><a href=\"#\">" + item.Name + "</a>");
                    GetChildRecursive(child.ToList());
                }
                else
                {
                    if (string.IsNullOrEmpty(item.AreaName))
                        url = item.ControllerName + "/" + item.ActionName;
                    else
                        url = item.AreaName + "/" + item.ControllerName + "/" + item.ActionName;
                    buildTree.Append("<li><a href=\"" + GetAppPath() + "/" + url + "\">" + item.Name + "</a>");
                }

                buildTree.Append("</li>");

            }
            buildTree.Append("</ul>");
        }

        // GET: Path cua he thong
        public static string GetAppPath()
        {
            string appPath = System.Web.HttpContext.Current.Request.ApplicationPath;
            if (appPath == string.Empty || appPath.Equals("/")) appPath = "";
            return appPath;
        }
    }
}