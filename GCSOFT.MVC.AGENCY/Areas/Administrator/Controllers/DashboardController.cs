﻿using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers
{
    public class DashboardController : BaseController
    {
        private readonly IVuViecService _vuViecService;
        private readonly IAgencyService _agencyService;
        private readonly IStoreProcedureService _storeService;
        private string connection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        public DashboardController(IVuViecService vuViecService, IAgencyService agencyService, IStoreProcedureService storeService) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _agencyService = agencyService;
            _storeService = storeService;
        }

        // GET: Administrator/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}