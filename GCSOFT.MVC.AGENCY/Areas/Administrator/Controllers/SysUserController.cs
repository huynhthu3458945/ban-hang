﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers
{
    [CompressResponseAttribute]
    public class SysUserController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IUserService _userService;
        private readonly IUserGroupService _userGroupService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IAgencyService _agenctyService;
        private string sysCategory = "U";
        private bool? permission = false;
        private UserCard userCard;
        private HT_Users user;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public SysUserController
            (
                IVuViecService vuViecService
                , IUserService userService
                , IUserGroupService userGroupService
                , IUserThuocNhomService userThuocNhomService
                , IAgencyService agenctyService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _userService = userService;
            _userGroupService = userGroupService;
            _userThuocNhomService = userThuocNhomService;
            _agenctyService = agenctyService;
        }
        #endregion
        public ActionResult OpenPopupUser()
        {
            return PartialView("_PopupUser");
        }

        public ActionResult ChangePassword(int? id)
        {
            if (id == null)
                return PartialView("_ChangePassword", GetUser());
            var user = Mapper.Map<UserLoginVMs>(_userService.GetBy(id ?? 0));
            return PartialView("_ChangePassword", user);
        }

        //public ActionResult InfoAgency()
        //{
        //    var agency = _agenctyService.GetBy(GetUser().agencyInfo.Id);
        //    var model = Mapper.Map<AgencyList>(agency);
        //    return PartialView("_InfoAgency", model);
        //}

       

        public ActionResult ChangePasswordFirtLogin()
        {
            
            var user = Mapper.Map<UserLoginVMs>(_userService.GetBy(GetUser().ID));
            return View("ChangePasswordFirtLogin", user);
        }
        /// <summary>
        /// CheckIsUserUpdate
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/06/2021]
        /// </history>
        public JsonResult CheckIsUserUpdate()
        {
            var user = _userService.GetBy(GetUser().ID);
            DateTime date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            if (!user.LastDateUpdate.HasValue)
            {
                if (!user.IsUpdatePass)
                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!user.IsUpdatePass || DateTime.Compare(user.LastDateUpdate.Value, firstDayOfMonth) == -1)
                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangePassword(UserLoginVMs userVM)
        {
            var user = _userService.GetBy(userVM.ID);
            user.Password = CryptorEngine.Encrypt(userVM.Password, true, "Hieu.Le-GC.Soft");
            user.IsUpdatePass = true;
            user.LastDateUpdate = DateTime.Now.Date;
            return Json(_userService.ChangePassword(user));
        }
        public JsonResult CandAdd(string u, string xu)
        {
            return Json(_userService.CandAdd(u, xu));
        }
    }
}