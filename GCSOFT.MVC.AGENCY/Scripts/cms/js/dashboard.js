$(function() {
    // dashboard init functions
    altair_dashboard.init();
	// run animations after page is fully loaded
	$window.on('load',function(){
		altair_dashboard.count_animated();
	});
});
altair_dashboard = {
	init: function () {
        'use strict';
        // small charts
        altair_dashboard.peity_charts();
    },
	// small charts
    peity_charts: function () {
        $(".peity_orders").peity("donut", {
            height: 24,
            width: 24,
            fill: ["#8bc34a", "#eee"]
        });
        $(".peity_visitors").peity("bar", {
            height: 28,
            width: 48,
            fill: ["#d84315"],
            padding: 0.2
        });
        $(".peity_sale").peity("line", {
            height: 28,
            width: 64,
            fill: "#d1e4f6",
            stroke: "#0288d1"
        });
        $(".peity_conversions_large").peity("bar", {
            height: 64,
            width: 96,
            fill: ["#d84315"],
            padding: 0.2
        });
		$(".peity_live").peity("line", {
            height: 28,
			width: 64,
			fill: "#efebe9",
			stroke: "#5d4037"
        });
    },
	// animated numerical values
    count_animated: function () {
        $('.countUpMe').each(function () {
            var target = this,
                countTo = $(target).text();
            theAnimation = new CountUp(target, 0, countTo, 0, 2);
            theAnimation.start();
        });
    }
}