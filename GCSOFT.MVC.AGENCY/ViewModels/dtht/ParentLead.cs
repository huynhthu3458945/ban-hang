﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.dtht
{
    public class DocumentList
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string DocType { get; set; }
        public string Title { get; set; }
        public string DateSign { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string IdDoc { get; set; }
        public string TitleDoc { get; set; }
        public string Url { get; set; }
    }
    public class ParentLeadList
    {
        public int Id { get; set; }
        public string MainContactName { get; set; }
        public string PrefexName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    public class ParentLeadCard
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        [DisplayName("Loại Liên Hệ (*)")]
        public string MainContactCode { get; set; }
        public string MainContactName { get; set; }
        public IEnumerable<SelectListItem> MainContactList { get; set; }
        [DisplayName("Đại lý tuyến trên (*)")]
        public string UplineAgency { get; set; }
        [DisplayName("Đại lý cấp (*)")]
        public string AgencyLevel { get; set; }
        [DisplayName("Danh xưng (Anh/chị) (*)")]
        public string PrefexCode { get; set; }
        public IEnumerable<SelectListItem> PrefexCodeList { get; set; }
        public string PrefexName { get; set; }
        [DisplayName("Họ và tên phụ huynh (*)")]
        public string FullName { get; set; }
        [DisplayName("Mối quan hệ với Outlier (*)")]
        public string OutlierRelationship { get; set; }
        [DisplayName("Số điện thoại (*)")]
        public string Phone { get; set; }
        [DisplayName("Email phụ huynh (*)")]
        public string Email { get; set; }
        [DisplayName("Ngày Sinh (*)")]
        public int? DateOfBirth { get; set; }
        public IEnumerable<SelectListItem> DateOfBirthList { get; set; }
        [DisplayName("Tháng Sinh (*)")]

        public int? MonthOfBirth { get; set; }
        public IEnumerable<SelectListItem> MonthOfBirthList { get; set; }
        [DisplayName("Năm Sinh (*)")]

        public int? YearOfBirth { get; set; }

        public DateTime? BirthDay { get; set; }
        [DisplayName("Giới Tính (*)")]
        public string GenderCode { get; set; }
        public string GenderName { get; set; }
        [DisplayName("Địa Chỉ (*)")]
        public string Address { get; set; }
        [DisplayName("Quận/Huyện (*)")]
        public int? DistrictId { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
        public string DistrictName { get; set; }
        [DisplayName("Tỉnh/Thành Phố (*)")]
        public int? CityId { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public string CityName { get; set; }
        [DisplayName("Tài Khoản Ngân Hàng (*)")]
        public string AccountNo { get; set; }
        [DisplayName("Tên Ngân Hàng (*)")]
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public IEnumerable<SelectListItem> BankList { get; set; }
        [DisplayName("Chi Nhánh Ngân Hàng (*)")]
        public string BankBranchName { get; set; }
        [DisplayName("Tên Người Thụ Hưởng (*)")]
        public string BeneficiaryName { get; set; }
        [DisplayName("Facebook Link (*)")]
        public string FacebookLink { get; set; }
        [DisplayName("Thời gian thuận tiện trong ngày (*)")]
        public string TimeWorking { get; set; }
        [DisplayName("Nghề Nghiệp (*)")]
        public string JobCode { get; set; }
        public IEnumerable<SelectListItem> JobCodeList { get; set; }
        public string JobName { get; set; }
        [DisplayName("Sở Thích (*)")]
        public string Hobby { get; set; }
        [DisplayName("Tình Trạng Hôn Nhân (*)")]
        public string MaritalStatusCode { get; set; }
        public IEnumerable<SelectListItem> MaritalStatusCodeList { get; set; }
        public string MaritalStatusName { get; set; }
        [DisplayName("Số Con (*)")]
        public int? NoOfChild { get; set; }
        [DisplayName("Chia sẻ một tấm hình gia đình yêu thích (*)")]
        public string FamilyImage { get; set; }

        [DisplayName("Nhận những giá trị (*)")]
        public string Remark { get; set; }

        [DisplayName("Những khó khăn (*)")]
        public string DifficultCodes { get; set; }
        public string[] DifficultCodeArrs { get; set; }
        public IEnumerable<SelectListItem> DifficultCodesList { get; set; }
        public string DifficultName { get; set; }

        [DisplayName("5 tính từ miêu tả cảm xúc (*)")]
        public string Feeling { get; set; }

        [DisplayName("Phần mềm sử dụng (*)")]
        public string SoftwareCodes { get; set; }
        public string[] SoftwareCodeArrs { get; set; }
        public string SoftwareName { get; set; }
        public IEnumerable<SelectListItem> SoftwareCodesList { get; set; }

        [DisplayName("Thời gian tương tác với con (*)")]
        public string TimeForChildrenCode { get; set; }
        public IEnumerable<SelectListItem> TimeForChildrenCodeList { get; set; }

        public string TimeForChildrenName { get; set; }
        [DisplayName("Các chia sẻ thêm của Anh/Chị (nếu có)")]
        public string ShareInfo { get; set; }
        [DisplayName("Quốc Gia (*)")]
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public IEnumerable<SelectListItem> RegionList { get; set; }

    }
}