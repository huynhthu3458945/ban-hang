﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.dtht
{
    public class StudentLeadList
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string CourseName { get; set; }
        public string TimeOfCourseName { get; set; }
    }
    public class StudentLeadCard
    {
        public int Id { get; set; }

        public string ParentIDno { get; set; }
        [DisplayName("Học viên đăng ký khóa (*)")]
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public IEnumerable<SelectListItem> CourseList { get; set; } //HT_Course
        [DisplayName("Thời lượng khóa học (*)")] //Thời lượng khóa học đã đăng ký (tùy thuộc vào chính sách mua hàng và chính sách tặng cho Đối tác Siêu Trí Nhớ Học Đường vào mỗi thời điểm, vui lòng liên hệ nhân viên tư vấn trực tiếp của Tâm Trí Lực để được hỗ trợ xác nhận số năm học của Outlier)
        public string TimeOfCourseCode { get; set; }
        public string TimeOfCourseName { get; set; }
        public IEnumerable<SelectListItem> TimeOfCourseList { get; set; } //HT_TimeOfCourse
        [DisplayName("Họ và tên Outlier (*)")]
        public string FullName { get; set; }
        [DisplayName("Giới Tính (*)")]
        public string GenderCode { get; set; }
        public string GenderName { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; } //Gender
        [DisplayName("Giấy Khai Sinh (*)")]
        public string BirthCertificate { get; set; }
        public string FileUpload { get; set; }
        [DisplayName("Ngày Sinh (*)")]

        public int? DateOfBirth { get; set; }
        public IEnumerable<SelectListItem> DateOfBirthList { get; set; }
        [DisplayName("Tháng Sinh (*)")]
        public int? MonthOfBirth { get; set; }
        public IEnumerable<SelectListItem> MonthOfBirthList { get; set; }
        [DisplayName("Năm Sinh (*)")]
        public int? YearOfBirth { get; set; }

        public DateTime? BirthDay { get; set; }
        [DisplayName("Trường học (*)")]
        public string SchoolName { get; set; }
        [DisplayName("Lớp (*)")]
        public string SchoolClass { get; set; }
        [DisplayName("Size áo (*)")]
        public string SizeShirtCode { get; set; }
        public string SizeShirtName { get; set; }
        public IEnumerable<SelectListItem> SizeShirtList { get; set; } //HT_SizeShirt
        [DisplayName("Thành tích con đã đạt (*)")] // Một số thành tích mà con đã đạt được là gì? (Ví dụ: học sinh giỏi, đạt giải các cuộc thi…)

        public string Achievement { get; set; }
        [DisplayName("Điều gì đáng tự hào nhất về con? (*)")]
        public string Proud { get; set; }
        [DisplayName("Điểm yếu của con là gì? (*)")]
        public string Weakness { get; set; }
        [DisplayName("Ước mơ của con là gì? (*)")]
        public string Dream { get; set; }
        [DisplayName("Sở thích của con là gì? (*)")]
        public string hobby { get; set; }
        [DisplayName("Con thường có những thói quen nào? (*)")] //Con thường có những thói quen nào? (Theo ý kiến chia sẻ từ Outlier)
        public string Habits { get; set; }
        [DisplayName("5 khó khăn mà Con thường gặp ở trường (*)")] // Vui lòng liệt kê 5 khó khăn mà Con của Anh/Chị thường gặp ở trường (Theo ý kiến chia sẻ từ Outlier)
        public string DifficultiesAtSchool { get; set; }
        [DisplayName("5 khó khăn mà Con thường gặp ở nhà (*)")] //Vui lòng liệt kê 5 khó khăn mà Con của Anh/Chị thường gặp ở nhà (Theo ý kiến chia sẻ từ Outlier):
        public string DifficultiesAtHouse { get; set; }
        [DisplayName("Con thường lo lắng về những vấn đề nào? (*)")] //Con thường lo lắng về những vấn đề nào? (Theo ý kiến chia sẻ từ Outlier)
        public string Worries { get; set; }
        [DisplayName("Ai đang giúp con giải quyết các lo lắng đó? (*)")] //Những ai đang giúp con giải quyết các lo lắng đó? (Theo ý kiến chia sẻ từ Outlier)
        public string WhoHelping { get; set; }
        [DisplayName("Lo lắng đó được giải quyết như thế nào (*)")] //Con muốn những nỗi lo lắng đó được giải quyết như thế nào? (Theo ý kiến chia sẻ từ Outlier)
        public string WantToSolve { get; set; }
        [DisplayName("Có thường xuyên tâm sự với Anh/Chị (*)")] //Các con có thường xuyên tâm sự với Anh/Chị về những điều trong cuộc sống của con không?
        public string TalkTimeCode { get; set; }
        public string TalTimeName { get; set; }
        public IEnumerable<SelectListItem> TalkTimeList { get; set; } //HT_TalkTime
        [DisplayName("5 tính từ miêu tả cảm xúc mà con thường có (*)")] //Chia sẻ 5 tính từ miêu tả cảm xúc mà con thường có. (Theo ý kiến chia sẻ từ Outlier)
        public string Feelings { get; set; }
        [DisplayName("Món quà mà con mong muốn được tặng nhất (*)")] //Những món quà mà con mong muốn được tặng nhất (ví dụ: tiền, đồ chơi, sách…)
        public string GiftToReceive { get; set; }
    }
}