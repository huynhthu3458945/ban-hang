﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.MasterData
{
    public class VM_Class
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}