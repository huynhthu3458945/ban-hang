﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class SP2_RPT_STAR_INPROCESS_Page
    {
        public string SBD { get; set; }
        public DateTime DateRpt { get; set; }
        public string DateRpt2 { get; set; }
        public string DateRptStr { get { return string.Format("{0:dd/MM/yyyy}", DateRpt); } }
        public int? SchoolClassId { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public int? TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public int? StudentId { get; set; }
        public int? StatusId { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        public IEnumerable<SP2_RPT_STAR_INPROCESS> RptStarInProcessList { get; set; }
        public IEnumerable<SP2_RPT_STAR_INPROCESS_REPORT> RptStarInProcessReport { get; set; }
    }
    public class SP2_RPT_STAR_INPROCESS
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public string ParentName { get; set; }
        public string StarName { get; set; }
        public DateTime LastDateReport { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string Phone { get; set; }
        public int CountDate { get; set; }
        public string Title { get; set; }
    }

    public class SP2_RPT_STAR_INPROCESS_REPORT
    {
        public int NoReport { get; set; }
        public int Report { get; set; }
    }
}