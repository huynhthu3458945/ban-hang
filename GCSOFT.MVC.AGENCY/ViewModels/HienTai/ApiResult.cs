﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class ApiResult<T>
    {
        public int? RetCode { get; set; }

        public string RetText { get; set; }

        public T Data { get; set; }
    }
    public class ApiResults<T>
    {
        public int? RetCode { get; set; }

        public string RetText { get; set; }

        public List<T> Data { get; set; }
    }
    public class ApiResultPaging<T>
    {
        public int? RetCode { get; set; }

        public string RetText { get; set; }

        public T Data { get; set; }
    }
    public class ApiResultPagings<T>
    {
        public int? RetCode { get; set; }

        public string RetText { get; set; }
        public Paging paging { get; set; }

        public List<T> Data { get; set; }
    }
    public class RecordOfPage
    {
        public int TotalRow { get; set; }
    }
    public class MaxCode
    {
        public string Code { get; set; }
    }
    public class MaxId
    {
        public string Id { get; set; }
    }
    public class Paging
    {
        public int TotalRows { get; set; }
        public int PerPage { get; set; }
        public int CurPage { get; set; }
        public decimal TotalPage
        {
            get { return Math.Ceiling(TotalRows / (decimal)PerPage); }
        }
        public int start
        {
            get { return (CurPage - 1) * PerPage; }
        }
        public int offset { get { return PerPage; } }
        public int startIndex { get { return start; } }
        public Paging()
        {
            PerPage = 20;
        }
        public Paging(int _TotalRows, int _CurPage)
        {
            TotalRows = _TotalRows;
            CurPage = _CurPage;
            PerPage = 20;
        }
    }
}