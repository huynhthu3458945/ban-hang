﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_Import
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public int CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public string FileImport { get; set; }
    }

    public class O_ImportPage
    {
        public string Title { get; set; }
        public Paging paging { get; set; }
        public List<O_Import> ImportList { get; set; }
    }

    public class O_ImportCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Tiêu đề")]
        public string Title { get; set; }
        [DisplayName("Ghi chú")]
        public string Note { get; set; }
        [DisplayName("Người tạo")]
        public int CreateBy { get; set; }
        [DisplayName("Ngày tạo")]
        public DateTime CreateTime { get; set; }
        [DisplayName("File Import")]
        public HttpPostedFileBase FileImportF { get; set; }
        public string FileImport { get; set; }
    }

    public class O_ImportDetail
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public string FileImport { get; set; }
        public List<O_ImportLog> ImportLogs { get; set; }
    }

    public class O_ImportLog
    {
        public int Stt { get; set; }
        public int StudentId { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int StarID { get; set; }
        public string StarName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int NumberOfTimes { get; set; }
        public string Note { get; set; }
        public bool IsPractice { get; set; }
        public int ImportId { get; set; }
        public bool IsImport { get; set; }
        public bool IsDelete { get; set; }
    }

}