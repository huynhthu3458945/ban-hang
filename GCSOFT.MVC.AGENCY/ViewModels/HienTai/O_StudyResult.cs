﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_StudyResult
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int SchoolClassStarId { get; set; }
        public string SchoolName { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int NoOfStatus { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }

    public class O_StudyResultPage
    {
        public string SBD { get; set; }
        public int? SchoolClassId { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public int? TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public int? StudentId { get; set; }
        public int? currPage { get; set; }
        public Paging paging { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        public IEnumerable<O_StudyResult> StudyResults { get; set; }
    }
}