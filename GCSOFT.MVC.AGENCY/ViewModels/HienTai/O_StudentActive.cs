﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_StudentActiveListPage
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public int SchoolClassId { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public int TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public Paging paging { get; set; }
        public List<O_StudentActive> StudentActiveList { get; set; }
    }
    public class O_StudentActive
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public string TeacherName { get; set; }
        public string SchoolClassName { get; set; }
        public int ActivationCode { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public int ActiveBy { get; set; }
        public DateTime DateActive { get; set; }
        public bool NoOfSendSMS { get; set; }      
        public string ActiveByName { get; set; }
      
    }

    public class O_StudentActiveCreateEdit
    {
        public int Id { get; set; }

        [DisplayName("Mã Kích Hoạt")]
        public int ActivationCode { get; set; }

        [DisplayName("IDNo")]
        public string IDNo { get; set; }

        [DisplayName("Họ Và Tên Học Viên")]
        public int StudentId { get; set; }

        [DisplayName("Họ Và Tên Phụ Huynh")]
        public int ParentId { get; set; }

        [DisplayName("Số Điện Thoại")]
        public string PhoneNumber { get; set; }

        [DisplayName("Đã Kích Hoạt Tài Khoản")]
        public bool IsActive { get; set; }

        [DisplayName("Trạng Thái Gửi Mã Kích Hoạt")]
        public bool NoOfSendSMS { get; set; }

        [DisplayName("Người Kích Hoạt")]
        public int ActiveBy { get; set; }

        [DisplayName("Ngày Kích Hoạt")]
        public DateTime DateActive { get; set; }
    }
    public class O_StudentActiveInfo
    {
        public int Id { get; set; }
        public int ActivationCode { get; set; }
        public string IDNo { get; set; }
        public int StudentId { get; set; }
        public int ParentId { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public bool NoOfSendSMS { get; set; }
        public int ActiveBy { get; set; }
        public DateTime DateActive { get; set; }
        public int AgencyId { get; set; }
    }
    public class O_StudentActiveDetails
    {
        public int Id { get; set; }

        [DisplayName("Mã Kích Hoạt")]
        public int ActivationCode { get; set; }

        [DisplayName("IDNo")]
        public string IDNo { get; set; }

        [DisplayName("Họ Và Tên Học Viên")]
        public string FullName { get; set; }

        [DisplayName("Họ Và Tên Phụ Huynh")]
        public string ParentName { get; set; }

        [DisplayName("Số Điện Thoại")]
        public string PhoneNumber { get; set; }

        [DisplayName("Đã Kích Hoạt Tài Khoản")]
        public bool IsActive { get; set; }

        [DisplayName("Người Kích Hoạt")]
        public int ActiveBy { get; set; }

        [DisplayName("Ngày Kích Hoạt")]
        public DateTime DateActive { get; set; }
    }
}