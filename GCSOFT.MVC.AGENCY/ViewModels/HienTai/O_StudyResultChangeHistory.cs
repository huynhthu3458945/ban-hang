﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_StudyResultChangeHistory
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int StudentStarId { get; set; }
        public int StarId { get; set; }
        [DisplayName("Ticket")]
        public int TicketId { get; set; }
        [DisplayName("Hành trình đang xử lý")]
        public int ProcessHeaderId { get; set; }
        [DisplayName("Hành trình phải hủy")]
        public int ProcessHeaderIdDelete { get; set; }
        public int ProcessLineId { get; set; }
        [DisplayName("Ghi chú")]
        public string Note { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string Date { get; set; }

        public IEnumerable<SelectListItem> Tickets { get; set; }
        public IEnumerable<SelectListItem> ProcessHeaders { get; set; }
    }
}