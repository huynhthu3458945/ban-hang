﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_SummaryStar
    {
        public int Id { get; set; }
        [DisplayName("Mã phiếu (Số hệ thống tự sinh)")]
        public string Code { get; set; }
        public int ProcessHeaderId { get; set; }
        public int StudentStarId { get; set; }
        public int ProcessLineId { get; set; }
        public int StudentId { get; set; }
        [DisplayName("Kết quả bài tổng kết")]
        public string StatusCode { get; set; }
        [DisplayName("Kết quả hành trình")]
        public string Status { get; set; }
        [DisplayName("Đánh giá")]
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }

        public IEnumerable<SelectListItem> SummaryStarTypes { get; set; }
        public IEnumerable<SelectListItem> StatusProcess { get; set; }
    }

    public class O_SummaryStarView
    {
        public int Id { get; set; }
        [DisplayName("Mã phiếu")]
        public string Code { get; set; }
        [DisplayName("Số báo danh")]
        public string IDNo { get; set; }
        [DisplayName("Họ và tên")]
        public string FullName { get; set; }
        public int StarId { get; set; }
        [DisplayName("Tên sao")]
        public string StarName { get; set; }
        [DisplayName("Ngay bắt đầu")]
        public DateTime StartProcessDate { get; set; }
        [DisplayName("Ngày kết thúc")]
        public DateTime EndProcessDate { get; set; }
        public int ProcessHeaderId { get; set; }
        public int StudentStarId { get; set; }
        public int ProcessLineId { get; set; }
        public int StudentId { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Ghi chú")]
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        [DisplayName("Tên lớp")]
        public string SchoolName { get; set; }
        [DisplayName("Tên giáo viên")]
        public string TeacherName { get; set; }
        [DisplayName("Trạng thái sao")]
        public string StatusName { get; set; }
    }

    public class O_SummaryStarSend
    {
        public int Id { get; set; }
        [DisplayName("Mã phiếu")]
        public string Code { get; set; }
        [DisplayName("Số báo danh")]
        public string IDNo { get; set; }
        [DisplayName("Họ và tên")]
        public string FullName { get; set; }
        public int StarId { get; set; }
        [DisplayName("Tên sao")]
        public string StarName { get; set; }
        [DisplayName("Ngay bắt đầu")]
        public DateTime StartProcessDate { get; set; }
        [DisplayName("Ngày kết thúc")]
        public DateTime EndProcessDate { get; set; }
        public int ProcessHeaderId { get; set; }
        public int StudentStarId { get; set; }
        public int ProcessLineId { get; set; }
        public int StudentId { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Ghi chú")]
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        [DisplayName("Tên lớp")]
        public string SchoolName { get; set; }
        [DisplayName("Tên giáo viên")]
        public string TeacherName { get; set; }
        [DisplayName("Trạng thái sao")]
        public string StatusName { get; set; }
        public string Email { get; set; }
    }

    public class O_SummaryStarPage
    {
        public int? SchoolClassId { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public int? TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public int? StudentId { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        public int? currPage { get; set; }
        public Paging paging { get; set; }
        public List<O_SummaryStarView> SummaryStars { get; set; }
    }

    public class O_ProcessLineSummary : O_ProcessLine
    {
        public int SummaryStarId { get; set; }
        public string StatusCode { get; set; }
        public string Note { get; set; }
        public string Code { get; set; }
        public List<File> ListFiles { get { return Files != null ? JsonConvert.DeserializeObject<List<File>>(Files) : new List<File>(); } }
    }

    public class O_SummaryStarDetail
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        [DisplayName("Họ và tên")]
        public string FullName { get; set; }
        public int StarId { get; set; }
        [DisplayName("Tên sao")]
        public string StarName { get; set; }
        [DisplayName("Ngày bắt đầu")]
        public DateTime StartProcessDate { get; set; }
        [DisplayName("Ngày kết thúc")]
        public DateTime EndProcessDate { get; set; }
        [DisplayName("Lần thực hiện")]
        public string ProcessHeaderTitle { get; set; }
        public int ProcessHeaderId { get; set; }
        public int StudentStarId { get; set; }
        public int StudentId { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Khóa học")]
        public string SchoolName { get; set; }
        [DisplayName("Giáo viên phụ trách")]
        public string TeacherName { get; set; }
        [DisplayName("Tình trạng sao")]
        public string StatusName { get; set; }
        public Paging paging { get; set; }
        public List<O_ProcessLineSummary> ProcessLines { get; set; }
    }

    public class File {
        public string link { get; set; }
        public string Type { get; set; }
        public int SortOrder { get; set; }
        public object Thumbnail { get; set; }
    }

}