﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_ClassListPage
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Paging paging { get; set; }
        public List<O_ClassList> ClasList { get; set; }
    }
    public class O_ClassList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
    public class O_ClassCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Tên Lớp")]
        public string Title { get; set; }
        [DisplayName("Mô tả")]
        public string Description { get; set; }
        [DisplayName("Trạng Thái")]
        public bool Status { get; set; }
        public int AgencyId { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public int TeacherId { get; set; }
    }
    public class O_ClassInfo
    {
        public int Id { get; set; }
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Tên Lớp")]
        public string Title { get; set; }
        [DisplayName("Mô tả")]
        public string Description { get; set; }
        [DisplayName("Trạng Thái")]
        public bool Status { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int AgencyId { get; set; }
        public int ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public int TeacherId { get; set; }
    }

    public class O_ClassDetails
    {
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Tên Lớp")]
        public string Title { get; set; }
        [DisplayName("Mô tả")]
        public string Description { get; set; }
        [DisplayName("Trạng Thái")]
        public bool Status { get; set; }
    }
}