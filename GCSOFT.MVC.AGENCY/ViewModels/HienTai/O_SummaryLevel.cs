﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_SummaryLevel
    {
        public int Id { get; set; }
        [DisplayName("Mã phiếu (Số hệ thống tự sinh)")]
        public string Code { get; set; }
        [DisplayName("Số sao đã hoàn thành")]
        public int NumberOfStarCompleted { get; set; }
        public int StudentId { get; set; }
        [DisplayName("Đánh giá")]
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        [DisplayName("Level của hiền tài")]
        public int LevelId { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
    }

    public class O_SummaryLevelHistory
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int NumberOfStarCompleted { get; set; }
        public int StudentId { get; set; }
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        public string CreateName { get; set; }
        public int LevelId { get; set; }
        public string LevelName { get; set; }
        public string FullName { get; set; }
        public string IDNo { get; set; }
    }

    public class O_SummaryLevelStar
    {
        public int Id { get; set; }
        public string BasisName { get; set; }
        public string QualityName { get; set; }
        public string StarName { get; set; }
        public int NoOfDay { get; set; }
        public DateTime FinishDate { get; set; }
        public string StatusName { get; set; }
    }

    public class O_SummaryLevelView
    {
        public int Id { get; set; }
        [DisplayName("Số báo danh")]
        public string IDNo { get; set; }
        [DisplayName("Họ và tên")]
        public string FullName { get; set; }
        [DisplayName("Tên lớp")]
        public string SchoolName { get; set; }
        [DisplayName("Tên giáo viên")]
        public string TeacherName { get; set; }
        [DisplayName("Trạng thái sao")]
        public string StatusName { get; set; }
        public int NumberOfStar { get; set; }
    }

    public class O_SummaryLevelPage
    {
        public int? SchoolClassId { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public int? TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public int? LevelId { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public int? StudentId { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        public int? currPage { get; set; }
        public Paging paging { get; set; }
        public List<O_SummaryLevelView> SummaryLevels { get; set; }
    }
}