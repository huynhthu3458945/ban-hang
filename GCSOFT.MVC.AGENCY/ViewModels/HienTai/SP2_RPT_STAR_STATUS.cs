﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class SP2_RPT_STAR_STATUS_SUMMARY_Page
    {
        public string SBD { get; set; }
        public int? SchoolClassId { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public int? TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public int? StudentId { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        public IEnumerable<SP2_RPT_STAR_STATUS_SUMMARY> RptStarSummary { get; set; }
    }
    public class SP2_RPT_STAR_STATUS_DETAIL_Page
    {
        public string SBD { get; set; }
        public int? StudentId { get; set; }
        public int? SchoolClassStarId { get; set; }
        public string StatusCode { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public IEnumerable<SP2_RPT_STAR_STATUS_DETAIL> RptStarDetail { get; set; }
    }
    public class SP2_RPT_STAR_STATUS_SUMMARY
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int SchoolClassStarId { get; set; }
        public string SchoolName { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int NoOfStatus { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }
    public class SP2_RPT_STAR_STATUS_DETAIL
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int SchoolClassStarId { get; set; }
        public string SchoolName { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string BasisName { get; set; }
        public string QualityName { get; set; }
        public string StarName { get; set; }
        public int NoOfDay { get; set; }
        public int TotalDay { get; set; }
        public DateTime StartProcessDate { get; set; }
        public DateTime EndProcessDate { get; set; }
        public string StatusName { get; set; }
    }
}