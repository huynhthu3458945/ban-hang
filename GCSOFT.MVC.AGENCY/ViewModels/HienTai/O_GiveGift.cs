﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_GiveGiftListPage
    {
        public string Code { get; set; }
        public int StudentId { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        public int GiftId { get; set; }
        public IEnumerable<SelectListItem> GiftList { get; set; }
        public Paging paging { get; set; }
        public List<O_GiveGiftList> GiveGiftList { get; set; }
    }
    public class O_GiveGiftList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string GiftId { get; set; }
        public string GiftName { get; set; }
        public string Reason { get; set; }
        public int Quantity { get; set; }
    }
    public class O_GiveGiftCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Mã Tặng Quà")]
        public string Code { get; set; }
        [DisplayName("Họ Và Tên Học Viên")]
        public string StudentId { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        [DisplayName("Quà Tặng")]
        public string GiftId { get; set; }
        public IEnumerable<SelectListItem> GiftList { get; set; }
        [DisplayName("Lý Do Được Tặng")]
        public string Reason { get; set; }
        [DisplayName("Số Lượng")]
        public int Quantity { get; set; }
        [DisplayName("Đã Nhận Quà")]
        public bool IsReceived { get; set; }
        [DisplayName("Mô Tả Tặng Quà")]
        public string Description { get; set; }
        public int AgencyId { get; set; }

    }
    public class O_GiveGiftInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string StudentId { get; set; }
        public string GiftId { get; set; }
        public int Quantity { get; set; }
        public bool IsReceived { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public int AgencyId { get; set; }
    }

    public class O_GiveGiftDetails
    {
        [DisplayName("Mã Tặng Quà")]
        public string Code { get; set; }
        [DisplayName("Họ Và Tên Học Viên")]
        public string StudentName { get; set; }
        [DisplayName("Quà Tặng")]
        public string GiftName { get; set; }
        [DisplayName("Mô Tả Tặng Quà")]
        public string Description { get; set; }
        [DisplayName("Số Lượng")]
        public int Quantity { get; set; }
        [DisplayName("Đã Nhận Quà")]
        public bool IsReceived { get; set; }
        [DisplayName("Lý Do Được Tặng")]
        public string Reason { get; set; }
        public string ReceivedString { get; set; }

    }
}