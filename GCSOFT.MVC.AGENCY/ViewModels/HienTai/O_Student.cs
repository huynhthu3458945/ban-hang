﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_StudentPage
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public int SchoolClassId { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public int TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public Paging paging { get; set; }
        public List<O_StudentList> StudentList { get; set; }
    }
    public class O_StudentList
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public string GenderName { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public int SchoolClassStarId { get; set; }
        public string SchoolClassName { get; set; }
        public int IsActive { get; set; }
        public string HasSendActiveCode { get; set; }
        public string StudentComfirmInApp { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int StudentId { get; set; }
    }
    public class O_StudentCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Số Báo Danh")]
        public string Code { get; set; }
        public string IDNo { get; set; }
        [DisplayName("Họ Và Tên Hiền Tài")]
        public string FullName { get; set; }
        [DisplayName("Khóa")]
        public int SchoolClassStarId { get; set; }
        [DisplayName("Ngày Sinh")]
        public DateTime? DateOfBirth { get; set; }
        public string DateOfBirthStr { get; set; }
        [DisplayName("Ngày Khai Giảng")]
        public DateTime StartDate { get; set; }
        public string StartDateStr { get; set; }
        [DisplayName("Ngày Kết Thúc Khóa Học")]
        public DateTime EndDate { get; set; }
        public string EndDateStr { get; set; }
        [DisplayName("Kích Thước Áo")]
        public string ShirtSize { get; set; }
        [DisplayName("Học Tại Trường")]
        public string SchoolName { get; set; }
        [DisplayName("Lớp")]
        public int SchoolClassId { get; set; }
        [DisplayName("Họ Và Tên Phụ Huynh")]
        public int ParentId { get; set; }
        [DisplayName("Giới Tính")]
        public string Gender { get; set; }
        [DisplayName("Tình Trạng Hiền Tài")]
        public string Status { get; set; }
        [DisplayName("Đối Tác")]
        public int AgencyId { get; set; }
        [DisplayName("Trạng Thái Gửi Mã Kích Hoạt")]
        public string HasSendActiveCode { get; set; }
        [DisplayName("Đã Kích Hoạt Tài Khoản")]
        public string StudentComfirmInApp { get; set; }
        [DisplayName("Giáo Viên Đồng Hành")]
        public int TeacherId { get; set; }
        [DisplayName("Cấp làm SAO (*)")]
        public int[] LevelArrs { get; set; }
        public string AgencyCode { get; set; }
        public string AgencyNo { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public IEnumerable<StudentStar> StudentStarList { get; set; }
        public IEnumerable<SelectListItem> SchoolClassStarList { get; set; }
        public IEnumerable<SelectListItem> ShirtSizeList { get; set; }
        public IEnumerable<SelectListItem> ParentList { get; set; }
        public IEnumerable<SelectListItem> SchoolClassList { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public IEnumerable<SelectListItem> AgencyList { get; set; }
        public O_StudentCreateEdit()
        {
            StudentStarList = new List<StudentStar>();
        }
    }
    public class O_StudentInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int SchoolClassStarId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ShirtSize { get; set; }
        public string SchoolName { get; set; }
        public int SchoolClassId { get; set; }
        public int ParentId { get; set; }
        public string Gender { get; set; }
        public int AgencyId { get; set; }
        public string Status { get; set; }
        public string LevelArr { get; set; }
        public int TeacherId { get; set; }
        public string AgencyCode { get; set; }
        public string AgencyNo { get; set; }
        public int StudentOrigin { get; set; }
    }
    public class StudentStar
    {

        public int StudentId { get; set; }
        public int StarId { get; set; }
        public int ProcessHeaderID { get; set; }
        public string BasisName { get; set; }
        public string QualityName { get; set; }
        public string StarName { get; set; }
        public int NoOfDay { get; set; }
        public string StartProcessDate { get; set; }
        public string EndProcessDate { get; set; }
        public string StatusName { get; set; }
        public int LevelId { get; set; }
        public string LevelName { get; set; }
        public int StudentStarId { get; set; }
    }
    public class StudentActiveInfo
    {
        public int StudentId { get; set; }
        public string IDNo { get; set; }
        public string ActivationCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
    }
    public class O_StudentDetails
    {
        public string Id { get; set; }
        [DisplayName("Số Báo Danh")]
        public string Code { get; set; }
        [DisplayName("Họ Và Tên Học Viên")]
        public string FullName { get; set; }
        [DisplayName("Lớp làm SAO")]
        public string SchoolClassStarName { get; set; }
        [DisplayName("Ngày Sinh")]
        public DateTime DateOfBirth { get; set; }
        [DisplayName("Ngày Khai Giảng")]
        public DateTime StartDate { get; set; }
        [DisplayName("Ngày Kết Thúc Khóa Học")]
        public DateTime EndDate { get; set; }
        [DisplayName("Kích Thước Áo")]
        public string ShirtSize { get; set; }
        [DisplayName("Học Tại Trường")]
        public string SchoolName { get; set; }
        [DisplayName("Lớp")]
        public string SchoolClassName { get; set; }
        [DisplayName("Họ Và Tên Phụ Huynh")]
        public string ParentName { get; set; }
        [DisplayName("Giới Tính")]
        public string GenderName { get; set; }
        [DisplayName("Đối Tác")]
        public string AgencyId { get; set; }
        [DisplayName("Trạng Thái Gửi Mã Kích Hoạt")]
        public string HasSendActiveCode { get; set; }
        [DisplayName("Đã Kích Hoạt Tài Khoản")]
        public string StudentComfirmInApp { get; set; }
        [DisplayName("Trạng Thái Học Viên")]
        public string StatusName { get; set; }
        public IEnumerable<StudentStar> StudentStarList { get; set; }
        public O_StudentDetails()
        {
            StudentStarList = new List<StudentStar>();
        }
    }
    public class StudentLevel
    {
        public int StudentId { get; set; }
        public int LevelId { get; set; }
    }
    public class SchoolClassInfo
    {
        public string OpeningDay { get; set; }
        public string ClosingDay { get; set; }
        public int TeacherId { get; set; }
    }
    public class StudentAccount
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class O_StudentExport
    {
        public int Id { get; set; }
        public string IDNo { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public int SchoolClassStarId { get; set; }
        public string SchoolClassName { get; set; }
        public int IsActive { get; set; }
        public string HasSendActiveCode { get; set; }
        public string StudentComfirmInApp { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ShirtSize { get; set; }
        public string OpeningDay { get; set; }
        public string ClosingDay { get; set; }
        public string Lever { get; set; }
    }
}