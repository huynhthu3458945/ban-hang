﻿namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class CategoryUser
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string AreaName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string ParentCode { get; set; }
        public int Priority { get; set; }
    }
}