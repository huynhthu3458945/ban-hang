﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTaiOMI
{
    public class AccessToken
    {
        public string access_token { get; set; }
        public string access_type { get; set; }
        public string token_type { get; set; }
    }

    public class ResponseAccessToken : ResultResponse
    {
        public AccessToken payload { get; set; }
    }
}