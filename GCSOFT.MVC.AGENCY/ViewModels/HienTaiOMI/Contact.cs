﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTaiOMI
{

    public class ContactPayload
    {
        public string _id { get; set; }
        public CreateBy create_by { get; set; }
        public CreateBy last_update_by { get; set; }
        public long created_date { get; set; }
        public string last_updated_date { get; set; }
        public string public_id { get; set; }
        public string contact_type { get; set; }
        public List<object> tags_view { get; set; }
        public bool is_deleted { get; set; }
        public List<object> tags { get; set; }
        public List<AttributeStructure> attribute_structure { get; set; }
    }

    public class ContactDetailPayload
    {
        public string _id { get; set; }
    }

    public class ContactUpdatePayload
    {
        public string publicId { get; set; }
        public string fullName { get; set; }
        public string displayName { get; set; }
        public string gender { get; set; }
        public string passport { get; set; }
        public string address { get; set; }
        public string note { get; set; }
        public List<DataValue> emails { get; set; }
        public List<DataValue> phones { get; set; }
        public string contactType { get; set; }
    }

    public class ContactPage 
    {
        public int next_page { get; set; }
        public int page_number { get; set; }
        public bool has_previous { get; set; }
        public bool has_next { get; set; }
        public int total_pages { get; set; }
        public int previous_page { get; set; }
        public List<ContactPayload> items { get; set; }
        public int total_items { get; set; }
        public int page_size { get; set; }

    }

    public class ResponseContactList : ResultResponse
    {
        public ContactPage payload { get; set; }
    }

    public class ResponseContactAdd : ResultResponse
    {
        public bool payload { get; set; }
    }

    public class ResponseContactDetail : ResultResponse
    {
        public ContactDetailPayload payload { get; set; }
    }

    public class ResponseContactUpdate : ResultResponse
    {
        public ContactUpdatePayload payload { get; set; }
    }

    public class ResponseContactDelete : ResultResponse
    {
        public bool payload { get; set; }
    }

    public class ContactAdd
    {
        public List<string> tags { get; set; }
        public List<Value> more_infomation { get; set; }
        public string user_owner_email { get; set; }
        public string refId { get; set; }
        public string refCode { get; set; }
        public string job_title { get; set; }
        public string note { get; set; }
        public string birthday { get; set; }
        public string gender { get; set; }
        public string fullName { get; set; }
        public string passport { get; set; }
        public string address { get; set; }
        public List<DataValue> emails { get; set; }
        public List<DataValue> phones { get; set; }
    }
}