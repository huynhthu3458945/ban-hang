﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTaiOMI
{
    #region Danh sach nhan vien dung omicall
    public class CreateBy
    {
        public string name { get; set; }
    }

    public class LastUpdateBy
    {
        public string name { get; set; }
    }

    public class AttributeStructure
    {
        public object identify { get; set; }
        public string field_code { get; set; }
        public string field_type { get; set; }
        public List<Value> value { get; set; }
    }

    public class ListAgent
    {
        public CreateBy create_by { get; set; }
        public LastUpdateBy last_update_by { get; set; }
        public object created_date { get; set; }
        public object last_updated_date { get; set; }
        public object public_id { get; set; }
        public string contact_type { get; set; }
        public List<object> tags_view { get; set; }
        public bool is_deleted { get; set; }
        public List<object> tags { get; set; }
        public bool is_active { get; set; }
        public string identify_info { get; set; }
        public List<AttributeStructure> attribute_structure { get; set; }
    }

    public class ListAgentPayload
    {
        public int next_page { get; set; }
        public int page_number { get; set; }
        public bool has_previous { get; set; }
        public bool has_next { get; set; }
        public int total_pages { get; set; }
        public int previous_page { get; set; }
        public List<ListAgent> items { get; set; }
        public int total_items { get; set; }
        public int page_size { get; set; }
    }

    public class ResponseListAgent : ResultResponse
    {
        public ListAgentPayload payload { get; set; }
    }
    #endregion

    #region Moi nhan vien dung omicall
    public class AgentInvite
    {
        public string owner_email { get; set; }
        public string role_name { get; set; }
        public string password { get; set; }
        public string identify_info { get; set; }
        public string full_name { get; set; }
    }

    public class AgentPayload
    {
        public string full_name { get; set; }
        public string email { get; set; }
    }

    public class PbxAccountPayload
    {
        public string sip_user { get; set; }
        public string password { get; set; }
        public object domain { get; set; }
        public object outbound_proxy { get; set; }
        public object public_number { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public long last_updated_date { get; set; }
        public long created_date { get; set; }
    }

    public class ResultPayload
    {
        public AgentPayload agent { get; set; }
        public PbxAccountPayload pbx_account { get; set; }
    }

    public class ResponseAgentInvite : ResultResponse
    {
        public ResultPayload payload { get; set; }
    }

    public class ResponseAgentDelete : ResultResponse
    {
        public bool payload { get; set; }
    }
    #endregion
}