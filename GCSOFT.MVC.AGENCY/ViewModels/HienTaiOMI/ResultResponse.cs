﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTaiOMI
{
    public class ResultResponse
    {
        public int status_code { get; set; }
        public string instance_id { get; set; }
        public string instance_version { get; set; }
        public string instance_name { get; set; }
        public bool key_enabled { get; set; }
    }
}