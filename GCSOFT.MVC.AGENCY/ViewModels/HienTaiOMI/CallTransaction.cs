﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTaiOMI
{

    public class ResponseCallTransaction : ResultResponse 
    {
        public CallTransactionPage payload { get; set; }
    }

    public class ResponseCallTransactionDetail : ResultResponse
    {
        public CallTransaction payload { get; set; }
    }

    public class ResponseCallTransactionUpdate
    {
        public int status_code { get; set; }
        public CallTransaction payload { get; set; }
    }

    public class CallTransaction
    {
        public string transaction_id { get; set; }
        public string direction { get; set; }
        public string source_number { get; set; }
        public string destination_number { get; set; }
        public string disposition { get; set; }
        public int bill_sec { get; set; }
        public long time_start_to_answer { get; set; }
        public string recording_file { get; set; }
        public string sip_user { get; set; }
        public int duration { get; set; }
        public string provider { get; set; }
        public string note { get; set; }
        public List<string> tag { get; set; }
        public int record_seconds { get; set; }
        public string call_out_price { get; set; }
        public long created_date { get; set; }
        public string last_updated_date { get; set; }
        public List<User> user { get; set; }
        public Customer customer { get; set; }
        public bool is_have_forward_out { get; set; }
        public string bill_sec_forward_out { get; set; }
    }

    public class CallTransactionPage 
    {
        public List<CallTransaction> items { get; set; }
        public int page_number { get; set; }
        public int page_size { get; set; }
        public int total_items { get; set; }
        public int total_pages { get; set; }
        public bool has_next { get; set; }
        public int next_page { get; set; }
        public bool has_previous { get; set; }
        public int previous_page { get; set; }
    }

    public class Tag
    {
        public string name { get; set; }
    }

    public class User
    {
        public string full_name { get; set; }
        public string full_name_unsigned { get; set; }
        public string note { get; set; }
        public List<Tag> tag { get; set; }
    }

    public class Customer
    {
        public string full_name { get; set; }
        public string full_name_unsigned { get; set; }
    }

    //---------------------------------
    public class ParamGetList
    {
        public List<string> agents { get; set; }
        public bool is_auto_call { get; set; }
        public string keyword { get; set; }
        public string sip_user { get; set; }
        //Trạng thái : 'cancelled' : Không trả lời, 'answered' : Trả lời
        public string disposition { get; set; }
        //Hướng gọi ('outbound' : Gọi đi ; 'inbound' : Gọi đến; 'local' : Gọi nội bộ)
        public List<string> direction { get; set; }
        public int page { get; set; }
        public int size { get; set; }
        //Đến ngày (Timestamp in milliseconds) 
        public long from_date { get; set; }
        //Đến ngày (Timestamp in milliseconds) 
        public long to_date { get; set; }
        public string source_number { get; set; }
        public string destination_number { get; set; }

    }

    public class ParamUpdateTran 
    { 
        public List<string> tag { get; set; }
        public string note { get; set; }
    }

}