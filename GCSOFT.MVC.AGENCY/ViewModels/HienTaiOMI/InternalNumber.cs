﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTaiOMI
{
    public class ResponseListInternalNumber : ResultResponse 
    {
        public InternalNumberListPayload payload { get; set; }
    }

    public class InternalNumberListPayload 
    {
        public int next_page { get; set; }
        public int page_number { get; set; }
        public bool has_previous { get; set; }
        public bool has_next { get; set; }
        public int total_pages { get; set; }
        public int previous_page { get; set; }
        public List<InternalNumberPayload> items { get; set; }
        public int total_items { get; set; }
        public int page_size { get; set; }
    }

    public class InternalNumberPayload
    {
        public string domain { get; set; }
        public string outbound_proxy { get; set; }
        public string sip_user { get; set; }
        public string password { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string public_number { get; set; }
        public object last_updated_date { get; set; }
        public object created_date { get; set; }
    }
}