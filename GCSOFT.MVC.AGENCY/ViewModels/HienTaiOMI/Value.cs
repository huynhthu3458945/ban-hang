﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTaiOMI
{
    public class Value
    {
        public string display_value { get; set; }
        public string data_type { get; set; }
        public string value_type { get; set; }
    }

    public class DataValue
    {
        public string data { get; set; }
        public string value { get; set; }
        public string valueType { get; set; }
    }


}