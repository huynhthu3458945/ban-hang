﻿using GCSOFT.MVC.Model.FrontEnd;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels
{
    public class ECouseCard
    {
        public CategoryMenu Category { get; set; }
        public CourseCard Course { get; set; }
        public TeacherCard Teacher { get; set; }
        public IEnumerable<CouseCategory> OtherCourses { get; set; }
    }
}