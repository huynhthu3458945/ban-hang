﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.AGENCY.ViewModels.MasterData;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<UserLoginVMs, HT_Users>();
            Mapper.CreateMap<UserVM, HT_Users>();
            Mapper.CreateMap<VuViecVMs, HT_VuViec>();
            Mapper.CreateMap<CategoryUser, HT_CongViec>();
            Mapper.CreateMap<CategoryListVMs, HT_CongViec>();
            Mapper.CreateMap<CategoryCard, HT_CongViec>();
            Mapper.CreateMap<CategoryFunctionVMs, HT_VuViecCuaCongViec>();
            Mapper.CreateMap<NhomUsers, HT_NhomUser>();
            Mapper.CreateMap<NhomUserCard, HT_NhomUser>();
            Mapper.CreateMap<UserThuocNhomVM, HT_UserThuocNhom>();
            Mapper.CreateMap<UserList, HT_Users>();
            Mapper.CreateMap<UserCard, HT_Users>();
            Mapper.CreateMap<VM_OptionType, M_OptionType>();
            Mapper.CreateMap<VM_OptionLine, M_OptionLine>();

        }
    }
}