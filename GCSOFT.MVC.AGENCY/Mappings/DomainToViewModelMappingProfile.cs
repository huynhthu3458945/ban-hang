﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.AGENCY.ViewModels.MasterData;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<HT_Users, UserLoginVMs>();
            Mapper.CreateMap<HT_Users, UserVM>();
            
            Mapper.CreateMap<HT_VuViec, VuViecVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryUser>();
            Mapper.CreateMap<HT_CongViec, CategoryListVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryCard>();
            Mapper.CreateMap<HT_VuViecCuaCongViec, CategoryFunctionVMs>();
            //-- Nhom User
            Mapper.CreateMap<HT_NhomUser, NhomUsers>();
            Mapper.CreateMap<HT_NhomUser, NhomUserCard>();
            Mapper.CreateMap<HT_UserThuocNhom, UserThuocNhomVM>();
            //-- Nhom User End
            //-- User
            Mapper.CreateMap<HT_Users, UserList>();
            Mapper.CreateMap<HT_Users, UserCard>();
            //-- User End
            Mapper.CreateMap<M_OptionType, VM_OptionType>();
            Mapper.CreateMap<M_OptionLine, VM_OptionLine>();
        }
    }
}