﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.SystemModels
{
    //[Table("Sys_Role")]
    public class HT_CongViecVaVuViec
    {
        [Key, Column(Order = 0, TypeName = "varchar")]
        [MaxLength(20)]
        public string CategoryCode { get; set; }
        [Key, Column(Order = 1, TypeName = "varchar")]
        [MaxLength(20)]
        public string FunctionCode { get; set; }
        [Key, Column(Order = 3, TypeName = "varchar")]
        [MaxLength(20)]
        public string UserGroupCode { get; set; }
    }
}