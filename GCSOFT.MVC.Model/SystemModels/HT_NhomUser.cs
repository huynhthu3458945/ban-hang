﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.SystemModels
{
   // [Table("Sys_UserGroup")]
    public class HT_NhomUser
    {
        [Key]
        [Column(TypeName = "varchar")]
        [MaxLength(20)]
        public string Code { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }
        public virtual IEnumerable<HT_UserThuocNhom> UserOfGroups { get; set; }
    }
}