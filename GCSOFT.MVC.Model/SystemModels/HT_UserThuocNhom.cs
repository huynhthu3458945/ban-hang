﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.SystemModels
{
    //[Table("Sys_UserAndGroup")]
    public class HT_UserThuocNhom
    {
        [Key, Column(Order = 0, TypeName = "varchar")]
        [MaxLength(20)]
        public string UserGroupCode { get; set; }
        [Key, Column(Order = 1)]
        public int UserID { get; set; }
    }
}