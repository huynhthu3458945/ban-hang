﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NewSocial
{
    public class info_exams_review
    {
        [Key]
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public int knowledge_topic_Id { get; set; }
        public int time_second { get; set; }
        public int total_question { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
    }
    public class info_exams_review_level
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public Guid exams_review_id { get; set; }
        [MaxLength(50)]
        public string code_level { get; set; }
        public int percent { get; set; }
        public int question_number { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
    }
    public class info_exams_review_question
    {
        [Key]
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public int knowledge_topic_Id { get; set; }
        public Guid info_exams_review_id { get; set; }
        public Guid question_id { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
    }
    public class info_exams_review_question_result
    {
        [Key]
        public int Id { get; set; }
        public Guid question_user_id { get; set; }
        public int user_id { get; set; }
        public string answer_content { get; set; }
        public string links { get; set; }
        public bool answer_flag { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public Guid question_id { get; set; }
        public Guid answer_id { get; set; }
    }
    public class info_exams_review_question_user
    {
        [Key]
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public Guid info_exams_review_id { get; set; }
        public int user_Id { get; set; }
        public int total_question { get; set; }
        public int total_quesstion_sucess { get; set; }
        public int point_system { get; set; }
        public int point_success { get; set; }
        public int time_system { get; set; }
        public int time_success { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int top { get; set; }
    }
    public class info_review_question_answer
    {
        [Key]
        public Guid Id { get; set; }
        public Guid question_id { get; set; }
        public string title { get; set; }
        public string answer { get; set; }
        public string description { get; set; }
        public string links { get; set; }
        public bool answer_flag { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int sort_order { get; set; }
    }
    public class info_review_question_result
    {
        [Key]
        public int Id { get; set; }
        public Guid question_user_id { get; set; }
        public int user_id { get; set; }
        public string answer_content { get; set; }
        public string links { get; set; }
        public bool answer_flag { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public Guid question_id { get; set; }
        public Guid answer_id { get; set; }
    }
    public class info_review_question_user
    {
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        [MaxLength(100)]
        public string level { get; set; }
        public int user_Id { get; set; }
        public int total_question { get; set; }
        public int total_quesstion_sucess { get; set; }
        public int point_system { get; set; }
        public int point_success { get; set; }
        public int time_system { get; set; }
        public int time_success { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int top { get; set; }
    }
    public class mst_groupcode
    {
        [Key]
        [MaxLength(50)]
        public string code { get; set; }
        [MaxLength(100)]
        public string name { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int position { get; set; }
    }
    public class mst_knowledge_topic
    {
        [Key]
        public int id { get; set; }
        public int mst_topics_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int parent_id { get; set; }
        public int level { get; set; }
        public int position { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public bool status { get; set; }
    }
    public class mst_options
    {
        [Key]
        [MaxLength(100)]
        public string code { get; set; }
        [MaxLength(100)]
        public string group_code { get; set; }
        public string description { get; set; }
        [MaxLength(250)]
        public string name { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int position { get; set; }
    }
    public class mst_review_question
    {
        [Key]
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public int knowledge_topic_Id { get; set; }
        public string question { get; set; }
        [Column(TypeName = "ntext")]
        public string description { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int point { get; set; }
        [MaxLength(100)]
        public string level { get; set; }
        [MaxLength(125)]
        public string topics_id { get; set; }
        public string explain_the_answer { get; set; }
        public string jamming_scheme { get; set; }
        public int time_second { get; set; }
        public string linkImages { get; set; }
    }
    public class mst_topics
    {
        [Key]
        [MaxLength(125)]
        public string id { get; set; }
        [MaxLength(250)]
        public string name { get; set; }
        public string description { get; set; }
        public int position { get; set; }
        public int total_question { get; set; }
        [MaxLength(250)]
        public string teacher_create { get; set; }
        public DateTime date_create { get; set; }
        [MaxLength(250)]
        public string approved_by { get; set; }
        public DateTime date_approved { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public bool status { get; set; }
    }
}
