﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpCardSoldByAgency
    {
        public int AgencyId { get; set; }
        public string AagencyName { get; set; }
        public string AgencyAddress { get; set; }
        public string AgencyPhone { get; set; }
        public string AgencyEmail { get; set; }
        public string SerialNumber { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime? DateBuy { get; set; }
        public bool IsDelete { get; set; }
        public string DateBuyStr { get { return string.Format("{0:dd/MM/yyyy}", DateBuy); } }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string StatusActvie { get; set; }
        public bool IsActive { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
    }
}
