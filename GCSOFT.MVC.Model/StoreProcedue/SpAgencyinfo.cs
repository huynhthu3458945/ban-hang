﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpAgencyinfo
    {
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string LeavelName { get; set; }
        public string PackageName { get; set; }
        public string Rating { get; set; }
        public decimal TienTruocCK { get; set; }
        public decimal DiscountAmt { get; set; }
        public decimal TienSauCK { get; set; }
        public int NoOfCard { get; set; }
        public int GiftCard { get; set; }
        public int TotalCard1Lop { get; set; }
        public int TotalCard2Lop { get; set; }
        public int TotalCard3Lop { get; set; }
        public int TotalCard4Lop { get; set; }
        public int TotalCard5Lop { get; set; }
        public int TotalCardCap { get; set; }
        public int TotalCardVIP { get; set; }
        public int TotalCard { get; set; }
        public int AgencyDaBanDL1Lop { get; set; }
        public int AgencyDaBanDL2Lop { get; set; }
        public int AgencyDaBanDL3Lop { get; set; }
        public int AgencyDaBanDL4Lop { get; set; }
        public int AgencyDaBanDL5Lop { get; set; }
        public int AgencyDaBanDLCap { get; set; }
        public int AgencyDaBanDLVIP { get; set; }
        public int AgencyDaBanDL { get; set; }
        public int BanMKH { get; set; }
        public int TuKichHoat { get; set; }
        public int TongTheDaBan1Lop { get; set; }
        public int TongTheDaBan2Lop { get; set; }
        public int TongTheDaBan3Lop { get; set; }
        public int TongTheDaBan4Lop { get; set; }
        public int TongTheDaBan5Lop { get; set; }
        public int TongTheDaBanCap { get; set; }
        public int TongTheDaBanVIP { get; set; }
        public int TongTheDaBan { get; set; }
        public int NoOfAgency { get; set; }
        public int NoOfPartner { get; set; }
        public int SumAgencyAndPartner { get; set; }
        public int TheDaActive1Lop { get; set; }
        public int TheDaActive2Lop { get; set; }
        public int TheDaActive3Lop { get; set; }
        public int TheDaActive4Lop { get; set; }
        public int TheDaActive5Lop { get; set; }
        public int TheDaActiveCap { get; set; }
        public int TheDaActiveVIP { get; set; }
        public int TheDaActive { get; set; }
    }
}
