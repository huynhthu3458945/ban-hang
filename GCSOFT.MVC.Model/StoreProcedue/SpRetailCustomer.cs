﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpRetailCustomer
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string LevelName { get; set; }
        public DateTime? DateBuy { get; set; }
        public DateTime? ApplyDate { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get; set; }
    }
}
