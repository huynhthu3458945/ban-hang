﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class EbookUserByCity
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int NoOfAccount { get; set; }
    }
    public class EbookUserDetailByCity
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string fullName { get; set; }
        public string Phone { get; set; }
        public string ChildFullName1 { get; set; }
        public string ClassName1 { get; set; }
        public string ChildFullName2 { get; set; }
        public string ClassName2 { get; set; }
    }
}
