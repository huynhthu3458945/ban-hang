﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpCustomerInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string IsEmail { get; set; }
        public string Email { get; set; }
        public DateTime? RegisterDate  { get; set; }
        public string Owner { get; set; }
        public int? NoOfCard { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public string UserOwner { get; set; }
        public string Active { get; set; }
        public string CardNo2 { get; set; }
        public string DurationName { get; set; }
    }
}
