﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SPBaiThi
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NgayBatDauStr { get; set; }
        public string NgayKetThucStr { get; set; }
        public bool IsActive { get; set; }
        public int Id { get; set; }
        public int StatusCode { get; set; }
        public string StatusName { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
    }
}
