﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    06/09/2021	    Huỳnh Thử 		Create   
// ##################################################################

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class ReNewCardNo
    {
        public long Id { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string FullName { get; set; }
        public string UserActiveFullName { get; set; }
    }
}
