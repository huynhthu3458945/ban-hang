﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class AgencyChildBuyGeneralCount
    {
        public int TotalRecord { get; set; }
    }
    public class AgencyChildBuyGeneral
    {
        public string ParentAgencyCode { get; set; }
        public string ParentAgencyName { get; set; }
        public string ChildAgencyName { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int NoOfCard { get; set; }
        public int NoOfCardActive { get; set; }
    }
}
