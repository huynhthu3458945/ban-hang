﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class AgencyBuyGeneralCount
    {
        public int TotalRecord { get; set; }
    }
    public class AgencyBuyGeneral
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public int Total_VIP { get; set; }
        public int Total_LEVEL { get; set; }
        public int Total_GRADE { get; set; }
        public int Total_GRADE1 { get; set; }
        public int Total_GRADE2 { get; set; }
        public int Total_GRADE3 { get; set; }
        public int Total_GRADE4 { get; set; }
        public int Total_GRADE5 { get; set; }
        public int CardBuy_VIP { get; set; }
        public int CardBuy_LEVEL { get; set; }
        public int CardBuy_GRADE { get; set; }
        public int CardBuy_GRADE1 { get; set; }
        public int CardBuy_GRADE2 { get; set; }
        public int CardBuy_GRADE3 { get; set; }
        public int CardBuy_GRADE4 { get; set; }
        public int CardBuy_GRADE5 { get; set; }
        public int CardActive_VIP { get; set; }
        public int CardActive_LEVEL { get; set; }
        public int CardActive_GRADE { get; set; }
        public int CardActive_GRADE1 { get; set; }
        public int CardActive_GRADE2 { get; set; }
        public int CardActive_GRADE3 { get; set; }
        public int CardActive_GRADE4 { get; set; }
        public int CardActive_GRADE5 { get; set; }
        public int CardBuyAll_VIP { get; set; }
        public int CardBuyAll_LEVEL { get; set; }
        public int CardBuyAll_GRADE { get; set; }
        public int CardBuyAll_GRADE1 { get; set; }
        public int CardBuyAll_GRADE2 { get; set; }
        public int CardBuyAll_GRADE3 { get; set; }
        public int CardBuyAll_GRADE4 { get; set; }
        public int CardBuyAll_GRADE5 { get; set; }
        public int TotalCardBuyAll_VIP { get { return Total_VIP - CardBuyAll_VIP; } }
        public int TotalCardBuyAll_LEVEL { get { return Total_LEVEL - CardBuyAll_LEVEL; } }
        public int TotalCardBuyAll_GRADE { get { return Total_GRADE - CardBuyAll_GRADE; } }
        public int TotalCardBuyAll_GRADE1 { get { return Total_GRADE1 - CardBuyAll_GRADE1; } }
        public int TotalCardBuyAll_GRADE2 { get { return Total_GRADE2 - CardBuyAll_GRADE2; } }
        public int TotalCardBuyAll_GRADE3 { get { return Total_GRADE3 - CardBuyAll_GRADE3; } }
        public int TotalCardBuyAll_GRADE4 { get { return Total_GRADE4 - CardBuyAll_GRADE4; } }
        public int TotalCardBuyAll_GRADE5 { get { return Total_GRADE5 - CardBuyAll_GRADE5; } }
        public string Code { get; set; }
    }
}
