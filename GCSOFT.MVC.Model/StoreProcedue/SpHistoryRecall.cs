﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpHistoryRecall
    {
        public int Id { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Serinumber { get; set; }
        public string CardNo { get; set; }
        public string CardNo02 { get; set; }
        public DateTime? DateRecall { get; set; }
        public string DateRecallStr { get { return string.Format("{0:dd/MM/yyyy}", DateRecall); } }
        public string Reason { get; set; }
    }
}
