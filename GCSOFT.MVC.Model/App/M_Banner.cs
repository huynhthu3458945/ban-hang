﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.App
{
    public enum BannerType : byte
    {
        [Description("Trang Chủ")]
        Home,
        [Description("Tin Tức")]
        News,
        [Description("STNHĐ")]
        STNHD
    }
    public class M_Banner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public BannerType BannerType { get; set; }
        [MaxLength(2000)]
        public string BannerURL { get; set; }
        [MaxLength(2000)]
        public string BannerIpadURL { get; set; }
        [MaxLength(2000)]
        public string BannerDesktopURL { get; set; }
        [MaxLength(250)]
        public string BannerName { get; set; }
        [MaxLength(250)]
        public string Title { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        public DateTime DateInput { get; set; }
        [MaxLength(2000)]
        public string Link { get; set; }
        public int SortOrder { get; set; }
        public bool IsShow { get; set; }
        public string OpenType { get; set; }
    }
}
