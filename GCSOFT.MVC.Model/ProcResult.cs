﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model
{
    public class ProcResult
    {
        public string HasValue { get; set; }
    }
    public class ProcResultInt
    {
        public int NoOfNumber { get; set; }
    }
}
