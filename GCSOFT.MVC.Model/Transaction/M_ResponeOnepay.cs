﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Transaction
{
    public class M_ResponeOnepay
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Respone { get; set; }
        [MaxLength(250)]
        public string Message { get; set; }
        [MaxLength(40)]
        public string code { get; set; }
        public string vpc_MerchTxnRef { get; set; }
        public string vpc_SecureHash { get; set; }
        [MaxLength(40)]
        public string OrderID { get; set; }
    }
}
