﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Transaction
{
    public class M_PurchaseHeader
    {
        [Key, Column(Order = 0)]
        public PurchaseCardType Type { get; set; }
        [Key, Column(Order = 1)]
        public string Code { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? DatePrint { get; set; }
        public int NoOfCard { get; set; }
        public string Remark { get; set; }
    }
    public enum PurchaseCardType : byte
    {
        [Description("Đơn Hàng In")]
        PurchaseOrder,
        [Description("Đã Xác Nhận In")]
        PurchaseAccept,
        [Description("Hủy Lệnh In")]
        PurchaseReturnOrder,
        [Description("Phiếu Nhập Kho")]
        PurchaseReceipt
    }
}
