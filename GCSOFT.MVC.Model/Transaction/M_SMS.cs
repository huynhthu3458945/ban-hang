﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Transaction
{
    public class M_SMS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public SMSType Type { get; set; }
        [MaxLength(50)]
        public string Serinumber { get; set; }
        [MaxLength(50)]
        public string PhoneNo { get; set; }
        [MaxLength(10)]
        public string OTP { get; set; }
        public string Content { get; set; }
        public DateTime DateInput { get; set; }
        [MaxLength(80)]
        public string Username { get; set; }
        [MaxLength(150)]
        public string FullName { get; set; }
        public string ResultContent { get; set; }
        public bool IsConfirm { get; set; }
    }
    public enum SMSType : byte
    {
        [Description("SMS")]
        SMS,
        [Description("OTP")]
        OTP
    }
}
