﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Transaction
{
    public class M_RetailSalesOrder
    {
        [Key]
        [MaxLength(40)]
        public string Code { get; set; }
        public int UserId { get; set; }
        [MaxLength(20)]
        public string Username { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        [MaxLength(250)]
        public string CustomerName { get; set; }
        [MaxLength(250)]
        public string CustomerEmail { get; set; }
        [MaxLength(250)]
        public string CustomerPhone { get; set; }
        public int CityId { get; set; }
        [MaxLength(80)]
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        [MaxLength(80)]
        public string DistrictName { get; set; }
        [MaxLength(500)]
        public string Address { get; set; }
        [MaxLength(20)]
        public string ItemCode { get; set; }
        [MaxLength(250)]
        public string ItemName { get; set; }
        public double TotalAmount { get; set; }
        [MaxLength(40)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public DateTime CreateTime { get; set; }
        [Column(TypeName = "ntext")]
        public string TransactionInfo { get; set; }
        public string ActionCode { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
    }
}
