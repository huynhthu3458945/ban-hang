﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NamPhutThuocBai
{
    public class TB_PointEntry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(40)]
        public string userName { get; set; }
        [MaxLength(40)]
        public string SBD { get; set; }
        public string Deskcripton { get; set; }
        public int Point { get; set; }
        [MaxLength(40)]
        public string FromSource { get; set; }
        [MaxLength(40)]
        public string FromSourceNo { get; set; }
    }
}
