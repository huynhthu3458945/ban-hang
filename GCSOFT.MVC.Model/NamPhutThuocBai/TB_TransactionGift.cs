﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NamPhutThuocBai
{
    public class TB_TransactionGift
    {
        [Key]
        [MaxLength(40)]
        public string Code { get; set; }
        [MaxLength(250)]
        public string userName { get; set; }
        [MaxLength(250)]
        public string FullName { get; set; }
        [MaxLength(250)]
        public string Address { get; set; }
        [MaxLength(40)]
        public string PhoneNo { get; set; }
        [MaxLength(80)]
        public string Email { get; set; }
        [MaxLength(40)]
        public string GiftCode { get; set; }
        [MaxLength(250)]
        public string GiftName { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime LastDateUpdate { get; set; }
        public string Remark { get; set; }
        [MaxLength(40)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public string HistoryTransfer { get; set; }
        public int Point { get; set; }
        public string ModifyBy { get; set; }
    }
}
