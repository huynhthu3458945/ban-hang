﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NamPhutThuocBai
{
    public class TB_TrongTai
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string TrongTaiCode { get; set; }
        public string TrongTaiName { get; set; }
        [MaxLength(80)]
        public string FullName { get; set; }
        [MaxLength(80)]
        public string Phone { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }
        [MaxLength(40)]
        public string UserName { get; set; }
        public int ClassId { get; set; }
        [MaxLength(40)]
        public string ClassName { get; set; }
        [MaxLength(2000)]
        public string GoogleDrive { get; set; }
        public string Password { get; set; }
        public string Rule { get; set; }
        public string ParentUserName { get; set; }
        public string Group { get; set; }
        public string TrongTaiVideo { get; set; }
    }
    public enum TrongTaiType : byte
    {
        [Description("GVDH")]
        GVDH,
        [Description("Chấm Chuyên Môn 1")]
        ChamChuyenMon1,
        [Description("Chấm Chuyên Môn 2")]
        ChamChuyenMon2
    }
}
