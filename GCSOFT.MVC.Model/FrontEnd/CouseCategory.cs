﻿using System;

namespace GCSOFT.MVC.Model.FrontEnd
{
    public class CouseCategory
    {
        public int Id { get; set; }
        public int IdTeacher { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public string ShortDescription { get; set; }
        public DateTime DateModify { get; set; }
        public string Image { get; set; }
        public string LinkYoutube { get; set; }
        public int IdCategory { get; set; }

    }
}
