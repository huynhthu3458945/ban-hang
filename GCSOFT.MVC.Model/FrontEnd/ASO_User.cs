﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.FrontEnd
{
    public class ASO_User
    {
        [Key,]
        public Guid Id { get; set; }
        public int user_Id { get; set; }
        public int star { get; set; }
        public string note { get; set; }
        [MaxLength(250)]
        public string device_type { get; set; }
        public DateTime created_at { get; set; }
        public bool delete_flag { get; set; }
    }
    public class M_UploadImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(125)]
        public string type { get; set; }
        [MaxLength(125)]
        public string title { get; set; }
        public string image_mobile { get; set; }
        public string image_tablet { get; set; }
        public string image_desktop { get; set; }
        public string url_link { get; set; }
        public int position { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
    }
}
