﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(40)]
        public string Code { get; set; }
        [MaxLength(250)]
        public string FullName { get; set; }
        [MaxLength(40)]
        public string PhoneNo { get; set; }
        [MaxLength(20)]
        public string IsEmail { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }
        public DateTime? RegisterDate { get; set; }
        [MaxLength(80)]
        public string Owner { get; set; }
        public int? NoOfCard { get; set; }
        [MaxLength(80)]
        public string LotNumber { get; set; }
        [MaxLength(50)]
        public string SerialNumber { get; set; }
        [MaxLength(50)]
        public string CardNo { get; set; }
        [MaxLength(50)]
        public string UserOwner { get; set; }
        public DateTime? DateCard { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(80)]
        public string StatusName { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        [MaxLength(500)]
        public string Address { get; set; }
        public int? CityId { get; set; }
        [MaxLength(250)]
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        [MaxLength(250)]
        public string DistrictName { get; set; }

        [MaxLength(100)]
        public string UserName { get; set; }
        public int? GradeId { get; set; }
    }
}
