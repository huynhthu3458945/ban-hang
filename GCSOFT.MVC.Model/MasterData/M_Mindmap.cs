﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Mindmap
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int UserId { get; set; }
        [MaxLength(250)]
        public string FullName { get; set; }
        public DateTime DateCreate { get; set; }
        public int GradeId { get; set; }
        [MaxLength(40)]
        public string GradeName { get; set; }
        public int IdCourse { get; set; }
        [MaxLength(80)]
        public string CourseName { get; set; }
        public string Content { get; set; }
        public string Files { get; set; }
        [MaxLength(40)]
        public string StatusCode { get; set; }
        [MaxLength(80)]
        public string StatusName { get; set; }
        public int NoOfLike { get; set; }
    }
    public class M_Like
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int DocumentId { get; set; }
        public int UserId { get; set; }
        public DateTime DateInput { get; set; }
    }
}
