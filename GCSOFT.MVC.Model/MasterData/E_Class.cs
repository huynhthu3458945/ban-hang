﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_Class
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public ClassType ClassType { get; set; }
        [MaxLength(20)]
        public string Code { get; set; }
        [MaxLength(250)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string NameEN { get; set; }
        [Column(TypeName = "ntext")]
        public string Remark { get; set; }
        public int SortOrder { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
    }
    public class E_ClassRefer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ClassId { get; set; }
        [MaxLength(250)]
        public string ClassName { get; set; }
        public int ClassReferId { get; set; }
        [MaxLength(250)]
        public string ClassReferName { get; set; }
    }
    public enum ClassType : byte
    {
        [Description("Lớp")]
        Class,
        [Description("Cấp")]
        Level
    }
}
