﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
  public  class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int CategoryProductId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Processing { get; set; }
        public decimal Price { get; set; }
        public decimal PriceDiscount { get; set; }
        public string Title_SEO { get; set; }
        public string KeyWord_SEO { get; set; }
        public string Description_SEO { get; set; }
    }
}
