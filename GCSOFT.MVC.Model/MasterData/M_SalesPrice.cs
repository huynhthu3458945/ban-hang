﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_SalesPrice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public double Price { get; set; }
        public DateTime DateInput { get; set; }
    }
}
