﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Supporter
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(120)]
        public string Name { get; set; }
        public string NameEN { get; set; }
        public int SortOrder { get; set; }
        public int NoOfColumn { get; set; }
        public IEnumerable<M_FileResource> FileResources { get; set; }
    }
}
