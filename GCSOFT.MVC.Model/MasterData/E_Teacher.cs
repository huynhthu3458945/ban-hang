﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_Teacher
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(80)]
        public string FirstName { get; set; }
        [MaxLength(80)]
        public string LastName { get; set; }
        [MaxLength(160)]
        public string FullName { get; set; }
        [MaxLength(80)]
        public string FirstNameEN { get; set; }
        [MaxLength(80)]
        public string LastNameEN { get; set; }
        [MaxLength(160)]
        public string FullNameEN { get; set; }
        [MaxLength(40)]
        public string PhoneNo { get; set; }
        [MaxLength(80)]
        public string Email { get; set; }
        [Column(TypeName = "ntext")]
        public string Remark { get; set; }
        [MaxLength(500)]
        public string Image { get; set; }
        public int SortOrder { get; set; }
        [MaxLength(120)]
        public string Position { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string SchoolName { get; set; }
    }
}
