﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public ResultType Type { get; set; }
        public int ReferId { get; set; }
        public DateTime DateCreate { get; set; }
        public int? UserId { get; set; }
        [MaxLength(80)]
        public string UserName { get; set; }
        [MaxLength(250)]
        public string FullName { get; set; }
        public DateTime DateModified { get; set; }
        public int? ParentId { get; set; }
        public int? ReplyId { get; set; }
        [Column(TypeName = "ntext")]
        public string Content { get; set; }
        [MaxLength(500)]
        public string ProfilePicture { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public IEnumerable<M_FileResource> FileResources { get; set; }
    }
}
