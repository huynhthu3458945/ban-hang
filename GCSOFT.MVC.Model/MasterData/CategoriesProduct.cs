﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class CategoriesProduct
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(80)]
        public string Name { get; set; }
        public string Benefit { get; set; }
        public int SortOrder { get; set; }
        public bool Status { get; set; }

        public string Title_SEO { get; set; }
        public string KeyWord_SEO { get; set; }
        public string Description_SEO { get; set; }
    }
}
