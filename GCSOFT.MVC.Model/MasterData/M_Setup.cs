﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Setup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(120)]
        public string DomainFontEnd { get; set; }
        [MaxLength(120)]
        public string DomainBackEnd { get; set; }
        public int ImageSize { get; set; }
        public float Expired { get; set; }
        public string Token { get; set; }
        public int SpecialId { get; set; }
    }
}
