﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Vote
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public ResultType Type { get; set; }
        public int ReferId { get; set; }
        public DateTime DateInput { get; set; }
        public int UserId { get; set; }
        [MaxLength(80)]
        public string UserName { get; set; }
        [MaxLength(250)]
        public string FullName { get; set; }
        public string Remark { get; set; }
    }

}
