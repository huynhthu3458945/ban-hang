﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_HistoryRecall
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string AgencyPhone { get; set; }
        public string AgencyEmail { get; set; }
        public string Serinumber { get; set; }
        public string CardNo { get; set; }
        public string CardNo02 { get; set; }
        public DateTime? DateRecall { get; set; }
        public string DateRecallStr { get { return string.Format("{0:dd/MM/yyyy}", DateRecall); } }
        public string Reason { get; set; }
        public string SMS { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime DateBuy { get; set; }
        public int? ParentAgencyId { get; set; }
    }
}
