﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_Course
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public int IdTeacher { get; set; }
        [MaxLength(160)]
        public string TeacherName { get; set; }
        [MaxLength(500)]
        public string Title { get; set; }
        [MaxLength(500)]
        public string TitleEN { get; set; }
        [MaxLength(500)]
        public string URL { get; set; }
        [Column(TypeName = "ntext")]
        public string ShortDescription { get; set; }
        [Column(TypeName = "ntext")]
        public string FullDescription { get; set; }
        public bool Status { get; set; }
        public int SortOrder { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateModify { get; set; }
        [MaxLength(250)]
        public string Image { get; set; }
        public int AvartarLineNo { get; set; }
        [MaxLength(500)]
        public string LinkYoutube { get; set; }
        public int ClassId { get; set; }
        public int IdVideos { get; set; }
        [MaxLength(160)]
        public string ClassName { get; set; }
        public bool IsExperience { get; set; }
        public bool IsTNTD { get; set; }
        [MaxLength(20)]
        public string FontColor { get; set; }
        public virtual IEnumerable<E_CourseContent> CourseContents { get; set; }
        public virtual IEnumerable<E_CourseContent> CourseVideos { get; set; }
        public virtual IEnumerable<E_CourseSupport> CourseSupports { get; set; }
        public virtual IEnumerable<E_CourseCategory> CourseCategories { get; set; }
        public virtual IEnumerable<E_CourseResource> CourseResources { get; set; }
    }
}
