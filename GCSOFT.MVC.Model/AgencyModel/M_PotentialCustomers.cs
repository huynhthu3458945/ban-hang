﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.AgencyModel
{
    public class M_PotentialCard
    {
        [Key]
        [MaxLength(40)]
        public string Code { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateModify { get; set; }
        public int AgencyId { get; set; }
        [MaxLength(250)]
        public string AgencyNames { get; set; }
        public int CityId { get; set; }
        [MaxLength(80)]
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        [MaxLength(80)]
        public string DistrictName { get; set; }
        public int StatusId { get; set; }
        [MaxLength(40)]
        public string StatusCode { get; set; }
        [MaxLength(120)]
        public string StatusName { get; set; }
        [MaxLength(40)]
        public string UserCreate { get; set; }
        public string Remark { get; set; }
    }
    public class M_PotentialCustomer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(40)]
        public string PotentialCode { get; set; }
        public int AgencyId { get; set; }
        [MaxLength(80)]
        public string userName { get; set; }
        [MaxLength(250)]
        public string fullName { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }
        [MaxLength(120)]
        public string Phone { get; set; }
        public bool ActivePhone { get; set; }
        public bool HasEbook { get; set; }
        [MaxLength(80)]
        public string ChildFullName1 { get; set; }
        public int Child1_ClassId { get; set; }
        [MaxLength(80)]
        public string ChildFullName2 { get; set; }
        public int Child2_ClassId { get; set; }
        public int? CityId { get; set; }
        [MaxLength(250)]
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        [MaxLength(250)]
        public string DistrictName { get; set; }
        public DateTime? DateActive { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateExpired { get; set; }
        public int StatusId { get; set; }
        [MaxLength(40)]
        public string StatusCode { get; set; }
        [MaxLength(120)]
        public string StatusName { get; set; }
        [MaxLength(250)]
        public string Child1_SchoolName { get; set; }
        [MaxLength(250)]
        public string Child2_SchoolName { get; set; }
        public string Serinumber { get; set; }
        public string ActionCode { get; set; }
        public bool IsActive { get; set; }
    }
}
